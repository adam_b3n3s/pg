import socket

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind(("127.0.0.1", 10000))
    s.listen(1)
    conn, addr = s.accept()
    print(addr)
    all = []
    x = conn.recv(1024).decode("utf-8")
    while x != "":
        all.append(x)
        x = conn.recv(1024).decode("utf-8")

print(all)