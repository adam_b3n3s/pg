import socket


with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect(("localhost", 10000))
    s.sendall(b"1.")
    s.sendall(b"2.")
    s.sendall(b"3.")
    s.sendall(b"4.")
    s.sendall(b"5.")
    s.sendall(b"")