from wolframclient.evaluation import WolframLanguageSession
from wolframclient.language import wl, wlexpr
session = WolframLanguageSession()
wlexpr('f[x_] := x^2')
session.evaluate(wl.WolframAlpha("number of moons of Saturn", "Result"))