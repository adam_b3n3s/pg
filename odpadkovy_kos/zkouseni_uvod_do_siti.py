import random

questions_and_answers = {
"Jaké jsou výhody a nevýhody přepojování paketů?": "paket může jít jinou cestou, potenciálně pomalejší, ale výpadky nejsou fatální",
"Jak se na síťových protokolech projevilo to, že vznik sítí iniciovala armáda z důvodů zvýšení bezpečnosti komunikace?": "fyzicky jsou zabezpešené, informačně ne",
"Co je smyslem požadavku na škálovatelnost sítě?": "přidávání nových sekcí",
"Jak se liší nároky elektronické pošty a telefonování po IP síti na přenosové parametry sítě?": "latence, rychlost, ztrátovost a pravidelnost doručení",
"Jaká je definice LAN?": "Local Area Network - menší síť",
"Co je podstatou VPN?": "VPN (Virtual Private Network) - privátní síť spojená veřejnými prostory",
"Popište rozdíl mezi TCP, UDP a IP.": "TCP - (connection-oriented) spolehlivé doručení dat, UDP - (connectionless) rychlý, IP je nespolehlivé + víc kontroly, aplikace může řídit komunikaci",
"Jaké adresy se používají na aplikační, transportní, síťové, linkové a fyzické vrstvě?": "aplikační - doménová, transportní - port, síťová - IP, linková + fyzická - MAC",
"Jaký je rozdíl mezi klient-server a peer-to-peer modelem?": "pořadavky na server vs každý je clientem a serverem",
"Jak jsou spravována doménová jména?": "Nejvyšší úroveň - gov, net, edu (spravuje ICANN), TLD (Top level domain) - cz, ru (spravuje ISP), SLD (Second level domains) - mff (spravuje vlastník)",
"Jak se přidělují IP adresy?": "veřejne přiděluje ISP, uvnitř dynamicky nebo pevně",
"Jaký je smysl a princip NAT?": "NAT (Network address translation) - nemáme dostatek ipv4 adres a překládáme privátní na veřejné",
"Co je multiplexing a zapouzdření?": "multiplexing - předávání několika signálů stejnou linkou pomocí zapouzdření, zapouzdření - způsob balení datových jednotek pro interoperabilitu mezi vrstvami",
"Popište tři základní skupiny kryptografických algoritmů a rozdíly mezi nimi.": "symetrické - klíč je třeba předat veřejnou cestou, asymetrické - klíč mohou mít každý vlastní a vygeneruje se veřejný, hashovací funkce - jednostranná, nelze zpětně získat data",
"Popište princip šifrování a elektronického podpisu.": "šifrování: text = veřejný klíč příjemce => šifrovaný text = tajný klíč příjemce => text, elektronický podpis: text = tajný klíč podeposujícího => šifrovaný text = veřejný klíč podepisujícího =>text",
"Popište účel a podstatu Diffie-Hellmanova algoritmu.": "způsob jak vyměnit tajnou informaci veřejně\ntajná čísla \(a,b\), veřejná prvočísla \(p, q\)\n\(A = p^a\mod q\); \(B = p^b \mod q\)\n\(s = B^a \mod q\); \(s = A^b \mod q\)\n\(A^b=(p^a)^b = p^{ab} = p^{ba} = (p^b)^a = B^a\)",
"Jakým způsobem se ověřuje pravost veřejného klíče?": "buď z více nezávislých zdrojů nebo pomocí podpisu třetí strany, certifikační autorita - pavučina důvěry",
"Jaký je rozdíl mezi protokoly HTTP a HTTPS?": "S - Secure, HTTPS je šifrovaný, má pár vygenerovaných klíčů potvrzených certifikátem k šifrování dat",
"Kdy se v DNS používá UDP a kdy TCP?": "větší datové výměny v TCP, jinak UDP",
"Popište účel a chování sekundárního nameserveru domény.": "uchovává kopie dat o doméně",
"Popište postup řešení DNS dotazu.": "1. lokální DNS server\n2. kořenové DNS servery\n3. autoritativní servery nejvyšší úrovně s odkazy na další servery",
"Popište bezpečnostní rizika DNS.": "není snadno se dostat k samotnému dotazu\ncache poisoning - přidají se falešné údaje o doméně - řeší se postupováním od kořenových DNS serverů a ukládáním do cache pouze autoritativních odpovědí",
"Jaká jsou nejdůležitější bezpečnostní rizika FTP?": "otevřený přenos hesla",
"Co je podstatou problémů při otevírání dodatečných datových kanálů?": "adresy kanálů jsou veřejné a privátní adresy komplikují systém -- musí se řešít připojení FTP serveru přes NAT",
"Vysvětlete pojmy MTA a MX a jejich roli v přenosu pošty.": "MTA (Mail Transfer Agent) - uzel pro příjem a doručení pošty\nMX (Mail Exchanger) - DNS záznamy pro MTA servery",
"Co je „obálka“ a jak souvisí s obsahem hlaviček dopisu?": "adresy v protokolu, nemusí souviset s hlavičkou dopisu",
"Jak lze používat non-ASCII znaky v hlavičkách a těle dopisu? Jak se ty způsoby liší?": "UUENCODE - starší Base64\nBase64 - kódování symbolů alfanumerickými znaky, jiný formát řádek\nquoted-printable - hexadecimální hodnoty pro speciální znaky",
"V čem spočívá problém open-relay serverů?": "open-relay - SMTP server dovolí komukoliv posílat kamkoliv, zneužití spam-boty",
"Vysvětlete podstatu grey-listingu a DKIM.": "grey-listing - odmítání zprávy poprvé, protože spam-engine zpravidla pokus neopakuje\nDKIM (Domain Keys Identified Mail) - podepisování a ověřování zpráv klíčemi - ochrana proti spoofingu (vydávání se za někoho jiného)",
"K čemu slouží IMAP protokol?": "Internet Message Access Protocol - nástupce POP - přístup k poštovní schránce",
"K čemu slouží HTML?": "HyperText Markdown Language - popisuje obsah webu",
"Popište co se odehraje v HTTP/1.1 protokolu, pokud uživatel požádá o stránku se dvěma vloženými obrázky.": "vygenerují se 2 nezávislé dotazy",
"Stručně popište účel a vlastnosti nejznámějších metod HTTP.": "GET - odpověď dokument\nPOST - tělo parametry, odpověď dokument\nPUT - tělo dokument\nDELETE - odpověď výsledek",
"K čemu slouží cookies a jaké představují riziko?": "ukládají informace mezi sessions\npředstavují riziko, pokud se dostanou do cizích rukou",
"Porovnejte protokoly telnet a SSH.": "telnet je starší a není zabezpečený, otevřený přenos dat\nSSH může otevírat víc kanálů",
"Porovnejte protokoly H.323 a SIP.": "H.323 - přenos média, binární\nSIP (Session Initiation Protocol) - modernější, ascii, řeší jen vyhledání partnera a navázání spojení",
"Proč a jak se synchronizuje čas na počítačích v síti?": "NTP (Network Time Protocol) - pro stejné timestamps souborů a porovnání času událostí",
"K čemu slouží DHCP?": "DHCP (Dynamic Host Configuration Protocol) - automaticky přiřazuje adresy v síti",
"Jak se v TCP/IP řeší chybějící prezentační vrstva?": "definice přímo začleněna do aplikačních protokolů",
"Vysvětlete rozdíl mezi TCP a UDP.": "Transmission Control Protocol - stream, proud dat, méně pravidelné bezeztratové doručení\nUser Datagram Protocol - nespojované služby, nezávislé zprávy, vyšší ztrátovost, pravidelný to",
"Jak souvisejí pojmy potvrzování a TCP okno?": "okno - rozsah dat, který lze poslat bez potvrzení",
"Co je three-way handshake?": "navázání TCP spojení\n1. SYN, navázání spoje\n2. ACK, SYN - potvrzení navázání, odpověď\n3. ACK - potvrzení odpovědi",
"Jak souvisejí třídy IPv4, subnetting a CIDR?": "CIDR (Classless InterDomain Routing) - supernetting nad sousedními síťmi pro šetření místa v routovacích tabulkách, použití síťových masek (subnetting) podle IPv4 tříd",
"Proč je v síti třídy C pouze 254 adres pro počítače?": "protože počítačová část adresy má velikost 8 bit",
"Jaké typy broadcastových adres známe?": "network - všem v cílové síti\nlimited - nesmí opustit síť přes router",
"Jak funguje směrovací algoritmus?": "1. prohlédat směrovací tabulku (pokud není, není cesta)\n2. nejspeciálnější záznam - můj stroj, moje síť (poslat příjemci/směrovači)",
"Popište typy sloupců a záznamů směrovací tabulky.": "cíl, maska, gateway",
"Čím se liší statické a dynamické řízení správy směrovacích tabulek?": "dynamické - v průběhu práce\nstatické - ručně, příkazem",
"K čemu slouží routovací protokoly?": "předávání řídících informací mezi routery, hlédání cesty packetu",
"Porovnejte distance-vector a link-state protokoly.": "distance-vector - vzdálenosti v směrovací tábulce\nlink-state - routery mají mapu sítě a sdílejí si navzájem stav linek",
"Co je autonomní systém?": "jednotlivé bloky sítě se společnou routovací politikou\nsmyslem je sjednotit směrování pro skupinu sítí\nexterní protokoly, např. BGP",
"Vysvětlete význam zpráv ICMP Echo, Time Exceeded a Destination Unreachable.": "ICMP (Internet Control Message Protocol) - řídící informace pro IP\nEcho - ping cílového stroje\nTime Exceeded - převýšen TTL\nDestination Unreachable - nemá jak doručit paket",
"K čemu slouží pole TTL v IP záhlaví?": "počet zbývajících hopů mezi routery",
"Co označuje pojem IP filtrování?": "bezpečnostní politika transportní vrstvy, jaký provoz povolen\nprovoz na jakých portech je dovolen z a do LAN",
"K čemu slouží proxy server?": "SW na routeru zachytí spojení a předá proxy serveru, ten ho zkontroluje dle bezpečnostních politik\npro netransparentní je třeba nakonfigurovat klienty, ať ví, kde je proxy server, ale nemusí být router",
"Popište účel a bezpečnostní rizika protokolu ARP.": "ARP (Address Resolution Protocol) - konverze MAC na IP mezi síťovou a linkovou vrstvou\nbroadcast dorazí všem a odpovědět může kdokoli - bezpečnostní riziko falešných ARP zpráv",
"Popište účel vrstev LLC a MAC.": "LLC (Logical Link Control) - multiplexing, přístup různých protokolů ke stejnému médiu\nMAC (Media Access Control) - adresace, řízení přístupu (kdo, kdy a jak může odesílat a přijímat da",
"Jaký je rozdíl mezi fyzickou a logickou topologií sítě?": "druhy zařízení a kabelů vs jak uzly komunikují",
"Co je kolize, kde se vyskytuje a jak se řeší?": "odesílání a přijímání zpráv zároveň\nCSMA (Carrier Sense with Multiple Access) - uzel poslouchá a čeká, dokud není volno\nCSMA/CD (Collision Detection) - vysílá se, zastavení vysílání, upozornění, čekání\nCSMA/CA (Collision Avoidance) - čeká na ACK",
"K čemu slouží a jak se realizuje VLAN?": "virtuální síť - způsob rozdělení fyzické sítě na několik logických\nmají identifikator 12bit VLANID a 32bit dlouhý tag koncového zařízení/switche",
"K čemu slouží CRC a jak pracuje?": "CRC (Cyclic Redundancy Check) - hashovací funkce pro kontrolu konzistence dat\nhash je tvořen bity dat dělenými bity charakteristického polynomu\n100% detekuje chyby s lichým počtem bitů a kratší než n bitů",
"Popište způsob přenosu dat v závislosti na médiu.": "bezdrátově - modulace vln\nmetalické - elektrické pulzy\noptické - světelné pulzy",
"Popište typy metalických a optických kabelů.": "metalické:\nUTP (unshielded twisted pair)\nSTP\noptické:\nsinglemode - 1 paprsek\nmultimode - víc paprsků",
"Co je learning bridge a STP?": "bridge - spojuje segmenty sítě na linkové vrstvě, switch\nlearning bridge - pro každý port se udržuje tabulka MAC adres\nSTP (Spanning Tree Protocol) - nalezení kostry grafu:\n- spanning tree algorithm - pro cyklické grafy sítě\n- řešení - najít kostru grafu - acyklickou podmnožinu\n- switche se domluví, který potlačí forward, ať se síť nezahltí",
"Jaký je rozdíl mezi jednovidovým (SM) a mnohovidovým (MM) optickým kabelem?": "SM - 1 paprsek, MM - víc paprsků",
}

keys = list(questions_and_answers.keys())
random.shuffle(keys)

neumim = {key: questions_and_answers[key] for key in keys[:15]}

while neumim != {}:
    print("new round")
    print()
    print()
    print()
    keys = list(neumim.keys())
    random.shuffle(keys)
    for key in keys:
        print(key, end="")
        x = input("")
        print(questions_and_answers[key])
        x = input("")
        if (x == ""):
            del neumim[key]
