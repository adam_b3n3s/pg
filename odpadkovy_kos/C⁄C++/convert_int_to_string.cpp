#include <string>
#include <iostream>

using namespace std;

string int_to_string(int i) {
    string s;
    while(i != 0) {
        s = (char) ((i % 10) + '0') + s;
        i = i / 10;
    }
    return s;
}

int main() {
    cout << int_to_string(13) << endl;
}