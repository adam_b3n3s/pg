#include <stdio.h>

#include <cstdlib>
#include <iostream>
using namespace std;
  
int main()
{
    srandom(time(0));
    int randomNumber = (random() % (10-1) + 1);
    int guess;
    cout << "Write the number from 1 to 10:\n";
    cin >> guess;
    if(guess == randomNumber){
        cout << "True";
    }
    else{
        cout << "False\n";
        cout << "The number was: ";
        cout << randomNumber;
    }
}