#include <string>
#include <iostream>

using namespace std;


bool prvocislo(int i){
    for(int j = 2; j < i; j++){
        if (i % j == 0){
            return false;
        }
    }
}

int main(){
    int counter = 0;
    int i = 0;
    while (counter != 10001+1){
        i++;
        if (prvocislo(i)){
            counter++;
        }
    }
    cout << i << endl;
}
