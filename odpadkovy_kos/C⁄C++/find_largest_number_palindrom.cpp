
#include <string>
#include <iostream>

using namespace std;

bool palindrom(int vysledek) {

    int pamatovak = vysledek;

    int opacne = 0;
    while (vysledek != 0) {
        int cislo = vysledek % 10;
        opacne = opacne * 10 + cislo;
        vysledek /= 10;
    }
    if(pamatovak == opacne){
        return true;
    }
    return false;
    
}


int main(){
    int maximum = 0;
    for(int i = 100; i < 1000; i++){
        for(int j = 100; j < 1000; j++){
            int vysledek = i*j;
            if(palindrom(vysledek)){
                if (maximum < i*j){
                    maximum = i*j;
                }
            }
        }
    }
    cout << maximum << endl;
}

