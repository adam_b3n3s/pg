// Function to show the input value and send it to the server
function showInputValue() {

  // Get the output label element and clear it
  var output_label = document.getElementById("output-label");
  output_label.innerHTML = "";
  output_label.style.display = "block";

  // Get the input elements and their values
  var emaile = document.getElementById("email");
  var email = emaile.value;

  var password_1 = document.getElementById("password_1");
  var value_1 = password_1.value;

  var password_2 = document.getElementById("password_2");
  var value_2 = password_2.value;

  var username = document.getElementById("username");
  var value = username.value;

  // Check if the passwords match and are of the correct length, and that the username is not empty or too long
  if (value_1 == value_2) {
    if (value_1.length > 4 && value_1.length < 30 && value.length > 0 && value.length < 30) {

      // Check if the username or password contains spaces
      if (value.includes(" ") || value_1.includes(" ") || value.includes("@") || email.includes(" ")) {

        // Update the output label with an error message if either the username or password contains spaces
        var output_label = document.getElementById("output-label");
        output_label.innerHTML = "Password, username or email cant contain spaces or @!";
        output_label.style.display = "block";
      } else {

        if (email.includes("@")) {

          // Create a new XMLHttpRequest object to send the data to the server
          var xhr = new XMLHttpRequest();

          xhr.open("POST", "http://localhost:3000/check_variable2", true); // Replace localhost:3000 with your server's address
          xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

          // Set up a callback function for when the response is received
          xhr.onreadystatechange = function () {
            if (xhr.readyState == XMLHttpRequest.DONE) {
              if (xhr.status == 200) {
                console.log("Received response from server:", xhr.responseText);
                if (JSON.parse(xhr.responseText)) {
                  var output_label = document.getElementById("output-label");
                  output_label.innerHTML = "Username or email already exists!";
                  output_label.style.display = "block";

                } else {
                  var xhr2 = new XMLHttpRequest();

                  console.log(xhr.responseText);
                  // Configure the request to send a POST request with the value as the request body
                  xhr2.open("POST", "http://localhost:3000", true); // Replace localhost:3000 with your server's address
                  xhr2.setRequestHeader("Content-Type", "text/plain");
                  
                  let min = 10000000;
                  let max = 99999999;
                  let randomNumber8 = Math.floor(Math.random() * (max - min + 1)) + min;
                  // Send the request with the value and password as the request body
                  xhr2.send(email + " " + value + " " + value_1 + " " + randomNumber8);

                  // Clear the input fields
                  username.value = "";
                  password_1.value = "";
                  password_2.value = "";
                  localStorage.setItem("hasSeenHelloWorld2", email);
                  localStorage.setItem("hasSeenHelloWorld", randomNumber8);

                  // Navigate to the new page
                  window.location.href = "register_2.html";
                }
              }
            }
          };
          xhr.send(value + " " + email);

        }else {
          // Update the output label with an error message if the username or password is empty
          var output_label = document.getElementById("output-label");
          output_label.innerHTML = "Email must contain @!";
          output_label.style.display = "block";
      } 
      }
    } else {
      // Update the output label with an error message if the username is too long or the password is too short/long
      var output_label = document.getElementById("output-label");
      output_label.innerHTML = "Password cant be <br> shorter than 4 or longer than 30! <br> Username cant be <br> empty or longer than 30!";
      output_label.style.display = "block";
    }
  } else {
    // Update the output label with an error message if the passwords do not match
    var output_label = document.getElementById("output-label");
    output_label.innerHTML = "Passwords do not match!";
    output_label.style.display = "block";
  }
}

// Add an event listener to the input fields for the "Enter" key to call the showInputValue function when the key is pressed
var iusername = document.getElementById("username");
iusername.addEventListener("keydown", function (event) {
  if (event.key === "Enter") {
    event.preventDefault();
    showInputValue();
  }
});
var ipassword_1 = document.getElementById("password_1");
ipassword_1.addEventListener("keydown", function (event) {
  if (event.key === "Enter") {
    event.preventDefault();
    showInputValue();
  }
});
var ipassword_2 = document.getElementById("password_2");
ipassword_2.addEventListener("keydown", function (event) {
  if (event.key === "Enter") {
    event.preventDefault();
    showInputValue();
  }
});
var iemail = document.getElementById("email");
iemail.addEventListener("keydown", function (event) {
  if (event.key === "Enter") {
    event.preventDefault();
    showInputValue();
  }
});