function showInputValue() {
  // Get the output label element and reset its contents
  var outputLabel = document.getElementById("output-label");
  outputLabel.innerHTML = "";
  outputLabel.style.display = "block";

  // Get the input elements and their values
  var password_1 = document.getElementById("password_1");
  var value_1 = password_1.value;

  var username = document.getElementById("username");
  var value = username.value;
  
  let min = 10000000;
  let max = 99999999;
  let randomNumber8 = Math.floor(Math.random() * (max - min + 1)) + min;

  // Combine the input values into a single variable
  var combined = " " + value + " " + value_1 + " " + randomNumber8;

  // Create a new XMLHttpRequest object
  var xhr = new XMLHttpRequest();

  // Configure the request
  xhr.open("POST", "http://localhost:3000/check_variable", true);
  xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

  // Set up a callback function for when the response is received
  xhr.onreadystatechange = function () {
    if (xhr.readyState == XMLHttpRequest.DONE) {
      if (xhr.status == 200) {
        console.log("Received response from server:", xhr.responseText);
        // If the response is "true", update the output label with a success message
        if (xhr.responseText == "true") {
          var outputLabel = document.getElementById("output-label");
          outputLabel.style.display = "block";
          localStorage.setItem("hasSeenHelloWorld", randomNumber8);

          // Navigate to the new page
          window.location.href = "test.html";
        } else {
          // If the response is not "true", update the output label with a failure message
          var outputLabel = document.getElementById("output-label");
          outputLabel.innerHTML = "Try again";
          outputLabel.style.display = "block";
        }
      } else {
        // If the request fails, log an error message
        console.log("AJAX request failed:", xhr.statusText);
      }
    }
  };

  // Encode the input variable and send it as the request body
  var data = encodeURIComponent(combined);
  xhr.send(data);

  // Clear the input fields
  username.value = "";
  password_1.value = "";
}

// Add event listeners to the input fields for the "Enter" key
var iusername = document.getElementById("username");
iusername.addEventListener("keydown", function (event) {
  if (event.key === "Enter") {
    event.preventDefault();
    showInputValue();
  }
});

var ipassword_1 = document.getElementById("password_1");
ipassword_1.addEventListener("keydown", function (event) {
  if (event.key === "Enter") {
    event.preventDefault();
    showInputValue();
  }
});
