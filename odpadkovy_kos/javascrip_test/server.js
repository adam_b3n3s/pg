// Import the Node.js built-in modules for creating a server and interacting with the file system
const http = require('http');
const fs = require('fs');
const { promisify } = require('util');
const crypto = require('crypto');

// Create a new HTTP server that listens for incoming requests
const server = http.createServer((req, res) => {
  let min = 1000;
  let max = 9999;
  let randomNumber = Math.floor(Math.random() * (max - min + 1)) + min;


  if (req.method === 'POST' && req.url === '/check_variable3') {
    let data = '';

    // Collect the request data in chunks
    req.on('data', chunk => {
      data += chunk.toString();
    });
    req.on('end', () => {
      var decoded_data = decodeURIComponent(data);
      let parts = decoded_data.split(" ");
      let part_1 = parts[0];
      let part_2 = parts[1];





























      // Set the headers for the response to allow cross-origin requests and specify the content type
      res.setHeader('Access-Control-Allow-Origin', '*');
      res.setHeader('Content-Type', 'text/plain');

      // Set the search string to the decoded data and the file path to the received variable file
      const file_path = '.usernames_passwords.txt'; // replace with your file path

      // Read the contents of the received variable file
      const fs = require('fs');

fs.readFile(file_path, 'utf-8', (err, data) => {
  if (err) {
    console.error(err);

    // Send 'false' response to the client if there was an error reading the file
    res.setHeader('Content-Type', 'text/plain');
    res.end('false');
    return;
  }

  // Split the file contents into lines and check if the search string is present
  const lines = data.split('\n');
  const minParts = 3; // minimum number of parts required in a line

  const found_line_index = lines.findIndex(line => {
    const parts = line.split(" ");
    return parts.includes(part_1) && parts.includes("@"+part_2) && parts.length > minParts;
  });

  if (found_line_index !== -1) {
    const found_line = lines[found_line_index];
    const parts = found_line.split(" ");
    const last_word_index = parts.length - 1;
    const updated_line = found_line.substring(0, found_line.lastIndexOf(parts[last_word_index])).slice(0, -1);
    lines[found_line_index] = updated_line;
    console.log("Updated line: " + updated_line);

    // Overwrite the file with the updated lines
    const updated_data = lines.join('\n');
    fs.writeFile(file_path, updated_data, 'utf-8', (err) => {
      if (err) {
        console.error(err);

        // Send 'false' response to the client if there was an error writing to the file
        res.setHeader('Content-Type', 'text/plain');
        res.end('false');
        return;
      }

      // Send 'true' response to the client if the file was updated successfully
      res.setHeader('Content-Type', 'text/plain');
      res.end('true');
    });
  } else {
    console.log("No line found with " + minParts + " parts containing " + part_1 + " and " + part_2);
    res.setHeader('Content-Type', 'text/plain');
    res.end('false')
  }
});



























    });



    // Check if the request method is POST and the URL is '/check_variable'
  } else if (req.method === 'POST' && req.url === '/check_code') {
    let data = '';

    // Collect the request data in chunks
    req.on('data', chunk => {
      data += chunk.toString();
    });

    // Process the request data when it has been fully received
    req.on('end', () => {
      var decoded_data = decodeURIComponent(data);
      
      

      // Combine the two parts of the received data again

      // Set the headers for the response to allow cross-origin requests and specify the content type
      res.setHeader('Access-Control-Allow-Origin', '*');
      res.setHeader('Content-Type', 'text/plain');

      // Set the search string to the decoded data and the file path to the received variable file
      const file_path = '.usernames_passwords.txt'; // replace with your file path

      // Read the contents of the received variable file
      fs.readFile(file_path, 'utf-8', (err, data) => {
        if (err) {
          console.error(err);
      
          // Send 'false' response to the client if there was an error reading the file
          res.setHeader('Content-Type', 'text/plain');
          res.end('false');
          return;
        }
      
        // Split the file contents into lines and check if the search string is present
        const lines = data.split('\n');
        const minParts = 4; // minimum number of parts required in a line
      
        const found_line_index = lines.findIndex(line => {
          const parts = line.split(" ");
          console.log(parts.includes(decoded_data));
          console.log(parts.length <= minParts);
          return parts.includes(decoded_data) && parts.length <= minParts;
        });
        
        // Send 'true' response to the client if the search string was found in the file, otherwise send 'false'
        if (found_line_index !== -1) {
          console.log(`The string "${decoded_data}" was found in the file.`);
          res.setHeader('Content-Type', 'text/plain');
          res.end('true');
        } else {
          console.log(`The string "${decoded_data}" was not found in the file.`);
          res.setHeader('Content-Type', 'text/plain');
          res.end('false');
        }
      });

            
    });
  }
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  else if (req.method === 'POST' && req.url === '/check_variable') {
    let data = '';

    // Collect the request data in chunks
    req.on('data', chunk => {
      data += chunk.toString();
    });

    // Process the request data when it has been fully received
    req.on('end', () => {
      var decoded_data = decodeURIComponent(data);
      decoded_data = decoded_data.substring(1);

      // Split the received data into two parts
      let parts = decoded_data.split(" ");
      let part_1 = parts[0];
      let part_2 = parts[1];
      let part_3 = parts[2];

      // Hash the second part of the received data using SHA-256
      part_2 = crypto.createHash('sha256').update(part_2).digest('hex');

      // Combine the two parts of the received data again

      // Set the headers for the response to allow cross-origin requests and specify the content type
      res.setHeader('Access-Control-Allow-Origin', '*');
      res.setHeader('Content-Type', 'text/plain');

      // Set the search string to the decoded data and the file path to the received variable file
      const file_path = '.usernames_passwords.txt'; // replace with your file path

      // Read the contents of the received variable file
      fs.readFile(file_path, 'utf-8', (err, data) => {
        if (err) {
          console.error(err);
      
          // Send 'false' response to the client if there was an error reading the file
          res.setHeader('Content-Type', 'text/plain');
          res.end('false');
          return;
        }
      
        // Split the file contents into lines and check if the search string is present
        const lines = data.split('\n');
        const minParts = 4; // minimum number of parts required in a line
      
        const found_line_index = lines.findIndex(line => {
          const parts = line.split(" ");
          return parts.includes(part_1) && parts.includes(part_2) && parts.length <= minParts;
        });
      
        // Modify the last word of the found line
        if (found_line_index !== -1) {
          const found_line = lines[found_line_index];
          const parts = found_line.split(" ");
          parts[parts.length - 1] = part_3;
          lines[found_line_index] = parts.join(" ");
      
          // Write the modified contents back to the file
          const updated_data = lines.join('\n');
          fs.writeFile(file_path, updated_data, 'utf-8', err => {
            if (err) {
              console.error(err);
      
              // Send 'false' response to the client if there was an error writing to the file
              res.setHeader('Content-Type', 'text/plain');
              res.end('false');
            } else {
              console.log(`The string "${part_1 + " and " + part_2}" was found in the file and the last word was changed to "${part_3}".`);
              res.setHeader('Content-Type', 'text/plain');
              res.end('true');
            }
          });
        } else {
          console.log(`The string "${part_1 + " and " + part_2}" was not found in the file.`);
          res.setHeader('Content-Type', 'text/plain');
          res.end('false');
        }
      });
            
    });
  }
  // Handle only POST requests
  else {
    if (req.method === 'POST' && req.url === '/check_variable2') {







      let data = '';

      // Collect the request data in chunks
      req.on('data', chunk => {
        data += chunk.toString();
      });

      // Process the request data when it has been fully received
      req.on('end', () => {
        var decoded_data = decodeURIComponent(data);

        // Split the received data into two parts
        let parts = decoded_data.split(" ");
        let part_1 = parts[0];
        let part_2 = parts[1];
        


        // Set the headers for the response to allow cross-origin requests and specify the content type
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Content-Type', 'text/plain');

        const search_string = part_1; // replace with your desired search string
        const file_path = '.usernames_passwords.txt'; // replace with your file path

        // Read the contents of the received variable file
        fs.readFile(file_path, 'utf-8', (err, data) => {
          if (err) {
            console.error(err);

            // Send 'false' response to the client if there was an error reading the file
            res.setHeader('Content-Type', 'text/plain');
            res.end('false');
            return;
          }

          // Split the file contents into lines and check if the search string is present
          const lines = data.split('\n');
          const found_line = lines.find(line => line.includes(" " + search_string + " "));


          // Send 'true' response to the client if the search string was found in the file, otherwise send 'false'
          if (found_line) {
            console.log(`The string "${search_string}" was found in the file.`);
            res.setHeader('Content-Type', 'text/plain');
            res.end('true');
          } else {
            console.log(`The string "${search_string}" was not found in the file.`);



            const search_string2 = part_2; // replace with your desired search string
            const file_path2 = '.usernames_passwords.txt'; // replace with your file path

            // Read the contents of the received variable file
            fs.readFile(file_path2, 'utf-8', (err2, data2) => {
              if (err2) {
                console.error(err2);

                // Send 'false' response to the client if there was an error reading the file
                res.setHeader('Content-Type', 'text/plain');
                res.end('false');
                return;
              }

              // Split the file contents into lines and check if the search string is present
              const lines2 = data2.split('\n');
              const found_line2 = lines2.find(line2 => line2.startsWith(search_string2 + " "));
              

              // Send 'true' response to the client if the search string was found in the file, otherwise send 'false'
              if (found_line2) {
                console.log(`The string "${search_string2}" was found in the file.`);
                res.setHeader('Content-Type', 'text/plain');
                res.end('true');
              } else {
                console.log(`The string "${search_string2}" was not found in the file.`);
                res.setHeader('Content-Type', 'text/plain');
                res.end('false');
              }
            });



          }









        });
      });













    }
    else {
      if (req.method === 'POST') {

        // Initialize a variable to hold the request body
        let body = '';

        // Listen for data events from the request
        req.on('data', chunk => {
          // Concatenate the incoming chunks of data to form the complete request body
          body += chunk.toString(); // convert buffer to string
        });

        // Listen for the end event of the request
        req.on('end', () => {

          // Split the request body into parts and encode the second part using the TextEncoder API
          let parts = body.split(" ");
          let part_1 = parts[0];
          let part_2 = parts[1];
          let part_3 = parts[2];
          let part_4 = parts[3];
          const messageBytes = new TextEncoder().encode(part_3);

          // Hash the encoded message using the SHA-256 algorithm
          crypto.subtle.digest('SHA-256', messageBytes)
            .then(hashBytes => {

              // Convert the hash to a hexadecimal string
              const hashArray = Array.from(new Uint8Array(hashBytes));
              const hashHex = hashArray.map(b => b.toString(16).padStart(2, '0')).join('');
              part_3 = hashHex;
              body = part_1 + " " + part_2 + " " + part_3 + " " + part_4;

              // Log the body to the console
              console.log(randomNumber);
              console.log("Adding " + "\"" + body + "\"" + " to file");
              // Append the hash to an existing file called ".received_variable.txt"
              const appendFile = promisify(fs.appendFile);
              appendFile('.usernames_passwords.txt', body + " @" + randomNumber + '\n')
                .then(() => {
                  // If the hash is successfully appended to the file, send a 200 success response
                  res.setHeader('Access-Control-Allow-Origin', '*'); // allow requests from any domain
                  res.writeHead(200, { 'Content-Type': 'text/plain' });
                  res.end('Hash received and appended to file');
                })
                .catch(err => {
                  console.error(err);
                  // If there's an error while appending the hash to the file, send a 500 error response
                  res.writeHead(500, { 'Content-Type': 'text/plain' });
                  res.end('Error appending hash to file');
                });
            })
            .catch(console.error);
        });
      } else {
        // Send a 400 error response if the request method is not POST
        res.writeHead(400, { 'Content-Type': 'text/plain' });
        res.end('Invalid request');
      }
    }
  }
});

// Start the server and listen on port 3000
server.listen(3000, () => {
  console.log('Server listening on port 3000');
});