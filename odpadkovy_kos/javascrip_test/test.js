var email = "";
if (localStorage.getItem("hasSeenHelloWorld")) {
    code8 = localStorage.getItem("hasSeenHelloWorld");
}
else {
    console.log("The user has not seen the 'Hello World' label before.");
    // Redirect the user to the login page
    window.location.href = "register_in.html";
}


// Combine the input values into a single variable
var combined = code8;

// Create a new XMLHttpRequest object
var xhr = new XMLHttpRequest();

// Configure the request
xhr.open("POST", "http://localhost:3000/check_code", true);
xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

// Set up a callback function for when the response is received
xhr.onreadystatechange = function () {
    if (xhr.readyState == XMLHttpRequest.DONE) {
        if (xhr.status == 200) {
            console.log("Received response from server:", xhr.responseText);
            // If the response is "true", update the output label with a success message
            if (xhr.responseText == "false") {
                console.log("The user has not seen the 'Hello World' label before.");
                // Redirect the user to the login page
                window.location.href = "register_in.html";
            }
        } else {
            // If the request fails, log an error message
            console.log("AJAX request failed:", xhr.statusText);
        }
    }
};

// Encode the input variable and send it as the request body
var data = encodeURIComponent(combined);
xhr.send(data);

















