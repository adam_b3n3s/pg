def first(word):
    return word[0]

def last(word):
    return word[-1]

def middle(word):
    return word[1:-1]


def isPalindrome(word):
    if (first(word) == last(word)):
        if(not middle(word)):
            return True
        return isPalindrome(middle(word))
    else:
        return False



print(isPalindrome("honinoh"))