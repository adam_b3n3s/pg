import time
t = time.time()
daysSince1January1970 = int(((((t/60)/60)/24)))
print(daysSince1January1970)

hoursThisDay = int((((((t/60)/60)/24))-daysSince1January1970)*24) + 2
print(hoursThisDay)

minutes = int((((((((t/60)/60)/24))-daysSince1January1970)*24 + 2) - hoursThisDay) * 60)
print(minutes)

seconds = int(((((((((t/60)/60)/24))-daysSince1January1970)*24 + 2) - hoursThisDay) * 60 - minutes) * 60)
print(seconds)