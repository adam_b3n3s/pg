const fs = require('fs');


// specify the path of the file to read
const filePath = '/home/adam/email_jmeno_heslo.txt';

// read the file asynchronously
fs.readFile(filePath, 'utf8', (err, data) => {
    if (err) throw err;

    // split the file contents by newlines
    const lines = data.trim().split('\n');

    // extract the first two lines
    const firstLine = lines[0];
    const secondLine = lines[1];
    const nodemailer = require("nodemailer");
    const { callbackPromise } = require("nodemailer/lib/shared");

    const transporter = nodemailer.createTransport({
        service: "hotmail",
        auth: {
            user: firstLine, // generated ethereal user
            pass: secondLine // generated ethereal password
        }
    });

    const options = {
        from: "Adam Benes",
        to: "ad.benes@gmail.com",
        subject: "verifikace",
        text: "Ahoj, tohle je verifikace. Adam."
    };

    transporter.sendMail(options, function (err, info) {
        if (err) {
            console.log(err);
            return;
        }
        console.log(info.response);
    });

});

