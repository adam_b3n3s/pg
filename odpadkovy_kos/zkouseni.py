import random

tezkeVety = [
"– Dokažte, že Zn je pole právě tehdy, když je n prvočíslo.",
"– Dokažte, že n lineárně nezávislých vektorů nelze vyjádřit lineárními kombinacemi n−1 vektorů.",
"– Přesně formulujte větu o vzájemném vztahu homomorfismu a jeho matice a dokažte ji (maticová reprezentace homomorfismů).",
"– Přesně formulujte větu o vzájemném vztahu hodnosti homomorfismu a hodnosti jeho matice a dokažte ji.",
"– Přesně formulujte větu o rozšíření lineárně nezávislé množiny vektorů na bázi celého prostoru a dokažte ji.",
"– Uveďte přesné znění věty charakterizující bázi pomocí rozšiřování zobrazení na homomorfismus. Dokažte jednu implikaci.",
"– Uveďte přesné znění věty o hodnosti a defektu homomorfismu a dokažte ji.",
"– Uveďte přesné znění věty o dimenzích spojení a průniku a dokažte ji.",
"– Přesně formulujte větu o řešitelnosti a množině všech řešení soustavy lineárních rovnic a dokažte ji."
]

vse = [
"– Definujte pojem komutativní těleso. Příklady.",
"– Definujte násobení matic. Příklady.",
"– Definujte pojem vektorový prostor. Příklady.",
"– Definujte pojem lineární obal podmnožiny vektorového prostoru. Příklady.",
"– Definujte pojem lineárně nezávislá množina vektorů. Příklady.",
"– Definujte pojem množina generátorů vektorového prostoru. Příklady.",
"– Definujte pojem báze vektorového prostoru. Příklady.",
"– Definujte pojem spojení podprostorů vektorového prostoru. Příklady.",
"– Definujte pojem homomorfismus. Příklady.",
"– Definujte pojem hodnost homomorfismu. Příklady.",
"– Definujte pojem matice homomorfismu. Příklady.",
"– Definujte pojmy symetrická matice a antisymetrická matice. Příklady.",
"– Definujte pojem hermitovská matice. Příklady.",
"– Definujte pojem invertibilní matice. Příklady.",
"– Definujte pojmy konečně a nekonečně generovaný prostor. Příklady.",
"– Definujte pojem množina generátorů vektorového prostoru. Příklady.",
"– Definujte pojem matice přechodu. Příklady.",
"– Definujte pojem lineární algebra. Příklady.",
"– Definujte pojem matice homomorfismu. Příklady.",
"– Definujte pojem elementární úprava matice. Příklady.",
"– Definujte pojem hodnost matice. Příklady.",
"– Přesně formulujte větu o převedení matice na odstupňovaný tvar.",
"– Přesně formulujte větu o převedení symetrické matice na diagonální tvar.",
"– Přesně formulujte větu o rozšíření lineárně nezávislé podmnožiny vektorového prostoru na jeho bázi.",
"– Přesně formulujte větu o vyjádření lineárního obalu podmnožiny vektorového prostoru pomocí lineárních kombinací.",
"– Přesně formulujte větu o základní vlastnosti báze týkající se jednoznačnosti vyjádření vektorů.",
"– Přesně formulujte větu o vzájemně jednoznačném vztahu homomorfismů a matic (maticová reprezentace homomorfismů).",
"– Přesně formulujte větu o vztahu dimenzí dvou podprostorů vektorového prostoru a jejich direktního součtu.",
"– Ekvivalentním způsobem vyjádřete skutečnost, že je množina vektorů lineárně nezávislá.",
"– Ekvivalentním způsobem vyjádřete skutečnost, že je množina vektorů lineárně závislá.",
"– Dokažte, že inverzním zobrazením k izomorfismu je izomorfismus.",
"– Dokažte, že úplným vzorem vektoru při homomorfismu je lineární množina.",
"– Dokažte, že úplným vzorem podprostoru při homomorfismu je podprostor.",
"– Dokažte, že homomorfismus zobrazuje množinu generátorů vektorového prostoru na množinu generátorů obrazu tohoto prostoru.",
"– Dokažte, že homomorfismus zobrazuje podprostor vždy na podprostor.",
"– Dokažte, že skládání homomorfismů odpovídá násobení matic.",
"– Dokažte, že průnik podprostorů vektorového prostoru je podprostor.",
"– Dokažte, že matice je regulární právě tehdy, když je invertibilní.",
"– Dokažte, že složení dvou homomorfismů je homomorfismus. Nejprve uveďte, na jakých vektorových prostorech působí.",
"– Dokažte, že součet dvou homomorfismů je homomorfismus. Nejprve uveďte, na jakých vektorových prostorech působí.",
"– Dokažte, že je-li fg izomorfismus, potom je g monomorfismus a f epimorfismus. Uveďte, na jakých vektorových prostorech homomorfismy f a g působí.",
"– Dokažte, že monomorfismus je charakterizován nulovým jádrem.",
"– Dokažte, že hodnost navzájem transponovaných matic je stejná.",
"– Dokažte, že Zn je pole právě tehdy, když je n prvočíslo.",
"– Dokažte, že n lineárně nezávislých vektorů nelze vyjádřit lineárními kombinacemi n−1 vektorů.",
"– Přesně formulujte větu o vzájemném vztahu homomorfismu a jeho matice a dokažte ji (maticová reprezentace homomorfismů).",
"– Přesně formulujte větu o vzájemném vztahu hodnosti homomorfismu a hodnosti jeho matice a dokažte ji.",
"– Přesně formulujte větu o rozšíření lineárně nezávislé množiny vektorů na bázi celého prostoru a dokažte ji.",
"– Uveďte přesné znění věty charakterizující bázi pomocí rozšiřování zobrazení na homomorfismus. Dokažte alespoň jednu implikaci.",
"– Uveďte přesné znění věty o hodnosti a defektu homomorfismu a dokažte ji.",
"– Uveďte přesné znění věty o dimenzích spojení a průniku a dokažte ji.",
"– Přesně formulujte větu o řešitelnosti a množině všech řešení soustavy lineárních rovnic a dokažte ji.",
]

random.shuffle(vse)
neumim = vse
while neumim != []:
    print("new round")
    vse = neumim
    random.shuffle(vse)
    neumim = []
    for i in range(len(vse)):
        print(vse[i])
        inp = input(len(vse)-i)
        if inp != "":
            neumim.append(vse[i])