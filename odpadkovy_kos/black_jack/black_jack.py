import random
import time
import FFnet

TWO = 2
THREE = 3
FOUR = 4
FIVE = 5
SIX = 6
SEVEN = 7
EIGHT = 8
NINE = 9
TEN = 10
ELEVEN = 11

def cardsInit():
    cards = []
    for i in range(4):
        for j in range(TWO, ELEVEN+1):
            cards.append(j)
    random.shuffle(cards)
    return cards


class Player:
    def __init__(self, AI = True):
        self.money = 500
        self.cards = []
        self.sum = 0
        self.AI = AI
        self.death = False
        self.bet = 0

class Master(Player):
    def __init__(self):
        super().__init__()
        self.money = 10000


ROUNDS = 10
PLAYERS = 2

players = [Player() for _ in range(PLAYERS)]
master = Master()






def masterPlay_(player):
    print("MASTER IS PLAYING")
    if player.AI == False:
        print("players cards are:", player.cards, "\n", "players money are:", player.money)
        whatDoesHeDo = int(input("Do you want another card?\nWrite 0 if you do not want another card and write 1 if you want to take card\n"))

        while not (whatDoesHeDo == 0 or whatDoesHeDo == 1):
            print("invalid")
            whatDoesHeDo = int(input("Do you want another card?\nWrite 0 if you do not want another card and write 1 if you want to take card\n"))
        if whatDoesHeDo == 1:
            print("The card you pick:", cards[-1])
            player.cards.append(cards[-1])
            player.sum += cards.pop()
        if player.sum > 21:
            player.death = True
            print("Masters is DEATH")
        else:
            print("Players deck is:", player.cards)

        time.sleep(1.5)
        print("______________________________")
        if whatDoesHeDo > 0 and player.death == False:
            masterPlay_(player)
    else:
        whatDoesHeDo = int(random.randint(0, 1))

        if whatDoesHeDo == 1:
            player.cards.append(cards[-1])
            player.sum += cards.pop()
        if player.sum > 21:
            player.death = True
        if whatDoesHeDo > 0 and player.death == False:
            masterPlay_(player)



def playerPlay(player):
    if player.AI == False:
        whatDoesHeDo = int(input("Do you want another card?\nWrite 0 if you do not want another card and write a number if you want to bet money and add another card\n"))
        while whatDoesHeDo < 0 or whatDoesHeDo > player.money:
            print("invalid")
            whatDoesHeDo = int(input("Do you want another card?\nWrite 0 if you do not want another card and write a number if you want to bet money and add another card\n"))
        if whatDoesHeDo > 0:
            print("The card you pick:", cards[-1])
            player.cards.append(cards[-1])
            player.sum += cards.pop()
            player.money -= whatDoesHeDo
            player.bet += whatDoesHeDo
        if player.sum > 21:
            player.death = True
            master.money += player.bet
            players[q].bet = 0
            print("Masters money increase by:", player.bet)
        else:
            print("Players deck is:", player.cards)

        time.sleep(1.5)
        print("______________________________")
        if whatDoesHeDo > 0 and player.death == False:
            print("players cards are:", player.cards, "\n", "players money are:", player.money)
            playerPlay(player)


def AIplay(player):
    neuron = FFnet(52, 10, 50)





def getReward(moneyOfPlayers, players, i):
    return players[i].money - moneyOfPlayers[i]
    

def notShuffling(cards, action, i):
    moneyOfPlayers = []
    for u in range(PLAYERS):
        moneyOfPlayers[u] = players[u].money
        players[u].cards = [cards[-1]]
        players[u].sum = cards.pop()
        players[u].cards.append(cards[-1])
        players[u].sum += cards.pop()

    master.cards = [cards[-1]]
    master.sum = cards.pop()
    master.cards.append(cards[-1])
    master.sum += cards.pop()

    for i in range(PLAYERS+1):
        if i == PLAYERS:
            masterPlay_(master)
        else:
            playerPlayAI(players[i])

    if master.death == True:
        for q in range(PLAYERS):
            players[q].money += 2*players[q].bet
            master.money -= players[q].bet
    else:
        for q in range(PLAYERS):
            if players[q].sum > master.sum:
                players[q].money += 2*players[q].bet
                master.money -= players[q].bet
                players[q].bet = 0
            if players[q].sum < master.sum:
                master.money += players[q].bet
                players[q].bet = 0
    return getReward(moneyOfPlayers, players, i)

    
def playerPlayAI(player, action, cards):
    if action < 0.01:
        pass
    else:
        player.sum


        playerPlayAI








def playerPlay_(player):
    if players[i].AI == False:
        print("PLAYER",i + 1 , "IS PLAYING")
        print("players cards are:", players[i].cards, "\n", "players money are:", players[i].money)
        print("Write number how much you want to bet (can be ZERO):")
        x = int(input())
        players[i].money -= x
        players[i].bet += x
        playerPlay(player)
    else:
        AIplay(player)

for i in range(ROUNDS):
    cards = cardsInit()
    for u in range(PLAYERS):
        players[u].cards = [cards[-1]]
        players[u].sum = cards.pop()
        players[u].cards.append(cards[-1])
        players[u].sum += cards.pop()

    master.cards = [cards[-1]]
    master.sum = cards.pop()
    master.cards.append(cards[-1])
    master.sum += cards.pop()

    for i in range(PLAYERS+1):
        if i == PLAYERS:
            masterPlay_(master)
        else:
            playerPlay_(players[i])

    if master.death == True:
        for q in range(PLAYERS):
            players[q].money += 2*players[q].bet
            master.money -= players[q].bet
    else:
        for q in range(PLAYERS):
            if players[q].sum > master.sum:
                players[q].money += 2*players[q].bet
                master.money -= players[q].bet
                players[q].bet = 0
            if players[q].sum < master.sum:
                master.money += players[q].bet
                players[q].bet = 0

    print("______________________________________")
    print("______________________________________")

    print("MASTER HAS:", master.money, "\n")

    for q in range(PLAYERS):
        print("Player", q+1 , "has:", players[q].money)
    print("______________________________________")
    print("______________________________________")
    input()