import tkinter as tk
from tkinter import filedialog

# Create the main window
root = tk.Tk()
root.title("Drawing Application")

# Create the canvas for drawing
canvas = tk.Canvas(root, width=400, height=400, bg='white')
canvas.pack()

# Create the "Save" button
def save_image():
    # Prompt the user to select a file to save the image to
    filepath = filedialog.asksaveasfilename(defaultextension='.png')
    
    # Save the image to the selected file
    canvas.postscript(file=filepath)

save_button = tk.Button(root, text="Save", command=save_image)
save_button.pack()

# Run the main loop
root.mainloop()
