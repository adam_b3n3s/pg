import random

tezkeVety = [
"– Přesně zformulujte a dokažte Cramerovo pravidlo.",
"– Přesně a kultivovaně zformulujte větu o vztahu bilineární formy a její matice.",
"– Zformulujte a dokažte větu o existenci normální báze symetrické formy.",
"– Zformulujte a dokažte Sylvesterův zákon o setrvačnosti.",
"– Zformulujte Gramův-Schmidtův ortogonalizační proces. Dokažte, že lineárně nezávislou množinu vektorů přetvoří na lineárně nezávislou ortogonální množinu vektorů.",
"– Zformulujte a dokažte větu o vlastních číslech reálné symetrické matice.",
"– Přesně a kultivovaně zformulujte větu o vlastnostech ortogonálního zobrazení a dokažte ji.",
"– Zformulujte a dokažte větu o existenci duální báze."
]

vse = [
"– Definujte pojem stopa matice.",
"– Definujte pojem spektrum matice.",
"– Definujte pojem vlastní číslo a vlastní vektor matice.",
"– Definujte pojem ortogonální matice.",
"– Definujte pojem matice lineární formy.",
"– Definujte pojem diagonalizovatelná matice.",
"– Definujte pojem signatura kvadratické formy.",
"– Definujte pojmy ortogonální zobrazení a izometrie.",
"– Definujte pojem determinant.",
"– Definujte pojem charakteristický polynom matice.",
"– Definujte pojem minimální polynom matice.",
"– Definujte pojem lineární forma.",
"– Definujte pojem algebraický doplněk.",
"– Definujte pojem bilineární forma.",
"– Definujte pojem kvadratická forma.",
"– Definujte pojem matice bilineární formy.",
"– Definujte pojem matice kvadratické formy.",
"– Definujte pojem skalární součin.",
"– Definujte pojem Jordanova matice.",
"– Napište přesné znění věty o násobení determinantů.",
"– Přesně zformulujte Cramerovo pravidlo.",
"– Přesně zformulujte větu o determinantu blokové matice.",
"– Napište rozvoj determinantu matice A = (aij ) řádu 7 podle 3. sloupce.",
"– Napište přesné znění věty o vyjádření inverzní matice pomocí determinantů a dokažte ji.",
"– Přesně a kultivovaně zformulujte Cayleyovu-Hamiltonovu větu.",
"– Přesně zformulujte větu o existenci Jordanova kanonického tvaru matice.",
"– Přesně zformulujte větu o diagonalizovatelnosti matice.",
"– Přesně a kultivovaně formulujte Sylvesterův zákon o setrvačnosti.",
"– Zformulujte a dokažte větu o ortogonální projekci a Fourierových koeficientech.",
"– Přesně a kultivovaně zformulujte Cauchyovu-Schwarzovu nerovnost a dokažte ji.",
"– Přesně a kultivovaně zformulujte trojúhelníkovou nerovnost a dokažte ji.",
"– Zformulujte a dokažte větu o rozvoji determinantu podle i-tého řádku.",
"– Přesně a kultivovaně zformulujte a dokažte větu o existenci ortonormální báze.",
"– Přesně a kultivovaně zformulujte a dokažte větu o vztahu dimenzí navzájem duálních prostorů.",
"– Zformulujte a dokažte větu o existenci nenulového anulujícího polynomu čtvercové matice.",
"– Zformulujte a dokažte Pythagorovu větu a Kosinovou větu.",
"– Přesně zformulujte a dokažte Cramerovo pravidlo.",
"– Přesně a kultivovaně zformulujte a dokažte větu o vztahu bilineární formy a její matice.",
"– Zformulujte a dokažte větu o existenci normální báze symetrické formy.",
"– Zformulujte a dokažte Sylvesterův zákon o setrvačnosti.",
"– Zformulujte Gramův-Schmidtův ortogonalizační proces. Dokažte, že lineárně nezávislou množinu vektorů přetvoří na lineárně nezávislou ortogonální množinu vektorů.",
"– Zformulujte a dokažte větu o vlastních číslech reálné symetrické matice.",
"– Přesně a kultivovaně zformulujte a dokažte větu o vlastnostech ortogonálního zobrazení.",
"– Zformulujte a dokažte větu o existenci duální báze."
]

random.shuffle(vse)
neumim = vse
while neumim != []:
    print("new round")
    vse = neumim
    random.shuffle(vse)
    neumim = []
    for i in range(len(vse)):
        print(len(vse)-i)
        print(vse[i])
        inp = input("")
        if inp == "n":
            neumim.append(vse[i])