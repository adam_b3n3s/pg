package gladiators;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Random;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import static java.time.Clock.system;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.SwingConstants;

public class game extends JFrame implements MouseListener {

    //podklad
    static JPanel basemant = new JPanel();
    static JLabel labBasemant = new JLabel();

    static JFrame jf = new JFrame("AREA");
    static JPanel mouseMovement = new JPanel();

    static int mysX;
    static int mysY;
    //bere velikost obrazovk
    //nakonec nebylo pouzito kvuli podkladu
    static Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
    static JPanel exit = new JPanel();

    //text fieldy di nich hrac pise kolik gladiatoru a kolik hracu bude hrat
    static JPanel panNumberOfPlayers = new JPanel();
    static JLabel labNumberOfPlayers = new JLabel();
    static JTextField textFieldNumberOfPlayers = new JTextField();

    static JPanel panNumberOfGladiatorsForEachPlayer = new JPanel();
    static JLabel labNumberOfGladiatorsForEachPlayer = new JLabel();
    static JTextField textFieldNumberOfGladiatorsForEachPlayer = new JTextField();

    //combo boxy
    static JComboBox whichGladiator = new JComboBox();
    static JComboBox whichWeapon = new JComboBox();
    static JComboBox whichHand = new JComboBox();

    static JComboBox whichGladiatorRepair = new JComboBox();
    static JComboBox whichHandRepair = new JComboBox();

    static JComboBox whichGladiatorYouChoose = new JComboBox();

    //potvrzuji kolik gladiatoru a kolik hracu
    static JPanel panWantToConfirmAll = new JPanel();
    static JLabel labWantToConfirmAll = new JLabel();

    //potvrzuje ze chces fight
    static JPanel fight = new JPanel();
    static JLabel labFight = new JLabel();

    static JPanel treatment = new JPanel();
    static JLabel labTreatment = new JLabel();

    static JPanel repair = new JPanel();
    static JLabel labRepair = new JLabel();

    static JPanel newWeapon = new JPanel();
    static JLabel labNewWeapon = new JLabel();

    //cely fight spusten
    static JPanel fightFrame = new JPanel();
    static JPanel treatmentFrame = new JPanel();

    static JPanel gladiatorOneLives = new JPanel();
    static JLabel labGladiatorOneLives = new JLabel();
    static JPanel gladiatorTwoLives = new JPanel();
    static JLabel labGladiatorTwoLives = new JLabel();

    static JPanel allGladiators11 = new JPanel();
    static JLabel labAllGladiators11 = new JLabel();

    static JPanel allGladiators12 = new JPanel();
    static JLabel labAllGladiators12 = new JLabel();

    static JPanel allGladiators13 = new JPanel();
    static JLabel labAllGladiators13 = new JLabel();

    static JPanel allGladiators14 = new JPanel();
    static JLabel labAllGladiators14 = new JLabel();

    static JPanel allGladiators15 = new JPanel();
    static JLabel labAllGladiators15 = new JLabel();

    static JPanel allGladiators21 = new JPanel();
    static JLabel labAllGladiators21 = new JLabel();

    static JPanel allGladiators22 = new JPanel();
    static JLabel labAllGladiators22 = new JLabel();

    static JPanel allGladiators23 = new JPanel();
    static JLabel labAllGladiators23 = new JLabel();

    static JPanel allGladiators24 = new JPanel();
    static JLabel labAllGladiators24 = new JLabel();

    static JPanel allGladiators25 = new JPanel();
    static JLabel labAllGladiators25 = new JLabel();

    static JPanel allGladiators31 = new JPanel();
    static JLabel labAllGladiators31 = new JLabel();

    static JPanel allGladiators32 = new JPanel();
    static JLabel labAllGladiators32 = new JLabel();

    static JPanel allGladiators33 = new JPanel();
    static JLabel labAllGladiators33 = new JLabel();

    static JPanel allGladiators34 = new JPanel();
    static JLabel labAllGladiators34 = new JLabel();

    static JPanel allGladiators35 = new JPanel();
    static JLabel labAllGladiators35 = new JLabel();

    static JPanel allGladiators41 = new JPanel();
    static JLabel labAllGladiators41 = new JLabel();

    static JPanel allGladiators42 = new JPanel();
    static JLabel labAllGladiators42 = new JLabel();

    static JPanel allGladiators43 = new JPanel();
    static JLabel labAllGladiators43 = new JLabel();

    static JPanel allGladiators44 = new JPanel();
    static JLabel labAllGladiators44 = new JLabel();

    static JPanel allGladiators45 = new JPanel();
    static JLabel labAllGladiators45 = new JLabel();

    static JPanel gladiatorOne = new JPanel();
    static JLabel labGladiatorOne = new JLabel();
    static JPanel gladiatorOne1 = new JPanel();
    static JLabel labGladiatorOne1 = new JLabel();

    static JPanel gladiatorTwo = new JPanel();
    static JLabel labGladiatorTwo = new JLabel();
    static JPanel gladiatorTwo2 = new JPanel();
    static JLabel labGladiatorTwo2 = new JLabel();

    static JPanel gladiatorThree = new JPanel();
    static JLabel labGladiatorThree = new JLabel();
    static JPanel gladiatorThree3 = new JPanel();
    static JLabel labGladiatorThree3 = new JLabel();

    static JPanel gladiatorFourth = new JPanel();
    static JLabel labGladiatorFourth = new JLabel();
    static JPanel gladiatorFourth4 = new JPanel();
    static JLabel labGladiatorFourth4 = new JLabel();

    static JPanel wantToEnd = new JPanel();
    static JLabel labWantToEnd = new JLabel();

    static JPanel redLight = new JPanel();

    static JPanel animal = new JPanel();
    static JLabel labAnimal = new JLabel();

    static JPanel gladiator = new JPanel();
    static JLabel labGladiator = new JLabel();

    static JPanel gladiatorOneHandOne = new JPanel();
    static JPanel gladiatorOneHandTwo = new JPanel();
    static JLabel labGladiatorOneHandOne = new JLabel();
    static JLabel labGladiatorOneHandTwo = new JLabel();
    static JPanel gladiatorOneHandThree = new JPanel();
    static JLabel labGladiatorOneHandThree = new JLabel();

    static JPanel gladiatorTwoHandOne = new JPanel();
    static JPanel gladiatorTwoHandTwo = new JPanel();
    static JLabel labGladiatorTwoHandOne = new JLabel();
    static JLabel labGladiatorTwoHandTwo = new JLabel();
    static JPanel gladiatorTwoHandThree = new JPanel();
    static JLabel labGladiatorTwoHandThree = new JLabel();

    static JPanel gladiatorOneFight = new JPanel();
    static JPanel gladiatorTwoFight = new JPanel();

    static JLabel labGladiatorOneFight = new JLabel();
    static JLabel labGladiatorTwoFight = new JLabel();

    static JPanel editorNext = new JPanel();
    static JLabel labEditorNext = new JLabel();

    //promene k funkcnosti algoritmu/programu
    public static int numberOfPlayers = 3;

    public static int numberOfGladiatorsForEachPlayer = 2;
    public static int numberOfGladiators = 6;
    //pro jistotu více nez by bylo potreba
    static gladiator G[] = new gladiator[1000];
    static weapon W[][][] = new weapon[80][80][70];
    static player P[] = new player[50];
    public static Random RND = new Random();
    public static int counterFirst = 2;
    public static int counterSecond = 2;
    public static int helpThrowerFirst = 0;
    public static int helpThrowerSecond = 0;
    public static int helpThrowerThird = 0;
    public static int helpThrowerFourth = 0;
    public static int x = 0;
    public static int y = 0;
    public static int numberOfWeapons = 0;

    public static int counterRound = 1;

    //pomoci techto promenych pocita kolik gladiatoru je jeste predemnou
    public static int beforeYou = 0;
    public static int beforeYou2 = 1;

    public static boolean throwingFirst = true;
    public static boolean throwingSecond = true;
    public static boolean throwingThird = true;
    public static boolean throwingFourth = true;

    public static boolean canPlay = false;
    public static boolean sineMissione = false;
    public static boolean fightTrue = false;

    public static boolean gladiatorThrowFirst = false;
    public static boolean gladiatorThrowSecond = false;
    public static boolean gladiatorThrowThird = false;
    public static boolean gladiatorThrowFourth = false;
    public static int gladiatorThrowFirstWeapon = 0;
    public static int gladiatorThrowSecondWeapon = 0;
    public static int gladiatorThrowThirdWeapon = 0;
    public static int gladiatorThrowFourthWeapon = 0;
    public static int gladiatorThrowFirstHarm = 0;
    public static int gladiatorThrowSecondHarm = 0;
    public static int gladiatorThrowThirdHarm = 0;
    public static int gladiatorThrowFourthHarm = 0;

    public static int gladiatorForFight1 = 0;
    public static int gladiatorForFight2 = 0;
    public static int gladiatorForFight3 = 0;
    public static int gladiatorForFight4 = 0;

    public static int A = 0;
    public static int B = 0;

    public static int howManyPlayersStillPlay = 0;

    public static int gladiatorTreatment = 1;
    public static boolean counterForTreating = true;

    public static int editor = 1;

    static JPanel treating = new JPanel();
    static JLabel labTreating = new JLabel();

    static JPanel treating2 = new JPanel();
    static JLabel labTreating2 = new JLabel();

    static JPanel training = new JPanel();
    static JLabel labTraining = new JLabel();

    static JPanel gettingGladiator = new JPanel();
    static JLabel labGettingGladiator = new JLabel();

    static JPanel sinemissione = new JPanel();
    static JLabel labSinemissione = new JLabel();

    //vsechy sendy na podobnem principu
    static JPanel send = new JPanel();
    static JLabel labSend = new JLabel();
    static JPanel send2 = new JPanel();
    static JLabel labSend2 = new JLabel();
    static JPanel send3 = new JPanel();
    static JLabel labSend3 = new JLabel();
    static JPanel send4 = new JPanel();
    static JLabel labSend4 = new JLabel();
    static JPanel send5 = new JPanel();
    static JLabel labSend5 = new JLabel();
    static JPanel send6 = new JPanel();
    static JLabel labSend6 = new JLabel();
    static JPanel send7 = new JPanel();
    static JLabel labSend7 = new JLabel();

    static JPanel headline = new JPanel();
    static JLabel labHeadline = new JLabel();

    static JPanel endScrean = new JPanel();
    static JLabel labEndScrean = new JLabel();

    static JPanel infoTable = new JPanel();
    static JLabel labInfoTable = new JLabel();

    static JPanel wantGladiator = new JPanel();
    static JLabel labWantGladiator = new JLabel();

    static JPanel howManyYouHave = new JPanel();
    static JLabel labHowManyYouHave = new JLabel();

    public static int whichPlayerIsChoosing = 0;

    public static int gladiatorsCounter = 1;
    public static int counterHowManyYouTakeAway = 0;

    static ImageIcon box = new ImageIcon();
    static ImageIcon borduraB = new ImageIcon();
    static ImageIcon borduraW = new ImageIcon();

    public static void main(String[] args) throws IOException {

        File fileIcon = new File("programovani_arabska/gladiators/ikony/box.png");
        BufferedImage icon = ImageIO.read(fileIcon);
        box = new ImageIcon(icon);

        fileIcon = new File("programovani_arabska/gladiators/ikony/bordura_black.png");
        icon = ImageIO.read(fileIcon);
        borduraB = new ImageIcon(icon);

        fileIcon = new File("programovani_arabska/gladiators/ikony/bordura_white.png");
        icon = ImageIO.read(fileIcon);
        borduraW = new ImageIcon(icon);

        jf.setSize(1450, 1080);
        jf.setUndecorated(true);
        jf.setBackground(Color.GRAY);
        jf.setLocation(screen.width / 2 - 765, screen.height / 2 - 540);
        jf.setVisible(true);
        jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jf.setLayout(null);
        jf.setAlwaysOnTop(true);
        //novy exit

        jf.add(exit);
        exit.setLocation(jf.getWidth() - 40, 0);
        exit.setSize(40, 30);
        exit.setBackground(Color.red);

        jf.add(headline);
        headline.setSize(400, 30);
        headline.setLocation(jf.getWidth() / 2 - headline.getWidth() / 2, 27);
        headline.setBackground(Color.darkGray);
        headline.add(labHeadline);
        headline.setVisible(false);
        labHeadline.setForeground(Color.WHITE);

        jf.add(infoTable);
        infoTable.setSize(300, 75);
        infoTable.setLocation(jf.getWidth() / 2 - infoTable.getWidth() - 650 / 2, 300);
        infoTable.setBackground(Color.darkGray);
        infoTable.add(labInfoTable);
        infoTable.setVisible(false);
        labInfoTable.setForeground(Color.WHITE);

        jf.add(fight);
        fight.setSize(400, 75);
        fight.setLocation(jf.getWidth() / 2 - fight.getWidth() / 2, 5);
        fight.setBackground(Color.darkGray);
        fight.add(labFight);
        fight.setVisible(false);
        labFight.setForeground(Color.WHITE);
        labFight.setText("zápas");

        jf.add(treatment);
        treatment.setSize(400, 75);
        treatment.setLocation(jf.getWidth() / 2 - treatment.getWidth() / 2, 5);
        treatment.setBackground(Color.darkGray);
        treatment.add(labTreatment);
        treatment.setVisible(false);
        labTreatment.setForeground(Color.WHITE);
        labTreatment.setText("léčit");

        jf.add(repair);
        repair.setSize(400, 75);
        repair.setLocation(jf.getWidth() / 2 - repair.getWidth() / 2, 5);
        repair.setBackground(Color.darkGray);
        repair.add(labRepair);
        repair.setVisible(false);
        labRepair.setForeground(Color.WHITE);
        labRepair.setText("opravovat");

        training.setSize(400, 75);
        training.setLocation(jf.getWidth() / 2 - training.getWidth() / 2, 5);
        training.setBackground(Color.darkGray);
        training.setVisible(false);
        training.add(labTraining);
        labTraining.setForeground(Color.WHITE);
        labTraining.setText("trénovat");

        jf.add(gettingGladiator);
        gettingGladiator.setSize(400, 75);
        gettingGladiator.setLocation(jf.getWidth() / 2 - gettingGladiator.getWidth() / 2, 5);
        gettingGladiator.setBackground(Color.darkGray);
        gettingGladiator.setVisible(false);
        gettingGladiator.add(labGettingGladiator);
        labGettingGladiator.setForeground(Color.WHITE);
        labGettingGladiator.setText("brát gladiátory?");

        jf.add(sinemissione);
        sinemissione.setSize(400, 75);
        sinemissione.setLocation(jf.getWidth() / 2 - sinemissione.getWidth() / 2, 5);
        sinemissione.setBackground(Color.darkGray);
        sinemissione.setVisible(false);
        sinemissione.add(labSinemissione);
        labSinemissione.setForeground(Color.WHITE);
        labSinemissione.setText("sinemissione");

        jf.add(newWeapon);
        newWeapon.setSize(400, 75);
        newWeapon.setLocation(jf.getWidth() / 2 - newWeapon.getWidth() / 2, 5);
        newWeapon.setBackground(Color.darkGray);
        newWeapon.add(labNewWeapon);
        newWeapon.setVisible(false);
        labNewWeapon.setForeground(Color.WHITE);
        labNewWeapon.setText("zbrojírna");

        jf.add(gladiatorOneLives);
        jf.add(gladiatorTwoLives);
        jf.add(gladiatorOneHandOne);
        jf.add(gladiatorOneHandTwo);
        jf.add(gladiatorTwoHandOne);
        jf.add(gladiatorTwoHandTwo);
        jf.add(gladiatorOneHandThree);
        jf.add(gladiatorTwoHandThree);

        jf.add(gladiatorOneFight);
        jf.add(gladiatorTwoFight);

        jf.add(wantToEnd);

        jf.add(redLight);

        jf.add(animal);
        jf.add(gladiator);

        jf.add(treating);
        jf.add(treating2);
        jf.add(training);
        jf.add(send);
        jf.add(send2);
        jf.add(send3);
        jf.add(send4);
        jf.add(send5);
        jf.add(send6);
        jf.add(send7);
        jf.add(endScrean);
        jf.add(wantGladiator);
        jf.add(editorNext);
        jf.add(howManyYouHave);

        jf.add(fightFrame);
        fightFrame.setLocation(0, 0);
        fightFrame.setSize(1450, 1080);
        fightFrame.setVisible(false);
        fightTrue = false;

        jf.add(treatmentFrame);
        treatmentFrame.setLocation(100, 75);
        treatmentFrame.setSize(150, 75);
        treatmentFrame.setBackground(Color.black);
        treatmentFrame.setVisible(false);

        gladiatorOneLives.setSize(75, 75);
        gladiatorOneLives.setLocation(fightFrame.getX() + 100, fightFrame.getY() + fightFrame.getHeight() - 200);
        gladiatorOneLives.setBackground(Color.blue);
        gladiatorOneLives.setVisible(false);
        gladiatorOneLives.add(labGladiatorOneLives);
        labGladiatorOneLives.setForeground(Color.WHITE);

        jf.add(gladiatorOne1);
        gladiatorOne1.setSize(150, 75);
        gladiatorOne1.setLocation(jf.getWidth() / 2 - gladiatorOne1.getWidth() / 2 - 500, 500 - 75);
        gladiatorOne1.setBackground(Color.blue);
        gladiatorOne1.setVisible(false);
        gladiatorOne1.add(labGladiatorOne1);
        labGladiatorOne1.setForeground(Color.WHITE);

        jf.add(gladiatorOne);
        gladiatorOne.setSize(150, 450);
        gladiatorOne.setLocation(jf.getWidth() / 2 - gladiatorOne.getWidth() / 2 - 500, 500);
        gladiatorOne.setBackground(Color.blue);
        gladiatorOne.setVisible(false);
        gladiatorOne.add(labGladiatorOne);
        labGladiatorOne.setForeground(Color.WHITE);

        jf.add(gladiatorTwo2);
        gladiatorTwo2.setSize(150, 75);
        gladiatorTwo2.setLocation(jf.getWidth() / 2 - gladiatorTwo2.getWidth() / 2 - 200, 500 - 75);
        gladiatorTwo2.setBackground(Color.blue);
        gladiatorTwo2.setVisible(false);
        gladiatorTwo2.add(labGladiatorTwo2);
        labGladiatorTwo2.setForeground(Color.WHITE);

        jf.add(gladiatorTwo);
        gladiatorTwo.setSize(150, 450);
        gladiatorTwo.setLocation(jf.getWidth() / 2 - gladiatorTwo.getWidth() / 2 - 200, 500);
        gladiatorTwo.setBackground(Color.blue);
        gladiatorTwo.setVisible(false);
        gladiatorTwo.add(labGladiatorTwo);
        labGladiatorTwo.setForeground(Color.WHITE);

        jf.add(gladiatorThree3);
        gladiatorThree3.setSize(150, 75);
        gladiatorThree3.setLocation(jf.getWidth() / 2 - gladiatorThree3.getWidth() / 2 + 200, 500 - 75);
        gladiatorThree3.setBackground(Color.blue);
        gladiatorThree3.setVisible(false);
        gladiatorThree3.add(labGladiatorThree3);
        labGladiatorThree3.setForeground(Color.WHITE);

        jf.add(gladiatorThree);
        gladiatorThree.setSize(150, 450);
        gladiatorThree.setLocation(jf.getWidth() / 2 - gladiatorThree.getWidth() / 2 + 200, 500);
        gladiatorThree.setBackground(Color.blue);
        gladiatorThree.setVisible(false);
        gladiatorThree.add(labGladiatorThree);
        labGladiatorThree.setForeground(Color.WHITE);

        jf.add(gladiatorFourth4);
        gladiatorFourth4.setSize(150, 75);
        gladiatorFourth4.setLocation(jf.getWidth() / 2 - gladiatorFourth4.getWidth() / 2 + 500, 500 - 75);
        gladiatorFourth4.setBackground(Color.blue);
        gladiatorFourth4.setVisible(false);
        gladiatorFourth4.add(labGladiatorFourth4);
        labGladiatorFourth4.setForeground(Color.WHITE);

        jf.add(gladiatorFourth);
        gladiatorFourth.setSize(150, 450);
        gladiatorFourth.setLocation(jf.getWidth() / 2 - gladiatorFourth.getWidth() / 2 + 500, 500);
        gladiatorFourth.setBackground(Color.blue);
        gladiatorFourth.setVisible(false);
        gladiatorFourth.add(labGladiatorFourth);
        labGladiatorFourth.setForeground(Color.WHITE);

        gladiatorTwoLives.setSize(75, 75);
        gladiatorTwoLives.setLocation(fightFrame.getX() + fightFrame.getWidth() - gladiatorTwoLives.getWidth() - 100, fightFrame.getY() + fightFrame.getHeight() - 200);
        gladiatorTwoLives.setBackground(Color.blue);
        gladiatorTwoLives.setVisible(false);
        gladiatorTwoLives.add(labGladiatorTwoLives);
        labGladiatorTwoLives.setForeground(Color.WHITE);

        wantToEnd.setSize(150, 75);
        wantToEnd.setLocation((fightFrame.getWidth()) / 2 - wantToEnd.getWidth() / 2, fightFrame.getY() + fightFrame.getHeight() - 300);
        wantToEnd.setBackground(Color.pink);
        wantToEnd.setVisible(false);
        wantToEnd.add(labWantToEnd);
        labWantToEnd.setForeground(Color.WHITE);
        labWantToEnd.setText("chceš se vzdát");

        redLight.setSize(150, 150);
        redLight.setLocation(100, 200);
        redLight.setBackground(Color.red);
        redLight.setVisible(false);

        animal.setSize(100, 30);
        animal.setLocation((fightFrame.getX() + fightFrame.getWidth()) / 2 + 100, fightFrame.getY() + fightFrame.getHeight() - 100);
        animal.setBackground(Color.blue);
        animal.setVisible(false);
        animal.add(labAnimal);
        labAnimal.setForeground(Color.WHITE);
        labAnimal.setText("zvíře");

        gladiator.setSize(100, 30);
        gladiator.setLocation((fightFrame.getX() + fightFrame.getWidth()) / 2 - gladiator.getWidth() / 2 - 100, fightFrame.getY() + fightFrame.getHeight() - 100);
        gladiator.setBackground(Color.blue);
        gladiator.setVisible(false);
        gladiator.add(labGladiator);
        labGladiator.setForeground(Color.WHITE);
        labGladiator.setText("gladiátor");

        jf.add(whichGladiator);
        whichGladiator.setVisible(false);
        whichGladiator.setLocation(100, 75);
        whichGladiator.setSize(150, 75);

        jf.add(whichWeapon);
        whichWeapon.setVisible(false);
        whichWeapon.setLocation(300, 75);
        whichWeapon.setSize(150, 75);

        jf.add(whichGladiatorRepair);
        whichGladiatorRepair.setVisible(false);
        whichGladiatorRepair.setLocation(100, 75);
        whichGladiatorRepair.setSize(150, 75);

        jf.add(whichGladiatorYouChoose);
        whichGladiatorYouChoose.setVisible(false);
        whichGladiatorYouChoose.setLocation(100, 75);
        whichGladiatorYouChoose.setSize(150, 75);

        jf.add(whichHandRepair);
        whichHandRepair.setVisible(false);
        whichHandRepair.setLocation(300, 75);
        whichHandRepair.setSize(150, 75);
        whichHandRepair.addItem("ruka: " + 1);
        whichHandRepair.addItem("ruka: " + 2);

        jf.add(whichHand);
        whichHand.setVisible(false);
        whichHand.setLocation(500, 75);
        whichHand.setSize(150, 75);
        whichHand.addItem("ruka: " + 1);
        whichHand.addItem("ruka: " + 2);
        whichHand.addItem("ruka: " + 3);

        //gladiatorOneHandOne
        gladiatorOneHandOne.setSize(150, 300);
        gladiatorOneHandOne.setLocation(fightFrame.getX() + 100, fightFrame.getY() + fightFrame.getHeight() - 700);
        gladiatorOneHandOne.setBackground(Color.blue);
        gladiatorOneHandOne.setVisible(false);
        gladiatorOneHandOne.add(labGladiatorOneHandOne);
        labGladiatorOneHandOne.setForeground(Color.WHITE);

        gladiatorOneHandTwo.setSize(150, 300);
        gladiatorOneHandTwo.setLocation(fightFrame.getX() + 300, fightFrame.getY() + fightFrame.getHeight() - 700);
        gladiatorOneHandTwo.setBackground(Color.blue);
        gladiatorOneHandTwo.setVisible(false);
        gladiatorOneHandTwo.add(labGladiatorOneHandTwo);
        labGladiatorOneHandTwo.setForeground(Color.WHITE);

        gladiatorOneHandThree.setSize(150, 300);
        gladiatorOneHandThree.setLocation(fightFrame.getX() + 300, fightFrame.getY() + fightFrame.getHeight() - 380);
        gladiatorOneHandThree.setBackground(Color.blue);
        gladiatorOneHandThree.setVisible(false);
        gladiatorOneHandThree.add(labGladiatorOneHandThree);
        labGladiatorOneHandThree.setForeground(Color.WHITE);

        gladiatorTwoHandOne.setSize(150, 300);
        gladiatorTwoHandOne.setLocation(fightFrame.getX() + fightFrame.getWidth() - gladiatorTwoHandOne.getWidth() - 300, fightFrame.getY() + fightFrame.getHeight() - 700);
        gladiatorTwoHandOne.setBackground(Color.blue);
        gladiatorTwoHandOne.setVisible(false);
        gladiatorTwoHandOne.add(labGladiatorTwoHandOne);
        labGladiatorTwoHandOne.setForeground(Color.WHITE);

        gladiatorTwoHandTwo.setSize(150, 300);
        gladiatorTwoHandTwo.setLocation(fightFrame.getX() + fightFrame.getWidth() - gladiatorTwoHandTwo.getWidth() - 100, fightFrame.getY() + fightFrame.getHeight() - 700);
        gladiatorTwoHandTwo.setBackground(Color.blue);
        gladiatorTwoHandTwo.setVisible(false);
        gladiatorTwoHandTwo.add(labGladiatorTwoHandTwo);
        labGladiatorTwoHandTwo.setForeground(Color.WHITE);

        gladiatorTwoHandThree.setSize(150, 300);
        gladiatorTwoHandThree.setLocation(fightFrame.getX() + fightFrame.getWidth() - gladiatorTwoHandThree.getWidth() - 300, fightFrame.getY() + fightFrame.getHeight() - 380);
        gladiatorTwoHandThree.setBackground(Color.blue);
        gladiatorTwoHandThree.setVisible(false);
        gladiatorTwoHandThree.add(labGladiatorTwoHandThree);
        labGladiatorTwoHandThree.setForeground(Color.WHITE);

        treating.setSize(150, 75);
        treating.setLocation(treatmentFrame.getX(), treatmentFrame.getY());
        treating.setBackground(Color.darkGray);
        treating.setVisible(false);
        treating.add(labTreating);
        labTreating.setForeground(Color.WHITE);

        treating2.setSize(150, 150);
        treating2.setLocation(treatmentFrame.getX(), treatmentFrame.getY() + treating.getHeight());
        treating2.setBackground(Color.darkGray);
        treating2.setVisible(false);
        treating2.add(labTreating2);
        labTreating2.setForeground(Color.WHITE);

        jf.add(allGladiators11);
        allGladiators11.setSize(300, 75);
        allGladiators11.setLocation(jf.getWidth() / 2 - allGladiators11.getWidth() / 2 - 500, 100);
        allGladiators11.setBackground(Color.blue);
        allGladiators11.setVisible(false);
        allGladiators11.add(labAllGladiators11);
        //allGladiators11.setLayout(null);
        labAllGladiators11.setForeground(Color.WHITE);

        jf.add(allGladiators12);
        allGladiators12.setSize(150, 375);
        allGladiators12.setLocation(jf.getWidth() / 2 - allGladiators11.getWidth() / 2 - 500, 175);
        allGladiators12.setBackground(Color.blue);
        allGladiators12.setVisible(false);
        allGladiators12.add(labAllGladiators12);

        jf.add(allGladiators13);
        allGladiators13.setSize(150, 375);
        allGladiators13.setLocation(jf.getWidth() / 2 - allGladiators11.getWidth() / 2 - 500 + 150, 175);
        allGladiators13.setBackground(Color.blue);
        allGladiators13.setVisible(false);
        allGladiators13.add(labAllGladiators13);

        jf.add(allGladiators14);
        allGladiators14.setSize(150, 375);
        allGladiators14.setLocation(jf.getWidth() / 2 - allGladiators11.getWidth() / 2 - 500, 175 + 375);
        allGladiators14.setBackground(Color.blue);
        allGladiators14.setVisible(false);
        allGladiators14.add(labAllGladiators14);

        jf.add(allGladiators15);
        allGladiators15.setSize(150, 375);
        allGladiators15.setLocation(jf.getWidth() / 2 - allGladiators11.getWidth() / 2 - 500 + 150, 175 + 375);
        allGladiators15.setBackground(Color.blue);
        allGladiators15.setVisible(false);
        allGladiators15.add(labAllGladiators15);

        jf.add(allGladiators21);
        allGladiators21.setSize(300, 75);
        allGladiators21.setLocation(jf.getWidth() / 2 - allGladiators21.getWidth() / 2 - 167, 100);
        allGladiators21.setBackground(Color.GREEN);
        allGladiators21.setVisible(false);
        allGladiators21.add(labAllGladiators21);
        //allGladiators21.setLayout(null);
        labAllGladiators21.setForeground(Color.WHITE);

        jf.add(allGladiators22);
        allGladiators22.setSize(150, 375);
        allGladiators22.setLocation(jf.getWidth() / 2 - allGladiators21.getWidth() / 2 - 167, 175);
        allGladiators22.setBackground(Color.GREEN);
        allGladiators22.setVisible(false);
        allGladiators22.add(labAllGladiators22);

        jf.add(allGladiators23);
        allGladiators23.setSize(150, 375);
        allGladiators23.setLocation(jf.getWidth() / 2 - allGladiators21.getWidth() / 2 - 167 + 150, 175);
        allGladiators23.setBackground(Color.GREEN);
        allGladiators23.setVisible(false);
        allGladiators23.add(labAllGladiators23);

        jf.add(allGladiators24);
        allGladiators24.setSize(150, 375);
        allGladiators24.setLocation(jf.getWidth() / 2 - allGladiators21.getWidth() / 2 - 167, 175 + 375);
        allGladiators24.setBackground(Color.GREEN);
        allGladiators24.setVisible(false);
        allGladiators24.add(labAllGladiators24);

        jf.add(allGladiators25);
        allGladiators25.setSize(150, 375);
        allGladiators25.setLocation(jf.getWidth() / 2 - allGladiators21.getWidth() / 2 - 167 + 150, 175 + 375);
        allGladiators25.setBackground(Color.GREEN);
        allGladiators25.setVisible(false);
        allGladiators25.add(labAllGladiators25);

        jf.add(allGladiators31);
        allGladiators31.setSize(300, 75);
        allGladiators31.setLocation(jf.getWidth() / 2 - allGladiators31.getWidth() / 2 + 167, 100);
        allGladiators31.setBackground(Color.YELLOW);
        allGladiators31.setVisible(false);
        allGladiators31.add(labAllGladiators31);
        //allGladiators31.setLayout(null);
        labAllGladiators31.setForeground(Color.WHITE);

        jf.add(allGladiators32);
        allGladiators32.setSize(150, 375);
        allGladiators32.setLocation(jf.getWidth() / 2 - allGladiators31.getWidth() / 2 + 167, 175);
        allGladiators32.setBackground(Color.YELLOW);
        allGladiators32.setVisible(false);
        allGladiators32.add(labAllGladiators32);

        jf.add(allGladiators33);
        allGladiators33.setSize(150, 375);
        allGladiators33.setLocation(jf.getWidth() / 2 - allGladiators31.getWidth() / 2 + 167 + 150, 175);
        allGladiators33.setBackground(Color.YELLOW);
        allGladiators33.setVisible(false);
        allGladiators33.add(labAllGladiators33);

        jf.add(allGladiators34);
        allGladiators34.setSize(150, 375);
        allGladiators34.setLocation(jf.getWidth() / 2 - allGladiators31.getWidth() / 2 + 167, 175 + 375);
        allGladiators34.setBackground(Color.YELLOW);
        allGladiators34.setVisible(false);
        allGladiators34.add(labAllGladiators34);

        jf.add(allGladiators35);
        allGladiators35.setSize(150, 375);
        allGladiators35.setLocation(jf.getWidth() / 2 - allGladiators31.getWidth() / 2 + 167 + 150, 175 + 375);
        allGladiators35.setBackground(Color.YELLOW);
        allGladiators35.setVisible(false);
        allGladiators35.add(labAllGladiators35);

        jf.add(allGladiators41);
        allGladiators41.setSize(300, 75);
        allGladiators41.setLocation(jf.getWidth() / 2 - allGladiators41.getWidth() / 2 + 500, 100);
        allGladiators41.setBackground(Color.ORANGE);
        allGladiators41.setVisible(false);
        allGladiators41.add(labAllGladiators41);
        //allGladiators41.setLayout(null);
        labAllGladiators41.setForeground(Color.WHITE);

        jf.add(allGladiators42);
        allGladiators42.setSize(150, 375);
        allGladiators42.setLocation(jf.getWidth() / 2 - allGladiators41.getWidth() / 2 + 500, 175);
        allGladiators42.setBackground(Color.ORANGE);
        allGladiators42.setVisible(false);
        allGladiators42.add(labAllGladiators42);

        jf.add(allGladiators43);
        allGladiators43.setSize(150, 375);
        allGladiators43.setLocation(jf.getWidth() / 2 - allGladiators41.getWidth() / 2 + 500 + 150, 175);
        allGladiators43.setBackground(Color.ORANGE);
        allGladiators43.setVisible(false);
        allGladiators43.add(labAllGladiators43);

        jf.add(allGladiators44);
        allGladiators44.setSize(150, 375);
        allGladiators44.setLocation(jf.getWidth() / 2 - allGladiators41.getWidth() / 2 + 500, 175 + 375);
        allGladiators44.setBackground(Color.ORANGE);
        allGladiators44.setVisible(false);
        allGladiators44.add(labAllGladiators44);

        jf.add(allGladiators45);
        allGladiators45.setSize(150, 375);
        allGladiators45.setLocation(jf.getWidth() / 2 - allGladiators41.getWidth() / 2 + 500 + 150, 175 + 375);
        allGladiators45.setBackground(Color.ORANGE);
        allGladiators45.setVisible(false);
        allGladiators45.add(labAllGladiators45);

        send.setSize(150, 75);
        send.setLocation(700, 75);
        send.setBackground(Color.darkGray);
        send.setVisible(false);
        send.add(labSend);
        labSend.setForeground(Color.WHITE);
        labSend.setText("odeslat");

        send2.setSize(150, 75);
        send2.setLocation(500, 75);
        send2.setBackground(Color.darkGray);
        send2.setVisible(false);
        send2.add(labSend2);
        labSend2.setForeground(Color.WHITE);
        labSend2.setText("odeslat");

        send3.setSize(150, 75);
        send3.setLocation(300, 75);
        send3.setBackground(Color.darkGray);
        send3.setVisible(false);
        send3.add(labSend3);
        labSend3.setForeground(Color.WHITE);
        labSend3.setText("odeslat");

        send4.setSize(150, 75);
        send4.setLocation(300, 75);
        send4.setBackground(Color.darkGray);
        send4.setVisible(false);
        send4.add(labSend4);
        labSend4.setForeground(Color.WHITE);
        labSend4.setText("odeslat");

        send5.setSize(150, 75);
        send5.setLocation(300, 75);
        send5.setBackground(Color.darkGray);
        send5.setVisible(false);
        send5.add(labSend5);
        labSend5.setForeground(Color.WHITE);
        labSend5.setText("odeslat");

        send6.setSize(150, 75);
        send6.setLocation(300, 75);
        send6.setBackground(Color.darkGray);
        send6.setVisible(false);
        send6.add(labSend6);
        labSend6.setForeground(Color.WHITE);
        labSend6.setText("odeslat");

        send7.setSize(150, 75);
        send7.setLocation(300, 75);
        send7.setBackground(Color.darkGray);
        send7.setVisible(false);
        send7.add(labSend7);
        labSend7.setForeground(Color.WHITE);
        labSend7.setText("odeslat");

        endScrean.setSize(400, 75);
        endScrean.setLocation(jf.getWidth() / 2 - endScrean.getWidth() / 2, 5);
        endScrean.setBackground(Color.red);
        endScrean.setVisible(false);
        endScrean.add(labEndScrean);
        labEndScrean.setForeground(Color.WHITE);
        labEndScrean.setText("");

        wantGladiator.setSize(150, 75);
        wantGladiator.setLocation(jf.getWidth() / 2 - wantGladiator.getWidth() / 2, 200);
        wantGladiator.setBackground(Color.darkGray);
        wantGladiator.setVisible(false);
        wantGladiator.add(labWantGladiator);
        labWantGladiator.setForeground(Color.WHITE);
        labWantGladiator.setText("chceme gladiátora");

        howManyYouHave.setSize(150, 75);
        howManyYouHave.setLocation(jf.getWidth() / 2 - howManyYouHave.getWidth() / 2, 400);
        howManyYouHave.setBackground(Color.darkGray);
        howManyYouHave.setVisible(false);
        howManyYouHave.add(labHowManyYouHave);
        labHowManyYouHave.setForeground(Color.WHITE);
        labHowManyYouHave.setText("");

        editorNext.setSize(150, 75);
        editorNext.setLocation(jf.getWidth() / 2 - editorNext.getWidth() / 2, 600);
        editorNext.setBackground(Color.darkGray);
        editorNext.setVisible(false);
        editorNext.add(labEditorNext);
        labEditorNext.setForeground(Color.WHITE);
        labEditorNext.setText("<html>" + "<br>" + "editor je další hráč" + "<br>" + "pozadí se zabarví");

        gladiatorOneFight.setSize(150, 75);
        gladiatorOneFight.setLocation(fightFrame.getX() + 100, fightFrame.getY() + fightFrame.getHeight() - 300);
        gladiatorOneFight.setBackground(Color.blue);
        gladiatorOneFight.setVisible(false);
        gladiatorOneFight.add(labGladiatorOneFight);
        labGladiatorOneFight.setText("útok");
        labGladiatorOneFight.setForeground(Color.WHITE);

        gladiatorTwoFight.setSize(150, 75);
        gladiatorTwoFight.setLocation(fightFrame.getX() + fightFrame.getWidth() - gladiatorTwoFight.getWidth() - 100, fightFrame.getY() + fightFrame.getHeight() - 300);
        gladiatorTwoFight.setBackground(Color.blue);
        gladiatorTwoFight.setVisible(false);
        gladiatorTwoFight.add(labGladiatorTwoFight);
        labGladiatorTwoFight.setText("útok");
        labGladiatorTwoFight.setForeground(Color.WHITE);

        //panely a textove pole na zadavani poctu hracu
        jf.add(panNumberOfPlayers);
        panNumberOfPlayers.setVisible(true);
        panNumberOfPlayers.setSize(200, 30);
        panNumberOfPlayers.setBackground(Color.black);
        panNumberOfPlayers.setLocation(jf.getWidth() / 2 - panNumberOfPlayers.getWidth() / 2 - 6, 530);
        panNumberOfPlayers.add(labNumberOfPlayers);
        labNumberOfPlayers.setForeground(Color.white);
        labNumberOfPlayers.setText("kolik hráčů hraje");

        jf.add(textFieldNumberOfPlayers);
        textFieldNumberOfPlayers.setVisible(true);
        textFieldNumberOfPlayers.setSize(100, 50);
        textFieldNumberOfPlayers.setLocation(jf.getWidth() / 2 - textFieldNumberOfPlayers.getWidth() / 2 - 6, 560);

        //panely a textove pole na zadavani poctu gladiatoru na hrace
        jf.add(panNumberOfGladiatorsForEachPlayer);
        panNumberOfGladiatorsForEachPlayer.setVisible(true);
        panNumberOfGladiatorsForEachPlayer.setSize(200, 30);
        panNumberOfGladiatorsForEachPlayer.setLocation(jf.getWidth() / 2 - panNumberOfGladiatorsForEachPlayer.getWidth() / 2 - 6, 630);
        panNumberOfGladiatorsForEachPlayer.setBackground(Color.black);
        panNumberOfGladiatorsForEachPlayer.add(labNumberOfGladiatorsForEachPlayer);
        labNumberOfGladiatorsForEachPlayer.setForeground(Color.white);
        labNumberOfGladiatorsForEachPlayer.setText("kolik gladiátorů má hráč");

        jf.add(textFieldNumberOfGladiatorsForEachPlayer);
        textFieldNumberOfGladiatorsForEachPlayer.setVisible(true);
        textFieldNumberOfGladiatorsForEachPlayer.setSize(100, 50);
        textFieldNumberOfGladiatorsForEachPlayer.setLocation(jf.getWidth() / 2 - textFieldNumberOfGladiatorsForEachPlayer.getWidth() / 2 - 6, 660);

        jf.add(panWantToConfirmAll);
        panWantToConfirmAll.setVisible(true);
        panWantToConfirmAll.setSize(170, 60);
        panWantToConfirmAll.setLocation(jf.getWidth() / 2 - panWantToConfirmAll.getWidth() / 2 - 6, 750);
        panWantToConfirmAll.setBackground(Color.gray);
        panWantToConfirmAll.add(labWantToConfirmAll);
        labWantToConfirmAll.setVisible(true);
        labWantToConfirmAll.setLocation(jf.getWidth() / 2 - labWantToConfirmAll.getWidth() / 2 - 6, 750);
        labWantToConfirmAll.setSize(170, 150);
        labWantToConfirmAll.setText("Chceš to potvrdit?");

        jf.add(basemant);
        basemant.setLocation(jf.getWidth() / 2 - 725, jf.getHeight() / 2 - 540);
        basemant.setSize(1450, 1080);
        basemant.setBackground(Color.gray);
        basemant.add(labBasemant);
        basemant.setVisible(true);
        basemant.setLayout(null);
        labBasemant.setForeground(Color.WHITE);
        labBasemant.setLocation(0, 0);
        labBasemant.setSize(1450, 1080);
        labBasemant.setIcon(box);

        //zalozeni gladiatoru
        for (int i = 0; i < 1000; i++) {
            G[i] = new gladiator();
        }

        //zalozeni zbrani
        for (int i = 0; i < 78; i++) {
            for (int z = 0; z < 78; z++) {
                for (int x = 0; x < 55; x++) {
                    W[i][z][x] = new weapon();
                }
            }
        }

        for (int i = 0; i < 40; i++) {
            P[i] = new player();
        }
        for (int i = 1; i <= 100; i++) {

            G[i].lives = 20;
            G[i].weaponOne = 2;
            G[i].weaponTwo = 2;
            G[i].weaponThree = 1;
            G[i].name = "" + i;
        }

        G[1].nationGladiator = "thrack";
        G[2].nationGladiator = "kartago";
        G[3].nationGladiator = "rek";
        G[4].nationGladiator = "kelt";
        G[5].nationGladiator = "rim";
        G[6].nationGladiator = "german";
        G[7].nationGladiator = "thrack";
        G[8].nationGladiator = "kartago";
        G[9].nationGladiator = "rek";
        G[10].nationGladiator = "kelt";
        G[11].nationGladiator = "rim";
        G[12].nationGladiator = "german";
        G[13].nationGladiator = "thrack";
        G[14].nationGladiator = "kartago";
        G[15].nationGladiator = "rek";
        G[16].nationGladiator = "kelt";
        G[17].nationGladiator = "rim";
        G[18].nationGladiator = "german";

        //rovnou mohu zapomenout jen pomocna promena k naplneni jaky gladiator ma jakeho hrace
        //velmi dulezite - zde definuji jake zbrane jsou pouzitelne a s jakymi hodnotami
        for (int i = 1; i < 70; i++) {
            for (int z = 1; z < 50; z++) {

                //DULEZITE - W[JAKY TYP ZBRANE][JAKEHO GLADIATORA][V JAKE RUCE]
                //nic v ruce
                W[1][i][z].power = 0;
                W[1][i][z].dexterity = 0;
                W[1][i][z].shield = 0;
                W[1][i][z].harm = -100;
                W[1][i][z].onWhichLevelCanBeThisWeaponAnable = 0;
                W[1][i][z].onWhichLevelCanBeNextWeaponAnable = 5;
                W[1][i][z].NameWeapon = "nic";
                //ruka
                W[2][i][z].power = 3;
                W[2][i][z].dexterity = 2;
                W[2][i][z].lvlUp1Power = 1;
                W[2][i][z].lvlUp2Power = 1;
                W[2][i][z].lvlUp2Dexterity = 1;
                W[2][i][z].harm = 0;
                W[2][i][z].onWhichLevelCanBeThisWeaponAnable = 1;
                W[2][i][z].onWhichLevelCanBeNextWeaponAnable = 4;
                W[2][i][z].NameWeapon = "ruka";

                W[3][i][z].power = 2;
                W[3][i][z].dexterity = 1;
                W[3][i][z].lvlUp1Power = 2;
                W[3][i][z].lvlUp1Dexterity = 1;
                W[3][i][z].lvlUp2Power = 1;
                W[3][i][z].lvlUp2Dexterity = 0;
                W[3][i][z].lvlUp3Power = 2;
                W[3][i][z].lvlUp3Dexterity = 1;
                W[3][i][z].healtIfItIsAnimal = 12;
                W[3][i][z].NameWeapon = "pes";
                W[3][i][z].NationWeapon = "kelt";
                W[17][i][z].onWhichLevelCanBeThisWeaponAnable = 0;
                W[17][i][z].onWhichLevelCanBeNextWeaponAnable = 5;

                W[4][i][z].power = 2;
                W[4][i][z].dexterity = 1;
                W[4][i][z].shield = 0;
                W[4][i][z].lvlUp1Power = 2;
                W[4][i][z].lvlUp1Dexterity = 1;
                W[4][i][z].lvlUp2Power = 2;
                W[4][i][z].lvlUp2CanPlayAgain = 1;
                W[4][i][z].lvlUp3Power = 2;
                W[4][i][z].lvlUp3Dexterity = 1;
                W[4][i][z].healtIfItIsAnimal = 14;
                W[4][i][z].NameWeapon = "kůň";
                W[4][i][z].NationWeapon = "thrack";
                W[17][i][z].onWhichLevelCanBeThisWeaponAnable = 0;
                W[17][i][z].onWhichLevelCanBeNextWeaponAnable = 5;

                W[5][i][z].power = 3;
                W[5][i][z].dexterity = 2;
                W[5][i][z].lvlUp1Power = 3;
                W[5][i][z].lvlUp1Dexterity = 1;
                W[5][i][z].lvlUp2Power = 1;
                W[5][i][z].lvlUp3Power = 3;
                W[5][i][z].NameWeapon = "meč";
                W[5][i][z].NationWeapon = "rek";
                W[5][i][z].onWhichLevelCanBeThisWeaponAnable = 1;
                W[5][i][z].onWhichLevelCanBeNextWeaponAnable = 4;

                W[6][i][z].power = 2;
                W[6][i][z].dexterity = 1;
                W[6][i][z].lvlUp1Power = 4;
                W[6][i][z].lvlUp1Dexterity = 1;
                W[6][i][z].lvlUp2Power = 1;
                W[6][i][z].lvlUp3Power = 1;
                W[6][i][z].lvlUp3Dexterity = 1;
                W[6][i][z].canBeThrowen = 3;
                W[6][i][z].NameWeapon = "oštěp";
                W[6][i][z].NationWeapon = "rek";
                W[6][i][z].onWhichLevelCanBeThisWeaponAnable = 3;
                W[6][i][z].onWhichLevelCanBeNextWeaponAnable = 2;

                W[7][i][z].power = 2;
                W[7][i][z].dexterity = 1;
                W[7][i][z].shield = 1;
                W[7][i][z].lvlUp1Power = 1;
                W[7][i][z].lvlUp1Dexterity = 1;
                W[7][i][z].lvlUp2Shield = 1;
                W[7][i][z].lvlUp2Power = 1;
                W[7][i][z].lvlUp3Power = 1;
                W[7][i][z].lvlUp3Dexterity = 1;
                W[7][i][z].NameWeapon = "malý štít";
                W[7][i][z].NationWeapon = "rek";
                W[7][i][z].onWhichLevelCanBeThisWeaponAnable = 3;
                W[7][i][z].onWhichLevelCanBeNextWeaponAnable = 2;

                W[8][i][z].power = 2;
                W[8][i][z].dexterity = 2;
                W[8][i][z].lvlUp1Power = 1;
                W[8][i][z].lvlUp1Dexterity = 1;
                W[8][i][z].lvlUp2Power = 2;
                W[8][i][z].lvlUp3Power = 1;
                W[8][i][z].lvlUp3Dexterity = 1;
                W[8][i][z].NameWeapon = "hůl";
                W[8][i][z].NationWeapon = "kelt";
                W[8][i][z].onWhichLevelCanBeThisWeaponAnable = 3;
                W[8][i][z].onWhichLevelCanBeNextWeaponAnable = 2;

                W[9][i][z].power = 1;
                W[9][i][z].dexterity = 1;
                W[9][i][z].lvlUp1Dexterity = 1;
                W[9][i][z].lvlUp3Power = 4;
                W[9][i][z].lvlUp3Dexterity = 1;
                W[9][i][z].canPlayAgain = 2;
                W[9][i][z].NameWeapon = "laso";
                W[9][i][z].NationWeapon = "kelt";
                W[9][i][z].onWhichLevelCanBeThisWeaponAnable = 1;
                W[9][i][z].onWhichLevelCanBeNextWeaponAnable = 4;

                W[10][i][z].power = 1;
                W[10][i][z].dexterity = 2;
                W[10][i][z].lvlUp1Dexterity = 1;
                W[10][i][z].lvlUp2Dexterity = 1;
                W[10][i][z].lvlUp3Dexterity = 1;
                W[10][i][z].lvlUp2Power = 1;
                W[10][i][z].lvlUp3Power = 3;
                W[10][i][z].NameWeapon = "bič";
                W[10][i][z].NationWeapon = "rim";
                W[10][i][z].onWhichLevelCanBeThisWeaponAnable = 1;
                W[10][i][z].onWhichLevelCanBeNextWeaponAnable = 4;

                W[11][i][z].power = 2;
                W[11][i][z].dexterity = 2;
                W[11][i][z].lvlUp1Dexterity = 1;
                W[11][i][z].lvlUp3Dexterity = 1;
                W[11][i][z].lvlUp1Power = 1;
                W[11][i][z].lvlUp2Power = 2;
                W[11][i][z].lvlUp3Power = 3;
                W[11][i][z].lvlUp2CanGoThroughShield = 2;
                W[11][i][z].NameWeapon = "kopí";
                W[11][i][z].NationWeapon = "rim";
                W[12][i][z].onWhichLevelCanBeThisWeaponAnable = 3;
                W[12][i][z].onWhichLevelCanBeNextWeaponAnable = 2;

                W[12][i][z].power = 1;
                W[12][i][z].dexterity = 1;
                W[12][i][z].shield = 1;
                W[12][i][z].lvlUp2Dexterity = 1;
                W[12][i][z].lvlUp2Power = 1;
                W[12][i][z].lvlUp3Power = 3;
                W[12][i][z].lvlUp1Shield = 1;
                W[12][i][z].lvlUp2Shield = 1;
                W[12][i][z].NameWeapon = "velký štít";
                W[12][i][z].NationWeapon = "rim";
                W[12][i][z].onWhichLevelCanBeThisWeaponAnable = 3;
                W[12][i][z].onWhichLevelCanBeNextWeaponAnable = 2;

                W[13][i][z].power = 2;
                W[13][i][z].dexterity = 2;
                W[13][i][z].lvlUp1Dexterity = 1;
                W[13][i][z].lvlUp3Dexterity = 1;
                W[13][i][z].lvlUp1Power = 1;
                W[13][i][z].lvlUp2Power = 3;
                W[13][i][z].lvlUp3Power = 4;
                W[13][i][z].NameWeapon = "trojzubec";
                W[13][i][z].NationWeapon = "kartago";
                W[13][i][z].onWhichLevelCanBeThisWeaponAnable = 3;
                W[13][i][z].onWhichLevelCanBeNextWeaponAnable = 2;

                W[14][i][z].power = 1;
                W[14][i][z].dexterity = 1;
                W[14][i][z].canPlayAgain = 4;
                W[14][i][z].shield = 1;
                W[14][i][z].lvlUp3Dexterity = 1;
                W[14][i][z].lvlUp2Power = 1;
                W[14][i][z].lvlUp3Power = 2;
                W[14][i][z].NameWeapon = "síť a rukáv";
                W[14][i][z].NationWeapon = "kartago";
                W[14][i][z].onWhichLevelCanBeThisWeaponAnable = 1;
                W[14][i][z].onWhichLevelCanBeNextWeaponAnable = 4;

                W[15][i][z].power = 1;
                W[15][i][z].dexterity = 1;
                W[15][i][z].lvlUp1Dexterity = 1;
                W[15][i][z].lvlUp3Dexterity = 1;
                W[15][i][z].lvlUp1Power = 3;
                W[15][i][z].lvlUp2Power = 1;
                W[15][i][z].lvlUp3Power = 1;
                W[15][i][z].lvlUp3CanDestroy = 2;
                W[15][i][z].NameWeapon = "řezák";
                W[15][i][z].NationWeapon = "kartago";
                W[15][i][z].onWhichLevelCanBeThisWeaponAnable = 1;
                W[15][i][z].onWhichLevelCanBeNextWeaponAnable = 4;

                W[16][i][z].power = 3;
                W[16][i][z].dexterity = 1;
                W[16][i][z].lvlUp1Dexterity = 1;
                W[16][i][z].lvlUp1Power = 4;
                W[16][i][z].lvlUp2Power = 3;
                W[16][i][z].lvlUp3Power = 2;
                W[16][i][z].NameWeapon = "sekera";
                W[16][i][z].NationWeapon = "german";
                W[16][i][z].onWhichLevelCanBeThisWeaponAnable = 1;
                W[16][i][z].onWhichLevelCanBeNextWeaponAnable = 4;

                W[17][i][z].power = 4;
                W[17][i][z].dexterity = 1;
                W[17][i][z].lvlUp1Dexterity = 1;
                W[17][i][z].lvlUp1Power = 4;
                W[17][i][z].lvlUp2Power = 4;
                W[17][i][z].lvlUp3Power = 2;
                W[17][i][z].lvlUp3Shield = 1;
                W[17][i][z].NameWeapon = "obouruční sekera";
                W[17][i][z].NationWeapon = "german";
                W[17][i][z].onWhichLevelCanBeThisWeaponAnable = 5;
                W[17][i][z].onWhichLevelCanBeNextWeaponAnable = 0;

                W[18][i][z].power = 4;
                W[18][i][z].dexterity = 1;
                W[18][i][z].lvlUp1Dexterity = 1;
                W[18][i][z].lvlUp1Power = 3;
                W[18][i][z].lvlUp2Power = 1;
                W[18][i][z].lvlUp3Power = 1;
                W[18][i][z].lvlUp2CanDestroy = 3;
                W[18][i][z].NameWeapon = "cep";
                W[18][i][z].NationWeapon = "german";
                W[18][i][z].onWhichLevelCanBeThisWeaponAnable = 5;
                W[18][i][z].onWhichLevelCanBeNextWeaponAnable = 0;

                W[19][i][z].power = 2;
                W[19][i][z].dexterity = 2;
                W[19][i][z].canBeThrowen = 3;
                W[19][i][z].lvlUp3Dexterity = 1;
                W[19][i][z].lvlUp1Power = 2;
                W[19][i][z].lvlUp2Power = 1;
                W[19][i][z].lvlUp3Power = 2;
                W[19][i][z].lvlUp3CanGoThroughShield = 3;
                W[19][i][z].NameWeapon = "luk";
                W[19][i][z].NationWeapon = "thrack";
                W[19][i][z].onWhichLevelCanBeThisWeaponAnable = 3;
                W[19][i][z].onWhichLevelCanBeNextWeaponAnable = 2;

                W[20][i][z].power = 2;
                W[20][i][z].dexterity = 2;
                W[20][i][z].canGoThroughShield = 6;
                W[20][i][z].lvlUp2Dexterity = 1;
                W[20][i][z].lvlUp1Power = 1;
                W[20][i][z].lvlUp2Power = 1;
                W[20][i][z].lvlUp3Power = 4;
                W[20][i][z].NameWeapon = "dýka";
                W[20][i][z].NationWeapon = "thrack";
                W[20][i][z].onWhichLevelCanBeThisWeaponAnable = 1;
                W[20][i][z].onWhichLevelCanBeNextWeaponAnable = 4;

            }
        }

        numberOfWeapons = 20;
        for (int i = 2; i <= numberOfWeapons; i++) {
            whichWeapon.addItem("Zbraň: " + W[i][20][20].NameWeapon);
        }

        //urcuje jaky ruce jsou jak volne a nevolne
        for (int i = 1; i < 100; i++) {
            G[i].isHandOneAnable = 5;
            G[i].isHandTwoAnable = 5;
            G[i].isHandThreeAnable = 5;
        }

        jf.add(mouseMovement);
        mouseMovement.setLocation(0, 0);
        mouseMovement.setSize(screen.width, screen.height);
        mouseMovement.setOpaque(false);
        mouseMovement.setVisible(true);
        game mys = new game();;
        mouseMovement.addMouseListener(mys);

        jf.repaint();
        timer.start();
    }

    public static void fightFirst(int m, int n) {
        int random = RND.nextInt(6);
        int random2 = RND.nextInt(6);
        int random3 = RND.nextInt(6);

        if (animal.isVisible() == true && W[G[m].weaponOne][m][1].dexterity >= random) {
            W[G[n].weaponThree][n][3].healtIfItIsAnimal = W[G[n].weaponThree][n][3].healtIfItIsAnimal - W[G[m].weaponOne][m][1].power;
            if (W[G[m].weaponOne][m][1].canPlayAgain < random2) {
                canPlay = true;
            }
            if (W[G[m].weaponOne][m][1].canDestroy < random2) {
                if (random3 < 3) {
                    G[n].weaponOne = 2;
                    setText();
                }
                if (random3 >= 3) {
                    G[n].weaponTwo = 2;
                    setText();
                }
            }
        }

        if (W[G[m].weaponOne][m][1].dexterity >= random && animal.isVisible() == false) {
            if (-W[G[m].weaponOne][m][1].power + W[G[n].weaponOne][n][1].shield + W[G[n].weaponTwo][n][2].shield < 0 && W[G[m].weaponOne][m][1].canGoThroughShield == 0) {
                G[n].lives = G[n].lives - W[G[m].weaponOne][m][1].power + W[G[n].weaponOne][n][1].shield + W[G[n].weaponTwo][n][2].shield;
            }

            if (W[G[m].weaponOne][m][1].canGoThroughShield > 0) {
                G[n].lives = G[n].lives - W[G[m].weaponOne][m][1].power;
            }

            if (W[G[m].weaponOne][m][1].canPlayAgain < random2) {
                canPlay = true;
            }

            if (W[G[m].weaponOne][m][1].canDestroy < random2) {
                if (random3 < 3) {
                    G[n].weaponOne = 2;
                    setText();
                }
                if (random3 >= 3) {
                    G[n].weaponTwo = 2;
                    setText();
                }
            }
        } else {
            if (random == 5) {
                W[G[m].weaponOne][m][1].harm++;

                setText();

            }
        }
        setText();

        if (G[n].lives <= 0) {
            checkLives();
        }
        if (G[m].lives <= 0) {
            checkLives();
        }
        if (wantToEnd.isVisible() == false && sineMissione == false) {
            checkLives();
        }

    }

    public static void fightSecond(int m, int n) {
        int random = RND.nextInt(6);
        int random2 = RND.nextInt(6);
        int random3 = RND.nextInt(6);
        if (animal.isVisible() == true && W[G[m].weaponTwo][m][2].dexterity >= random) {
            W[G[n].weaponThree][n][3].healtIfItIsAnimal = W[G[n].weaponThree][n][3].healtIfItIsAnimal - W[G[m].weaponTwo][m][2].power;
            if (W[G[m].weaponTwo][m][2].canPlayAgain < random2) {
                canPlay = true;
            }
            if (W[G[m].weaponTwo][m][2].canDestroy < random2) {
                if (random3 < 3) {
                    G[n].weaponOne = 2;
                    setText();
                }
                if (random3 >= 3) {
                    G[n].weaponTwo = 2;
                    setText();
                }
            }
        }

        if (W[G[m].weaponTwo][m][2].dexterity >= random && animal.isVisible() == false) {
            if (-W[G[m].weaponTwo][m][2].power + W[G[n].weaponOne][n][1].shield + W[G[n].weaponTwo][n][2].shield < 0 && W[G[m].weaponTwo][m][2].canGoThroughShield == 0) {
                G[n].lives = G[n].lives - W[G[m].weaponTwo][m][2].power + W[G[n].weaponOne][n][1].shield + W[G[n].weaponTwo][n][2].shield;
            }

            if (W[G[m].weaponTwo][m][2].canGoThroughShield > 0) {
                G[n].lives = G[n].lives - W[G[m].weaponTwo][m][2].power;
            }
            if (W[G[m].weaponTwo][m][2].canPlayAgain < random2) {
                canPlay = true;
            }
            if (W[G[m].weaponTwo][m][2].canDestroy < random2) {
                if (random3 < 3) {
                    G[n].weaponOne = 2;
                    setText();
                }
                if (random3 >= 3) {
                    G[n].weaponTwo = 2;
                    setText();
                }
            }

        } else {
            if (random == 5) {
                W[G[m].weaponTwo][m][2].harm++;

                setText();

            }
        }
        setText();
        if (G[n].lives <= 0) {
            checkLives();
        }
        if (G[m].lives <= 0) {
            checkLives();
        }
        if (wantToEnd.isVisible() == false && sineMissione == false) {
            checkLives();
        }

    }

    public static void fightThird(int m, int n) {

        int random = RND.nextInt(6);
        int random2 = RND.nextInt(6);
        int random3 = RND.nextInt(6);
        if (animal.isVisible() == true && W[G[m].weaponOne][m][1].dexterity >= random) {
            W[G[n].weaponThree][n][3].healtIfItIsAnimal = W[G[n].weaponThree][n][3].healtIfItIsAnimal - W[G[m].weaponOne][m][1].power;
            if (W[G[m].weaponOne][m][1].canPlayAgain < random2) {
                canPlay = true;
            }
            if (W[G[m].weaponOne][m][1].canDestroy < random2) {
                if (random3 < 3) {
                    G[n].weaponOne = 2;
                    setText();
                }
                if (random3 >= 3) {
                    G[n].weaponTwo = 2;
                    setText();
                }
            }
        }

        if (W[G[m].weaponOne][m][1].dexterity >= random && animal.isVisible() == false) {
            if (-W[G[m].weaponOne][m][1].power + W[G[n].weaponOne][n][1].shield + W[G[n].weaponTwo][n][2].shield < 0 && W[G[m].weaponOne][m][1].canGoThroughShield == 0) {
                G[n].lives = G[n].lives - W[G[m].weaponOne][m][1].power + W[G[n].weaponOne][n][1].shield + W[G[n].weaponTwo][n][2].shield;
            }

            if (W[G[m].weaponOne][m][1].canGoThroughShield > 0) {
                G[n].lives = G[n].lives - W[G[m].weaponOne][m][1].power;
            }
            if (W[G[m].weaponOne][m][1].canPlayAgain < random2) {
                canPlay = true;
            }
            if (W[G[m].weaponOne][m][1].canDestroy < random2) {
                if (random3 < 3) {
                    G[n].weaponOne = 2;
                    setText();
                }
                if (random3 >= 3) {
                    G[n].weaponTwo = 2;
                    setText();
                }
            }

        } else {
            if (random == 5) {
                W[G[m].weaponOne][m][1].harm++;

                setText();

            }
        }
        setText();

        if (G[n].lives <= 0) {
            checkLives();
        }
        if (G[m].lives <= 0) {
            checkLives();
        }
        if (wantToEnd.isVisible() == false && sineMissione == false) {
            checkLives();
        }

    }

    public static void fightFourth(int m, int n) {

        int random = RND.nextInt(6);
        int random2 = RND.nextInt(6);
        int random3 = RND.nextInt(6);

        if (animal.isVisible() == true && W[G[m].weaponTwo][m][2].dexterity >= random) {
            W[G[n].weaponThree][n][3].healtIfItIsAnimal = W[G[n].weaponThree][n][3].healtIfItIsAnimal - W[G[m].weaponTwo][m][2].power;
            if (W[G[m].weaponTwo][m][2].canPlayAgain < random2) {
                canPlay = true;
            }
            if (W[G[m].weaponTwo][m][2].canDestroy < random2) {
                if (random3 < 3) {
                    G[n].weaponOne = 2;
                    setText();
                }
                if (random3 >= 3) {
                    G[n].weaponTwo = 2;
                    setText();
                }
            }
        }

        if (W[G[m].weaponTwo][m][2].dexterity >= random && animal.isVisible() == false) {
            if (-W[G[m].weaponTwo][m][2].power + W[G[n].weaponOne][n][1].shield + W[G[n].weaponTwo][n][2].shield < 0 && W[G[m].weaponTwo][m][2].canGoThroughShield == 0) {
                G[n].lives = G[n].lives - W[G[m].weaponTwo][m][2].power + W[G[n].weaponOne][n][1].shield + W[G[n].weaponTwo][n][2].shield;
            }
            if (W[G[m].weaponTwo][m][2].canGoThroughShield > 0) {
                G[n].lives = G[n].lives - W[G[m].weaponTwo][m][2].power;
            }
            if (W[G[m].weaponTwo][m][2].canPlayAgain < random2) {
                canPlay = true;
            }
            if (W[G[m].weaponTwo][m][2].canDestroy < random2) {
                if (random3 < 3) {
                    G[n].weaponOne = 2;
                    setText();
                }
                if (random3 >= 3) {
                    G[n].weaponTwo = 2;
                    setText();
                }
            }

        } else {
            if (random == 5) {
                W[G[m].weaponTwo][m][2].harm++;

                setText();

            }
        }
        setText();

        if (G[n].lives <= 0) {
            checkLives();
        }
        if (G[m].lives <= 0) {
            checkLives();
        }
        if (wantToEnd.isVisible() == false && sineMissione == false) {
            checkLives();
        }
    }

    public static void fightFifth(int m, int n) {

        int random = RND.nextInt(6);
        int random2 = RND.nextInt(6);
        int random3 = RND.nextInt(6);

        if (animal.isVisible() == true && W[G[m].weaponThree][m][3].dexterity >= random) {
            W[G[n].weaponThree][n][3].healtIfItIsAnimal = W[G[n].weaponThree][n][3].healtIfItIsAnimal - W[G[m].weaponThree][m][3].power;
            if (W[G[m].weaponThree][m][3].canPlayAgain < random2) {
                canPlay = true;
            }
            if (W[G[m].weaponThree][m][3].canDestroy < random2) {
                if (random3 < 3) {
                    G[n].weaponOne = 2;
                    setText();
                }
                if (random3 >= 3) {
                    G[n].weaponTwo = 2;
                    setText();
                }
            }
        }

        if (W[G[m].weaponThree][m][3].dexterity >= random && animal.isVisible() == false) {
            if (-W[G[m].weaponThree][m][3].power + W[G[n].weaponOne][n][1].shield + W[G[n].weaponTwo][n][2].shield < 0 && W[G[m].weaponThree][m][3].canGoThroughShield == 0) {
                G[n].lives = G[n].lives - W[G[m].weaponThree][m][3].power + W[G[n].weaponOne][n][1].shield + W[G[n].weaponTwo][n][2].shield;
            }

            if (W[G[m].weaponThree][m][3].canGoThroughShield > 0) {
                G[n].lives = G[n].lives - W[G[m].weaponThree][m][3].power;
            }
            if (W[G[m].weaponThree][m][3].canPlayAgain < random2) {
                canPlay = true;
            }
            if (W[G[m].weaponThree][m][3].canDestroy < random2) {
                if (random3 < 3) {
                    G[n].weaponOne = 2;
                    setText();
                }
                if (random3 >= 3) {
                    G[n].weaponTwo = 2;
                    setText();
                }
            }

        } else {
            if (random == 5) {
                W[G[m].weaponThree][m][3].harm++;

                setText();

            }
        }
        setText();
        if (G[n].lives <= 0) {
            checkLives();
        }
        if (G[m].lives <= 0) {
            checkLives();
        }
        if (wantToEnd.isVisible() == false && sineMissione == false) {
            checkLives();
        }
    }

    public static void fightSixth(int m, int n) {

        int random = RND.nextInt(6);
        int random2 = RND.nextInt(6);
        int random3 = RND.nextInt(6);

        if (W[G[m].weaponThree][m][3].dexterity >= random && animal.isVisible() == false) {
            if (-W[G[m].weaponThree][m][3].power + W[G[n].weaponOne][n][1].shield + W[G[n].weaponTwo][n][2].shield < 0 && W[G[m].weaponThree][m][3].canGoThroughShield == 0) {
                G[n].lives = G[n].lives - W[G[m].weaponThree][m][3].power + W[G[n].weaponOne][n][1].shield + W[G[n].weaponTwo][n][2].shield;
            }

            if (W[G[m].weaponThree][m][3].canGoThroughShield > 0) {
                G[n].lives = G[n].lives - W[G[m].weaponThree][m][3].power;
            }
            if (W[G[m].weaponThree][m][3].canPlayAgain < random2) {
                canPlay = true;
            }
            if (W[G[m].weaponThree][m][3].canDestroy < random2) {
                if (random3 < 3) {
                    G[n].weaponOne = 2;
                    setText();
                }
                if (random3 >= 3) {
                    G[n].weaponTwo = 2;
                    setText();
                }
            }

        } else {
            if (random == 5) {
                W[G[m].weaponThree][m][3].harm++;

                setText();

            }
        }
        setText();

        if (G[n].lives <= 0) {
            checkLives();
        }
        if (G[m].lives <= 0) {
            checkLives();
        }
        if (wantToEnd.isVisible() == false && sineMissione == false) {
            checkLives();
        }

    }

    @Override
    public void mouseReleased(MouseEvent e) {
        mysX = e.getX();
        mysY = e.getY();
        //zavreni okna
        if (mysX > exit.getX() && mysX < exit.getX() + exit.getWidth() && mysY > exit.getY() && mysY < exit.getY() + exit.getHeight() && exit.isVisible()) {
            System.exit(0);
        }

        //zacatek hry
        if (mysX > panWantToConfirmAll.getX() && mysX < panWantToConfirmAll.getX() + panWantToConfirmAll.getWidth() && mysY > panWantToConfirmAll.getY() && mysY < panWantToConfirmAll.getY() + panWantToConfirmAll.getHeight() && panWantToConfirmAll.isVisible()) {
            labBasemant.setIcon(borduraW);
            String numberHelp = "";
            numberHelp = textFieldNumberOfPlayers.getText();
            numberHelp = numberHelp.replaceAll(" ", "");
            numberHelp = numberHelp.replaceAll("[^0-9]", "");
            try {
                numberOfPlayers = (int) Double.parseDouble(numberHelp);
                if (numberOfPlayers > 4) {
                    numberOfPlayers = 4;
                }
                if (numberOfPlayers < 1) {
                    numberOfPlayers = 1;
                }
            } catch (Exception ex) {
                numberOfPlayers = 2;
            }

            numberHelp = textFieldNumberOfGladiatorsForEachPlayer.getText();
            numberHelp = numberHelp.replaceAll(" ", "");
            numberHelp = numberHelp.replaceAll("[^0-9]", "");
            try {
                numberOfGladiatorsForEachPlayer = (int) Double.parseDouble(numberHelp);
                if (numberOfGladiatorsForEachPlayer > 4) {
                    numberOfGladiatorsForEachPlayer = 4;
                }
                if (numberOfGladiatorsForEachPlayer < 1) {
                    numberOfGladiatorsForEachPlayer = 1;
                }
            } catch (Exception ex) {
                numberOfGladiatorsForEachPlayer = 2;
            }
            numberOfGladiators = numberOfPlayers * numberOfGladiatorsForEachPlayer;
            for (int i = 1; i <= numberOfPlayers; i++) {
                P[i].howManyGladiators = numberOfGladiatorsForEachPlayer;
            }
            counterHowManyYouTakeAway = numberOfGladiators;

            panNumberOfPlayers.setVisible(false);
            textFieldNumberOfPlayers.setVisible(false);
            panNumberOfGladiatorsForEachPlayer.setVisible(false);
            textFieldNumberOfGladiatorsForEachPlayer.setVisible(false);
            panWantToConfirmAll.setVisible(false);
            labWantToConfirmAll.setVisible(false);
            fight.setVisible(false);
            treatment.setVisible(false);
            newWeapon.setVisible(true);
            repair.setVisible(false);
            training.setVisible(false);
            gettingGladiator.setVisible(false);
            sinemissione.setVisible(false);
            allGladiators11.setVisible(true);
            allGladiators12.setVisible(true);
            allGladiators13.setVisible(true);
            allGladiators14.setVisible(true);
            allGladiators15.setVisible(true);
            allGladiators21.setVisible(true);
            allGladiators22.setVisible(true);
            allGladiators23.setVisible(true);
            allGladiators24.setVisible(true);
            allGladiators25.setVisible(true);
            allGladiators31.setVisible(true);
            allGladiators32.setVisible(true);
            allGladiators33.setVisible(true);
            allGladiators34.setVisible(true);
            allGladiators35.setVisible(true);
            allGladiators41.setVisible(true);
            allGladiators42.setVisible(true);
            allGladiators43.setVisible(true);
            allGladiators44.setVisible(true);
            allGladiators45.setVisible(true);

            int whichPlayerHelper = 1;
            for (int i = 1; i <= numberOfGladiators; i++) {

                G[i].owener = whichPlayerHelper;
                if (i % numberOfGladiatorsForEachPlayer == 0) {
                    whichPlayerHelper++;
                }
                G[i].lives = 20;
                G[i].weaponOne = 2;
                G[i].weaponTwo = 2;
                G[i].weaponThree = 1;
            }

            for (int i = 1; i <= numberOfGladiatorsForEachPlayer; i++) {
                whichGladiator.addItem("gladiátor: " + i);
            }

            for (int i = 1; i <= numberOfGladiatorsForEachPlayer; i++) {
                whichGladiatorRepair.addItem("gladiátor: " + i);
            }

        }

        if (mysX > animal.getX() && mysX < animal.getX() + animal.getWidth() && mysY > animal.getY() && mysY < animal.getY() + animal.getHeight() && animal.isVisible()) {
            animal.setBackground(Color.pink);
            gladiator.setVisible(false);
        }
        if (mysX > gladiator.getX() && mysX < gladiator.getX() + gladiator.getWidth() && mysY > gladiator.getY() && mysY < gladiator.getY() + gladiator.getHeight() && gladiator.isVisible()) {
            gladiator.setBackground(Color.pink);
            animal.setVisible(false);
        }
        if (mysX > wantToEnd.getX() && mysX < wantToEnd.getX() + wantToEnd.getWidth() && mysY > wantToEnd.getY() && mysY < wantToEnd.getY() + wantToEnd.getHeight() && wantToEnd.isVisible()) {
            wantToEnd.setVisible(false);
            if (gladiatorOneFight.isVisible()) {
                G[y].level++;
            }
            if (gladiatorTwoFight.isVisible()) {
                G[x].level++;
            }
        }

        if (mysX > gladiatorOneFight.getX() && mysX < gladiatorOneFight.getX() + gladiatorOneFight.getWidth() && mysY > gladiatorOneFight.getY() && mysY < gladiatorOneFight.getY() + gladiatorOneFight.getHeight() && gladiatorOneFight.isVisible()) {

            if (animal.isVisible() == false || gladiator.isVisible() == false) {
                counterFirst++;
                if (throwingFirst == true) {
                    helpThrowerFirst = W[G[x].weaponOne][x][1].canBeThrowen;

                    throwingFirst = false;
                }

                if (throwingSecond == true) {
                    helpThrowerSecond = W[G[x].weaponTwo][x][2].canBeThrowen;
                    throwingSecond = false;
                }

                if (counterFirst % 3 == 0 && helpThrowerFirst == 0 && helpThrowerSecond == 0) {
                    fightFirst(x, y);
                }
                if (counterFirst % 3 == 1 && helpThrowerFirst == 0 && helpThrowerSecond == 0) {
                    fightSecond(x, y);
                }
                if (counterFirst % 3 == 1 && G[x].weaponThree == 1 && helpThrowerFirst == 0 && helpThrowerSecond == 0) {
                    counterFirst++;
                    if (canPlay == false) {
                        gladiatorTwoFight.setVisible(true);
                        gladiatorOneFight.setVisible(false);
                        if (W[G[x].weaponThree][x][3].healtIfItIsAnimal > 0) {
                            gladiator.setVisible(true);
                            animal.setVisible(true);
                            gladiator.setBackground(Color.blue);
                            animal.setBackground(Color.blue);
                        } else {
                            gladiator.setVisible(false);
                            animal.setVisible(false);
                        }
                    }
                }
                if (counterFirst % 3 == 2 && helpThrowerFirst == 0 && helpThrowerSecond == 0) {
                    fightFifth(x, y);
                    if (canPlay == false) {
                        gladiatorTwoFight.setVisible(true);
                        gladiatorOneFight.setVisible(false);
                        if (W[G[x].weaponThree][x][3].healtIfItIsAnimal > 0) {
                            gladiator.setVisible(true);
                            animal.setVisible(true);
                            gladiator.setBackground(Color.blue);
                            animal.setBackground(Color.blue);
                        } else {
                            gladiator.setVisible(false);
                            animal.setVisible(false);
                        }

                    }
                    canPlay = false;

                }

                if (helpThrowerFirst > 0 && (counterFirst % 2 != 0 || helpThrowerSecond == 0)) {
                    fightFirst(x, y);
                    helpThrowerFirst--;
                    if (helpThrowerFirst == 0) {
                        throwingFirst = true;

                        gladiatorThrowFirst = true;
                        gladiatorThrowFirstWeapon = G[x].weaponOne;
                        gladiatorThrowFirstHarm = W[G[x].weaponOne][x][1].harm;

                        G[x].weaponOne = 2;
                        W[G[x].weaponOne][x][1].harm = 0;
                        setText();
                        if (helpThrowerSecond == 0) {
                            counterFirst = 2;
                            gladiatorTwoFight.setVisible(true);
                            gladiatorOneFight.setVisible(false);
                            if (W[G[x].weaponThree][x][3].healtIfItIsAnimal > 0) {
                                gladiator.setVisible(true);
                                animal.setVisible(true);
                                gladiator.setBackground(Color.blue);
                                animal.setBackground(Color.blue);
                            } else {
                                gladiator.setVisible(false);
                                animal.setVisible(false);
                            }
                        }
                    }

                } else {

                    if (helpThrowerSecond > 0 && (counterFirst % 2 == 0 || helpThrowerFirst == 0)) {
                        fightSecond(x, y);
                        helpThrowerSecond--;
                        if (helpThrowerSecond == 0) {
                            throwingSecond = true;

                            gladiatorThrowSecond = true;
                            gladiatorThrowSecondWeapon = G[x].weaponTwo;
                            gladiatorThrowSecondHarm = W[G[x].weaponTwo][x][2].harm;

                            G[x].weaponTwo = 2;
                            W[G[x].weaponTwo][x][2].harm = 0;
                            setText();
                            if (helpThrowerFirst == 0) {
                                counterFirst = 2;
                                gladiatorTwoFight.setVisible(true);
                                gladiatorOneFight.setVisible(false);
                                if (W[G[x].weaponThree][x][3].healtIfItIsAnimal > 0) {
                                    gladiator.setVisible(true);
                                    animal.setVisible(true);
                                    gladiator.setBackground(Color.blue);
                                    animal.setBackground(Color.blue);
                                } else {
                                    gladiator.setVisible(false);
                                    animal.setVisible(false);
                                }
                            }
                        }
                    }
                }

                if (G[x].lives <= 0) {
                    checkLives();
                }
                if (G[y].lives <= 0) {
                    checkLives();
                }
                if (wantToEnd.isVisible() == false && sineMissione == false) {
                    checkLives();
                }
                if (G[x].lives <= 0) {
                    G[y].level++;
                }
                if (G[y].lives <= 0) {
                    G[x].level++;
                }

            }
        }
        if (mysX > gladiatorTwoFight.getX() && mysX < gladiatorTwoFight.getX() + gladiatorTwoFight.getWidth() && mysY > gladiatorTwoFight.getY() && mysY < gladiatorTwoFight.getY() + gladiatorTwoFight.getHeight() && gladiatorTwoFight.isVisible()) {

            if (animal.isVisible() == false || gladiator.isVisible() == false) {
                counterSecond++;

                if (throwingThird == true) {
                    helpThrowerThird = W[G[y].weaponOne][y][1].canBeThrowen;
                    throwingThird = false;
                }

                if (throwingFourth == true) {
                    helpThrowerFourth = W[G[y].weaponTwo][y][2].canBeThrowen;
                    throwingFourth = false;
                }

                if (counterSecond % 3 == 0 && helpThrowerThird == 0 && helpThrowerFourth == 0) {
                    fightThird(y, x);
                }
                if (counterSecond % 3 == 1 && helpThrowerThird == 0 && helpThrowerFourth == 0) {
                    fightFourth(y, x);
                }
                if (counterSecond % 3 == 1 && G[y].weaponThree == 1 && helpThrowerThird == 0 && helpThrowerFourth == 0) {
                    counterSecond++;
                    if (canPlay == false) {
                        gladiatorTwoFight.setVisible(false);
                        gladiatorOneFight.setVisible(true);
                        if (W[G[y].weaponThree][y][3].healtIfItIsAnimal > 0) {
                            gladiator.setVisible(true);
                            animal.setVisible(true);
                            gladiator.setBackground(Color.blue);
                            animal.setBackground(Color.blue);
                        } else {
                            gladiator.setVisible(false);
                            animal.setVisible(false);
                        }
                    }
                }
                if (counterSecond % 3 == 2 && helpThrowerThird == 0 && helpThrowerFourth == 0) {
                    fightSixth(y, x);
                    if (canPlay == false) {
                        gladiatorTwoFight.setVisible(false);
                        gladiatorOneFight.setVisible(true);
                        if (W[G[y].weaponThree][y][3].healtIfItIsAnimal > 0) {
                            gladiator.setVisible(true);
                            animal.setVisible(true);
                            gladiator.setBackground(Color.blue);
                            animal.setBackground(Color.blue);
                        } else {
                            gladiator.setVisible(false);
                            animal.setVisible(false);
                        }
                    }
                    canPlay = false;
                }

                if (helpThrowerThird > 0 && (counterSecond % 2 != 0 || helpThrowerFourth == 0)) {
                    fightThird(y, x);
                    helpThrowerThird--;
                    if (helpThrowerThird == 0) {
                        throwingThird = true;

                        gladiatorThrowThird = true;
                        gladiatorThrowThirdWeapon = G[y].weaponOne;
                        gladiatorThrowThirdHarm = W[G[y].weaponOne][y][1].harm;

                        G[y].weaponOne = 2;
                        W[G[y].weaponOne][y][1].harm = 0;
                        setText();
                        if (helpThrowerFourth == 0) {
                            counterSecond = 2;
                            gladiatorTwoFight.setVisible(false);
                            gladiatorOneFight.setVisible(true);
                            if (W[G[y].weaponThree][y][3].healtIfItIsAnimal > 0) {
                                gladiator.setVisible(true);
                                animal.setVisible(true);
                                gladiator.setBackground(Color.blue);
                                animal.setBackground(Color.blue);
                            } else {
                                gladiator.setVisible(false);
                                animal.setVisible(false);
                            }
                        }
                    }
                } else {

                    if (helpThrowerFourth > 0 && (counterSecond % 2 == 0 || helpThrowerThird == 0)) {
                        fightFourth(y, x);
                        helpThrowerFourth--;
                        if (helpThrowerFourth == 0) {
                            throwingFourth = true;

                            gladiatorThrowFourth = true;
                            gladiatorThrowFourthWeapon = G[y].weaponTwo;
                            gladiatorThrowFourthHarm = W[G[y].weaponTwo][y][2].harm;

                            G[y].weaponTwo = 2;
                            W[G[y].weaponTwo][y][2].harm = 0;
                            setText();
                            if (helpThrowerThird == 0) {
                                counterSecond = 2;
                                gladiatorTwoFight.setVisible(false);
                                gladiatorOneFight.setVisible(true);
                                if (W[G[y].weaponThree][y][3].healtIfItIsAnimal > 0) {
                                    gladiator.setVisible(true);
                                    animal.setVisible(true);
                                    gladiator.setBackground(Color.blue);
                                    animal.setBackground(Color.blue);
                                } else {
                                    gladiator.setVisible(false);
                                    animal.setVisible(false);
                                }
                            }
                        }
                    }
                }

                if (G[x].lives <= 0) {
                    checkLives();
                }
                if (G[y].lives <= 0) {
                    checkLives();
                }
                if (wantToEnd.isVisible() == false && sineMissione == false) {
                    checkLives();
                }
                if (G[x].lives <= 0) {
                    G[y].level++;
                }
                if (G[y].lives <= 0) {
                    G[x].level++;
                }

            }
        }

        //trenovani
        if (mysX > send3.getX() && mysX < send3.getX() + send3.getWidth() && mysY > send3.getY() && mysY < send3.getY() + send3.getHeight() && send3.isVisible()) {
            //zde se to pouziva jako pocitadlo gladiatoru ne hracu
            whichPlayerIsChoosing++;
            if (G[whichPlayerIsChoosing + 1].owener == 1) {
                basemant.setBackground(Color.blue);
            }
            if (G[whichPlayerIsChoosing + 1].owener == 2) {
                basemant.setBackground(Color.green);
            }
            if (G[whichPlayerIsChoosing + 1].owener == 3) {
                basemant.setBackground(Color.yellow);
            }
            if (G[whichPlayerIsChoosing + 1].owener == 4) {
                basemant.setBackground(Color.orange);
            }

            headline.setVisible(false);
            headline.setVisible(true);

            send3.setVisible(false);
            whichHand.setVisible(false);
            gladiatorOne.setVisible(false);
            gladiatorOne1.setVisible(false);

            send3.setVisible(true);
            whichHand.setVisible(true);
            gladiatorOne.setVisible(true);
            gladiatorOne1.setVisible(true);

            if (G[whichPlayerIsChoosing].level == 4) {
                G[whichPlayerIsChoosing].rudiarius = whichPlayerIsChoosing;
                numberOfGladiators--;
                P[G[whichPlayerIsChoosing].owener].howManyGladiators--;

                G[whichPlayerIsChoosing + 70].nationGladiator = G[whichPlayerIsChoosing].nationGladiator;
                G[whichPlayerIsChoosing + 70].isHandOneAnable = G[whichPlayerIsChoosing].isHandOneAnable;
                G[whichPlayerIsChoosing + 70].isHandTwoAnable = G[whichPlayerIsChoosing].isHandTwoAnable;
                G[whichPlayerIsChoosing + 70].isHandThreeAnable = G[whichPlayerIsChoosing].isHandThreeAnable;
                G[whichPlayerIsChoosing + 70].kaput = G[whichPlayerIsChoosing].kaput;
                G[whichPlayerIsChoosing + 70].level = G[whichPlayerIsChoosing].level;
                G[whichPlayerIsChoosing + 70].lives = G[whichPlayerIsChoosing].lives;
                G[whichPlayerIsChoosing + 70].lvlWeaponOne = G[whichPlayerIsChoosing].lvlWeaponOne;
                G[whichPlayerIsChoosing + 70].lvlWeaponThree = G[whichPlayerIsChoosing].lvlWeaponThree;
                G[whichPlayerIsChoosing + 70].lvlWeaponTwo = G[whichPlayerIsChoosing].lvlWeaponTwo;
                G[whichPlayerIsChoosing + 70].name = G[whichPlayerIsChoosing].name;
                G[whichPlayerIsChoosing + 70].owener = G[whichPlayerIsChoosing].owener;
                G[whichPlayerIsChoosing + 70].weaponOne = G[whichPlayerIsChoosing].weaponOne;
                G[whichPlayerIsChoosing + 70].weaponTwo = G[whichPlayerIsChoosing].weaponTwo;
                G[whichPlayerIsChoosing + 70].weaponOneAfter = G[whichPlayerIsChoosing].weaponOneAfter;
                G[whichPlayerIsChoosing + 70].weaponThree = G[whichPlayerIsChoosing].weaponThree;
                G[whichPlayerIsChoosing + 70].weaponTwoAfter = G[whichPlayerIsChoosing].weaponTwoAfter;

                for (int z = whichPlayerIsChoosing; z < numberOfGladiators + 1; z++) {
                    G[z].nationGladiator = G[z + 1].nationGladiator;
                    G[z].isHandOneAnable = G[z + 1].isHandOneAnable;
                    G[z].isHandTwoAnable = G[z + 1].isHandTwoAnable;
                    G[z].isHandThreeAnable = G[z + 1].isHandThreeAnable;
                    G[z].kaput = G[z + 1].kaput;
                    G[z].level = G[z + 1].level;
                    G[z].lives = G[z + 1].lives;
                    G[z].lvlWeaponOne = G[z + 1].lvlWeaponOne;
                    G[z].lvlWeaponThree = G[z + 1].lvlWeaponThree;
                    G[z].lvlWeaponTwo = G[z + 1].lvlWeaponTwo;
                    G[z].name = G[z + 1].name;
                    G[z].owener = G[z + 1].owener;
                    G[z].weaponOne = G[z + 1].weaponOne;
                    G[z].weaponTwo = G[z + 1].weaponTwo;
                    G[z].weaponOneAfter = G[z + 1].weaponOneAfter;
                    G[z].weaponThree = G[z + 1].weaponThree;
                    G[z].weaponTwoAfter = G[z + 1].weaponTwoAfter;
                }

            } else {

                int a1 = whichPlayerIsChoosing + 1;
                gladiatorOne.setBackground(Color.pink);
                gladiatorOne1.setBackground(Color.pink);
                labGladiatorOne1.setText("<html>" + G[a1].name);
                labGladiatorOne.setText("<html>" + "zbraň jedna: " + W[G[a1].weaponOne][a1][1].NameWeapon + "<br>" + "Power: "
                        + W[G[a1].weaponOne][a1][1].power + "<br>" + "Dexterity: " + W[G[a1].weaponOne][a1][1].dexterity
                        + "<br>" + "Shield: " + W[G[a1].weaponOne][a1][1].shield + "<br>" + "Harm: " + W[G[a1].weaponOne][a1][1].harm
                        + "<br>" + "<br>" + "zbraň dva: " + W[G[a1].weaponTwo][a1][2].NameWeapon + "<br>" + "Power: "
                        + W[G[a1].weaponTwo][a1][2].power + "<br>" + "Dexterity: " + W[G[a1].weaponTwo][a1][2].dexterity
                        + "<br>" + "Shield: " + W[G[a1].weaponTwo][a1][2].shield + "<br>" + "Harm: " + W[G[a1].weaponTwo][a1][2].harm
                        + "<br>" + "<br>" + "zbraň tři: " + W[G[a1].weaponThree][a1][3].NameWeapon + "<br>" + "Power: "
                        + W[G[a1].weaponThree][a1][3].power + "<br>" + "Dexterity: " + W[G[a1].weaponThree][a1][3].dexterity
                        + "<br>" + "Shield: " + W[G[a1].weaponThree][a1][3].shield + "<br>" + "Harm: " + W[G[a1].weaponThree][a1][3].harm);

                if (G[whichPlayerIsChoosing].lives > 6) {
                    if (whichHand.getSelectedIndex() + 1 == 1) {
                        G[whichPlayerIsChoosing].lvlWeaponOne++;
                        if (G[whichPlayerIsChoosing].nationGladiator == W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].NationWeapon) {

                            if (G[whichPlayerIsChoosing].lvlWeaponOne == 1) {
                                W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].power += W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].lvlUp1Power;
                                W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].dexterity += W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].lvlUp1Dexterity;
                                W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].shield += W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].lvlUp1Shield;
                                W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].canBeThrowen += W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].lvlUp1CanBeThrowen;
                                W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].canDestroy += W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].lvlUp1CanDestroy;
                                W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].canGoThroughShield += W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].lvlUp1CanGoThroughShield;
                                W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].canPlayAgain += W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].lvlUp1CanPlayAgain;
                            }
                            if (G[whichPlayerIsChoosing].lvlWeaponOne == 2) {
                                W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].power += W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].lvlUp2Power;
                                W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].dexterity += W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].lvlUp2Dexterity;
                                W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].shield += W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].lvlUp2Shield;
                                W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].canBeThrowen += W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].lvlUp2CanBeThrowen;
                                W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].canDestroy += W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].lvlUp1CanDestroy;
                                W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].canGoThroughShield += W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].lvlUp2CanGoThroughShield;
                                W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].canPlayAgain += W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].lvlUp2CanPlayAgain;
                            }
                            if (G[whichPlayerIsChoosing].lvlWeaponOne == 3) {
                                W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].power += W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].lvlUp3Power;
                                W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].dexterity += W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].lvlUp3Dexterity;
                                W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].shield += W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].lvlUp3Shield;
                                W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].canBeThrowen += W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].lvlUp3CanBeThrowen;
                                W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].canDestroy += W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].lvlUp3CanDestroy;
                                W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].canGoThroughShield += W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].lvlUp3CanGoThroughShield;
                                W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].canPlayAgain += W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].lvlUp3CanPlayAgain;
                            }

                        }

                        if (G[whichPlayerIsChoosing].nationGladiator != W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].NationWeapon) {
                            int random = RND.nextInt(6);
                            if (G[whichPlayerIsChoosing].lvlWeaponOne == 1 && random < 3) {
                                W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].power += W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].lvlUp1Power;
                                W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].dexterity += W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].lvlUp1Dexterity;
                                W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].shield += W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].lvlUp1Shield;
                                W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].canBeThrowen += W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].lvlUp1CanBeThrowen;
                                W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].canDestroy += W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].lvlUp1CanDestroy;
                                W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].canGoThroughShield += W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].lvlUp1CanGoThroughShield;
                                W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].canPlayAgain += W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].lvlUp1CanPlayAgain;
                            }
                            if (G[whichPlayerIsChoosing].lvlWeaponOne == 2 && random < 2) {
                                W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].power += W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].lvlUp2Power;
                                W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].dexterity += W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].lvlUp2Dexterity;
                                W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].shield += W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].lvlUp2Shield;
                                W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].canBeThrowen += W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].lvlUp2CanBeThrowen;
                                W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].canDestroy += W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].lvlUp1CanDestroy;
                                W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].canGoThroughShield += W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].lvlUp2CanGoThroughShield;
                                W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].canPlayAgain += W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].lvlUp2CanPlayAgain;
                            }
                            if (G[whichPlayerIsChoosing].lvlWeaponOne == 3 && random < 1) {
                                W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].power += W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].lvlUp3Power;
                                W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].dexterity += W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].lvlUp3Dexterity;
                                W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].shield += W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].lvlUp3Shield;
                                W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].canBeThrowen += W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].lvlUp3CanBeThrowen;
                                W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].canDestroy += W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].lvlUp3CanDestroy;
                                W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].canGoThroughShield += W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].lvlUp3CanGoThroughShield;
                                W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].canPlayAgain += W[G[whichPlayerIsChoosing].weaponOne][whichPlayerIsChoosing][1].lvlUp3CanPlayAgain;
                            }

                        }

                    }
                    if (whichHand.getSelectedIndex() + 1 == 2) {
                        G[whichPlayerIsChoosing].lvlWeaponTwo++;
                        if (G[whichPlayerIsChoosing].nationGladiator == W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].NationWeapon) {
                            if (G[whichPlayerIsChoosing].lvlWeaponTwo == 1) {
                                W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].power += W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].lvlUp1Power;
                                W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].dexterity += W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].lvlUp1Dexterity;
                                W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].shield += W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].lvlUp1Shield;
                                W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].canBeThrowen += W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].lvlUp1CanBeThrowen;
                                W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].canDestroy += W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].lvlUp1CanDestroy;
                                W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].canGoThroughShield += W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].lvlUp1CanGoThroughShield;
                                W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].canPlayAgain += W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].lvlUp1CanPlayAgain;
                            }
                            if (G[whichPlayerIsChoosing].lvlWeaponTwo == 2) {
                                W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].power += W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].lvlUp2Power;
                                W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].dexterity += W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].lvlUp2Dexterity;
                                W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].shield += W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].lvlUp2Shield;
                                W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].canBeThrowen += W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].lvlUp2CanBeThrowen;
                                W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].canDestroy += W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].lvlUp2CanDestroy;
                                W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].canGoThroughShield += W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].lvlUp2CanGoThroughShield;
                                W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].canPlayAgain += W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].lvlUp2CanPlayAgain;
                            }
                            if (G[whichPlayerIsChoosing].lvlWeaponTwo == 3) {
                                W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].power += W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].lvlUp3Power;
                                W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].dexterity += W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].lvlUp3Dexterity;
                                W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].shield += W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].lvlUp3Shield;
                                W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].canBeThrowen += W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].lvlUp3CanBeThrowen;
                                W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].canDestroy += W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].lvlUp3CanDestroy;
                                W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].canGoThroughShield += W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].lvlUp3CanGoThroughShield;
                                W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].canPlayAgain += W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].lvlUp3CanPlayAgain;
                            }
                        }
                        if (G[whichPlayerIsChoosing].nationGladiator != W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].NationWeapon) {
                            int random = RND.nextInt(6);
                            if (G[whichPlayerIsChoosing].lvlWeaponTwo == 1 && random < 3) {
                                W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].power += W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].lvlUp1Power;
                                W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].dexterity += W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].lvlUp1Dexterity;
                                W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].shield += W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].lvlUp1Shield;
                                W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].canBeThrowen += W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].lvlUp1CanBeThrowen;
                                W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].canDestroy += W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].lvlUp1CanDestroy;
                                W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].canGoThroughShield += W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].lvlUp1CanGoThroughShield;
                                W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].canPlayAgain += W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].lvlUp1CanPlayAgain;
                            }
                            if (G[whichPlayerIsChoosing].lvlWeaponTwo == 2 && random < 2) {
                                W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].power += W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].lvlUp2Power;
                                W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].dexterity += W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].lvlUp2Dexterity;
                                W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].shield += W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].lvlUp2Shield;
                                W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].canBeThrowen += W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].lvlUp2CanBeThrowen;
                                W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].canDestroy += W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].lvlUp2CanDestroy;
                                W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].canGoThroughShield += W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].lvlUp2CanGoThroughShield;
                                W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].canPlayAgain += W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].lvlUp2CanPlayAgain;
                            }
                            if (G[whichPlayerIsChoosing].lvlWeaponTwo == 3 && random < 1) {
                                W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].power += W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].lvlUp3Power;
                                W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].dexterity += W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].lvlUp3Dexterity;
                                W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].shield += W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].lvlUp3Shield;
                                W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].canBeThrowen += W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].lvlUp3CanBeThrowen;
                                W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].canDestroy += W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].lvlUp3CanDestroy;
                                W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].canGoThroughShield += W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].lvlUp3CanGoThroughShield;
                                W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].canPlayAgain += W[G[whichPlayerIsChoosing].weaponTwo][whichPlayerIsChoosing][2].lvlUp3CanPlayAgain;
                            }
                        }

                    }

                    if (whichHand.getSelectedIndex() + 1 == 3) {
                        G[whichPlayerIsChoosing].lvlWeaponThree++;
                        if (G[whichPlayerIsChoosing].nationGladiator == W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].NationWeapon) {
                            if (G[whichPlayerIsChoosing].lvlWeaponThree == 1) {
                                W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].power += W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].lvlUp1Power;
                                W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].dexterity += W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].lvlUp1Dexterity;
                                W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].shield += W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].lvlUp1Shield;
                                W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].canBeThrowen += W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].lvlUp1CanBeThrowen;
                                W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].canDestroy += W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].lvlUp1CanDestroy;
                                W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].canGoThroughShield += W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].lvlUp1CanGoThroughShield;
                                W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].canPlayAgain += W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].lvlUp1CanPlayAgain;
                            }
                            if (G[whichPlayerIsChoosing].lvlWeaponThree == 2) {
                                W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].power += W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].lvlUp2Power;
                                W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].dexterity += W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].lvlUp2Dexterity;
                                W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].shield += W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].lvlUp2Shield;
                                W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].canBeThrowen += W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].lvlUp2CanBeThrowen;
                                W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].canDestroy += W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].lvlUp2CanDestroy;
                                W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].canGoThroughShield += W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].lvlUp2CanGoThroughShield;
                                W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].canPlayAgain += W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].lvlUp2CanPlayAgain;
                            }
                            if (G[whichPlayerIsChoosing].lvlWeaponThree == 3) {
                                W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].power += W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].lvlUp3Power;
                                W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].dexterity += W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].lvlUp3Dexterity;
                                W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].shield += W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].lvlUp3Shield;
                                W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].canBeThrowen += W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].lvlUp3CanBeThrowen;
                                W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].canDestroy += W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].lvlUp3CanDestroy;
                                W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].canGoThroughShield += W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].lvlUp3CanGoThroughShield;
                                W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].canPlayAgain += W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].lvlUp3CanPlayAgain;
                            }
                        }
                        if (G[whichPlayerIsChoosing].nationGladiator != W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].NationWeapon) {
                            int random = RND.nextInt(6);
                            if (G[whichPlayerIsChoosing].lvlWeaponThree == 1 && random < 3) {
                                W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].power += W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].lvlUp1Power;
                                W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].dexterity += W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].lvlUp1Dexterity;
                                W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].shield += W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].lvlUp1Shield;
                                W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].canBeThrowen += W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].lvlUp1CanBeThrowen;
                                W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].canDestroy += W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].lvlUp1CanDestroy;
                                W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].canGoThroughShield += W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].lvlUp1CanGoThroughShield;
                                W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].canPlayAgain += W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].lvlUp1CanPlayAgain;
                            }
                            if (G[whichPlayerIsChoosing].lvlWeaponThree == 2 && random < 2) {
                                W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].power += W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].lvlUp2Power;
                                W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].dexterity += W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].lvlUp2Dexterity;
                                W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].shield += W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].lvlUp2Shield;
                                W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].canBeThrowen += W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].lvlUp2CanBeThrowen;
                                W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].canDestroy += W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].lvlUp2CanDestroy;
                                W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].canGoThroughShield += W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].lvlUp2CanGoThroughShield;
                                W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].canPlayAgain += W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].lvlUp2CanPlayAgain;
                            }
                            if (G[whichPlayerIsChoosing].lvlWeaponThree == 3 && random < 1) {
                                W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].power += W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].lvlUp3Power;
                                W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].dexterity += W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].lvlUp3Dexterity;
                                W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].shield += W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].lvlUp3Shield;
                                W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].canBeThrowen += W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].lvlUp3CanBeThrowen;
                                W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].canDestroy += W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].lvlUp3CanDestroy;
                                W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].canGoThroughShield += W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].lvlUp3CanGoThroughShield;
                                W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].canPlayAgain += W[G[whichPlayerIsChoosing].weaponThree][whichPlayerIsChoosing][3].lvlUp3CanPlayAgain;
                            }
                        }

                    }
                }

                setText();
                whichHand.setSelectedIndex(0);
                if (whichPlayerIsChoosing == numberOfGladiators) {
                    basemant.setBackground(Color.gray);
                    counterRound++;
                    allGladiators11.setVisible(true);
                    allGladiators12.setVisible(true);
                    allGladiators13.setVisible(true);
                    allGladiators14.setVisible(true);
                    allGladiators15.setVisible(true);
                    allGladiators21.setVisible(true);
                    allGladiators22.setVisible(true);
                    allGladiators23.setVisible(true);
                    allGladiators24.setVisible(true);
                    allGladiators25.setVisible(true);
                    allGladiators31.setVisible(true);
                    allGladiators32.setVisible(true);
                    allGladiators33.setVisible(true);
                    allGladiators34.setVisible(true);
                    allGladiators35.setVisible(true);
                    allGladiators41.setVisible(true);
                    allGladiators42.setVisible(true);
                    allGladiators43.setVisible(true);
                    allGladiators44.setVisible(true);
                    allGladiators45.setVisible(true);
                    send3.setVisible(false);
                    headline.setVisible(false);
                    whichGladiator.setVisible(false);
                    whichWeapon.setVisible(false);
                    whichHand.setVisible(false);
                    fightFrame.setVisible(false);
                    fightTrue = false;
                    fight.setVisible(true);
                    treatment.setVisible(true);
                    newWeapon.setVisible(true);
                    training.setVisible(true);
                    gettingGladiator.setVisible(true);
                    sinemissione.setVisible(true);
                    repair.setVisible(true);
                    whichPlayerIsChoosing = 0;
                    gladiatorOne.setVisible(false);
                    gladiatorOne.setBackground(Color.blue);
                    gladiatorOne1.setVisible(false);
                    gladiatorOne1.setBackground(Color.blue);
                    whichHand.setLocation(500, 75);
                    headline.setVisible(false);
                    ROUND();

                }

            }
        }

        //brani gladiatoru
        if (mysX > wantGladiator.getX() && mysX < wantGladiator.getX() + wantGladiator.getWidth() && mysY > wantGladiator.getY() && mysY < wantGladiator.getY() + wantGladiator.getHeight() && wantGladiator.isVisible()) {

            //pouzivano jinak nez se promena jmenuje
            whichPlayerIsChoosing++;

            if (counterHowManyYouTakeAway <= 18) {
                int random = RND.nextInt(5);
                if (whichPlayerIsChoosing == 1) {
                    for (int i = 1; i <= numberOfPlayers; i++) {
                        if (random > P[i].howManyGladiators) {
                            counterHowManyYouTakeAway++;
                            G[numberOfGladiators + gladiatorsCounter].owener = i;
                            G[numberOfGladiators + gladiatorsCounter].lives = 20;
                            G[numberOfGladiators + gladiatorsCounter].weaponOne = 2;
                            G[numberOfGladiators + gladiatorsCounter].weaponTwo = 2;
                            G[numberOfGladiators + gladiatorsCounter].weaponThree = 1;
                            G[i].isHandOneAnable = 5;
                            G[i].isHandTwoAnable = 5;
                            G[i].isHandThreeAnable = 5;
                            P[i].howManyGladiators++;
                            numberOfGladiators++;
                            if (counterHowManyYouTakeAway > 18) {
                                i = 100;
                            }
                        }

                    }
                    labHowManyYouHave.setText("<html>" + "gladiatora" + "<br>" + " bere ten" + "<br>" + "kdo má" + "<br>" + "méně než: " + random + " gladiátory");
                    labWantGladiator.setText("odeslat");
                }
            } else {
                labHowManyYouHave.setText("již jste vyčerpali všechny gladiátory, nebere nikdo");
                labWantGladiator.setText("odeslat");
            }
            if (whichPlayerIsChoosing == 2) {
                headline.setVisible(false);
                allGladiators11.setVisible(true);
                allGladiators12.setVisible(true);
                allGladiators13.setVisible(true);
                allGladiators14.setVisible(true);
                allGladiators15.setVisible(true);
                allGladiators21.setVisible(true);
                allGladiators22.setVisible(true);
                allGladiators23.setVisible(true);
                allGladiators24.setVisible(true);
                allGladiators25.setVisible(true);
                allGladiators31.setVisible(true);
                allGladiators32.setVisible(true);
                allGladiators33.setVisible(true);
                allGladiators34.setVisible(true);
                allGladiators35.setVisible(true);
                allGladiators41.setVisible(true);
                allGladiators42.setVisible(true);
                allGladiators43.setVisible(true);
                allGladiators44.setVisible(true);
                allGladiators45.setVisible(true);
                send.setVisible(false);
                whichGladiator.setVisible(false);
                whichWeapon.setVisible(false);
                whichHand.setVisible(false);
                fightFrame.setVisible(false);
                fightTrue = false;
                fight.setVisible(true);
                treatment.setVisible(true);
                newWeapon.setVisible(true);
                training.setVisible(true);
                gettingGladiator.setVisible(true);
                sinemissione.setVisible(true);
                repair.setVisible(true);
                whichPlayerIsChoosing = 0;
                wantGladiator.setVisible(false);
                howManyYouHave.setVisible(false);
                gladiatorsCounter = 1;
                labHowManyYouHave.setText("");
                labWantGladiator.setText("chceme gladiátora");
                counterRound = 0;
                ROUND();
            }
        }

        //brani zbrane
        if (mysX > send.getX() && mysX < send.getX() + send.getWidth() && mysY > send.getY() && mysY < send.getY() + send.getHeight() && send.isVisible()) {

            whichPlayerIsChoosing++;

            if (whichPlayerIsChoosing == 0) {
                basemant.setBackground(Color.blue);
            }
            if (whichPlayerIsChoosing == 1) {
                basemant.setBackground(Color.green);
            }
            if (whichPlayerIsChoosing == 2) {
                basemant.setBackground(Color.yellow);
            }
            if (whichPlayerIsChoosing == 3) {
                basemant.setBackground(Color.orange);
            }

            headline.setVisible(false);
            headline.setVisible(true);

            whichGladiator.setVisible(false);
            whichWeapon.setVisible(false);
            whichHand.setVisible(false);
            send.setVisible(false);
            gladiatorOne.setVisible(false);
            gladiatorOne1.setVisible(false);
            gladiatorTwo.setVisible(false);
            gladiatorTwo2.setVisible(false);
            gladiatorThree.setVisible(false);
            gladiatorThree3.setVisible(false);
            gladiatorFourth.setVisible(false);
            gladiatorFourth4.setVisible(false);

            whichGladiator.setVisible(true);
            whichWeapon.setVisible(true);
            whichHand.setVisible(true);
            send.setVisible(true);
            gladiatorOne.setVisible(true);
            gladiatorOne1.setVisible(true);
            gladiatorTwo.setVisible(true);
            gladiatorTwo2.setVisible(true);
            gladiatorThree.setVisible(true);
            gladiatorThree3.setVisible(true);
            gladiatorFourth.setVisible(true);
            gladiatorFourth4.setVisible(true);

            redLight.setVisible(false);
            int y = -1;

            for (int x = 1; x < whichPlayerIsChoosing; x++) {
                beforeYou = beforeYou + P[x].howManyGladiators;
            }
            for (int x = 1; x < whichPlayerIsChoosing + 1; x++) {
                beforeYou2 = beforeYou2 + P[x].howManyGladiators;
            }

            int i = (whichGladiator.getSelectedIndex() + 1) + beforeYou;

            int a1 = 1 + beforeYou2;
            int a2 = 2 + beforeYou2;
            int a3 = 3 + beforeYou2;
            int a4 = 4 + beforeYou2;

            if (P[whichPlayerIsChoosing].howManyGladiators > 0) {
                gladiatorOne.setBackground(Color.pink);
                gladiatorOne1.setBackground(Color.pink);
                labGladiatorOne1.setText("<html>" + G[a1].name);
                labGladiatorOne.setText("<html>" + "zbraň jedna: " + W[G[a1].weaponOne][a1][1].NameWeapon + "<br>" + "Power: "
                        + W[G[a1].weaponOne][a1][1].power + "<br>" + "Dexterity: " + W[G[a1].weaponOne][a1][1].dexterity
                        + "<br>" + "Shield: " + W[G[a1].weaponOne][a1][1].shield + "<br>" + "Harm: " + W[G[a1].weaponOne][a1][1].harm
                        + "<br>" + "<br>" + "zbraň dva: " + W[G[a1].weaponTwo][a1][2].NameWeapon + "<br>" + "Power: "
                        + W[G[a1].weaponTwo][a1][2].power + "<br>" + "Dexterity: " + W[G[a1].weaponTwo][a1][2].dexterity
                        + "<br>" + "Shield: " + W[G[a1].weaponTwo][a1][2].shield + "<br>" + "Harm: " + W[G[a1].weaponTwo][a1][2].harm
                        + "<br>" + "<br>" + "zbraň tři: " + W[G[a1].weaponThree][a1][3].NameWeapon + "<br>" + "Power: "
                        + W[G[a1].weaponThree][a1][3].power + "<br>" + "Dexterity: " + W[G[a1].weaponThree][a1][3].dexterity
                        + "<br>" + "Shield: " + W[G[a1].weaponThree][a1][3].shield + "<br>" + "Harm: " + W[G[a1].weaponThree][a1][3].harm);
            }
            if (P[whichPlayerIsChoosing].howManyGladiators > 1) {
                gladiatorTwo.setBackground(Color.pink);
                gladiatorTwo2.setBackground(Color.pink);
                labGladiatorTwo2.setText("<html>" + G[a2].name);
                labGladiatorTwo.setText("<html>" + "zbraň jedna: " + W[G[a2].weaponOne][a2][1].NameWeapon + "<br>" + "Power: "
                        + W[G[a2].weaponOne][a2][1].power + "<br>" + "Dexterity: " + W[G[a2].weaponOne][a2][1].dexterity
                        + "<br>" + "Shield: " + W[G[a2].weaponOne][a2][1].shield + "<br>" + "Harm: " + W[G[a2].weaponOne][a2][1].harm
                        + "<br>" + "<br>" + "zbraň dva: " + W[G[a2].weaponTwo][a2][2].NameWeapon + "<br>" + "Power: "
                        + W[G[a2].weaponTwo][a2][2].power + "<br>" + "Dexterity: " + W[G[a2].weaponTwo][a2][2].dexterity
                        + "<br>" + "Shield: " + W[G[a2].weaponTwo][a2][2].shield + "<br>" + "Harm: " + W[G[a2].weaponTwo][a2][2].harm
                        + "<br>" + "<br>" + "zbraň tři: " + W[G[a2].weaponThree][a2][3].NameWeapon + "<br>" + "Power: "
                        + W[G[a2].weaponThree][a2][3].power + "<br>" + "Dexterity: " + W[G[a2].weaponThree][a2][3].dexterity
                        + "<br>" + "Shield: " + W[G[a2].weaponThree][a2][3].shield + "<br>" + "Harm: " + W[G[a2].weaponThree][a2][3].harm);
            }
            if (P[whichPlayerIsChoosing].howManyGladiators > 2) {
                gladiatorThree.setBackground(Color.pink);
                gladiatorThree3.setBackground(Color.pink);
                labGladiatorThree3.setText("<html>" + G[a3].name);
                labGladiatorThree.setText("<html>" + "zbraň jedna: " + W[G[a3].weaponOne][a3][1].NameWeapon + "<br>" + "Power: "
                        + W[G[a3].weaponOne][a3][1].power + "<br>" + "Dexterity: " + W[G[a3].weaponOne][a3][1].dexterity
                        + "<br>" + "Shield: " + W[G[a3].weaponOne][a3][1].shield + "<br>" + "Harm: " + W[G[a3].weaponOne][a3][1].harm
                        + "<br>" + "<br>" + "zbraň dva: " + W[G[a3].weaponTwo][a3][2].NameWeapon + "<br>" + "Power: "
                        + W[G[a3].weaponTwo][a3][2].power + "<br>" + "Dexterity: " + W[G[a3].weaponTwo][a3][2].dexterity
                        + "<br>" + "Shield: " + W[G[a3].weaponTwo][a3][2].shield + "<br>" + "Harm: " + W[G[a3].weaponTwo][a3][2].harm
                        + "<br>" + "<br>" + "zbraň tři: " + W[G[a3].weaponThree][a3][3].NameWeapon + "<br>" + "Power: "
                        + W[G[a3].weaponThree][a3][3].power + "<br>" + "Dexterity: " + W[G[a3].weaponThree][a3][3].dexterity
                        + "<br>" + "Shield: " + W[G[a3].weaponThree][a3][3].shield + "<br>" + "Harm: " + W[G[a3].weaponThree][a3][3].harm);
            }
            if (P[whichPlayerIsChoosing].howManyGladiators > 3) {
                gladiatorFourth.setBackground(Color.pink);
                gladiatorFourth4.setBackground(Color.pink);
                labGladiatorFourth4.setText("<html>" + G[a4].name);
                labGladiatorFourth.setText("<html>" + "zbraň jedna: " + W[G[a4].weaponOne][a4][1].NameWeapon + "<br>" + "Power: "
                        + W[G[a4].weaponOne][a4][1].power + "<br>" + "Dexterity: " + W[G[a4].weaponOne][a4][1].dexterity
                        + "<br>" + "Shield: " + W[G[a4].weaponOne][a4][1].shield + "<br>" + "Harm: " + W[G[a4].weaponOne][a4][1].harm
                        + "<br>" + "<br>" + "zbraň dva: " + W[G[a4].weaponTwo][a4][2].NameWeapon + "<br>" + "Power: "
                        + W[G[a4].weaponTwo][a4][2].power + "<br>" + "Dexterity: " + W[G[a4].weaponTwo][a4][2].dexterity
                        + "<br>" + "Shield: " + W[G[a4].weaponTwo][a4][2].shield + "<br>" + "Harm: " + W[G[a4].weaponTwo][a4][2].harm
                        + "<br>" + "<br>" + "zbraň tři: " + W[G[a4].weaponThree][a4][3].NameWeapon + "<br>" + "Power: "
                        + W[G[a4].weaponThree][a4][3].power + "<br>" + "Dexterity: " + W[G[a4].weaponThree][a4][3].dexterity
                        + "<br>" + "Shield: " + W[G[a4].weaponThree][a4][3].shield + "<br>" + "Harm: " + W[G[a4].weaponThree][a4][3].harm);
            }

            if (gladiatorOne.getBackground() != Color.pink) {
                gladiatorOne.setVisible(false);
                gladiatorOne1.setVisible(false);
            }
            if (gladiatorTwo.getBackground() != Color.pink) {
                gladiatorTwo.setVisible(false);
                gladiatorTwo2.setVisible(false);
            }
            if (gladiatorThree.getBackground() != Color.pink) {
                gladiatorThree.setVisible(false);
                gladiatorThree3.setVisible(false);
            }
            if (gladiatorFourth.getBackground() != Color.pink) {
                gladiatorFourth.setVisible(false);
                gladiatorFourth4.setVisible(false);
            }

            int z = whichWeapon.getSelectedIndex() + 2;
            if (whichHand.getSelectedIndex() + 1 == 1) {
                y = G[i].isHandOneAnable;
            }
            if (whichHand.getSelectedIndex() + 1 == 2) {
                y = G[i].isHandTwoAnable;
            }
            if (whichHand.getSelectedIndex() + 1 == 3) {
                y = G[i].isHandThreeAnable;
            }

            if (W[z][i][1].onWhichLevelCanBeThisWeaponAnable >= y || (W[z][i][1].healtIfItIsAnimal > 0 && whichHand.getSelectedIndex() + 1 != 3)) {
                whichPlayerIsChoosing--;
                redLight.setVisible(true);
            } else {

                if (whichHand.getSelectedIndex() + 1 == 1) {
                    G[i].weaponOne = z;
                    G[i].lvlWeaponOne = 0;

                    W[G[i].weaponOne][i][1].power = W[G[i].weaponOne][i][4].power;
                    W[G[i].weaponOne][i][1].canBeThrowen = W[G[i].weaponOne][i][4].canBeThrowen;
                    W[G[i].weaponOne][i][1].canDestroy = W[G[i].weaponOne][i][4].canDestroy;
                    W[G[i].weaponOne][i][1].canGoThroughShield = W[G[i].weaponOne][i][4].canGoThroughShield;
                    W[G[i].weaponOne][i][1].dexterity = W[G[i].weaponOne][i][4].dexterity;
                    W[G[i].weaponOne][i][1].shield = W[G[i].weaponOne][i][4].shield;
                    W[G[i].weaponOne][i][1].canPlayAgain = W[G[i].weaponOne][i][4].canPlayAgain;

                    W[G[i].weaponOne][i][1].harm = 0;

                    G[i].isHandTwoAnable = W[G[i].weaponOne][i][1].onWhichLevelCanBeNextWeaponAnable;
                    if (W[G[i].weaponTwo][i][2].onWhichLevelCanBeThisWeaponAnable >= G[i].isHandTwoAnable) {
                        G[i].weaponTwo = 1;
                        if (G[i].isHandTwoAnable == 2) {
                            G[i].weaponTwo = 2;
                        }
                    }
                }
                if (whichHand.getSelectedIndex() + 1 == 2) {
                    G[i].weaponTwo = z;
                    G[i].lvlWeaponTwo = 0;

                    W[G[i].weaponTwo][i][2].power = W[G[i].weaponTwo][i][4].power;
                    W[G[i].weaponTwo][i][2].canBeThrowen = W[G[i].weaponTwo][i][4].canBeThrowen;
                    W[G[i].weaponTwo][i][2].canDestroy = W[G[i].weaponTwo][i][4].canDestroy;
                    W[G[i].weaponTwo][i][2].canGoThroughShield = W[G[i].weaponTwo][i][4].canGoThroughShield;
                    W[G[i].weaponTwo][i][2].dexterity = W[G[i].weaponTwo][i][4].dexterity;
                    W[G[i].weaponTwo][i][2].shield = W[G[i].weaponTwo][i][4].shield;
                    W[G[i].weaponTwo][i][2].canPlayAgain = W[G[i].weaponTwo][i][4].canPlayAgain;

                    W[G[i].weaponTwo][i][2].harm = 0;

                    G[i].isHandOneAnable = W[G[i].weaponTwo][i][2].onWhichLevelCanBeNextWeaponAnable;
                    if (W[G[i].weaponOne][i][2].onWhichLevelCanBeThisWeaponAnable >= G[i].isHandOneAnable) {
                        G[i].weaponOne = 1;
                        if (G[i].isHandOneAnable == 2) {
                            G[i].weaponOne = 2;
                        }
                    }
                }
                if (whichHand.getSelectedIndex() + 1 == 3) {

                    W[G[i].weaponThree][i][3].power = W[G[i].weaponThree][i][4].power;
                    W[G[i].weaponThree][i][3].canBeThrowen = W[G[i].weaponThree][i][4].canBeThrowen;
                    W[G[i].weaponThree][i][3].canDestroy = W[G[i].weaponThree][i][4].canDestroy;
                    W[G[i].weaponThree][i][3].canGoThroughShield = W[G[i].weaponThree][i][4].canGoThroughShield;
                    W[G[i].weaponThree][i][3].dexterity = W[G[i].weaponThree][i][4].dexterity;
                    W[G[i].weaponThree][i][3].shield = W[G[i].weaponThree][i][4].shield;
                    W[G[i].weaponThree][i][3].canPlayAgain = W[G[i].weaponThree][i][4].canPlayAgain;

                    W[G[i].weaponThree][i][3].healtIfItIsAnimal = W[G[i].weaponThree][i][4].healtIfItIsAnimal;

                    G[i].weaponThree = z;
                    G[i].lvlWeaponThree = 0;
                }

            }

            whichGladiator.setSelectedIndex(0);
            whichWeapon.setSelectedIndex(0);
            whichHand.setSelectedIndex(0);

            if (whichPlayerIsChoosing == numberOfPlayers) {
                basemant.setBackground(Color.gray);

                counterRound++;
                allGladiators11.setVisible(true);
                allGladiators12.setVisible(true);
                allGladiators13.setVisible(true);
                allGladiators14.setVisible(true);
                allGladiators15.setVisible(true);
                allGladiators21.setVisible(true);
                allGladiators22.setVisible(true);
                allGladiators23.setVisible(true);
                allGladiators24.setVisible(true);
                allGladiators25.setVisible(true);
                allGladiators31.setVisible(true);
                allGladiators32.setVisible(true);
                allGladiators33.setVisible(true);
                allGladiators34.setVisible(true);
                allGladiators35.setVisible(true);
                allGladiators41.setVisible(true);
                allGladiators42.setVisible(true);
                allGladiators43.setVisible(true);
                allGladiators44.setVisible(true);
                allGladiators45.setVisible(true);
                send.setVisible(false);
                headline.setVisible(false);
                whichGladiator.setVisible(false);
                whichWeapon.setVisible(false);
                whichHand.setVisible(false);
                fightFrame.setVisible(false);
                fightTrue = false;
                fight.setVisible(true);
                treatment.setVisible(true);
                newWeapon.setVisible(true);
                training.setVisible(true);
                gettingGladiator.setVisible(true);
                sinemissione.setVisible(true);
                repair.setVisible(true);
                whichPlayerIsChoosing = 0;
                gladiatorOne.setVisible(false);
                gladiatorOne1.setVisible(false);
                gladiatorTwo.setVisible(false);
                gladiatorTwo2.setVisible(false);
                gladiatorThree.setVisible(false);
                gladiatorThree3.setVisible(false);
                gladiatorFourth.setVisible(false);
                gladiatorFourth4.setVisible(false);
                gladiatorOne.setBackground(Color.blue);
                gladiatorOne1.setBackground(Color.blue);
                gladiatorTwo.setBackground(Color.blue);
                gladiatorTwo2.setBackground(Color.blue);
                gladiatorThree.setBackground(Color.blue);
                gladiatorThree3.setBackground(Color.blue);
                gladiatorFourth.setBackground(Color.blue);
                gladiatorFourth4.setBackground(Color.blue);
                ROUND();
            } else {
                if (whichPlayerIsChoosing == 0) {
                    basemant.setBackground(Color.blue);
                }
                if (whichPlayerIsChoosing == 1) {
                    basemant.setBackground(Color.green);
                }
                if (whichPlayerIsChoosing == 2) {
                    basemant.setBackground(Color.yellow);
                }
                if (whichPlayerIsChoosing == 3) {
                    basemant.setBackground(Color.orange);
                }

                headline.setVisible(false);
                headline.setVisible(true);

                whichGladiator.setVisible(false);
                whichWeapon.setVisible(false);
                whichHand.setVisible(false);
                send.setVisible(false);
                gladiatorOne.setVisible(false);
                gladiatorOne1.setVisible(false);
                gladiatorTwo.setVisible(false);
                gladiatorTwo2.setVisible(false);
                gladiatorThree.setVisible(false);
                gladiatorThree3.setVisible(false);
                gladiatorFourth.setVisible(false);
                gladiatorFourth4.setVisible(false);

                whichGladiator.setVisible(true);
                whichWeapon.setVisible(true);
                whichHand.setVisible(true);
                send.setVisible(true);
                gladiatorOne.setVisible(true);
                gladiatorOne1.setVisible(true);
                gladiatorTwo.setVisible(true);
                gladiatorTwo2.setVisible(true);
                gladiatorThree.setVisible(true);
                gladiatorThree3.setVisible(true);
                gladiatorFourth.setVisible(true);
                gladiatorFourth4.setVisible(true);

                beforeYou = 0;
                beforeYou2 = 0;
                for (int x = 1; x < whichPlayerIsChoosing; x++) {
                    beforeYou = beforeYou + P[x].howManyGladiators;
                }
                for (int x = 1; x < whichPlayerIsChoosing + 1; x++) {
                    beforeYou2 = beforeYou2 + P[x].howManyGladiators;
                }

                i = (whichGladiator.getSelectedIndex() + 1) + beforeYou;

                a1 = 1 + beforeYou2;
                a2 = 2 + beforeYou2;
                a3 = 3 + beforeYou2;
                a4 = 4 + beforeYou2;

                if (P[whichPlayerIsChoosing + 1].howManyGladiators > 0) {
                    gladiatorOne.setBackground(Color.pink);
                    gladiatorOne1.setBackground(Color.pink);
                    labGladiatorOne1.setText("<html>" + G[a1].name);
                    labGladiatorOne.setText("<html>" + "zbraň jedna: " + W[G[a1].weaponOne][a1][1].NameWeapon + "<br>" + "Power: "
                            + W[G[a1].weaponOne][a1][1].power + "<br>" + "Dexterity: " + W[G[a1].weaponOne][a1][1].dexterity
                            + "<br>" + "Shield: " + W[G[a1].weaponOne][a1][1].shield + "<br>" + "Harm: " + W[G[a1].weaponOne][a1][1].harm
                            + "<br>" + "<br>" + "zbraň dva: " + W[G[a1].weaponTwo][a1][2].NameWeapon + "<br>" + "Power: "
                            + W[G[a1].weaponTwo][a1][2].power + "<br>" + "Dexterity: " + W[G[a1].weaponTwo][a1][2].dexterity
                            + "<br>" + "Shield: " + W[G[a1].weaponTwo][a1][2].shield + "<br>" + "Harm: " + W[G[a1].weaponTwo][a1][2].harm
                            + "<br>" + "<br>" + "zbraň tři: " + W[G[a1].weaponThree][a1][3].NameWeapon + "<br>" + "Power: "
                            + W[G[a1].weaponThree][a1][3].power + "<br>" + "Dexterity: " + W[G[a1].weaponThree][a1][3].dexterity
                            + "<br>" + "Shield: " + W[G[a1].weaponThree][a1][3].shield + "<br>" + "Harm: " + W[G[a1].weaponThree][a1][3].harm);
                }
                if (P[whichPlayerIsChoosing + 1].howManyGladiators > 1) {
                    gladiatorTwo.setBackground(Color.pink);
                    gladiatorTwo2.setBackground(Color.pink);
                    labGladiatorTwo2.setText("<html>" + G[a2].name);
                    labGladiatorTwo.setText("<html>" + "zbraň jedna: " + W[G[a2].weaponOne][a2][1].NameWeapon + "<br>" + "Power: "
                            + W[G[a2].weaponOne][a2][1].power + "<br>" + "Dexterity: " + W[G[a2].weaponOne][a2][1].dexterity
                            + "<br>" + "Shield: " + W[G[a2].weaponOne][a2][1].shield + "<br>" + "Harm: " + W[G[a2].weaponOne][a2][1].harm
                            + "<br>" + "<br>" + "zbraň dva: " + W[G[a2].weaponTwo][a2][2].NameWeapon + "<br>" + "Power: "
                            + W[G[a2].weaponTwo][a2][2].power + "<br>" + "Dexterity: " + W[G[a2].weaponTwo][a2][2].dexterity
                            + "<br>" + "Shield: " + W[G[a2].weaponTwo][a2][2].shield + "<br>" + "Harm: " + W[G[a2].weaponTwo][a2][2].harm
                            + "<br>" + "<br>" + "zbraň tři: " + W[G[a2].weaponThree][a2][3].NameWeapon + "<br>" + "Power: "
                            + W[G[a2].weaponThree][a2][3].power + "<br>" + "Dexterity: " + W[G[a2].weaponThree][a2][3].dexterity
                            + "<br>" + "Shield: " + W[G[a2].weaponThree][a2][3].shield + "<br>" + "Harm: " + W[G[a2].weaponThree][a2][3].harm);
                }
                if (P[whichPlayerIsChoosing + 1].howManyGladiators > 2) {
                    gladiatorThree.setBackground(Color.pink);
                    gladiatorThree3.setBackground(Color.pink);
                    labGladiatorThree3.setText("<html>" + G[a3].name);
                    labGladiatorThree.setText("<html>" + "zbraň jedna: " + W[G[a3].weaponOne][a3][1].NameWeapon + "<br>" + "Power: "
                            + W[G[a3].weaponOne][a3][1].power + "<br>" + "Dexterity: " + W[G[a3].weaponOne][a3][1].dexterity
                            + "<br>" + "Shield: " + W[G[a3].weaponOne][a3][1].shield + "<br>" + "Harm: " + W[G[a3].weaponOne][a3][1].harm
                            + "<br>" + "<br>" + "zbraň dva: " + W[G[a3].weaponTwo][a3][2].NameWeapon + "<br>" + "Power: "
                            + W[G[a3].weaponTwo][a3][2].power + "<br>" + "Dexterity: " + W[G[a3].weaponTwo][a3][2].dexterity
                            + "<br>" + "Shield: " + W[G[a3].weaponTwo][a3][2].shield + "<br>" + "Harm: " + W[G[a3].weaponTwo][a3][2].harm
                            + "<br>" + "<br>" + "zbraň tři: " + W[G[a3].weaponThree][a3][3].NameWeapon + "<br>" + "Power: "
                            + W[G[a3].weaponThree][a3][3].power + "<br>" + "Dexterity: " + W[G[a3].weaponThree][a3][3].dexterity
                            + "<br>" + "Shield: " + W[G[a3].weaponThree][a3][3].shield + "<br>" + "Harm: " + W[G[a3].weaponThree][a3][3].harm);
                }
                if (P[whichPlayerIsChoosing + 1].howManyGladiators > 3) {
                    gladiatorFourth.setBackground(Color.pink);
                    gladiatorFourth4.setBackground(Color.pink);
                    labGladiatorFourth4.setText("<html>" + G[a4].name);
                    labGladiatorFourth.setText("<html>" + "zbraň jedna: " + W[G[a4].weaponOne][a4][1].NameWeapon + "<br>" + "Power: "
                            + W[G[a4].weaponOne][a4][1].power + "<br>" + "Dexterity: " + W[G[a4].weaponOne][a4][1].dexterity
                            + "<br>" + "Shield: " + W[G[a4].weaponOne][a4][1].shield + "<br>" + "Harm: " + W[G[a4].weaponOne][a4][1].harm
                            + "<br>" + "<br>" + "zbraň dva: " + W[G[a4].weaponTwo][a4][2].NameWeapon + "<br>" + "Power: "
                            + W[G[a4].weaponTwo][a4][2].power + "<br>" + "Dexterity: " + W[G[a4].weaponTwo][a4][2].dexterity
                            + "<br>" + "Shield: " + W[G[a4].weaponTwo][a4][2].shield + "<br>" + "Harm: " + W[G[a4].weaponTwo][a4][2].harm
                            + "<br>" + "<br>" + "zbraň tři: " + W[G[a4].weaponThree][a4][3].NameWeapon + "<br>" + "Power: "
                            + W[G[a4].weaponThree][a4][3].power + "<br>" + "Dexterity: " + W[G[a4].weaponThree][a4][3].dexterity
                            + "<br>" + "Shield: " + W[G[a4].weaponThree][a4][3].shield + "<br>" + "Harm: " + W[G[a4].weaponThree][a4][3].harm);
                }

                if (gladiatorOne.getBackground() != Color.pink) {
                    gladiatorOne.setVisible(false);
                    gladiatorOne1.setVisible(false);
                }
                if (gladiatorTwo.getBackground() != Color.pink) {
                    gladiatorTwo.setVisible(false);
                    gladiatorTwo2.setVisible(false);
                }
                if (gladiatorThree.getBackground() != Color.pink) {
                    gladiatorThree.setVisible(false);
                    gladiatorThree3.setVisible(false);
                }
                if (gladiatorFourth.getBackground() != Color.pink) {
                    gladiatorFourth.setVisible(false);
                    gladiatorFourth4.setVisible(false);
                }

            }

            whichGladiator.removeAllItems();

            for (int d = 1; d <= P[whichPlayerIsChoosing + 1].howManyGladiators; d++) {
                whichGladiator.addItem("gladiátor: " + d);
            }
            beforeYou = 0;
            beforeYou2 = 0;

        }

        //opravovani
        if (mysX > send2.getX() && mysX < send2.getX() + send2.getWidth() && mysY > send2.getY() && mysY < send2.getY() + send2.getHeight() && send2.isVisible()) {

            whichPlayerIsChoosing++;

            if (whichPlayerIsChoosing == 0) {
                basemant.setBackground(Color.blue);
            }
            if (whichPlayerIsChoosing == 1) {
                basemant.setBackground(Color.green);
            }
            if (whichPlayerIsChoosing == 2) {
                basemant.setBackground(Color.yellow);
            }
            if (whichPlayerIsChoosing == 3) {
                basemant.setBackground(Color.orange);
            }
            headline.setVisible(false);
            headline.setVisible(true);

            whichGladiatorRepair.setVisible(false);
            whichHandRepair.setVisible(false);
            send2.setVisible(false);
            gettingGladiator.setVisible(false);
            gladiatorOne.setVisible(false);
            gladiatorOne1.setVisible(false);
            gladiatorTwo.setVisible(false);
            gladiatorTwo2.setVisible(false);
            gladiatorThree.setVisible(false);
            gladiatorThree3.setVisible(false);
            gladiatorFourth.setVisible(false);
            gladiatorFourth4.setVisible(false);

            whichGladiatorRepair.setVisible(true);
            whichHandRepair.setVisible(true);
            send2.setVisible(true);
            gettingGladiator.setVisible(false);
            gladiatorOne.setVisible(true);
            gladiatorOne1.setVisible(true);
            gladiatorTwo.setVisible(true);
            gladiatorTwo2.setVisible(true);
            gladiatorThree.setVisible(true);
            gladiatorThree3.setVisible(true);
            gladiatorFourth.setVisible(true);
            gladiatorFourth4.setVisible(true);

            for (int x = 1; x < whichPlayerIsChoosing; x++) {
                beforeYou = beforeYou + P[x].howManyGladiators;
            }
            for (int x = 1; x < whichPlayerIsChoosing + 1; x++) {
                beforeYou2 = beforeYou2 + P[x].howManyGladiators;
            }

            int a1 = 1 + beforeYou2;
            int a2 = 2 + beforeYou2;
            int a3 = 3 + beforeYou2;
            int a4 = 4 + beforeYou2;

            if (P[whichPlayerIsChoosing].howManyGladiators > 0) {
                gladiatorOne.setBackground(Color.pink);
                gladiatorOne1.setBackground(Color.pink);
                labGladiatorOne1.setText("<html>" + G[a1].name);
                labGladiatorOne.setText("<html>" + "zbraň jedna: " + W[G[a1].weaponOne][a1][1].NameWeapon + "<br>" + "Power: "
                        + W[G[a1].weaponOne][a1][1].power + "<br>" + "Dexterity: " + W[G[a1].weaponOne][a1][1].dexterity
                        + "<br>" + "Shield: " + W[G[a1].weaponOne][a1][1].shield + "<br>" + "Harm: " + W[G[a1].weaponOne][a1][1].harm
                        + "<br>" + "<br>" + "zbraň dva: " + W[G[a1].weaponTwo][a1][2].NameWeapon + "<br>" + "Power: "
                        + W[G[a1].weaponTwo][a1][2].power + "<br>" + "Dexterity: " + W[G[a1].weaponTwo][a1][2].dexterity
                        + "<br>" + "Shield: " + W[G[a1].weaponTwo][a1][2].shield + "<br>" + "Harm: " + W[G[a1].weaponTwo][a1][2].harm
                        + "<br>" + "<br>" + "zbraň tři: " + W[G[a1].weaponThree][a1][3].NameWeapon + "<br>" + "Power: "
                        + W[G[a1].weaponThree][a1][3].power + "<br>" + "Dexterity: " + W[G[a1].weaponThree][a1][3].dexterity
                        + "<br>" + "Shield: " + W[G[a1].weaponThree][a1][3].shield + "<br>" + "Harm: " + W[G[a1].weaponThree][a1][3].harm);
            }
            if (P[whichPlayerIsChoosing].howManyGladiators > 1) {
                gladiatorTwo.setBackground(Color.pink);
                gladiatorTwo2.setBackground(Color.pink);
                labGladiatorTwo2.setText("<html>" + G[a2].name);
                labGladiatorTwo.setText("<html>" + "zbraň jedna: " + W[G[a2].weaponOne][a2][1].NameWeapon + "<br>" + "Power: "
                        + W[G[a2].weaponOne][a2][1].power + "<br>" + "Dexterity: " + W[G[a2].weaponOne][a2][1].dexterity
                        + "<br>" + "Shield: " + W[G[a2].weaponOne][a2][1].shield + "<br>" + "Harm: " + W[G[a2].weaponOne][a2][1].harm
                        + "<br>" + "<br>" + "zbraň dva: " + W[G[a2].weaponTwo][a2][2].NameWeapon + "<br>" + "Power: "
                        + W[G[a2].weaponTwo][a2][2].power + "<br>" + "Dexterity: " + W[G[a2].weaponTwo][a2][2].dexterity
                        + "<br>" + "Shield: " + W[G[a2].weaponTwo][a2][2].shield + "<br>" + "Harm: " + W[G[a2].weaponTwo][a2][2].harm
                        + "<br>" + "<br>" + "zbraň tři: " + W[G[a2].weaponThree][a2][3].NameWeapon + "<br>" + "Power: "
                        + W[G[a2].weaponThree][a2][3].power + "<br>" + "Dexterity: " + W[G[a2].weaponThree][a2][3].dexterity
                        + "<br>" + "Shield: " + W[G[a2].weaponThree][a2][3].shield + "<br>" + "Harm: " + W[G[a2].weaponThree][a2][3].harm);
            }
            if (P[whichPlayerIsChoosing].howManyGladiators > 2) {
                gladiatorThree.setBackground(Color.pink);
                gladiatorThree3.setBackground(Color.pink);
                labGladiatorThree3.setText("<html>" + G[a3].name);
                labGladiatorThree.setText("<html>" + "zbraň jedna: " + W[G[a3].weaponOne][a3][1].NameWeapon + "<br>" + "Power: "
                        + W[G[a3].weaponOne][a3][1].power + "<br>" + "Dexterity: " + W[G[a3].weaponOne][a3][1].dexterity
                        + "<br>" + "Shield: " + W[G[a3].weaponOne][a3][1].shield + "<br>" + "Harm: " + W[G[a3].weaponOne][a3][1].harm
                        + "<br>" + "<br>" + "zbraň dva: " + W[G[a3].weaponTwo][a3][2].NameWeapon + "<br>" + "Power: "
                        + W[G[a3].weaponTwo][a3][2].power + "<br>" + "Dexterity: " + W[G[a3].weaponTwo][a3][2].dexterity
                        + "<br>" + "Shield: " + W[G[a3].weaponTwo][a3][2].shield + "<br>" + "Harm: " + W[G[a3].weaponTwo][a3][2].harm
                        + "<br>" + "<br>" + "zbraň tři: " + W[G[a3].weaponThree][a3][3].NameWeapon + "<br>" + "Power: "
                        + W[G[a3].weaponThree][a3][3].power + "<br>" + "Dexterity: " + W[G[a3].weaponThree][a3][3].dexterity
                        + "<br>" + "Shield: " + W[G[a3].weaponThree][a3][3].shield + "<br>" + "Harm: " + W[G[a3].weaponThree][a3][3].harm);
            }
            if (P[whichPlayerIsChoosing].howManyGladiators > 3) {
                gladiatorFourth.setBackground(Color.pink);
                gladiatorFourth4.setBackground(Color.pink);
                labGladiatorFourth4.setText("<html>" + G[a4].name);
                labGladiatorFourth.setText("<html>" + "zbraň jedna: " + W[G[a4].weaponOne][a4][1].NameWeapon + "<br>" + "Power: "
                        + W[G[a4].weaponOne][a4][1].power + "<br>" + "Dexterity: " + W[G[a4].weaponOne][a4][1].dexterity
                        + "<br>" + "Shield: " + W[G[a4].weaponOne][a4][1].shield + "<br>" + "Harm: " + W[G[a4].weaponOne][a4][1].harm
                        + "<br>" + "<br>" + "zbraň dva: " + W[G[a4].weaponTwo][a4][2].NameWeapon + "<br>" + "Power: "
                        + W[G[a4].weaponTwo][a4][2].power + "<br>" + "Dexterity: " + W[G[a4].weaponTwo][a4][2].dexterity
                        + "<br>" + "Shield: " + W[G[a4].weaponTwo][a4][2].shield + "<br>" + "Harm: " + W[G[a4].weaponTwo][a4][2].harm
                        + "<br>" + "<br>" + "zbraň tři: " + W[G[a4].weaponThree][a4][3].NameWeapon + "<br>" + "Power: "
                        + W[G[a4].weaponThree][a4][3].power + "<br>" + "Dexterity: " + W[G[a4].weaponThree][a4][3].dexterity
                        + "<br>" + "Shield: " + W[G[a4].weaponThree][a4][3].shield + "<br>" + "Harm: " + W[G[a4].weaponThree][a4][3].harm);
            }

            if (gladiatorOne.getBackground() != Color.pink) {
                gladiatorOne.setVisible(false);
                gladiatorOne1.setVisible(false);
            }
            if (gladiatorTwo.getBackground() != Color.pink) {
                gladiatorTwo.setVisible(false);
                gladiatorTwo2.setVisible(false);
            }
            if (gladiatorThree.getBackground() != Color.pink) {
                gladiatorThree.setVisible(false);
                gladiatorThree3.setVisible(false);
            }
            if (gladiatorFourth.getBackground() != Color.pink) {
                gladiatorFourth.setVisible(false);
                gladiatorFourth4.setVisible(false);
            }

            int i = (whichGladiatorRepair.getSelectedIndex() + 1) + beforeYou;
            if (whichHandRepair.getSelectedIndex() + 1 == 1) {
                W[G[i].weaponOne][i][1].harm = 0;
            }
            if (whichHandRepair.getSelectedIndex() + 1 == 2) {
                W[G[i].weaponTwo][i][2].harm = 0;
            }
            if (whichHandRepair.getSelectedIndex() + 1 == 3) {
                W[G[i].weaponThree][i][3].harm = 0;
            }

            whichGladiatorRepair.setSelectedIndex(0);
            whichHandRepair.setSelectedIndex(0);

            if (whichPlayerIsChoosing == numberOfPlayers) {

                basemant.setBackground(Color.gray);

                labBasemant.setIcon(borduraW);
                counterRound++;
                allGladiators11.setVisible(true);
                allGladiators12.setVisible(true);
                allGladiators13.setVisible(true);
                allGladiators14.setVisible(true);
                allGladiators15.setVisible(true);
                allGladiators21.setVisible(true);
                allGladiators22.setVisible(true);
                allGladiators23.setVisible(true);
                allGladiators24.setVisible(true);
                allGladiators25.setVisible(true);
                allGladiators31.setVisible(true);
                allGladiators32.setVisible(true);
                allGladiators33.setVisible(true);
                allGladiators34.setVisible(true);
                allGladiators35.setVisible(true);
                allGladiators41.setVisible(true);
                allGladiators42.setVisible(true);
                allGladiators43.setVisible(true);
                allGladiators44.setVisible(true);
                allGladiators45.setVisible(true);
                send2.setVisible(false);
                headline.setVisible(false);
                whichGladiator.setVisible(false);
                whichWeapon.setVisible(false);
                whichHand.setVisible(false);
                fightFrame.setVisible(false);
                fightTrue = false;
                fight.setVisible(true);
                treatment.setVisible(true);
                newWeapon.setVisible(true);
                training.setVisible(true);
                gettingGladiator.setVisible(true);
                sinemissione.setVisible(true);
                repair.setVisible(true);
                whichGladiatorRepair.setVisible(false);
                whichHandRepair.setVisible(false);
                whichPlayerIsChoosing = 0;
                gladiatorOne.setVisible(false);
                gladiatorOne1.setVisible(false);
                gladiatorTwo.setVisible(false);
                gladiatorTwo2.setVisible(false);
                gladiatorThree.setVisible(false);
                gladiatorThree3.setVisible(false);
                gladiatorFourth.setVisible(false);
                gladiatorFourth4.setVisible(false);
                gladiatorOne.setBackground(Color.blue);
                gladiatorOne1.setBackground(Color.blue);
                gladiatorTwo.setBackground(Color.blue);
                gladiatorTwo2.setBackground(Color.blue);
                gladiatorThree.setBackground(Color.blue);
                gladiatorThree3.setBackground(Color.blue);
                gladiatorFourth.setBackground(Color.blue);
                gladiatorFourth4.setBackground(Color.blue);
                ROUND();
            }
            whichGladiatorRepair.removeAllItems();
            for (int d = 1; d <= P[whichPlayerIsChoosing + 1].howManyGladiators; d++) {
                whichGladiatorRepair.addItem("gladiátor: " + d);
            }
            beforeYou = 0;
            beforeYou2 = 0;
        }

        //zapas druha cast
        if (mysX > send6.getX() && mysX < send6.getX() + send6.getWidth() && mysY > send6.getY() && mysY < send6.getY() + send6.getHeight() && send6.isVisible()) {

            whichPlayerIsChoosing++;

            if (whichPlayerIsChoosing == 1) {

                String S = (String) whichGladiatorYouChoose.getSelectedItem();
                S = S.replaceAll(" ", "");
                S = S.replaceAll("[^0-9]", "");
                A = (int) Double.parseDouble(S);
                whichGladiatorYouChoose.removeItemAt(whichGladiatorYouChoose.getSelectedIndex());
            }

            if (whichPlayerIsChoosing == 2) {
                String S = (String) whichGladiatorYouChoose.getSelectedItem();
                S = S.replaceAll(" ", "");
                S = S.replaceAll("[^0-9]", "");
                B = (int) Double.parseDouble(S);
            }

            if (whichPlayerIsChoosing == 2) {
                basemant.setBackground(Color.gray);
                labBasemant.setIcon(borduraW);

                counterRound++;
                send5.setVisible(false);
                whichGladiator.setVisible(false);
                whichWeapon.setVisible(false);
                whichHand.setVisible(false);
                fightFrame.setVisible(false);
                fightTrue = false;
                fight.setVisible(true);
                treatment.setVisible(true);
                newWeapon.setVisible(true);
                training.setVisible(true);
                gettingGladiator.setVisible(true);
                sinemissione.setVisible(true);
                repair.setVisible(true);
                whichGladiatorYouChoose.setVisible(false);
                whichGladiatorYouChoose.removeAllItems();
                whichPlayerIsChoosing = 0;
                gladiatorOne.setVisible(false);
                gladiatorOne1.setVisible(false);
                gladiatorTwo.setVisible(false);
                gladiatorTwo2.setVisible(false);
                gladiatorThree.setVisible(false);
                gladiatorThree3.setVisible(false);
                gladiatorFourth.setVisible(false);
                gladiatorFourth4.setVisible(false);
                gladiatorOne.setBackground(Color.blue);
                gladiatorOne1.setBackground(Color.blue);
                gladiatorTwo.setBackground(Color.blue);
                gladiatorTwo2.setBackground(Color.blue);
                gladiatorThree.setBackground(Color.blue);
                gladiatorThree3.setBackground(Color.blue);
                gladiatorFourth.setBackground(Color.blue);
                gladiatorFourth4.setBackground(Color.blue);
                FIGHT(A, B, false);
                A = 0;
                B = 0;
                send6.setVisible(false);
                infoTable.setVisible(false);

            }

        }

        //zapas
        if (mysX > send5.getX() && mysX < send5.getX() + send5.getWidth() && mysY > send5.getY() && mysY < send5.getY() + send5.getHeight() && send5.isVisible()) {

            whichPlayerIsChoosing++;

            if (whichPlayerIsChoosing == 0) {
                basemant.setBackground(Color.blue);
            }
            if (whichPlayerIsChoosing == 1) {
                basemant.setBackground(Color.green);
            }
            if (whichPlayerIsChoosing == 2) {
                basemant.setBackground(Color.yellow);
            }
            if (whichPlayerIsChoosing == 3) {
                basemant.setBackground(Color.orange);
            }
            infoTable.setVisible(false);
            infoTable.setVisible(true);
            headline.setVisible(false);
            headline.setVisible(true);
            send5.setVisible(false);
            send5.setVisible(true);
            whichGladiatorYouChoose.setVisible(false);
            whichGladiatorYouChoose.setVisible(true);

            for (int x = 1; x < whichPlayerIsChoosing; x++) {
                beforeYou = beforeYou + P[x].howManyGladiators;
            }

            if (whichPlayerIsChoosing == 1) {
                gladiatorForFight1 = whichGladiatorYouChoose.getSelectedIndex() + 1 + beforeYou;
            }
            if (whichPlayerIsChoosing == 2) {
                gladiatorForFight2 = whichGladiatorYouChoose.getSelectedIndex() + 1 + beforeYou;
            }
            if (whichPlayerIsChoosing == 3) {
                gladiatorForFight3 = whichGladiatorYouChoose.getSelectedIndex() + 1 + beforeYou;
            }
            if (whichPlayerIsChoosing == 4) {
                gladiatorForFight4 = whichGladiatorYouChoose.getSelectedIndex() + 1 + beforeYou;
            }
            beforeYou = 0;

            whichGladiatorRepair.setSelectedIndex(0);
            whichHandRepair.setSelectedIndex(0);

            if (whichPlayerIsChoosing == numberOfPlayers) {
                labInfoTable.setText("editor vybere dva gladiátory k boji");
                send5.setVisible(false);
                whichGladiator.setVisible(false);
                whichWeapon.setVisible(false);
                whichHand.setVisible(false);
                fightFrame.setVisible(false);
                fightTrue = false;
                whichPlayerIsChoosing = 0;
                gladiatorOne.setVisible(false);
                gladiatorOne1.setVisible(false);
                gladiatorTwo.setVisible(false);
                gladiatorTwo2.setVisible(false);
                gladiatorThree.setVisible(false);
                gladiatorThree3.setVisible(false);
                gladiatorFourth.setVisible(false);
                gladiatorFourth4.setVisible(false);
                gladiatorOne.setBackground(Color.blue);
                gladiatorOne1.setBackground(Color.blue);
                gladiatorTwo.setBackground(Color.blue);
                gladiatorTwo2.setBackground(Color.blue);
                gladiatorThree.setBackground(Color.blue);
                gladiatorThree3.setBackground(Color.blue);
                gladiatorFourth.setBackground(Color.blue);
                gladiatorFourth4.setBackground(Color.blue);

                if (editor == 1) {
                    basemant.setBackground(Color.blue);
                }
                if (editor == 2) {
                    basemant.setBackground(Color.green);
                }
                if (editor == 3) {
                    basemant.setBackground(Color.yellow);
                }
                if (editor == 4) {
                    basemant.setBackground(Color.orange);
                }
                infoTable.setVisible(false);
                infoTable.setVisible(true);
                headline.setVisible(false);
                whichGladiatorYouChoose.setVisible(true);
                send6.setVisible(true);
                whichGladiatorYouChoose.removeAllItems();

                if (gladiatorForFight1 != 0) {
                    whichGladiatorYouChoose.addItem("gladiátor: " + gladiatorForFight1);
                }

                if (gladiatorForFight2 != 0) {
                    whichGladiatorYouChoose.addItem("gladiátor: " + gladiatorForFight2);
                }
                if (gladiatorForFight3 != 0) {
                    whichGladiatorYouChoose.addItem("gladiátor: " + gladiatorForFight3);
                }
                if (gladiatorForFight4 != 0) {
                    whichGladiatorYouChoose.addItem("gladiátor: " + gladiatorForFight4);
                }

            } else {

                if (whichPlayerIsChoosing != numberOfPlayers) {
                    whichGladiatorYouChoose.removeAllItems();
                    for (int d = 1; d <= P[whichPlayerIsChoosing + 1].howManyGladiators; d++) {
                        whichGladiatorYouChoose.addItem("gladiátor: " + d);
                    }
                }
            }

        }

        //leceni
        if (treatmentFrame.isVisible()) {

            if (mysX > send4.getX() && mysX < send4.getX() + send4.getWidth() && mysY > send4.getY() && mysY < send4.getY() + send4.getHeight() && send4.isVisible() && counterForTreating == false) {
                counterForTreating = true;
                gladiatorTreatment++;
                if (G[gladiatorTreatment].owener == 1) {
                    basemant.setBackground(Color.blue);
                }
                if (G[gladiatorTreatment].owener == 2) {
                    basemant.setBackground(Color.green);
                }
                if (G[gladiatorTreatment].owener == 3) {
                    basemant.setBackground(Color.yellow);
                }
                if (G[gladiatorTreatment].owener == 4) {
                    basemant.setBackground(Color.orange);
                }
                headline.setVisible(false);
                headline.setVisible(true);
                treatmentFrame.setVisible(false);
                treating.setVisible(false);
                treating2.setVisible(false);

                send4.setVisible(false);
                treatmentFrame.setVisible(true);
                treating.setVisible(true);
                treating2.setVisible(true);

                send4.setVisible(true);
                labTreating.setText("<html>" + "gladiátor hráče: " + G[gladiatorTreatment].owener + "<br>" + "číslo: " + gladiatorTreatment + "<br>" + "má tolik životů: " + G[gladiatorTreatment].lives);
                if (G[gladiatorTreatment].lives == 20) {
                    counterForTreating = false;
                }
            }

            if ((mysX > treating.getX() && mysX < treating.getX() + treating.getWidth() && mysY > treating.getY() && mysY < treating.getY() + treating.getHeight() && treating.isVisible() && counterForTreating == true) || (mysX > treating2.getX() && mysX < treating2.getX() + treating2.getWidth() && mysY > treating2.getY() && mysY < treating2.getY() + treating2.getHeight() && treating2.isVisible() && counterForTreating == true)) {

                if (G[gladiatorTreatment].lives == 20) {
                    counterForTreating = false;
                }

                labTreating.setText("<html>" + "gladiátor hráče: " + G[gladiatorTreatment].owener + "<br>" + "číslo: " + gladiatorTreatment + "<br>" + "má tolik životů: " + G[gladiatorTreatment].lives);

                if (G[gladiatorTreatment].lives > 6) {
                    G[gladiatorTreatment].lives = G[gladiatorTreatment].lives + 4;
                    if (G[gladiatorTreatment].lives > 20) {
                        G[gladiatorTreatment].lives = 20;
                    }
                }
                if (G[gladiatorTreatment].lives <= 6) {
                    int random = RND.nextInt(6);
                    if (random + 1 < G[gladiatorTreatment].lives) {
                        G[gladiatorTreatment].lives = G[gladiatorTreatment].lives + 2;
                    } else {
                        G[gladiatorTreatment].lives--;
                    }
                }
                labTreating.setText("<html>" + "gladiátor hráče: " + G[gladiatorTreatment].owener + "<br>" + "číslo: " + gladiatorTreatment + "<br>" + "má tolik životů: " + G[gladiatorTreatment].lives);

                counterForTreating = false;
            }
            if (gladiatorTreatment > numberOfGladiators) {
                basemant.setBackground(Color.gray);
                counterRound++;
                allGladiators11.setVisible(true);
                allGladiators12.setVisible(true);
                allGladiators13.setVisible(true);
                allGladiators14.setVisible(true);
                allGladiators15.setVisible(true);
                allGladiators21.setVisible(true);
                allGladiators22.setVisible(true);
                allGladiators23.setVisible(true);
                allGladiators24.setVisible(true);
                allGladiators25.setVisible(true);
                allGladiators31.setVisible(true);
                allGladiators32.setVisible(true);
                allGladiators33.setVisible(true);
                allGladiators34.setVisible(true);
                allGladiators35.setVisible(true);
                allGladiators41.setVisible(true);
                allGladiators42.setVisible(true);
                allGladiators43.setVisible(true);
                allGladiators44.setVisible(true);
                allGladiators45.setVisible(true);
                treatmentFrame.setVisible(false);
                treating.setVisible(false);
                treating2.setVisible(false);
                headline.setVisible(false);
                fight.setVisible(true);
                treatment.setVisible(true);
                repair.setVisible(true);
                newWeapon.setVisible(true);
                training.setVisible(true);
                gettingGladiator.setVisible(true);
                sinemissione.setVisible(true);
                gladiatorTreatment = 1;
                send4.setVisible(false);
                ROUND();
            }

            gladiatorOne.setBackground(Color.blue);
            gladiatorOne1.setBackground(Color.blue);
            gladiatorTwo.setBackground(Color.blue);
            gladiatorTwo2.setBackground(Color.blue);
            gladiatorThree.setBackground(Color.blue);
            gladiatorThree3.setBackground(Color.blue);
            gladiatorFourth.setBackground(Color.blue);
            gladiatorFourth4.setBackground(Color.blue);

            labTreating.setText("<html>" + "gladiátor hráče: " + G[gladiatorTreatment].owener + "<br>" + "číslo: " + gladiatorTreatment + "<br>" + "má tolik životů: " + G[gladiatorTreatment].lives);

        }

        //sinemissione
        if (mysX > send7.getX() && mysX < send7.getX() + send7.getWidth() && mysY > send7.getY() && mysY < send7.getY() + send7.getHeight() && send7.isVisible()) {

            basemant.setBackground(Color.gray);
            labBasemant.setIcon(borduraW);

            send7.setVisible(false);
            whichGladiatorYouChoose.setVisible(false);
            headline.setVisible(false);
            infoTable.setVisible(false);

            send7.setVisible(true);
            whichGladiatorYouChoose.setVisible(true);
            headline.setVisible(true);
            infoTable.setVisible(true);

            String S = (String) whichGladiatorYouChoose.getSelectedItem();
            S = S.replaceAll(" ", "");
            S = S.replaceAll("[^0-9]", "");
            A = (int) Double.parseDouble(S);
            whichGladiatorYouChoose.removeItemAt(whichGladiatorYouChoose.getSelectedIndex());
            B = RND.nextInt(numberOfGladiators) + 1;
            while (A == B || G[A].owener == G[B].owener) {
                B = RND.nextInt(numberOfGladiators) + 1;
            }

            FIGHT(A, B, true);
            A = 0;
            B = 0;

            send7.setVisible(false);
            whichGladiatorYouChoose.removeAllItems();
            whichPlayerIsChoosing = 0;
            whichGladiatorYouChoose.setVisible(false);
            counterRound++;
            headline.setVisible(false);
            infoTable.setVisible(false);
        }

        if (mysX > repair.getX() && mysX < repair.getX() + repair.getWidth() && mysY > repair.getY() && mysY < repair.getY() + repair.getHeight() && repair.isVisible()) {

            REPAIR();

        }

        if (mysX > fight.getX() && mysX < fight.getX() + fight.getWidth() && mysY > fight.getY() && mysY < fight.getY() + fight.getHeight() && fight.isVisible()) {

            FIGHT2();

        }

        if (mysX > treatment.getX() && mysX < treatment.getX() + treatment.getWidth() && mysY > treatment.getY() && mysY < treatment.getY() + treatment.getHeight() && treatment.isVisible()) {

            TREATMENT();

        }

        if (mysX > newWeapon.getX() && mysX < newWeapon.getX() + newWeapon.getWidth() && mysY > newWeapon.getY() && mysY < newWeapon.getY() + newWeapon.getHeight() && newWeapon.isVisible()) {

            NEWWEAPON();

        }

        if (mysX > training.getX() && mysX < training.getX() + training.getWidth() && mysY > training.getY() && mysY < training.getY() + training.getHeight() && training.isVisible()) {

            TRAINING();

        }

        if (mysX > gettingGladiator.getX() && mysX < gettingGladiator.getX() + gettingGladiator.getWidth() && mysY > gettingGladiator.getY() && mysY < gettingGladiator.getY() + gettingGladiator.getHeight() && gettingGladiator.isVisible()) {

            GETTINGGLADIATOR();

        }
        if (mysX > sinemissione.getX() && mysX < sinemissione.getX() + sinemissione.getWidth() && mysY > sinemissione.getY() && mysY < sinemissione.getY() + sinemissione.getHeight() && sinemissione.isVisible()) {

            SINEMISSIONE();

        }

    }

    //zde se urcuje jaka aktivita se zrovna dela
    public static void ROUND() {
        if (counterRound == 0) {
            fight.setVisible(false);
            treatment.setVisible(true);
            repair.setVisible(false);
            newWeapon.setVisible(false);
            training.setVisible(false);
            gettingGladiator.setVisible(false);
            sinemissione.setVisible(false);
        }
        if (counterRound == 1) {
            fight.setVisible(false);
            treatment.setVisible(false);
            repair.setVisible(false);
            newWeapon.setVisible(true);
            training.setVisible(false);
            gettingGladiator.setVisible(false);
            sinemissione.setVisible(false);
        }
        if (counterRound == 2) {
            fight.setVisible(false);
            treatment.setVisible(false);
            repair.setVisible(false);
            newWeapon.setVisible(false);
            training.setVisible(true);
            gettingGladiator.setVisible(false);
            sinemissione.setVisible(false);
        }
        if (counterRound == 3) {
            fight.setVisible(true);
            treatment.setVisible(false);
            repair.setVisible(false);
            newWeapon.setVisible(false);
            training.setVisible(false);
            gettingGladiator.setVisible(false);
            sinemissione.setVisible(false);
        }
        if (counterRound == 4) {
            fight.setVisible(false);
            treatment.setVisible(true);
            repair.setVisible(false);
            newWeapon.setVisible(false);
            training.setVisible(false);
            gettingGladiator.setVisible(false);
            sinemissione.setVisible(false);
        }
        if (counterRound == 5) {
            fight.setVisible(false);
            treatment.setVisible(false);
            repair.setVisible(true);
            newWeapon.setVisible(false);
            training.setVisible(false);
            gettingGladiator.setVisible(false);
            sinemissione.setVisible(false);
        }
        if (counterRound == 6) {
            fight.setVisible(false);
            treatment.setVisible(false);
            repair.setVisible(false);
            newWeapon.setVisible(false);
            training.setVisible(true);
            gettingGladiator.setVisible(false);
            sinemissione.setVisible(false);
        }
        if (counterRound == 7) {
            fight.setVisible(true);
            treatment.setVisible(false);
            repair.setVisible(false);
            newWeapon.setVisible(false);
            training.setVisible(false);
            gettingGladiator.setVisible(false);
            sinemissione.setVisible(false);
        }
        if (counterRound == 8) {
            fight.setVisible(false);
            treatment.setVisible(true);
            repair.setVisible(false);
            newWeapon.setVisible(false);
            training.setVisible(false);
            gettingGladiator.setVisible(false);
            sinemissione.setVisible(false);
        }
        if (counterRound == 9) {
            fight.setVisible(false);
            treatment.setVisible(false);
            repair.setVisible(false);
            newWeapon.setVisible(true);
            training.setVisible(false);
            gettingGladiator.setVisible(false);
            sinemissione.setVisible(false);
        }
        if (counterRound == 10) {
            fight.setVisible(false);
            treatment.setVisible(false);
            repair.setVisible(false);
            newWeapon.setVisible(false);
            training.setVisible(true);
            gettingGladiator.setVisible(false);
            sinemissione.setVisible(false);
        }
        if (counterRound == 11) {
            fight.setVisible(false);
            treatment.setVisible(false);
            repair.setVisible(false);
            newWeapon.setVisible(false);
            training.setVisible(false);
            gettingGladiator.setVisible(false);
            sinemissione.setVisible(true);
        }
        if (counterRound == 12) {
            fight.setVisible(false);
            treatment.setVisible(false);
            repair.setVisible(false);
            newWeapon.setVisible(false);
            training.setVisible(false);
            gettingGladiator.setVisible(true);
            sinemissione.setVisible(false);
        }
    }

    //pripravi vse pro trenovani sinemissione
    public static void SINEMISSIONE() {

        if (editor == 1) {
            basemant.setBackground(Color.blue);
        }
        if (editor == 2) {
            basemant.setBackground(Color.green);
        }
        if (editor == 3) {
            basemant.setBackground(Color.yellow);
        }
        if (editor == 4) {
            basemant.setBackground(Color.orange);
        }
        headline.setVisible(true);
        labHeadline.setText("sinemissione výběr gladiátorů");
        allGladiators11.setVisible(false);
        allGladiators12.setVisible(false);
        allGladiators13.setVisible(false);
        allGladiators14.setVisible(false);
        allGladiators15.setVisible(false);
        allGladiators21.setVisible(false);
        allGladiators22.setVisible(false);
        allGladiators23.setVisible(false);
        allGladiators24.setVisible(false);
        allGladiators25.setVisible(false);
        allGladiators31.setVisible(false);
        allGladiators32.setVisible(false);
        allGladiators33.setVisible(false);
        allGladiators34.setVisible(false);
        allGladiators35.setVisible(false);
        allGladiators41.setVisible(false);
        allGladiators42.setVisible(false);
        allGladiators43.setVisible(false);
        allGladiators44.setVisible(false);
        allGladiators45.setVisible(false);
        fight.setVisible(false);
        treatment.setVisible(false);
        newWeapon.setVisible(false);
        repair.setVisible(false);
        training.setVisible(false);
        gettingGladiator.setVisible(false);
        sinemissione.setVisible(false);
        send7.setVisible(true);
        whichGladiatorYouChoose.setVisible(true);
        infoTable.setVisible(true);
        labInfoTable.setText("editor (v barve) vybere jaké gladiátory dá k boji");
        whichGladiatorYouChoose.removeAllItems();
        int t = 0;
        for (int d = 1; d <= numberOfGladiators; d++) {
            if (G[d].level > t) {
                t = G[d].level;

            }
        }
        for (int d = 1; d <= numberOfGladiators; d++) {
            if (G[d].level == t) {
                whichGladiatorYouChoose.addItem("gladiátor: " + d);
            }
        }
    }

    //pripravi vse pro fight
    public static void FIGHT2() {

        basemant.setBackground(Color.blue);
        headline.setVisible(true);
        labHeadline.setText("zápas výběr gladiátorů");
        allGladiators11.setVisible(false);
        allGladiators12.setVisible(false);
        allGladiators13.setVisible(false);
        allGladiators14.setVisible(false);
        allGladiators15.setVisible(false);
        allGladiators21.setVisible(false);
        allGladiators22.setVisible(false);
        allGladiators23.setVisible(false);
        allGladiators24.setVisible(false);
        allGladiators25.setVisible(false);
        allGladiators31.setVisible(false);
        allGladiators32.setVisible(false);
        allGladiators33.setVisible(false);
        allGladiators34.setVisible(false);
        allGladiators35.setVisible(false);
        allGladiators41.setVisible(false);
        allGladiators42.setVisible(false);
        allGladiators43.setVisible(false);
        allGladiators44.setVisible(false);
        allGladiators45.setVisible(false);
        infoTable.setVisible(true);
        labInfoTable.setText("zde vybre hráč dané barvy jaké gladiátory dá k boji");
        fight.setVisible(false);
        treatment.setVisible(false);
        newWeapon.setVisible(false);
        repair.setVisible(false);
        training.setVisible(false);
        gettingGladiator.setVisible(false);
        sinemissione.setVisible(false);
        send5.setVisible(true);
        labBasemant.setIcon(borduraB);
        whichGladiatorYouChoose.setVisible(true);
        whichGladiatorYouChoose.removeAllItems();
        for (int d = 1; d <= P[whichPlayerIsChoosing + 1].howManyGladiators; d++) {
            whichGladiatorYouChoose.addItem("gladiátor: " + d);
        }
    }

    //pripravi vse pro opravovani zbrani
    public static void REPAIR() {

        if (whichPlayerIsChoosing == 0) {
            basemant.setBackground(Color.blue);
        }
        headline.setVisible(true);
        labHeadline.setText("oprava zbraní");
        labBasemant.setIcon(borduraB);
        allGladiators11.setVisible(false);
        allGladiators12.setVisible(false);
        allGladiators13.setVisible(false);
        allGladiators14.setVisible(false);
        allGladiators15.setVisible(false);
        allGladiators21.setVisible(false);
        allGladiators22.setVisible(false);
        allGladiators23.setVisible(false);
        allGladiators24.setVisible(false);
        allGladiators25.setVisible(false);
        allGladiators31.setVisible(false);
        allGladiators32.setVisible(false);
        allGladiators33.setVisible(false);
        allGladiators34.setVisible(false);
        allGladiators35.setVisible(false);
        allGladiators41.setVisible(false);
        allGladiators42.setVisible(false);
        allGladiators43.setVisible(false);
        allGladiators44.setVisible(false);
        allGladiators45.setVisible(false);
        fight.setVisible(false);
        treatment.setVisible(false);
        newWeapon.setVisible(false);
        repair.setVisible(false);
        whichGladiatorRepair.setVisible(true);
        whichHandRepair.setVisible(true);
        send2.setVisible(true);
        training.setVisible(false);
        gettingGladiator.setVisible(false);
        sinemissione.setVisible(false);
        whichGladiatorRepair.removeAllItems();
        gladiatorOne.setVisible(true);
        gladiatorOne1.setVisible(true);
        gladiatorTwo.setVisible(true);
        gladiatorTwo2.setVisible(true);
        gladiatorThree.setVisible(true);
        gladiatorThree3.setVisible(true);
        gladiatorFourth.setVisible(true);
        gladiatorFourth4.setVisible(true);

        for (int d = 1; d <= P[whichPlayerIsChoosing + 1].howManyGladiators; d++) {
            whichGladiatorRepair.addItem("gladiátor: " + d);
        }

        int a1 = 1;
        int a2 = 2;
        int a3 = 3;
        int a4 = 4;

        if (P[1].howManyGladiators > 0) {
            gladiatorOne.setBackground(Color.pink);
            gladiatorOne1.setBackground(Color.pink);
            labGladiatorOne1.setText("<html>" + G[a1].name);
            labGladiatorOne.setText("<html>" + "zbraň jedna: " + W[G[a1].weaponOne][a1][1].NameWeapon + "<br>" + "Power: "
                    + W[G[a1].weaponOne][a1][1].power + "<br>" + "Dexterity: " + W[G[a1].weaponOne][a1][1].dexterity
                    + "<br>" + "Shield: " + W[G[a1].weaponOne][a1][1].shield + "<br>" + "Harm: " + W[G[a1].weaponOne][a1][1].harm
                    + "<br>" + "<br>" + "zbraň dva: " + W[G[a1].weaponTwo][a1][2].NameWeapon + "<br>" + "Power: "
                    + W[G[a1].weaponTwo][a1][2].power + "<br>" + "Dexterity: " + W[G[a1].weaponTwo][a1][2].dexterity
                    + "<br>" + "Shield: " + W[G[a1].weaponTwo][a1][2].shield + "<br>" + "Harm: " + W[G[a1].weaponTwo][a1][2].harm
                    + "<br>" + "<br>" + "zbraň tři: " + W[G[a1].weaponThree][a1][3].NameWeapon + "<br>" + "Power: "
                    + W[G[a1].weaponThree][a1][3].power + "<br>" + "Dexterity: " + W[G[a1].weaponThree][a1][3].dexterity
                    + "<br>" + "Shield: " + W[G[a1].weaponThree][a1][3].shield + "<br>" + "Harm: " + W[G[a1].weaponThree][a1][3].harm);
        }
        if (P[1].howManyGladiators > 1) {
            gladiatorTwo.setBackground(Color.pink);
            gladiatorTwo2.setBackground(Color.pink);
            labGladiatorTwo2.setText("<html>" + G[a2].name);
            labGladiatorTwo.setText("<html>" + "zbraň jedna: " + W[G[a2].weaponOne][a2][1].NameWeapon + "<br>" + "Power: "
                    + W[G[a2].weaponOne][a2][1].power + "<br>" + "Dexterity: " + W[G[a2].weaponOne][a2][1].dexterity
                    + "<br>" + "Shield: " + W[G[a2].weaponOne][a2][1].shield + "<br>" + "Harm: " + W[G[a2].weaponOne][a2][1].harm
                    + "<br>" + "<br>" + "zbraň dva: " + W[G[a2].weaponTwo][a2][2].NameWeapon + "<br>" + "Power: "
                    + W[G[a2].weaponTwo][a2][2].power + "<br>" + "Dexterity: " + W[G[a2].weaponTwo][a2][2].dexterity
                    + "<br>" + "Shield: " + W[G[a2].weaponTwo][a2][2].shield + "<br>" + "Harm: " + W[G[a2].weaponTwo][a2][2].harm
                    + "<br>" + "<br>" + "zbraň tři: " + W[G[a2].weaponThree][a2][3].NameWeapon + "<br>" + "Power: "
                    + W[G[a2].weaponThree][a2][3].power + "<br>" + "Dexterity: " + W[G[a2].weaponThree][a2][3].dexterity
                    + "<br>" + "Shield: " + W[G[a2].weaponThree][a2][3].shield + "<br>" + "Harm: " + W[G[a2].weaponThree][a2][3].harm);
        }
        if (P[1].howManyGladiators > 2) {
            gladiatorThree.setBackground(Color.pink);
            gladiatorThree3.setBackground(Color.pink);
            labGladiatorThree3.setText("<html>" + G[a3].name);
            labGladiatorThree.setText("<html>" + "zbraň jedna: " + W[G[a3].weaponOne][a3][1].NameWeapon + "<br>" + "Power: "
                    + W[G[a3].weaponOne][a3][1].power + "<br>" + "Dexterity: " + W[G[a3].weaponOne][a3][1].dexterity
                    + "<br>" + "Shield: " + W[G[a3].weaponOne][a3][1].shield + "<br>" + "Harm: " + W[G[a3].weaponOne][a3][1].harm
                    + "<br>" + "<br>" + "zbraň dva: " + W[G[a3].weaponTwo][a3][2].NameWeapon + "<br>" + "Power: "
                    + W[G[a3].weaponTwo][a3][2].power + "<br>" + "Dexterity: " + W[G[a3].weaponTwo][a3][2].dexterity
                    + "<br>" + "Shield: " + W[G[a3].weaponTwo][a3][2].shield + "<br>" + "Harm: " + W[G[a3].weaponTwo][a3][2].harm
                    + "<br>" + "<br>" + "zbraň tři: " + W[G[a3].weaponThree][a3][3].NameWeapon + "<br>" + "Power: "
                    + W[G[a3].weaponThree][a3][3].power + "<br>" + "Dexterity: " + W[G[a3].weaponThree][a3][3].dexterity
                    + "<br>" + "Shield: " + W[G[a3].weaponThree][a3][3].shield + "<br>" + "Harm: " + W[G[a3].weaponThree][a3][3].harm);
        }
        if (P[1].howManyGladiators > 3) {
            gladiatorFourth.setBackground(Color.pink);
            gladiatorFourth4.setBackground(Color.pink);
            labGladiatorFourth4.setText("<html>" + G[a4].name);
            labGladiatorFourth.setText("<html>" + "zbraň jedna: " + W[G[a4].weaponOne][a4][1].NameWeapon + "<br>" + "Power: "
                    + W[G[a4].weaponOne][a4][1].power + "<br>" + "Dexterity: " + W[G[a4].weaponOne][a4][1].dexterity
                    + "<br>" + "Shield: " + W[G[a4].weaponOne][a4][1].shield + "<br>" + "Harm: " + W[G[a4].weaponOne][a4][1].harm
                    + "<br>" + "<br>" + "zbraň dva: " + W[G[a4].weaponTwo][a4][2].NameWeapon + "<br>" + "Power: "
                    + W[G[a4].weaponTwo][a4][2].power + "<br>" + "Dexterity: " + W[G[a4].weaponTwo][a4][2].dexterity
                    + "<br>" + "Shield: " + W[G[a4].weaponTwo][a4][2].shield + "<br>" + "Harm: " + W[G[a4].weaponTwo][a4][2].harm
                    + "<br>" + "<br>" + "zbraň tři: " + W[G[a4].weaponThree][a4][3].NameWeapon + "<br>" + "Power: "
                    + W[G[a4].weaponThree][a4][3].power + "<br>" + "Dexterity: " + W[G[a4].weaponThree][a4][3].dexterity
                    + "<br>" + "Shield: " + W[G[a4].weaponThree][a4][3].shield + "<br>" + "Harm: " + W[G[a4].weaponThree][a4][3].harm);
        }
        if (gladiatorOne.getBackground() != Color.pink) {
            gladiatorOne.setVisible(false);
            gladiatorOne1.setVisible(false);
        }
        if (gladiatorTwo.getBackground() != Color.pink) {
            gladiatorTwo.setVisible(false);
            gladiatorTwo2.setVisible(false);
        }
        if (gladiatorThree.getBackground() != Color.pink) {
            gladiatorThree.setVisible(false);
            gladiatorThree3.setVisible(false);
        }
        if (gladiatorFourth.getBackground() != Color.pink) {
            gladiatorFourth.setVisible(false);
            gladiatorFourth4.setVisible(false);
        }

    }

    //pripravi vse pro leceni
    public static void TREATMENT() {

        if (G[gladiatorTreatment].owener == 1) {
            basemant.setBackground(Color.blue);
        }
        headline.setVisible(true);
        labHeadline.setText("léčení gladiátorů");
        allGladiators11.setVisible(false);
        allGladiators12.setVisible(false);
        allGladiators13.setVisible(false);
        allGladiators14.setVisible(false);
        allGladiators15.setVisible(false);
        allGladiators21.setVisible(false);
        allGladiators22.setVisible(false);
        allGladiators23.setVisible(false);
        allGladiators24.setVisible(false);
        allGladiators25.setVisible(false);
        allGladiators31.setVisible(false);
        allGladiators32.setVisible(false);
        allGladiators33.setVisible(false);
        allGladiators34.setVisible(false);
        allGladiators35.setVisible(false);
        allGladiators41.setVisible(false);
        allGladiators42.setVisible(false);
        allGladiators43.setVisible(false);
        allGladiators44.setVisible(false);
        allGladiators45.setVisible(false);
        fight.setVisible(false);
        treatment.setVisible(false);
        treatmentFrame.setVisible(true);
        treating.setVisible(true);
        treating2.setVisible(true);
        newWeapon.setVisible(false);
        repair.setVisible(false);
        training.setVisible(false);
        gettingGladiator.setVisible(false);
        sinemissione.setVisible(false);

        send4.setVisible(true);
        labTreating.setText("<html>" + "gladiátor hráče: " + G[gladiatorTreatment].owener + "<br>" + "číslo: " + gladiatorTreatment + "<br>" + "má tolik životů: " + G[gladiatorTreatment].lives);
        if (G[1].lives == 20) {
            counterForTreating = false;
        }
    }

    //pripravi vse pro brani zbrani
    public static void NEWWEAPON() {

        if (whichPlayerIsChoosing == 0) {
            basemant.setBackground(Color.blue);
        }
        headline.setVisible(true);
        labHeadline.setText("braní zbraní");
        allGladiators11.setVisible(false);
        allGladiators12.setVisible(false);
        allGladiators13.setVisible(false);
        allGladiators14.setVisible(false);
        allGladiators15.setVisible(false);
        allGladiators21.setVisible(false);
        allGladiators22.setVisible(false);
        allGladiators23.setVisible(false);
        allGladiators24.setVisible(false);
        allGladiators25.setVisible(false);
        allGladiators31.setVisible(false);
        allGladiators32.setVisible(false);
        allGladiators33.setVisible(false);
        allGladiators34.setVisible(false);
        allGladiators35.setVisible(false);
        allGladiators41.setVisible(false);
        allGladiators42.setVisible(false);
        allGladiators43.setVisible(false);
        allGladiators44.setVisible(false);
        allGladiators45.setVisible(false);
        fight.setVisible(false);
        treatment.setVisible(false);
        treating.setVisible(false);
        treating2.setVisible(false);
        newWeapon.setVisible(false);
        whichGladiator.setVisible(true);
        whichWeapon.setVisible(true);
        whichHand.setVisible(true);
        send.setVisible(true);
        repair.setVisible(false);
        training.setVisible(false);
        gettingGladiator.setVisible(false);
        sinemissione.setVisible(false);
        gladiatorOne.setVisible(true);
        gladiatorOne1.setVisible(true);
        gladiatorTwo.setVisible(true);
        gladiatorTwo2.setVisible(true);
        gladiatorThree.setVisible(true);
        gladiatorThree3.setVisible(true);
        gladiatorFourth.setVisible(true);
        gladiatorFourth4.setVisible(true);
        whichGladiator.removeAllItems();
        for (int d = 1; d <= P[whichPlayerIsChoosing + 1].howManyGladiators; d++) {
            whichGladiator.addItem("gladiátor: " + d);
        }

        int a1 = 1;
        int a2 = 2;
        int a3 = 3;
        int a4 = 4;

        //zobrazi jen ty gladitory ktere ma hrac
        if (P[1].howManyGladiators > 0) {
            gladiatorOne.setBackground(Color.pink);
            gladiatorOne1.setBackground(Color.pink);
            labGladiatorOne1.setText("<html>" + G[a1].name);
            labGladiatorOne.setText("<html>" + "zbraň jedna: " + W[G[a1].weaponOne][a1][1].NameWeapon + "<br>" + "Power: "
                    + W[G[a1].weaponOne][a1][1].power + "<br>" + "Dexterity: " + W[G[a1].weaponOne][a1][1].dexterity
                    + "<br>" + "Shield: " + W[G[a1].weaponOne][a1][1].shield + "<br>" + "Harm: " + W[G[a1].weaponOne][a1][1].harm
                    + "<br>" + "<br>" + "zbraň dva: " + W[G[a1].weaponTwo][a1][2].NameWeapon + "<br>" + "Power: "
                    + W[G[a1].weaponTwo][a1][2].power + "<br>" + "Dexterity: " + W[G[a1].weaponTwo][a1][2].dexterity
                    + "<br>" + "Shield: " + W[G[a1].weaponTwo][a1][2].shield + "<br>" + "Harm: " + W[G[a1].weaponTwo][a1][2].harm
                    + "<br>" + "<br>" + "zbraň tři: " + W[G[a1].weaponThree][a1][3].NameWeapon + "<br>" + "Power: "
                    + W[G[a1].weaponThree][a1][3].power + "<br>" + "Dexterity: " + W[G[a1].weaponThree][a1][3].dexterity
                    + "<br>" + "Shield: " + W[G[a1].weaponThree][a1][3].shield + "<br>" + "Harm: " + W[G[a1].weaponThree][a1][3].harm);
        }
        if (P[1].howManyGladiators > 1) {
            gladiatorTwo.setBackground(Color.pink);
            gladiatorTwo2.setBackground(Color.pink);
            labGladiatorTwo2.setText("<html>" + G[a2].name);
            labGladiatorTwo.setText("<html>" + "zbraň jedna: " + W[G[a2].weaponOne][a2][1].NameWeapon + "<br>" + "Power: "
                    + W[G[a2].weaponOne][a2][1].power + "<br>" + "Dexterity: " + W[G[a2].weaponOne][a2][1].dexterity
                    + "<br>" + "Shield: " + W[G[a2].weaponOne][a2][1].shield + "<br>" + "Harm: " + W[G[a2].weaponOne][a2][1].harm
                    + "<br>" + "<br>" + "zbraň dva: " + W[G[a2].weaponTwo][a2][2].NameWeapon + "<br>" + "Power: "
                    + W[G[a2].weaponTwo][a2][2].power + "<br>" + "Dexterity: " + W[G[a2].weaponTwo][a2][2].dexterity
                    + "<br>" + "Shield: " + W[G[a2].weaponTwo][a2][2].shield + "<br>" + "Harm: " + W[G[a2].weaponTwo][a2][2].harm
                    + "<br>" + "<br>" + "zbraň tři: " + W[G[a2].weaponThree][a2][3].NameWeapon + "<br>" + "Power: "
                    + W[G[a2].weaponThree][a2][3].power + "<br>" + "Dexterity: " + W[G[a2].weaponThree][a2][3].dexterity
                    + "<br>" + "Shield: " + W[G[a2].weaponThree][a2][3].shield + "<br>" + "Harm: " + W[G[a2].weaponThree][a2][3].harm);
        }
        if (P[1].howManyGladiators > 2) {
            gladiatorThree.setBackground(Color.pink);
            gladiatorThree3.setBackground(Color.pink);
            labGladiatorThree3.setText("<html>" + G[a3].name);
            labGladiatorThree.setText("<html>" + "zbraň jedna: " + W[G[a3].weaponOne][a3][1].NameWeapon + "<br>" + "Power: "
                    + W[G[a3].weaponOne][a3][1].power + "<br>" + "Dexterity: " + W[G[a3].weaponOne][a3][1].dexterity
                    + "<br>" + "Shield: " + W[G[a3].weaponOne][a3][1].shield + "<br>" + "Harm: " + W[G[a3].weaponOne][a3][1].harm
                    + "<br>" + "<br>" + "zbraň dva: " + W[G[a3].weaponTwo][a3][2].NameWeapon + "<br>" + "Power: "
                    + W[G[a3].weaponTwo][a3][2].power + "<br>" + "Dexterity: " + W[G[a3].weaponTwo][a3][2].dexterity
                    + "<br>" + "Shield: " + W[G[a3].weaponTwo][a3][2].shield + "<br>" + "Harm: " + W[G[a3].weaponTwo][a3][2].harm
                    + "<br>" + "<br>" + "zbraň tři: " + W[G[a3].weaponThree][a3][3].NameWeapon + "<br>" + "Power: "
                    + W[G[a3].weaponThree][a3][3].power + "<br>" + "Dexterity: " + W[G[a3].weaponThree][a3][3].dexterity
                    + "<br>" + "Shield: " + W[G[a3].weaponThree][a3][3].shield + "<br>" + "Harm: " + W[G[a3].weaponThree][a3][3].harm);
        }
        if (P[1].howManyGladiators > 3) {
            gladiatorFourth.setBackground(Color.pink);
            gladiatorFourth4.setBackground(Color.pink);
            labGladiatorFourth4.setText("<html>" + G[a4].name);
            labGladiatorFourth.setText("<html>" + "zbraň jedna: " + W[G[a4].weaponOne][a4][1].NameWeapon + "<br>" + "Power: "
                    + W[G[a4].weaponOne][a4][1].power + "<br>" + "Dexterity: " + W[G[a4].weaponOne][a4][1].dexterity
                    + "<br>" + "Shield: " + W[G[a4].weaponOne][a4][1].shield + "<br>" + "Harm: " + W[G[a4].weaponOne][a4][1].harm
                    + "<br>" + "<br>" + "zbraň dva: " + W[G[a4].weaponTwo][a4][2].NameWeapon + "<br>" + "Power: "
                    + W[G[a4].weaponTwo][a4][2].power + "<br>" + "Dexterity: " + W[G[a4].weaponTwo][a4][2].dexterity
                    + "<br>" + "Shield: " + W[G[a4].weaponTwo][a4][2].shield + "<br>" + "Harm: " + W[G[a4].weaponTwo][a4][2].harm
                    + "<br>" + "<br>" + "zbraň tři: " + W[G[a4].weaponThree][a4][3].NameWeapon + "<br>" + "Power: "
                    + W[G[a4].weaponThree][a4][3].power + "<br>" + "Dexterity: " + W[G[a4].weaponThree][a4][3].dexterity
                    + "<br>" + "Shield: " + W[G[a4].weaponThree][a4][3].shield + "<br>" + "Harm: " + W[G[a4].weaponThree][a4][3].harm);
        }

        if (gladiatorOne.getBackground() != Color.pink) {
            gladiatorOne.setVisible(false);
            gladiatorOne1.setVisible(false);

        }
        if (gladiatorTwo.getBackground() != Color.pink) {
            gladiatorTwo.setVisible(false);
            gladiatorTwo2.setVisible(false);
        }
        if (gladiatorThree.getBackground() != Color.pink) {
            gladiatorThree.setVisible(false);
            gladiatorThree3.setVisible(false);
        }
        if (gladiatorFourth.getBackground() != Color.pink) {
            gladiatorFourth.setVisible(false);
            gladiatorFourth4.setVisible(false);
        }
    }

    //pripravi vse pro trenovani gladiatoru
    public static void TRAINING() {

        if (G[whichPlayerIsChoosing + 1].owener == 1) {
            basemant.setBackground(Color.blue);
        }
        headline.setVisible(true);
        labHeadline.setText("trénování");
        allGladiators11.setVisible(false);
        allGladiators12.setVisible(false);
        allGladiators13.setVisible(false);
        allGladiators14.setVisible(false);
        allGladiators15.setVisible(false);
        allGladiators21.setVisible(false);
        allGladiators22.setVisible(false);
        allGladiators23.setVisible(false);
        allGladiators24.setVisible(false);
        allGladiators25.setVisible(false);
        allGladiators31.setVisible(false);
        allGladiators32.setVisible(false);
        allGladiators33.setVisible(false);
        allGladiators34.setVisible(false);
        allGladiators35.setVisible(false);
        allGladiators41.setVisible(false);
        allGladiators42.setVisible(false);
        allGladiators43.setVisible(false);
        allGladiators44.setVisible(false);
        allGladiators45.setVisible(false);
        fight.setVisible(false);
        treatment.setVisible(false);
        treating.setVisible(false);
        treating2.setVisible(false);
        newWeapon.setVisible(false);
        training.setVisible(false);
        gettingGladiator.setVisible(false);
        sinemissione.setVisible(false);
        send3.setVisible(true);
        repair.setVisible(false);
        whichHand.setVisible(true);
        whichHand.setLocation(100, 75);
        gettingGladiator.setVisible(false);
        gladiatorOne.setVisible(true);
        gladiatorOne1.setVisible(true);
        gladiatorOne.setBackground(Color.pink);
        gladiatorOne1.setBackground(Color.pink);
        labGladiatorOne1.setText("<html>" + G[1].name);
        labGladiatorOne.setText("<html>" + "zbraň jedna: " + W[G[1].weaponOne][1][1].NameWeapon + "<br>" + "Power: "
                + W[G[1].weaponOne][1][1].power + "<br>" + "Dexterity: " + W[G[1].weaponOne][1][1].dexterity
                + "<br>" + "Shield: " + W[G[1].weaponOne][1][1].shield + "<br>" + "Harm: " + W[G[1].weaponOne][1][1].harm
                + "<br>" + "<br>" + "zbraň dva: " + W[G[1].weaponTwo][1][2].NameWeapon + "<br>" + "Power: "
                + W[G[1].weaponTwo][1][2].power + "<br>" + "Dexterity: " + W[G[1].weaponTwo][1][2].dexterity
                + "<br>" + "Shield: " + W[G[1].weaponTwo][1][2].shield + "<br>" + "Harm: " + W[G[1].weaponTwo][1][2].harm
                + "<br>" + "<br>" + "zbraň tři: " + W[G[1].weaponThree][1][3].NameWeapon + "<br>" + "Power: "
                + W[G[1].weaponThree][1][3].power + "<br>" + "Dexterity: " + W[G[1].weaponThree][1][3].dexterity
                + "<br>" + "Shield: " + W[G[1].weaponThree][1][3].shield + "<br>" + "Harm: " + W[G[1].weaponThree][1][3].harm);

    }

    //pripravi vse pro brani gladiatoru
    public static void GETTINGGLADIATOR() {
        headline.setVisible(true);
        labHeadline.setText("braní gladiátorů");
        editor++;
        if (editor > numberOfPlayers) {
            editor = 1;
        }
        allGladiators11.setVisible(false);
        allGladiators12.setVisible(false);
        allGladiators13.setVisible(false);
        allGladiators14.setVisible(false);
        allGladiators15.setVisible(false);
        allGladiators21.setVisible(false);
        allGladiators22.setVisible(false);
        allGladiators23.setVisible(false);
        allGladiators24.setVisible(false);
        allGladiators25.setVisible(false);
        allGladiators31.setVisible(false);
        allGladiators32.setVisible(false);
        allGladiators33.setVisible(false);
        allGladiators34.setVisible(false);
        allGladiators35.setVisible(false);
        allGladiators41.setVisible(false);
        allGladiators42.setVisible(false);
        allGladiators43.setVisible(false);
        allGladiators44.setVisible(false);
        allGladiators45.setVisible(false);
        fight.setVisible(false);
        treatment.setVisible(false);
        treating.setVisible(false);
        treating2.setVisible(false);
        newWeapon.setVisible(false);
        training.setVisible(false);
        gettingGladiator.setVisible(false);
        sinemissione.setVisible(false);
        send3.setVisible(false);
        repair.setVisible(false);
        whichHand.setVisible(false);
        wantGladiator.setVisible(true);
        howManyYouHave.setVisible(true);
        editorNext.setVisible(true);
    }

    //nastavi "aktualizuje" text při boji
    public static void setText() {
        if (W[G[x].weaponOne][x][1].canBeThrowen == 0 && W[G[x].weaponTwo][x][2].canBeThrowen == 0 && (W[G[y].weaponOne][y][1].canBeThrowen != 0 || W[G[y].weaponTwo][y][2].canBeThrowen != 0)) {
            gladiatorOneFight.setVisible(false);
            gladiatorTwoFight.setVisible(true);
        }

        labGladiatorOneHandOne.setText("<html>" + W[G[x].weaponOne][x][1].NameWeapon + "<br>" + "síla: " + W[G[x].weaponOne][x][1].power + "<br>" + "obratnost: " + W[G[x].weaponOne][x][1].dexterity
                + "<br>" + "štít: " + W[G[x].weaponOne][x][1].shield + "<br>" + "může jít skrz štít: " + W[G[x].weaponOne][x][1].canGoThroughShield + "<br>" + "může hrát znovu: " + W[G[x].weaponOne][x][1].canPlayAgain + "<br>" + "může být hozen: " + W[G[x].weaponOne][x][1].canBeThrowen + "<br>" + "může zničit: " + W[G[x].weaponOne][x][1].canDestroy + "<br>" + "poničení: " + W[G[x].weaponOne][x][1].harm);
        labGladiatorOneHandTwo.setText("<html>" + W[G[x].weaponTwo][x][2].NameWeapon + "<br>" + "síla: " + W[G[x].weaponTwo][x][2].power + "<br>" + "obratnost: " + W[G[x].weaponTwo][x][2].dexterity
                + "<br>" + "štít: " + W[G[x].weaponTwo][x][2].shield + "<br>" + "může jít skrz štít: " + W[G[x].weaponTwo][x][2].canGoThroughShield + "<br>" + "může hrát znovu: " + W[G[x].weaponTwo][x][2].canPlayAgain + "<br>" + "může být hozen: " + W[G[x].weaponTwo][x][2].canBeThrowen + "<br>" + "může zničit: " + W[G[x].weaponTwo][x][2].canDestroy + "<br>" + "poničení: " + W[G[x].weaponTwo][x][2].harm);

        labGladiatorTwoHandOne.setText("<html>" + W[G[y].weaponOne][y][1].NameWeapon + "<br>" + "síla: " + W[G[y].weaponOne][y][1].power + "<br>" + "obratnost: " + W[G[y].weaponOne][y][1].dexterity
                + "<br>" + "štít: " + W[G[y].weaponOne][y][1].shield + "<br>" + "může jít skrz štít: " + W[G[y].weaponOne][y][1].canGoThroughShield + "<br>" + "může hrát znovu: " + W[G[y].weaponOne][y][1].canPlayAgain + "<br>" + "může být hozen: " + W[G[y].weaponOne][y][1].canBeThrowen + "<br>" + "může zničit: " + W[G[y].weaponOne][y][1].canDestroy + "<br>" + "poničení: " + W[G[y].weaponOne][y][1].harm);
        labGladiatorTwoHandTwo.setText("<html>" + W[G[y].weaponTwo][y][2].NameWeapon + "<br>" + "síla: " + W[G[y].weaponTwo][y][2].power + "<br>" + "obratnost: " + W[G[y].weaponTwo][y][2].dexterity
                + "<br>" + "štít: " + W[G[y].weaponTwo][y][2].shield + "<br>" + "může jít skrz štít: " + W[G[y].weaponTwo][y][2].canGoThroughShield + "<br>" + "může hrát znovu: " + W[G[y].weaponTwo][y][2].canPlayAgain + "<br>" + "může být hozen: " + W[G[y].weaponTwo][y][2].canBeThrowen + "<br>" + "může zničit: " + W[G[y].weaponTwo][y][2].canDestroy + "<br>" + "poničení: " + W[G[y].weaponTwo][y][2].harm);

        labGladiatorOneHandThree.setText("<html>" + W[G[x].weaponThree][x][3].NameWeapon + "<br>" + "síla: " + W[G[x].weaponThree][x][3].power + "<br>" + "obratnost: " + W[G[x].weaponThree][x][3].dexterity
                + "<br>" + "štít: " + W[G[x].weaponThree][x][3].shield + "<br>" + "může jít skrz štít: " + W[G[x].weaponThree][x][3].canGoThroughShield + "<br>" + "může hrát znovu: " + W[G[x].weaponThree][x][3].canPlayAgain + "<br>" + "může být hozen: " + W[G[x].weaponThree][x][3].canBeThrowen + "<br>" + "může zničit: " + W[G[x].weaponThree][x][3].canDestroy + "<br>" + "životy zvířete: " + W[G[x].weaponThree][x][3].healtIfItIsAnimal);

        labGladiatorTwoHandThree.setText("<html>" + W[G[y].weaponThree][y][3].NameWeapon + "<br>" + "síla: " + W[G[y].weaponThree][y][3].power + "<br>" + "obratnost: " + W[G[y].weaponThree][y][3].dexterity
                + "<br>" + "štít: " + W[G[y].weaponThree][y][3].shield + "<br>" + "může jít skrz štít: " + W[G[y].weaponThree][y][3].canGoThroughShield + "<br>" + "může hrát znovu: " + W[G[y].weaponThree][y][3].canPlayAgain + "<br>" + "může být hozen: " + W[G[y].weaponThree][y][3].canBeThrowen + "<br>" + "může zničit: " + W[G[y].weaponThree][y][3].canDestroy + "<br>" + "životy zvířete: " + W[G[y].weaponThree][y][3].healtIfItIsAnimal);

        labGladiatorOneLives.setText("" + G[x].lives);
        labGladiatorTwoLives.setText("" + G[y].lives);

        if (W[G[x].weaponOne][x][1].canPlayAgain > 50) {
            labGladiatorOneHandOne.setText("<html>" + W[G[x].weaponOne][x][1].NameWeapon + "<br>" + "síla: " + W[G[x].weaponOne][x][1].power + "<br>" + "obratnost: " + W[G[x].weaponOne][x][1].dexterity
                    + "<br>" + "štít: " + W[G[x].weaponOne][x][1].shield + "<br>" + "může jít skrz štít: " + W[G[x].weaponOne][x][1].canGoThroughShield + "<br>" + "může být hozen: " + W[G[x].weaponOne][x][1].canBeThrowen + "<br>" + "může zničit: " + W[G[x].weaponOne][x][1].canDestroy + "<br>" + "poničení: " + W[G[x].weaponOne][x][1].harm);
        }
        if (W[G[x].weaponOne][x][1].canDestroy > 50) {
            labGladiatorOneHandOne.setText("<html>" + W[G[x].weaponOne][x][1].NameWeapon + "<br>" + "síla: " + W[G[x].weaponOne][x][1].power + "<br>" + "obratnost: " + W[G[x].weaponOne][x][1].dexterity
                    + "<br>" + "štít: " + W[G[x].weaponOne][x][1].shield + "<br>" + "může jít skrz štít: " + W[G[x].weaponOne][x][1].canGoThroughShield + "<br>" + "může hrát znovu: " + W[G[x].weaponOne][x][1].canPlayAgain + "<br>" + "může být hozen: " + W[G[x].weaponOne][x][1].canBeThrowen + "<br>" + "poničení: " + W[G[x].weaponOne][x][1].harm);
        }

        if (W[G[x].weaponOne][x][1].canPlayAgain > 50 && W[G[x].weaponOne][x][1].canDestroy > 50) {
            labGladiatorOneHandOne.setText("<html>" + W[G[x].weaponOne][x][1].NameWeapon + "<br>" + "síla: " + W[G[x].weaponOne][x][1].power + "<br>" + "obratnost: " + W[G[x].weaponOne][x][1].dexterity
                    + "<br>" + "štít: " + W[G[x].weaponOne][x][1].shield + "<br>" + "může jít skrz štít: " + W[G[x].weaponOne][x][1].canGoThroughShield + "<br>" + "může být hozen: " + W[G[x].weaponOne][x][1].canBeThrowen + "<br>" + "poničení: " + W[G[x].weaponOne][x][1].harm);
        }

        if (W[G[x].weaponTwo][x][2].canPlayAgain > 50) {
            labGladiatorOneHandTwo.setText("<html>" + W[G[x].weaponTwo][x][2].NameWeapon + "<br>" + "síla: " + W[G[x].weaponTwo][x][2].power + "<br>" + "obratnost: " + W[G[x].weaponTwo][x][2].dexterity
                    + "<br>" + "štít: " + W[G[x].weaponTwo][x][2].shield + "<br>" + "může jít skrz štít: " + W[G[x].weaponTwo][x][2].canGoThroughShield + "<br>" + "může být hozen: " + W[G[x].weaponTwo][x][2].canBeThrowen + "<br>" + "může zničit: " + W[G[x].weaponTwo][x][2].canDestroy + "<br>" + "poničení: " + W[G[x].weaponTwo][x][2].harm);
        }
        if (W[G[x].weaponTwo][x][2].canDestroy > 50) {
            labGladiatorOneHandTwo.setText("<html>" + W[G[x].weaponTwo][x][2].NameWeapon + "<br>" + "síla: " + W[G[x].weaponTwo][x][2].power + "<br>" + "obratnost: " + W[G[x].weaponTwo][x][2].dexterity
                    + "<br>" + "štít: " + W[G[x].weaponTwo][x][2].shield + "<br>" + "může jít skrz štít: " + W[G[x].weaponTwo][x][2].canGoThroughShield + "<br>" + "může hrát znovu: " + W[G[x].weaponTwo][x][2].canPlayAgain + "<br>" + "může být hozen: " + W[G[x].weaponTwo][x][2].canBeThrowen + "<br>" + "poničení: " + W[G[x].weaponTwo][x][2].harm);
        }

        if (W[G[x].weaponTwo][x][2].canPlayAgain > 50 && W[G[x].weaponTwo][x][2].canDestroy > 50) {
            labGladiatorOneHandTwo.setText("<html>" + W[G[x].weaponTwo][x][2].NameWeapon + "<br>" + "síla: " + W[G[x].weaponTwo][x][2].power + "<br>" + "obratnost: " + W[G[x].weaponTwo][x][2].dexterity
                    + "<br>" + "štít: " + W[G[x].weaponTwo][x][2].shield + "<br>" + "může jít skrz štít: " + W[G[x].weaponTwo][x][2].canGoThroughShield + "<br>" + "může být hozen: " + W[G[x].weaponTwo][x][2].canBeThrowen + "<br>" + "poničení: " + W[G[x].weaponTwo][x][2].harm);
        }

        if (W[G[y].weaponOne][y][1].canPlayAgain > 50) {
            labGladiatorTwoHandOne.setText("<html>" + W[G[y].weaponOne][y][1].NameWeapon + "<br>" + "síla: " + W[G[y].weaponOne][y][1].power + "<br>" + "obratnost: " + W[G[y].weaponOne][y][1].dexterity
                    + "<br>" + "štít: " + W[G[y].weaponOne][y][1].shield + "<br>" + "může jít skrz štít: " + W[G[y].weaponOne][y][1].canGoThroughShield + "<br>" + "může být hozen: " + W[G[y].weaponOne][y][1].canBeThrowen + "<br>" + "může zničit: " + W[G[y].weaponOne][y][1].canDestroy + "<br>" + "poničení: " + W[G[y].weaponOne][y][1].harm);
        }
        if (W[G[y].weaponOne][y][1].canDestroy > 50) {
            labGladiatorTwoHandOne.setText("<html>" + W[G[x].weaponOne][x][1].NameWeapon + "<br>" + "síla: " + W[G[x].weaponOne][x][1].power + "<br>" + "obratnost: " + W[G[x].weaponOne][x][1].dexterity
                    + "<br>" + "štít: " + W[G[y].weaponOne][y][1].shield + "<br>" + "může jít skrz štít: " + W[G[y].weaponOne][y][1].canGoThroughShield + "<br>" + "může hrát znovu: " + W[G[y].weaponOne][y][1].canPlayAgain + "<br>" + "může být hozen: " + W[G[y].weaponOne][y][1].canBeThrowen + "<br>" + "poničení: " + W[G[y].weaponOne][y][1].harm);
        }

        if (W[G[y].weaponOne][y][1].canPlayAgain > 50 && W[G[y].weaponOne][y][1].canDestroy > 50) {
            labGladiatorTwoHandOne.setText("<html>" + W[G[y].weaponOne][y][1].NameWeapon + "<br>" + "síla: " + W[G[y].weaponOne][y][1].power + "<br>" + "obratnost: " + W[G[y].weaponOne][y][1].dexterity
                    + "<br>" + "štít: " + W[G[y].weaponOne][y][1].shield + "<br>" + "může jít skrz štít: " + W[G[y].weaponOne][y][1].canGoThroughShield + "<br>" + "může být hozen: " + W[G[y].weaponOne][y][1].canBeThrowen + "<br>" + "poničení: " + W[G[y].weaponOne][y][1].harm);
        }

        if (W[G[y].weaponTwo][y][2].canPlayAgain > 50) {
            labGladiatorTwoHandTwo.setText("<html>" + W[G[y].weaponTwo][y][2].NameWeapon + "<br>" + "síla: " + W[G[y].weaponTwo][y][2].power + "<br>" + "obratnost: " + W[G[y].weaponTwo][y][2].dexterity
                    + "<br>" + "štít: " + W[G[y].weaponTwo][y][2].shield + "<br>" + "může jít skrz štít: " + W[G[y].weaponTwo][y][2].canGoThroughShield + "<br>" + "může být hozen: " + W[G[y].weaponTwo][y][2].canBeThrowen + "<br>" + "může zničit: " + W[G[y].weaponTwo][y][2].canDestroy + "<br>" + "poničení: " + W[G[y].weaponTwo][y][2].harm);
        }
        if (W[G[y].weaponTwo][y][2].canDestroy > 50) {
            labGladiatorTwoHandTwo.setText("<html>" + W[G[y].weaponTwo][y][2].NameWeapon + "<br>" + "síla: " + W[G[y].weaponTwo][y][2].power + "<br>" + "obratnost: " + W[G[y].weaponTwo][y][2].dexterity
                    + "<br>" + "štít: " + W[G[y].weaponTwo][y][2].shield + "<br>" + "může jít skrz štít: " + W[G[y].weaponTwo][y][2].canGoThroughShield + "<br>" + "může hrát znovu: " + W[G[y].weaponTwo][y][2].canPlayAgain + "<br>" + "může být hozen: " + W[G[y].weaponTwo][y][2].canBeThrowen + "<br>" + "poničení: " + W[G[y].weaponTwo][y][2].harm);
        }

        if (W[G[y].weaponTwo][y][2].canPlayAgain > 50 && W[G[y].weaponTwo][y][2].canDestroy > 50) {
            labGladiatorTwoHandTwo.setText("<html>" + W[G[y].weaponTwo][y][2].NameWeapon + "<br>" + "síla: " + W[G[y].weaponTwo][y][2].power + "<br>" + "obratnost: " + W[G[y].weaponTwo][y][2].dexterity
                    + "<br>" + "štít: " + W[G[y].weaponTwo][y][2].shield + "<br>" + "může jít skrz štít: " + W[G[y].weaponTwo][y][2].canGoThroughShield + "<br>" + "může být hozen: " + W[G[y].weaponTwo][y][2].canBeThrowen + "<br>" + "poničení: " + W[G[y].weaponTwo][y][2].harm);
        }

        if (W[G[x].weaponThree][x][3].canPlayAgain > 50) {
            labGladiatorOneHandThree.setText("<html>" + W[G[x].weaponThree][x][3].NameWeapon + "<br>" + "síla: " + W[G[x].weaponThree][x][3].power + "<br>" + "obratnost: " + W[G[x].weaponThree][x][3].dexterity
                    + "<br>" + "štít: " + W[G[x].weaponThree][x][3].shield + "<br>" + "může jít skrz štít: " + W[G[x].weaponThree][x][3].canGoThroughShield + "<br>" + "může být hozen: " + W[G[x].weaponThree][x][3].canBeThrowen + "<br>" + "může zničit: " + W[G[x].weaponThree][x][3].canDestroy + "<br>" + "poničení: " + W[G[x].weaponThree][x][3].harm + "<br>" + "životy zvířete: " + W[G[x].weaponThree][x][3].healtIfItIsAnimal);
        }
        if (W[G[x].weaponThree][x][3].canDestroy > 50) {
            labGladiatorOneHandThree.setText("<html>" + W[G[x].weaponThree][x][3].NameWeapon + "<br>" + "síla: " + W[G[x].weaponThree][x][3].power + "<br>" + "obratnost: " + W[G[x].weaponThree][x][3].dexterity
                    + "<br>" + "štít: " + W[G[x].weaponThree][x][3].shield + "<br>" + "může jít skrz štít: " + W[G[x].weaponThree][x][3].canGoThroughShield + "<br>" + "může hrát znovu: " + W[G[x].weaponThree][x][3].canPlayAgain + "<br>" + "může být hozen: " + W[G[x].weaponThree][x][3].canBeThrowen + "<br>" + "poničení: " + W[G[x].weaponThree][x][3].harm + "<br>" + "životy zvířete: " + W[G[x].weaponThree][x][3].healtIfItIsAnimal);
        }

        if (W[G[x].weaponThree][x][3].canPlayAgain > 50 && W[G[x].weaponThree][x][3].canDestroy > 50) {
            labGladiatorOneHandThree.setText("<html>" + W[G[x].weaponThree][x][3].NameWeapon + "<br>" + "síla: " + W[G[x].weaponThree][x][3].power + "<br>" + "obratnost: " + W[G[x].weaponThree][x][3].dexterity
                    + "<br>" + "štít: " + W[G[x].weaponThree][x][3].shield + "<br>" + "může jít skrz štít: " + W[G[x].weaponThree][x][3].canGoThroughShield + "<br>" + "může být hozen: " + W[G[x].weaponThree][x][3].canBeThrowen + "<br>" + "poničení: " + W[G[x].weaponThree][x][3].harm + "<br>" + "životy zvířete: " + W[G[x].weaponThree][x][3].healtIfItIsAnimal);
        }

        if (W[G[y].weaponThree][y][3].canPlayAgain > 50) {
            labGladiatorTwoHandThree.setText("<html>" + W[G[y].weaponThree][y][3].NameWeapon + "<br>" + "síla: " + W[G[y].weaponThree][y][3].power + "<br>" + "obratnost: " + W[G[y].weaponThree][y][3].dexterity
                    + "<br>" + "štít: " + W[G[y].weaponThree][y][3].shield + "<br>" + "může jít skrz štít: " + W[G[y].weaponThree][y][3].canGoThroughShield + "<br>" + "může být hozen: " + W[G[y].weaponThree][y][3].canBeThrowen + "<br>" + "může zničit: " + W[G[y].weaponThree][y][3].canDestroy + "<br>" + "poničení: " + W[G[y].weaponThree][y][3].harm + "<br>" + "životy zvířete: " + W[G[y].weaponThree][y][3].healtIfItIsAnimal);
        }
        if (W[G[y].weaponThree][y][3].canDestroy > 50) {
            labGladiatorTwoHandThree.setText("<html>" + W[G[y].weaponThree][y][3].NameWeapon + "<br>" + "síla: " + W[G[y].weaponThree][y][3].power + "<br>" + "obratnost: " + W[G[y].weaponThree][y][3].dexterity
                    + "<br>" + "štít: " + W[G[y].weaponThree][y][3].shield + "<br>" + "může jít skrz štít: " + W[G[y].weaponThree][y][3].canGoThroughShield + "<br>" + "může hrát znovu: " + W[G[y].weaponThree][y][3].canPlayAgain + "<br>" + "může být hozen: " + W[G[y].weaponThree][y][3].canBeThrowen + "<br>" + "poničení: " + W[G[y].weaponThree][y][3].harm + "<br>" + "životy zvířete: " + W[G[y].weaponThree][y][3].healtIfItIsAnimal);
        }

        if (W[G[y].weaponThree][y][3].canPlayAgain > 50 && W[G[y].weaponThree][y][3].canDestroy > 50) {
            labGladiatorTwoHandThree.setText("<html>" + W[G[y].weaponThree][y][3].NameWeapon + "<br>" + "síla: " + W[G[y].weaponThree][y][3].power + "<br>" + "obratnost: " + W[G[y].weaponThree][y][3].dexterity
                    + "<br>" + "štít: " + W[G[y].weaponThree][y][3].shield + "<br>" + "může jít skrz štít: " + W[G[y].weaponThree][y][3].canGoThroughShield + "<br>" + "může být hozen: " + W[G[y].weaponThree][y][3].canBeThrowen + "<br>" + "poničení: " + W[G[y].weaponThree][y][3].harm + "<br>" + "životy zvířete: " + W[G[y].weaponThree][y][3].healtIfItIsAnimal);
        }
    }

    //pokud jeden z gladiatoru umre nastavi vse do "normalu"
    public static void checkLives() {
        allGladiators11.setVisible(true);
        allGladiators12.setVisible(true);
        allGladiators13.setVisible(true);
        allGladiators14.setVisible(true);
        allGladiators15.setVisible(true);
        allGladiators21.setVisible(true);
        allGladiators22.setVisible(true);
        allGladiators23.setVisible(true);
        allGladiators24.setVisible(true);
        allGladiators25.setVisible(true);
        allGladiators31.setVisible(true);
        allGladiators32.setVisible(true);
        allGladiators33.setVisible(true);
        allGladiators34.setVisible(true);
        allGladiators35.setVisible(true);
        allGladiators41.setVisible(true);
        allGladiators42.setVisible(true);
        allGladiators43.setVisible(true);
        allGladiators44.setVisible(true);
        allGladiators45.setVisible(true);
        fightFrame.setVisible(false);
        fightTrue = false;
        fight.setVisible(true);
        treatment.setVisible(true);
        training.setVisible(true);
        newWeapon.setVisible(true);
        repair.setVisible(true);
        gettingGladiator.setVisible(true);
        sinemissione.setVisible(true);
        gladiatorOneLives.setVisible(false);
        gladiatorTwoLives.setVisible(false);
        gladiatorOneHandOne.setVisible(false);
        gladiatorOneHandTwo.setVisible(false);
        gladiatorTwoHandOne.setVisible(false);
        gladiatorTwoHandTwo.setVisible(false);
        gladiatorOneHandThree.setVisible(false);
        gladiatorTwoHandThree.setVisible(false);
        gladiatorOneFight.setVisible(false);
        gladiatorTwoFight.setVisible(false);
        animal.setBackground(Color.blue);
        gladiator.setBackground(Color.blue);
        animal.setVisible(false);
        gladiator.setVisible(false);
        wantToEnd.setVisible(false);
        ROUND();

        if (gladiatorThrowFirst == true) {
            G[x].weaponOne = gladiatorThrowFirstWeapon;
            W[G[x].weaponOne][x][1].harm = gladiatorThrowFirstHarm;
        }

        if (gladiatorThrowSecond == true) {
            G[x].weaponTwo = gladiatorThrowSecondWeapon;
            W[G[x].weaponTwo][x][2].harm = gladiatorThrowSecondHarm;
        }

        if (gladiatorThrowThird == true) {
            G[y].weaponOne = gladiatorThrowThirdWeapon;
            W[G[y].weaponOne][y][1].harm = gladiatorThrowThirdHarm;
        }

        if (gladiatorThrowFourth == true) {
            G[y].weaponTwo = gladiatorThrowFourthWeapon;
            W[G[y].weaponTwo][y][2].harm = gladiatorThrowFourthHarm;
        }
        gladiatorThrowFirst = false;
        gladiatorThrowSecond = false;
        gladiatorThrowThird = false;
        gladiatorThrowFourth = false;

    }

    //táto metoda pošle dva gladiátory do boje a urci zda jsou
    public static void FIGHT(int X, int Y, boolean SineMissione) {
        x = X;
        y = Y;
        sineMissione = SineMissione;

        wantToEnd.setVisible(true);
        if (sineMissione == true) {
            wantToEnd.setVisible(false);
        }

        if (W[G[y].weaponThree][y][3].healtIfItIsAnimal > 0 && W[G[y].weaponOne][y][1].canBeThrowen == 0 && W[G[y].weaponTwo][y][2].canBeThrowen == 0) {
            gladiator.setVisible(true);
            animal.setVisible(true);
        }
        if (W[G[y].weaponOne][y][1].canBeThrowen != 0 || W[G[y].weaponTwo][y][2].canBeThrowen != 0) {
            if (W[G[x].weaponThree][x][3].healtIfItIsAnimal > 0 && W[G[x].weaponOne][x][1].canBeThrowen == 0 && W[G[x].weaponTwo][x][2].canBeThrowen == 0) {
                gladiator.setVisible(true);
                animal.setVisible(true);
            }
            if (W[G[y].weaponThree][y][3].healtIfItIsAnimal > 0 && (W[G[x].weaponOne][x][1].canBeThrowen != 0 || W[G[x].weaponTwo][x][2].canBeThrowen != 0)) {
                gladiator.setVisible(true);
                animal.setVisible(true);
            }
        }

        fightTrue = true;
        fight.setVisible(false);
        treatment.setVisible(false);
        training.setVisible(false);
        newWeapon.setVisible(false);
        repair.setVisible(false);
        gettingGladiator.setVisible(false);
        sinemissione.setVisible(false);
        gladiatorOneLives.setVisible(true);
        gladiatorTwoLives.setVisible(true);
        gladiatorOneHandOne.setVisible(true);
        gladiatorOneHandTwo.setVisible(true);
        gladiatorTwoHandOne.setVisible(true);
        gladiatorTwoHandTwo.setVisible(true);
        gladiatorOneHandThree.setVisible(true);
        gladiatorTwoHandThree.setVisible(true);
        gladiatorOneFight.setVisible(true);
        gladiatorTwoFight.setVisible(false);

        setText();

    }

    //konec hry
    public static void END() {

        String P = "";
        send.setVisible(false);
        send2.setVisible(false);
        send3.setVisible(false);
        send4.setVisible(false);
        send5.setVisible(false);
        send6.setVisible(false);
        whichGladiator.setVisible(false);
        whichWeapon.setVisible(false);
        whichHand.setVisible(false);
        fightFrame.setVisible(false);
        fightTrue = false;
        fight.setVisible(false);
        treatment.setVisible(false);
        newWeapon.setVisible(false);
        training.setVisible(false);
        gettingGladiator.setVisible(false);
        repair.setVisible(false);
        whichPlayerIsChoosing = 0;
        gladiatorOne.setVisible(false);
        gladiatorOne1.setVisible(false);
        gladiatorTwo.setVisible(false);
        gladiatorTwo2.setVisible(false);
        gladiatorThree.setVisible(false);
        gladiatorThree3.setVisible(false);
        gladiatorFourth.setVisible(false);
        gladiatorFourth4.setVisible(false);
        gladiatorOne.setBackground(Color.blue);
        gladiatorOne1.setBackground(Color.blue);
        gladiatorTwo.setBackground(Color.blue);
        gladiatorTwo2.setBackground(Color.blue);
        gladiatorThree.setBackground(Color.blue);
        gladiatorThree3.setBackground(Color.blue);
        gladiatorFourth.setBackground(Color.blue);
        gladiatorFourth4.setBackground(Color.blue);

        endScrean.setVisible(true);

        int numberRuidariu1 = 0;
        int numberRuidariu2 = 0;
        int numberRuidariu3 = 0;
        int numberRuidariu4 = 0;

        //pocita kolik ma hrac rudiariu
        for (int i = 0; i <= 100; i++) {
            if (G[i].rudiarius == 1) {
                numberRuidariu1++;
            }
            if (G[i].rudiarius == 2) {
                numberRuidariu2++;
            }
            if (G[i].rudiarius == 3) {
                numberRuidariu3++;
            }
            if (G[i].rudiarius == 4) {
                numberRuidariu4++;
            }
        }

        //vypis vyhercu
        if (numberRuidariu1 >= numberRuidariu2 && numberRuidariu1 >= numberRuidariu3 && numberRuidariu1 >= numberRuidariu4 && numberOfPlayers >= 1) {
            P = P + "1 ";
        }
        if (numberRuidariu2 >= numberRuidariu1 && numberRuidariu2 >= numberRuidariu3 && numberRuidariu2 >= numberRuidariu4 && numberOfPlayers >= 2) {
            P = P + "2 ";
        }
        if (numberRuidariu3 >= numberRuidariu1 && numberRuidariu3 >= numberRuidariu2 && numberRuidariu3 >= numberRuidariu4 && numberOfPlayers >= 3) {
            P = P + "3 ";
        }
        if (numberRuidariu4 >= numberRuidariu1 && numberRuidariu4 >= numberRuidariu2 && numberRuidariu4 >= numberRuidariu3 && numberOfPlayers >= 4) {
            P = P + "4 ";
        }

        labEndScrean.setText("vyhrál hráč: " + P);

    }

    @Override
    public void mouseClicked(MouseEvent e) {
        mysX = e.getX();
        mysY = e.getY();
    }

    @Override
    public void mousePressed(MouseEvent e) {
        mysX = e.getX();
        mysY = e.getY();
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        mysX = e.getX();
        mysY = e.getY();
    }

    @Override
    public void mouseExited(MouseEvent e) {
        mysX = e.getX();
        mysY = e.getY();
    }
    static javax.swing.Timer timer = new javax.swing.Timer(10, new ActionListener() {

        public void actionPerformed(ActionEvent e) {
            int C1 = 1;
            int C2 = 1;
            int C3 = 1;
            int C4 = 1;

            String allG11 = "hráč 1 ";
            String allG12 = "";
            String allG13 = "";
            String allG14 = "";
            String allG15 = "";
            String allG21 = "hráč 2 ";
            String allG22 = "";
            String allG23 = "";
            String allG24 = "";
            String allG25 = "";
            String allG31 = "hráč 3 ";
            String allG32 = "";
            String allG33 = "";
            String allG34 = "";
            String allG35 = "";
            String allG41 = "hráč 4 ";
            String allG42 = "";
            String allG43 = "";
            String allG44 = "";
            String allG45 = "";

            for (int i = 1; i <= numberOfGladiators; i++) {
                if (G[i].owener == 1) {
                    if (C1 == 1) {
                        allG12 = ("<html>" + "<br>" + "<br>" + "level: " + G[i].level + "<br>" + W[G[i].weaponOne][i][1].NameWeapon + "<br>" + "Power: " + W[G[i].weaponOne][i][1].power + "<br>" + "Dexterity: " + W[G[i].weaponOne][i][1].dexterity
                                + "<br>" + "Harm: " + W[G[i].weaponOne][i][1].harm + "<br>" + W[G[i].weaponTwo][i][2].NameWeapon + "<br>" + "Power: " + W[G[i].weaponTwo][i][2].power + "<br>" + "Dexterity: " + W[G[i].weaponTwo][i][2].dexterity
                                + "<br>" + "Harm: " + W[G[i].weaponTwo][i][2].harm + "<br>" + W[G[i].weaponThree][i][3].NameWeapon + "<br>" + "Power: " + W[G[i].weaponThree][i][3].power + "<br>" + "Dexterity: " + W[G[i].weaponThree][i][3].dexterity
                                + "<br>" + "Harm: " + W[G[i].weaponThree][i][3].harm + "<br>");

                    }
                    if (C1 == 2) {
                        allG13 = ("<html>" + "<br>" + "<br>" + "level: " + G[i].level + "<br>" + W[G[i].weaponOne][i][1].NameWeapon + "<br>" + "Power: " + W[G[i].weaponOne][i][1].power + "<br>" + "Dexterity: " + W[G[i].weaponOne][i][1].dexterity
                                + "<br>" + "Harm: " + W[G[i].weaponOne][i][1].harm + "<br>" + W[G[i].weaponTwo][i][2].NameWeapon + "<br>" + "Power: " + W[G[i].weaponTwo][i][2].power + "<br>" + "Dexterity: " + W[G[i].weaponTwo][i][2].dexterity
                                + "<br>" + "Harm: " + W[G[i].weaponTwo][i][2].harm + "<br>" + W[G[i].weaponThree][i][3].NameWeapon + "<br>" + "Power: " + W[G[i].weaponThree][i][3].power + "<br>" + "Dexterity: " + W[G[i].weaponThree][i][3].dexterity
                                + "<br>" + "Harm: " + W[G[i].weaponThree][i][3].harm + "<br>");
                    }
                    if (C1 == 3) {
                        allG14 = ("<html>" + "<br>" + "<br>" + "level: " + G[i].level + "<br>" + W[G[i].weaponOne][i][1].NameWeapon + "<br>" + "Power: " + W[G[i].weaponOne][i][1].power + "<br>" + "Dexterity: " + W[G[i].weaponOne][i][1].dexterity
                                + "<br>" + "Harm: " + W[G[i].weaponOne][i][1].harm + "<br>" + W[G[i].weaponTwo][i][2].NameWeapon + "<br>" + "Power: " + W[G[i].weaponTwo][i][2].power + "<br>" + "Dexterity: " + W[G[i].weaponTwo][i][2].dexterity
                                + "<br>" + "Harm: " + W[G[i].weaponTwo][i][2].harm + "<br>" + W[G[i].weaponThree][i][3].NameWeapon + "<br>" + "Power: " + W[G[i].weaponThree][i][3].power + "<br>" + "Dexterity: " + W[G[i].weaponThree][i][3].dexterity
                                + "<br>" + "Harm: " + W[G[i].weaponThree][i][3].harm + "<br>");
                    }
                    if (C1 == 4) {
                        allG15 = ("<html>" + "<br>" + "<br>" + "level: " + G[i].level + "<br>" + W[G[i].weaponOne][i][1].NameWeapon + "<br>" + "Power: " + W[G[i].weaponOne][i][1].power + "<br>" + "Dexterity: " + W[G[i].weaponOne][i][1].dexterity
                                + "<br>" + "Harm: " + W[G[i].weaponOne][i][1].harm + "<br>" + W[G[i].weaponTwo][i][2].NameWeapon + "<br>" + "Power: " + W[G[i].weaponTwo][i][2].power + "<br>" + "Dexterity: " + W[G[i].weaponTwo][i][2].dexterity
                                + "<br>" + "Harm: " + W[G[i].weaponTwo][i][2].harm + "<br>" + W[G[i].weaponThree][i][3].NameWeapon + "<br>" + "Power: " + W[G[i].weaponThree][i][3].power + "<br>" + "Dexterity: " + W[G[i].weaponThree][i][3].dexterity
                                + "<br>" + "Harm: " + W[G[i].weaponThree][i][3].harm + "<br>");
                    }
                    C1++;
                }
                if (G[i].owener == 2) {
                    if (C2 == 1) {
                        allG22 = ("<html>" + "<br>" + "<br>" + "level: " + G[i].level + "<br>" + W[G[i].weaponOne][i][1].NameWeapon + "<br>" + "Power: " + W[G[i].weaponOne][i][1].power + "<br>" + "Dexterity: " + W[G[i].weaponOne][i][1].dexterity
                                + "<br>" + "Harm: " + W[G[i].weaponOne][i][1].harm + "<br>" + W[G[i].weaponTwo][i][2].NameWeapon + "<br>" + "Power: " + W[G[i].weaponTwo][i][2].power + "<br>" + "Dexterity: " + W[G[i].weaponTwo][i][2].dexterity
                                + "<br>" + "Harm: " + W[G[i].weaponTwo][i][2].harm + "<br>" + W[G[i].weaponThree][i][3].NameWeapon + "<br>" + "Power: " + W[G[i].weaponThree][i][3].power + "<br>" + "Dexterity: " + W[G[i].weaponThree][i][3].dexterity
                                + "<br>" + "Harm: " + W[G[i].weaponThree][i][3].harm + "<br>");

                    }
                    if (C2 == 2) {
                        allG23 = ("<html>" + "<br>" + "<br>" + "level: " + G[i].level + "<br>" + W[G[i].weaponOne][i][1].NameWeapon + "<br>" + "Power: " + W[G[i].weaponOne][i][1].power + "<br>" + "Dexterity: " + W[G[i].weaponOne][i][1].dexterity
                                + "<br>" + "Harm: " + W[G[i].weaponOne][i][1].harm + "<br>" + W[G[i].weaponTwo][i][2].NameWeapon + "<br>" + "Power: " + W[G[i].weaponTwo][i][2].power + "<br>" + "Dexterity: " + W[G[i].weaponTwo][i][2].dexterity
                                + "<br>" + "Harm: " + W[G[i].weaponTwo][i][2].harm + "<br>" + W[G[i].weaponThree][i][3].NameWeapon + "<br>" + "Power: " + W[G[i].weaponThree][i][3].power + "<br>" + "Dexterity: " + W[G[i].weaponThree][i][3].dexterity
                                + "<br>" + "Harm: " + W[G[i].weaponThree][i][3].harm + "<br>");
                    }
                    if (C2 == 3) {
                        allG24 = ("<html>" + "<br>" + "<br>" + "level: " + G[i].level + "<br>" + W[G[i].weaponOne][i][1].NameWeapon + "<br>" + "Power: " + W[G[i].weaponOne][i][1].power + "<br>" + "Dexterity: " + W[G[i].weaponOne][i][1].dexterity
                                + "<br>" + "Harm: " + W[G[i].weaponOne][i][1].harm + "<br>" + W[G[i].weaponTwo][i][2].NameWeapon + "<br>" + "Power: " + W[G[i].weaponTwo][i][2].power + "<br>" + "Dexterity: " + W[G[i].weaponTwo][i][2].dexterity
                                + "<br>" + "Harm: " + W[G[i].weaponTwo][i][2].harm + "<br>" + W[G[i].weaponThree][i][3].NameWeapon + "<br>" + "Power: " + W[G[i].weaponThree][i][3].power + "<br>" + "Dexterity: " + W[G[i].weaponThree][i][3].dexterity
                                + "<br>" + "Harm: " + W[G[i].weaponThree][i][3].harm + "<br>");
                    }
                    if (C2 == 4) {
                        allG25 = ("<html>" + "<br>" + "<br>" + "level: " + G[i].level + "<br>" + W[G[i].weaponOne][i][1].NameWeapon + "<br>" + "Power: " + W[G[i].weaponOne][i][1].power + "<br>" + "Dexterity: " + W[G[i].weaponOne][i][1].dexterity
                                + "<br>" + "Harm: " + W[G[i].weaponOne][i][1].harm + "<br>" + W[G[i].weaponTwo][i][2].NameWeapon + "<br>" + "Power: " + W[G[i].weaponTwo][i][2].power + "<br>" + "Dexterity: " + W[G[i].weaponTwo][i][2].dexterity
                                + "<br>" + "Harm: " + W[G[i].weaponTwo][i][2].harm + "<br>" + W[G[i].weaponThree][i][3].NameWeapon + "<br>" + "Power: " + W[G[i].weaponThree][i][3].power + "<br>" + "Dexterity: " + W[G[i].weaponThree][i][3].dexterity
                                + "<br>" + "Harm: " + W[G[i].weaponThree][i][3].harm + "<br>");
                    }
                    C2++;
                }
                if (G[i].owener == 3) {
                    if (C3 == 1) {
                        allG32 = ("<html>" + "<br>" + "<br>" + "level: " + G[i].level + "<br>" + W[G[i].weaponOne][i][1].NameWeapon + "<br>" + "Power: " + W[G[i].weaponOne][i][1].power + "<br>" + "Dexterity: " + W[G[i].weaponOne][i][1].dexterity
                                + "<br>" + "Harm: " + W[G[i].weaponOne][i][1].harm + "<br>" + W[G[i].weaponTwo][i][2].NameWeapon + "<br>" + "Power: " + W[G[i].weaponTwo][i][2].power + "<br>" + "Dexterity: " + W[G[i].weaponTwo][i][2].dexterity
                                + "<br>" + "Harm: " + W[G[i].weaponTwo][i][2].harm + "<br>" + W[G[i].weaponThree][i][3].NameWeapon + "<br>" + "Power: " + W[G[i].weaponThree][i][3].power + "<br>" + "Dexterity: " + W[G[i].weaponThree][i][3].dexterity
                                + "<br>" + "Harm: " + W[G[i].weaponThree][i][3].harm + "<br>");

                    }
                    if (C3 == 2) {
                        allG33 = ("<html>" + "<br>" + "<br>" + "level: " + G[i].level + "<br>" + W[G[i].weaponOne][i][1].NameWeapon + "<br>" + "Power: " + W[G[i].weaponOne][i][1].power + "<br>" + "Dexterity: " + W[G[i].weaponOne][i][1].dexterity
                                + "<br>" + "Harm: " + W[G[i].weaponOne][i][1].harm + "<br>" + W[G[i].weaponTwo][i][2].NameWeapon + "<br>" + "Power: " + W[G[i].weaponTwo][i][2].power + "<br>" + "Dexterity: " + W[G[i].weaponTwo][i][2].dexterity
                                + "<br>" + "Harm: " + W[G[i].weaponTwo][i][2].harm + "<br>" + W[G[i].weaponThree][i][3].NameWeapon + "<br>" + "Power: " + W[G[i].weaponThree][i][3].power + "<br>" + "Dexterity: " + W[G[i].weaponThree][i][3].dexterity
                                + "<br>" + "Harm: " + W[G[i].weaponThree][i][3].harm + "<br>");
                    }
                    if (C3 == 3) {
                        allG34 = ("<html>" + "<br>" + "<br>" + "level: " + G[i].level + "<br>" + W[G[i].weaponOne][i][1].NameWeapon + "<br>" + "Power: " + W[G[i].weaponOne][i][1].power + "<br>" + "Dexterity: " + W[G[i].weaponOne][i][1].dexterity
                                + "<br>" + "Harm: " + W[G[i].weaponOne][i][1].harm + "<br>" + W[G[i].weaponTwo][i][2].NameWeapon + "<br>" + "Power: " + W[G[i].weaponTwo][i][2].power + "<br>" + "Dexterity: " + W[G[i].weaponTwo][i][2].dexterity
                                + "<br>" + "Harm: " + W[G[i].weaponTwo][i][2].harm + "<br>" + W[G[i].weaponThree][i][3].NameWeapon + "<br>" + "Power: " + W[G[i].weaponThree][i][3].power + "<br>" + "Dexterity: " + W[G[i].weaponThree][i][3].dexterity
                                + "<br>" + "Harm: " + W[G[i].weaponThree][i][3].harm + "<br>");
                    }
                    if (C3 == 4) {
                        allG35 = ("<html>" + "<br>" + "<br>" + "level: " + G[i].level + "<br>" + W[G[i].weaponOne][i][1].NameWeapon + "<br>" + "Power: " + W[G[i].weaponOne][i][1].power + "<br>" + "Dexterity: " + W[G[i].weaponOne][i][1].dexterity
                                + "<br>" + "Harm: " + W[G[i].weaponOne][i][1].harm + "<br>" + W[G[i].weaponTwo][i][2].NameWeapon + "<br>" + "Power: " + W[G[i].weaponTwo][i][2].power + "<br>" + "Dexterity: " + W[G[i].weaponTwo][i][2].dexterity
                                + "<br>" + "Harm: " + W[G[i].weaponTwo][i][2].harm + "<br>" + W[G[i].weaponThree][i][3].NameWeapon + "<br>" + "Power: " + W[G[i].weaponThree][i][3].power + "<br>" + "Dexterity: " + W[G[i].weaponThree][i][3].dexterity
                                + "<br>" + "Harm: " + W[G[i].weaponThree][i][3].harm + "<br>");
                    }
                    C3++;
                }
                if (G[i].owener == 4) {
                    if (C4 == 1) {
                        allG42 = ("<html>" + "<br>" + "<br>" + "level: " + G[i].level + "<br>" + W[G[i].weaponOne][i][1].NameWeapon + "<br>" + "Power: " + W[G[i].weaponOne][i][1].power + "<br>" + "Dexterity: " + W[G[i].weaponOne][i][1].dexterity
                                + "<br>" + "Harm: " + W[G[i].weaponOne][i][1].harm + "<br>" + W[G[i].weaponTwo][i][2].NameWeapon + "<br>" + "Power: " + W[G[i].weaponTwo][i][2].power + "<br>" + "Dexterity: " + W[G[i].weaponTwo][i][2].dexterity
                                + "<br>" + "Harm: " + W[G[i].weaponTwo][i][2].harm + "<br>" + W[G[i].weaponThree][i][3].NameWeapon + "<br>" + "Power: " + W[G[i].weaponThree][i][3].power + "<br>" + "Dexterity: " + W[G[i].weaponThree][i][3].dexterity
                                + "<br>" + "Harm: " + W[G[i].weaponThree][i][3].harm + "<br>");

                    }
                    if (C4 == 2) {
                        allG43 = ("<html>" + "<br>" + "<br>" + "level: " + G[i].level + "<br>" + W[G[i].weaponOne][i][1].NameWeapon + "<br>" + "Power: " + W[G[i].weaponOne][i][1].power + "<br>" + "Dexterity: " + W[G[i].weaponOne][i][1].dexterity
                                + "<br>" + "Harm: " + W[G[i].weaponOne][i][1].harm + "<br>" + W[G[i].weaponTwo][i][2].NameWeapon + "<br>" + "Power: " + W[G[i].weaponTwo][i][2].power + "<br>" + "Dexterity: " + W[G[i].weaponTwo][i][2].dexterity
                                + "<br>" + "Harm: " + W[G[i].weaponTwo][i][2].harm + "<br>" + W[G[i].weaponThree][i][3].NameWeapon + "<br>" + "Power: " + W[G[i].weaponThree][i][3].power + "<br>" + "Dexterity: " + W[G[i].weaponThree][i][3].dexterity
                                + "<br>" + "Harm: " + W[G[i].weaponThree][i][3].harm + "<br>");
                    }
                    if (C4 == 3) {
                        allG44 = ("<html>" + "<br>" + "<br>" + "level: " + G[i].level + "<br>" + W[G[i].weaponOne][i][1].NameWeapon + "<br>" + "Power: " + W[G[i].weaponOne][i][1].power + "<br>" + "Dexterity: " + W[G[i].weaponOne][i][1].dexterity
                                + "<br>" + "Harm: " + W[G[i].weaponOne][i][1].harm + "<br>" + W[G[i].weaponTwo][i][2].NameWeapon + "<br>" + "Power: " + W[G[i].weaponTwo][i][2].power + "<br>" + "Dexterity: " + W[G[i].weaponTwo][i][2].dexterity
                                + "<br>" + "Harm: " + W[G[i].weaponTwo][i][2].harm + "<br>" + W[G[i].weaponThree][i][3].NameWeapon + "<br>" + "Power: " + W[G[i].weaponThree][i][3].power + "<br>" + "Dexterity: " + W[G[i].weaponThree][i][3].dexterity
                                + "<br>" + "Harm: " + W[G[i].weaponThree][i][3].harm + "<br>");
                    }
                    if (C4 == 4) {
                        allG45 = ("<html>" + "<br>" + "<br>" + "level: " + G[i].level + "<br>" + W[G[i].weaponOne][i][1].NameWeapon + "<br>" + "Power: " + W[G[i].weaponOne][i][1].power + "<br>" + "Dexterity: " + W[G[i].weaponOne][i][1].dexterity
                                + "<br>" + "Harm: " + W[G[i].weaponOne][i][1].harm + "<br>" + W[G[i].weaponTwo][i][2].NameWeapon + "<br>" + "Power: " + W[G[i].weaponTwo][i][2].power + "<br>" + "Dexterity: " + W[G[i].weaponTwo][i][2].dexterity
                                + "<br>" + "Harm: " + W[G[i].weaponTwo][i][2].harm + "<br>" + W[G[i].weaponThree][i][3].NameWeapon + "<br>" + "Power: " + W[G[i].weaponThree][i][3].power + "<br>" + "Dexterity: " + W[G[i].weaponThree][i][3].dexterity
                                + "<br>" + "Harm: " + W[G[i].weaponThree][i][3].harm + "<br>");
                    }
                    C4++;
                }
            }

            if (numberOfPlayers >= 1) {
                labAllGladiators11.setText("<html>" + allG11);
                labAllGladiators12.setText("<html>" + allG12);
                labAllGladiators13.setText("<html>" + allG13);
                labAllGladiators14.setText("<html>" + allG14);
                labAllGladiators15.setText("<html>" + allG15);
            } else {
                allGladiators11.setVisible(false);
                allGladiators12.setVisible(false);
                allGladiators13.setVisible(false);
                allGladiators14.setVisible(false);
                allGladiators15.setVisible(false);
            }
            if (numberOfPlayers >= 2) {
                labAllGladiators21.setText("<html>" + allG21);
                labAllGladiators22.setText("<html>" + allG22);
                labAllGladiators23.setText("<html>" + allG23);
                labAllGladiators24.setText("<html>" + allG24);
                labAllGladiators25.setText("<html>" + allG25);
            } else {
                allGladiators21.setVisible(false);
                allGladiators22.setVisible(false);
                allGladiators23.setVisible(false);
                allGladiators24.setVisible(false);
                allGladiators25.setVisible(false);
            }
            if (numberOfPlayers >= 3) {
                labAllGladiators31.setText("<html>" + allG31);
                labAllGladiators32.setText("<html>" + allG32);
                labAllGladiators33.setText("<html>" + allG33);
                labAllGladiators34.setText("<html>" + allG34);
                labAllGladiators35.setText("<html>" + allG35);
            } else {
                allGladiators31.setVisible(false);
                allGladiators32.setVisible(false);
                allGladiators33.setVisible(false);
                allGladiators34.setVisible(false);
                allGladiators35.setVisible(false);
            }
            if (numberOfPlayers >= 4) {
                labAllGladiators41.setText("<html>" + allG41);
                labAllGladiators42.setText("<html>" + allG42);
                labAllGladiators43.setText("<html>" + allG43);
                labAllGladiators44.setText("<html>" + allG44);
                labAllGladiators45.setText("<html>" + allG45);
            } else {
                allGladiators41.setVisible(false);
                allGladiators42.setVisible(false);
                allGladiators43.setVisible(false);
                allGladiators44.setVisible(false);
                allGladiators45.setVisible(false);
            }

            for (int i = 1; i <= numberOfPlayers; i++) {
                if (P[i].howManyGladiators > 0) {
                    howManyPlayersStillPlay++;
                }

            }

            //hra konci kdyz je aktivnich hracu mene nebo rovno nez jeden
            if (howManyPlayersStillPlay <= 1) {
                END();
            }
            howManyPlayersStillPlay = 0;

            //umirani gladiatoru
            for (int i = 1; i <= numberOfGladiators; i++) {
                if (G[i].lives <= 0) {
                    numberOfGladiators--;
                    G[i].kaput = true;
                    P[G[i].owener].howManyGladiators--;

                    G[i + 40].nationGladiator = G[i].nationGladiator;
                    G[i + 40].isHandOneAnable = G[i].isHandOneAnable;
                    G[i + 40].isHandTwoAnable = G[i].isHandTwoAnable;
                    G[i + 40].isHandThreeAnable = G[i].isHandThreeAnable;
                    G[i + 40].kaput = G[i].kaput;
                    G[i + 40].level = G[i].level;
                    G[i + 40].lives = G[i].lives;
                    G[i + 40].lvlWeaponOne = G[i].lvlWeaponOne;
                    G[i + 40].lvlWeaponThree = G[i].lvlWeaponThree;
                    G[i + 40].lvlWeaponTwo = G[i].lvlWeaponTwo;
                    G[i + 40].name = G[i].name;
                    G[i + 40].owener = G[i].owener;
                    G[i + 40].weaponOne = G[i].weaponOne;
                    G[i + 40].weaponTwo = G[i].weaponTwo;
                    G[i + 40].weaponOneAfter = G[i].weaponOneAfter;
                    G[i + 40].weaponThree = G[i].weaponThree;
                    G[i + 40].weaponTwoAfter = G[i].weaponTwoAfter;

                    for (int z = i; z < numberOfGladiators + 1; z++) {
                        G[z].nationGladiator = G[z + 1].nationGladiator;
                        G[z].isHandOneAnable = G[z + 1].isHandOneAnable;
                        G[z].isHandTwoAnable = G[z + 1].isHandTwoAnable;
                        G[z].isHandThreeAnable = G[z + 1].isHandThreeAnable;
                        G[z].kaput = G[z + 1].kaput;
                        G[z].level = G[z + 1].level;
                        G[z].lives = G[z + 1].lives;
                        G[z].lvlWeaponOne = G[z + 1].lvlWeaponOne;
                        G[z].lvlWeaponThree = G[z + 1].lvlWeaponThree;
                        G[z].lvlWeaponTwo = G[z + 1].lvlWeaponTwo;
                        G[z].name = G[z + 1].name;
                        G[z].owener = G[z + 1].owener;
                        G[z].weaponOne = G[z + 1].weaponOne;
                        G[z].weaponTwo = G[z + 1].weaponTwo;
                        G[z].weaponOneAfter = G[z + 1].weaponOneAfter;
                        G[z].weaponThree = G[z + 1].weaponThree;
                        G[z].weaponTwoAfter = G[z + 1].weaponTwoAfter;
                    }

                }

            }

            if (fightTrue == true) {

                setText();

                if (G[x].lives <= 0) {
                    checkLives();
                }
                if (G[y].lives <= 0) {
                    checkLives();
                }
                if (wantToEnd.isVisible() == false && sineMissione == false) {
                    checkLives();
                }

                //poškození
                if (W[G[x].weaponOne][x][1].harm == 2) {
                    if (W[G[x].weaponOne][x][1].canBeThrowen > 0) {
                        throwingFirst = true;
                        G[x].weaponOne = 2;
                        W[G[x].weaponOne][x][1].harm = 0;
                        setText();
                        if (helpThrowerSecond == 0) {
                            counterFirst = 2;
                            gladiatorTwoFight.setVisible(true);
                            gladiatorOneFight.setVisible(false);
                            if (W[G[x].weaponThree][x][3].healtIfItIsAnimal > 0) {
                                gladiator.setVisible(true);
                                animal.setVisible(true);
                                gladiator.setBackground(Color.blue);
                                animal.setBackground(Color.blue);
                            } else {
                                gladiator.setVisible(false);
                                animal.setVisible(false);
                            }
                        }
                        helpThrowerFirst = 0;
                    }
                    W[G[x].weaponOne][x][1].harm = 0;
                    G[x].weaponOne = 2;
                    W[G[x].weaponOne][x][1].harm = 0;
                    setText();
                }
                if (W[G[x].weaponTwo][x][2].harm == 2) {
                    if (W[G[x].weaponTwo][x][2].canBeThrowen > 0) {
                        throwingSecond = true;
                        G[x].weaponTwo = 2;
                        W[G[x].weaponTwo][x][2].harm = 0;
                        setText();
                        if (helpThrowerFirst == 0) {
                            counterFirst = 2;
                            gladiatorTwoFight.setVisible(true);
                            gladiatorOneFight.setVisible(false);
                            if (W[G[x].weaponThree][x][3].healtIfItIsAnimal > 0) {
                                gladiator.setVisible(true);
                                animal.setVisible(true);
                                gladiator.setBackground(Color.blue);
                                animal.setBackground(Color.blue);
                            } else {
                                gladiator.setVisible(false);
                                animal.setVisible(false);
                            }
                        }
                        helpThrowerSecond = 0;
                    }
                    W[G[x].weaponTwo][x][2].harm = 0;
                    G[x].weaponTwo = 2;
                    W[G[x].weaponTwo][x][2].harm = 0;
                    setText();

                }

                if (W[G[y].weaponOne][y][1].harm == 2) {
                    if (W[G[y].weaponOne][y][1].canBeThrowen > 0) {
                        throwingThird = true;
                        G[y].weaponOne = 2;
                        W[G[y].weaponOne][y][1].harm = 0;
                        setText();
                        if (helpThrowerFourth == 0) {
                            counterSecond = 2;
                            gladiatorTwoFight.setVisible(false);
                            gladiatorOneFight.setVisible(true);
                            if (W[G[y].weaponThree][y][3].healtIfItIsAnimal > 0) {
                                gladiator.setVisible(true);
                                animal.setVisible(true);
                                gladiator.setBackground(Color.blue);
                                animal.setBackground(Color.blue);
                            } else {
                                gladiator.setVisible(false);
                                animal.setVisible(false);
                            }
                        }
                        helpThrowerThird = 0;
                    }
                    W[G[y].weaponOne][y][1].harm = 0;
                    G[y].weaponOne = 2;
                    W[G[y].weaponOne][y][1].harm = 0;
                    setText();
                }

                if (W[G[y].weaponTwo][y][2].harm == 2) {

                    if (W[G[y].weaponTwo][y][2].canBeThrowen > 0) {
                        throwingFourth = true;
                        G[y].weaponTwo = 2;
                        W[G[y].weaponTwo][y][2].harm = 0;
                        setText();
                        if (helpThrowerThird == 0) {
                            counterSecond = 2;
                            gladiatorTwoFight.setVisible(false);
                            gladiatorOneFight.setVisible(true);
                            if (W[G[y].weaponThree][y][3].healtIfItIsAnimal > 0) {
                                gladiator.setVisible(true);
                                animal.setVisible(true);
                                gladiator.setBackground(Color.blue);
                                animal.setBackground(Color.blue);
                            } else {
                                gladiator.setVisible(false);
                                animal.setVisible(false);
                            }
                        }
                        helpThrowerFourth = 0;
                    }

                    W[G[y].weaponTwo][y][2].harm = 0;
                    G[y].weaponTwo = 2;
                    W[G[y].weaponTwo][y][2].harm = 0;
                    setText();

                }

                //zde se urcuje jaka zbran muze byt kde pouzita
                if (W[G[x].weaponThree][x][3].healtIfItIsAnimal <= 0) {
                    G[x].weaponThree = 1;
                    setText();
                }

                if (W[G[y].weaponThree][y][3].healtIfItIsAnimal <= 0) {
                    G[y].weaponThree = 1;
                    setText();
                }

                if (W[G[x].weaponOne][x][3].healtIfItIsAnimal > 0) {
                    G[x].weaponOne = 2;
                    setText();
                }

                if (W[G[y].weaponOne][y][3].healtIfItIsAnimal > 0) {
                    G[y].weaponOne = 2;
                    setText();
                }

                if (W[G[x].weaponTwo][x][3].healtIfItIsAnimal > 0) {
                    G[x].weaponTwo = 2;
                    setText();
                }

                if (W[G[y].weaponTwo][y][3].healtIfItIsAnimal > 0) {
                    G[y].weaponTwo = 2;
                    setText();
                }

                if (G[x].lives <= 0) {
                    checkLives();
                }
                if (G[y].lives <= 0) {
                    checkLives();
                }
                if (wantToEnd.isVisible() == false && sineMissione == false) {
                    checkLives();
                }

            }
        }

    });
}


