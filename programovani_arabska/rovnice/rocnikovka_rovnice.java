package rovnice;

import static java.lang.Math.random;
import java.util.Random;

public class rocnikovka_rovnice {

    public static String rovnice = "";
    public static double x = 0;
    public static double x2 = 0;
    public static double x3 = 0;
    public static int pocetAbs = 0;
    public static int zacatekAbs = 0;
    public static int zacatekAbs2 = -1;
    public static int konecAbs = 0;
    public static int konecAbs2 = -1;
    public static double sx2 = 0;
    public static double sx = 0;
    public static double bezx = 0;
    public static int pocetClenu = 0;
    public static double[] a;
    public static boolean[] ax;
    public static boolean[] ax2;
    public static int rovnitko = 0;
    public static Random RND = new Random();
    public static boolean[] podAbs;
    public static boolean[] podAbs2;
    public static boolean podAbsBude;
    public static boolean podAbsBude2;
    public static double kritickyBod1 = 0;
    public static double kritickyBod2 = 0;
    public static double sxAbs = 0;
    public static double bezxAbs = 0;
    public static double sxAbs2 = 0;
    public static double bezxAbs2 = 0;
    public static double parametr;
//začátek nefunkční absolutní hodnoty
//Tato část kódu by byla vhodná při plném zprovoznění generátoru rovnic s neznámou v absolutní hodnotě

    public static void konstruktorAbs() {
        podAbs = new boolean[pocetClenu];
        podAbs2 = new boolean[pocetClenu];
        pocetAbs = RND.nextInt(2) + 1;

        zacatekAbs = RND.nextInt(pocetClenu);
        konecAbs = RND.nextInt(2) + 1 + zacatekAbs;
        while (!((konecAbs < rovnitko && zacatekAbs < rovnitko) || (konecAbs > rovnitko && zacatekAbs > rovnitko))) {
            zacatekAbs = RND.nextInt(pocetClenu);
            konecAbs = RND.nextInt(2) + 1 + zacatekAbs;
        }
        if (pocetAbs == 2) {
            zacatekAbs2 = RND.nextInt(pocetClenu);
            konecAbs2 = RND.nextInt(2) + 1 + zacatekAbs2;
            while (!((konecAbs2 < rovnitko && zacatekAbs2 < rovnitko) || (konecAbs2 > rovnitko && zacatekAbs2 > rovnitko))
                    || zacatekAbs2 == zacatekAbs || zacatekAbs2 == konecAbs || konecAbs2 == zacatekAbs || konecAbs2 == konecAbs
                    || !((konecAbs2 < zacatekAbs && zacatekAbs2 < zacatekAbs) || (konecAbs2 > zacatekAbs && zacatekAbs2 > zacatekAbs))
                    || !((konecAbs2 < konecAbs && zacatekAbs2 < konecAbs) || (konecAbs2 > konecAbs && zacatekAbs2 > konecAbs))) {
                zacatekAbs2 = RND.nextInt(pocetClenu);
                konecAbs2 = RND.nextInt(2) + 1 + zacatekAbs2;
            }
        }
        if (pocetClenu <= konecAbs || pocetClenu <= konecAbs2) {
            konstruktorAbs();
        }
    }

    public static void abs() {
        sx = 0;
        bezx = 0;
        String rovnice2 = "";

        for (int i = 0; i < pocetClenu; i++) {
            int jeTamX = RND.nextInt(2);
            a[i] = RND.nextInt(20) - 7;
            if (podAbsBude == true) {
                podAbs[i] = true;
            } else {
                podAbs[i] = false;
            }

            if (podAbsBude2 == true) {
                podAbs2[i] = true;
            } else {
                podAbs2[i] = false;
            }

            if (i == zacatekAbs) {
                podAbsBude = true;
                podAbs[i] = true;
                rovnice2 = rovnice2 + "----";
                rovnice2 = rovnice2 + (a[i] + "x + ");
                ax[i] = true;
                if (rovnitko >= i) {
                    sx = sx + a[i];
                }
                if (rovnitko < i) {
                    sx = sx + (a[i] * -1);
                }
            } else {
                if (i == zacatekAbs2) {
                    podAbsBude2 = true;
                    podAbs2[i] = true;
                    rovnice2 = rovnice2 + "----";
                    rovnice2 = rovnice2 + (a[i] + "x + ");
                    ax[i] = true;

                    if (rovnitko >= i) {
                        sx = sx + a[i];
                    }
                    if (rovnitko < i) {
                        sx = sx + (a[i] * -1);
                    }
                } else {

                    if (jeTamX == 0) {

                        rovnice2 = rovnice2 + (a[i] + "x + ");
                        ax[i] = true;
                        if (rovnitko >= i) {
                            sx = sx + a[i];
                        }
                        if (rovnitko < i) {
                            sx = sx + (a[i] * -1);
                        }
                    }
                    if (jeTamX == 1) {
                        rovnice2 = rovnice2 + (a[i] + " + ");
                        ax[i] = false;
                        if (rovnitko >= i) {
                            bezx = bezx + a[i];
                        }
                        if (rovnitko < i) {
                            bezx = bezx + (a[i] * -1);
                        }
                    }
                    if (rovnitko == i) {
                        rovnice2 = rovnice2 + ("= ");
                    }
                }
            }
            if (i == konecAbs) {
                rovnice2 = rovnice2 + "----";
                podAbsBude = false;
            }
            if (i == konecAbs2) {
                rovnice2 = rovnice2 + "----";
                podAbsBude2 = false;
            }
        }
        absDopocet1();
        if (x < kritickyBod1 && pocetAbs == 1) {
            abs();
            rovnice2 = "";
        }
        if ((x < kritickyBod1 || x < kritickyBod2) && pocetAbs == 2) {
            abs();
            rovnice2 = "";
        }
        parametr = sx * x + bezx;
        rovnice2 = rovnice2 + parametr + "";
        if (parametr % 1 != 0 || sx == 0) {
            abs();
            rovnice2 = "";
        } else {
            if (parametr % 1 == 0) {
                rovnice = rovnice2;
                pocetClenu++;
            }
        }
        absDopocet2();
    }

    public static void absDopocet1() {
        for (int o = 0; o < pocetClenu - 1; o++) {
            if (podAbs[o] == true) {
                if (ax[o] == true) {
                    sxAbs = sxAbs + a[o];
                }
                if (ax[o] == false) {
                    bezxAbs = bezxAbs + a[o];
                }
            }
            kritickyBod1 = bezxAbs / sxAbs;

            if (podAbs2[o] == true) {
                if (ax[o] == true) {
                    sxAbs2 = sxAbs2 + a[o];
                }
                if (ax[o] == false) {
                    bezxAbs2 = bezxAbs2 + a[o];
                }
            }
            kritickyBod2 = bezxAbs2 / sxAbs2;
        }
    }

    public static void absDopocet2() {
        sx = sx - 2 * (sxAbs + sxAbs2);
        bezx = bezx - 2 * (bezxAbs + bezxAbs2) + parametr;
        x2 = bezx / sx;
    }
//konec nefunkční absolutní hodnoty

    public static void zaklad() {
        sx = 0;
        bezx = 0;
        String rovnice2 = "";

        for (int i = 0; i < pocetClenu; i++) {
            int jeTamX = RND.nextInt(2);
            a[i] = RND.nextInt(20) - 7;

            if (jeTamX == 0) {
                rovnice2 = rovnice2 + (a[i] + "x + ");
                ax[i] = true;
                if (rovnitko >= i) {
                    sx = sx + a[i];
                }
                if (rovnitko < i) {
                    sx = sx + (a[i] * -1);
                }
            }
            if (jeTamX == 1) {
                rovnice2 = rovnice2 + (a[i] + " + ");
                ax[i] = false;
                if (rovnitko >= i) {
                    bezx = bezx + a[i];
                }
                if (rovnitko < i) {
                    bezx = bezx + (a[i] * -1);
                }
            }
            if (rovnitko == i) {
                rovnice2 = rovnice2 + ("= ");
            }
        }
        parametr = sx * x + bezx;
        rovnice2 = rovnice2 + parametr + "";
        if (parametr % 1 != 0 || sx == 0) {
            zaklad();
            rovnice2 = "";
        } else {
            if (parametr % 1 == 0) {
                rovnice = rovnice2;
                pocetClenu++;
            }
        }
        rovnice = rovnice.replace("+ -", "- ");
        rovnice = rovnice.replace("+ =", "=");
        rovnice = rovnice.replace("x*x", "x²");
    }

    public static void kvadra() {
        sx2 = 0;
        sx = 0;
        bezx = 0;
        String rovnice2 = "";

        for (int i = 0; i < pocetClenu - 1; i++) {
            int jeTamX = RND.nextInt(3);
            a[i] = RND.nextInt(20) - 7;

            if (jeTamX == 2) {
                rovnice2 = rovnice2 + (a[i] + "x*x + ");
                ax[i] = false;
                ax2[i] = true;
                if (rovnitko >= i) {
                    sx2 = sx2 + a[i];
                }
                if (rovnitko < i) {
                    sx2 = sx2 + (a[i] * -1);
                }
            }

            if (jeTamX == 0) {
                rovnice2 = rovnice2 + (a[i] + "x + ");
                ax[i] = true;
                ax2[i] = false;
                if (rovnitko >= i) {
                    sx = sx + a[i];
                }
                if (rovnitko < i) {
                    sx = sx + (a[i] * -1);
                }
            }
            if (jeTamX == 1) {
                rovnice2 = rovnice2 + (a[i] + " + ");
                ax[i] = false;
                ax2[i] = false;
                if (rovnitko >= i) {
                    bezx = bezx + a[i];
                }
                if (rovnitko < i) {
                    bezx = bezx + (a[i] * -1);
                }
            }
            if (rovnitko == i) {
                rovnice2 = rovnice2 + ("= ");
            }
        }
        parametr = sx2 * x * x + sx * x + bezx;
        rovnice2 = rovnice2 + parametr + "";
        if (parametr % 1 != 0 || sx2 == 0) {
            kvadra();
            rovnice2 = "";
        } else {
            if (parametr % 1 == 0) {
                if (((sx * sx) - (4 * sx2 * (bezx + (- 1 * parametr)))) > 0) {
                    x2 = (-sx - ((sx * sx) - (4 * sx2 * (bezx + (- 1 * parametr))))) / (2 * sx2);
                    if (x == x2) {
                        x2 = (-sx + ((sx * sx) - (4 * sx2 * (bezx + (- 1 * parametr))))) / (2 * sx2);
                    }
                    if (((x2 * 10) + 10) % 1 != 0 || x2 > 50 || x2 < - 20) {
                        kvadra();
                        rovnice2 = "";
                    } else {

                        rovnice = rovnice2;
                        pocetClenu++;
                        a = new double[pocetClenu];
                        ax = new boolean[pocetClenu];
                        ax2 = new boolean[pocetClenu];
                        a[pocetClenu - 1] = parametr;
                    }
                } else {
                    if (((sx * sx) - (4 * sx2 * (bezx + (- 1 * parametr)))) == 0) {
                        int omezeni = RND.nextInt(2);
                        if (omezeni == 0) {
                            x2 = x;

                            rovnice = rovnice2;
                            pocetClenu++;
                            a = new double[pocetClenu];
                            ax = new boolean[pocetClenu];
                            ax2 = new boolean[pocetClenu];
                            a[pocetClenu - 1] = parametr;
                        } else {
                            kvadra();
                            rovnice2 = "";
                        }
                    } else {
                        if (((sx * sx) - (4 * sx2 * (bezx + (- 1 * parametr)))) < 0) {
                            int omezeni = RND.nextInt(2);
                            if (omezeni == 0) {
                                x2 = -0.001;
                                x = -0.001;

                                rovnice = rovnice2;
                                pocetClenu++;
                            } else {
                                kvadra();
                                rovnice2 = "";
                            }
                        }
                    }
                }
            }
        }
        rovnice = rovnice.replace("+ -", "- ");
        rovnice = rovnice.replace("+ =", "=");
        rovnice = rovnice.replace("x*x", "x²");
    }
}
