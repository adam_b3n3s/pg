package rovnice;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Random;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import static rovnice.rocnikovka_rovnice.RND;
import static rovnice.rocnikovka_rovnice.a;
import static rovnice.rocnikovka_rovnice.ax;
import static rovnice.rocnikovka_rovnice.ax2;
import static rovnice.rocnikovka_rovnice.kvadra;
import static rovnice.rocnikovka_rovnice.pocetAbs;
import static rovnice.rocnikovka_rovnice.pocetClenu;
import static rovnice.rocnikovka_rovnice.podAbs;
import static rovnice.rocnikovka_rovnice.podAbs2;
import static rovnice.rocnikovka_rovnice.rovnice;
import static rovnice.rocnikovka_rovnice.rovnitko;
import static rovnice.rocnikovka_rovnice.x;
import static rovnice.rocnikovka_rovnice.x2;
import static rovnice.rocnikovka_rovnice.zaklad;

public class grafika extends JFrame implements MouseListener {

    static JFrame jf = new JFrame("ROVNICE");
    static JPanel panNazev = new JPanel();
    static JLabel labNazev = new JLabel();
    static JPanel panVyber = new JPanel();
    static JPanel panOdeslat = new JPanel();
    static JLabel labOdeslat = new JLabel();
    static JPanel panHrac = new JPanel();
    static JLabel labHrac = new JLabel();
    static JPanel panRovnice = new JPanel();
    static JLabel labRovnice = new JLabel();
    static JPanel panBody = new JPanel();
    static JLabel labBody = new JLabel();
    static JPanel panCasomira = new JPanel();
    static JLabel labCasomira = new JLabel();
    static JPanel panOdeslatVysledek = new JPanel();
    static JLabel labOdeslatVysledek = new JLabel();
    static JPanel panOprava = new JPanel();
    static JLabel labOprava = new JLabel();
    static JComboBox pocetHracu1 = new JComboBox();
    static JComboBox druhRovnice1 = new JComboBox();
    static JComboBox obtiznost1 = new JComboBox();
    static JComboBox pocetKol1 = new JComboBox();
    static JTextField tvujVysledek1 = new JTextField();
    static JTextField tvujVysledek2 = new JTextField();
    static JPanel panVysledek1 = new JPanel();
    static JLabel labVysledek1 = new JLabel();
    static JPanel panVysledek2 = new JPanel();
    static JLabel labVysledek2 = new JLabel();
    static JPanel panPikachu = new JPanel();
    static JPanel panRaichu = new JPanel();

    static JPanel panTabule = new JPanel();
    static JLabel labTabule = new JLabel();

    static JPanel pohybMysi = new JPanel();
    static int mysX;
    static int mysY;
    static int pocetHracu;
    static int druhRovnice;
    static int obtiznost;
    static int pocetKol;
    static int kolovac = 0;
    static int hracCislo = 1;
    static boolean odecet = false;
    static double vysledek1;
    static double vysledek2;
    static String vysledekTXT = "";
    public static Random RND2 = new Random();
    static int vyberRovnice;
    static boolean AI = false;
    static int momentalniObtiznost;
    static int casomira;
    static int body;
    static boolean AIBude = false;
    static int score1 = 0;
    static int score2 = 0;
    static int score3 = 0;
    static int score4 = 0;
    static boolean odeslatVysledekJenJednou = true;
    static pikachu pikaPika = new pikachu();
    static raichu raiRai = new raichu();

    public static void main(String[] args) {

        jf.setSize(710, 380);
        jf.setBackground(Color.GRAY);
        jf.setLocation(0, 0);
        jf.setVisible(true);
        jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jf.setLayout(null);
        
        jf.add(panPikachu);
        panPikachu.setOpaque(false);
        panPikachu.setLocation(0, 0);
        panPikachu.setSize(710, 380);
        panPikachu.setVisible(true);
        panPikachu.setLayout(null);
        panPikachu.add(pikaPika);
        pikaPika.setLocation(290, 225);
        pikaPika.setSize(120, 120);
        pikaPika.setVisible(false);
        
        jf.add(panRaichu);
        panRaichu.setOpaque(false);
        panRaichu.setLocation(0, 0);
        panRaichu.setSize(710, 380);
        panRaichu.setVisible(true);
        panRaichu.setLayout(null);
        panRaichu.add(raiRai);
        raiRai.setLocation(290, 225);
        raiRai.setSize(120, 120);
        raiRai.setVisible(false);
        
        

        jf.add(panTabule);
        panTabule.setVisible(false);
        panTabule.setLocation(0, 150);
        panTabule.setSize(700, 220);
        panTabule.setBackground(Color.DARK_GRAY);
        panTabule.add(labTabule);
        labTabule.setForeground(Color.WHITE);

        jf.add(pocetHracu1);
        pocetHracu1.setVisible(true);
        pocetHracu1.setLocation(0, 75);
        pocetHracu1.setSize(125, 50);
        pocetHracu1.addItem("hrát solo");
        pocetHracu1.addItem("hrát ve 2");
        pocetHracu1.addItem("hrát ve 3");
        pocetHracu1.addItem("hrát ve 4");

        jf.add(druhRovnice1);
        druhRovnice1.setVisible(true);
        druhRovnice1.setLocation(125, 75);
        druhRovnice1.setSize(125, 50);
        druhRovnice1.addItem("klasické rovnice");
        druhRovnice1.addItem("kvadratické rovnice");
        druhRovnice1.addItem("oboje");

        jf.add(obtiznost1);
        obtiznost1.setVisible(true);
        obtiznost1.setLocation(250, 75);
        obtiznost1.setSize(125, 50);
        obtiznost1.addItem("obtížnost: lehká");
        obtiznost1.addItem("obtížnost: střední");
        obtiznost1.addItem("obtížnost: těžká");
        obtiznost1.addItem("obtížnost: nemožná");

        jf.add(pocetKol1);
        pocetKol1.setVisible(true);
        pocetKol1.setLocation(375, 75);
        pocetKol1.setSize(125, 50);
        pocetKol1.addItem("pocet kol: 1");
        pocetKol1.addItem("pocet kol: 3");
        pocetKol1.addItem("pocet kol: 6");
        pocetKol1.addItem("pocet kol: 12");

        jf.add(panNazev);
        panNazev.setVisible(true);
        panNazev.setLocation(0, 0);
        panNazev.setSize(700, 50);
        panNazev.setBackground(Color.DARK_GRAY);
        panNazev.add(labNazev);
        labNazev.setForeground(Color.WHITE);
        labNazev.setText("ŘEŠENÍ ROVNIC");

        jf.add(panOdeslat);
        panOdeslat.setVisible(true);
        panOdeslat.setLocation(500, 75);
        panOdeslat.setSize(100, 50);
        panOdeslat.setBackground(Color.DARK_GRAY);
        panOdeslat.add(labOdeslat);
        labOdeslat.setForeground(Color.WHITE);
        labOdeslat.setText("ODESLAT ZADANI");

        jf.add(panVyber);
        panVyber.setVisible(true);
        panVyber.setLocation(0, 50);
        panVyber.setSize(700, 100);
        panVyber.setBackground(Color.GRAY);

        jf.add(panHrac);
        panHrac.setVisible(false);
        panHrac.setLocation(0, 150);
        panHrac.setSize(700, 30);
        panHrac.setBackground(Color.DARK_GRAY);
        panHrac.add(labHrac);
        labHrac.setForeground(Color.WHITE);
        labHrac.setText("Hraje hrac cislo: " + hracCislo);

        jf.add(panRovnice);
        panRovnice.setVisible(false);
        panRovnice.setLocation(0, 180);
        panRovnice.setSize(700, 30);
        panRovnice.setBackground(Color.GRAY);
        panRovnice.add(labRovnice);
        labRovnice.setForeground(Color.WHITE);

        jf.add(tvujVysledek1);
        tvujVysledek1.setVisible(false);
        tvujVysledek1.setLocation(100, 210);
        tvujVysledek1.setSize(400, 50);
        jf.add(tvujVysledek2);
        tvujVysledek2.setVisible(false);
        tvujVysledek2.setLocation(100, 260);
        tvujVysledek2.setSize(400, 50);

        jf.add(panVysledek1);
        panVysledek1.setVisible(false);
        panVysledek1.setLocation(0, 210);
        panVysledek1.setSize(100, 50);
        panVysledek1.setBackground(Color.GRAY);
        panVysledek1.add(labVysledek1);
        labVysledek1.setForeground(Color.WHITE);
        labVysledek1.setText("Napis vysledek: ");

        jf.add(panVysledek2);
        panVysledek2.setVisible(false);
        panVysledek2.setLocation(0, 260);
        panVysledek2.setSize(100, 50);
        panVysledek2.setBackground(Color.GRAY);
        panVysledek2.add(labVysledek2);
        labVysledek2.setForeground(Color.WHITE);
        labVysledek2.setText("Napis vysledek: ");

        jf.add(panCasomira);
        panCasomira.setVisible(false);
        panCasomira.setLocation(600, 210);
        panCasomira.setSize(100, 50);
        panCasomira.setBackground(Color.GRAY);
        panCasomira.add(labCasomira);
        labCasomira.setForeground(Color.WHITE);
        labCasomira.setText("casomira");

        jf.add(panBody);
        panBody.setVisible(false);
        panBody.setLocation(600, 260);
        panBody.setSize(100, 50);
        panBody.setBackground(Color.GRAY);
        panBody.add(labBody);
        labBody.setForeground(Color.WHITE);
        labBody.setText("body");

        jf.add(panOdeslatVysledek);
        panOdeslatVysledek.setVisible(false);
        panOdeslatVysledek.setLocation(500, 210);
        panOdeslatVysledek.setSize(100, 100);
        panOdeslatVysledek.setBackground(Color.DARK_GRAY);
        panOdeslatVysledek.add(labOdeslatVysledek);
        labOdeslatVysledek.setForeground(Color.WHITE);
        labOdeslatVysledek.setText("odeslat vysledek");

        jf.add(panOprava);
        panOprava.setVisible(false);
        panOprava.setLocation(0, 310);
        panOprava.setSize(700, 30);
        panOprava.add(labOprava);
        labOprava.setForeground(Color.WHITE);

        jf.add(pohybMysi);
        pohybMysi.setLocation(0, 0);
        pohybMysi.setSize(710, 380);
        pohybMysi.setOpaque(false);
        pohybMysi.setVisible(true);

        grafika mys = new grafika();

        casovac.start();
        casovac2.start();
        pohybMysi.addMouseListener(mys);

        jf.repaint();
    }

    static int ubyvajiciCas = 15;
    static javax.swing.Timer casovac = new javax.swing.Timer(100, new ActionListener() {
        public void actionPerformed(ActionEvent e) {
            if (pocetHracu1.getSelectedIndex() == 0) {
                obtiznost1.setVisible(true);
            }
            if (pocetHracu1.getSelectedIndex() > 0) {
                obtiznost1.setVisible(false);
            }
            if (odecet == true) {
                ubyvajiciCas--;
                if (ubyvajiciCas == 0) {

                    x = RND.nextInt(10) - 3;
                    pocetClenu = RND.nextInt(7) + 8;
                    rovnitko = RND.nextInt(pocetClenu);
                    if (rovnitko != 0) {
                        rovnitko--;
                    }
                    a = new double[pocetClenu];
                    ax = new boolean[pocetClenu];
                    ax2 = new boolean[pocetClenu];
                    if (druhRovnice == 1) {
                        momentalniObtiznost = 0;
                        zaklad();
                        labRovnice.setText("rovnice je: " + rovnice);
                    }
                    if (druhRovnice == 2) {
                        momentalniObtiznost = 1;
                        kvadra();
                        labRovnice.setText("rovnice je: " + rovnice);
                    }
                    if (druhRovnice == 3) {
                        vyberRovnice = RND2.nextInt(2);
                        if (vyberRovnice == 0) {
                            momentalniObtiznost = 0;
                            zaklad();
                        }
                        if (vyberRovnice == 1) {
                            momentalniObtiznost = 1;
                            kvadra();
                        }
                        labRovnice.setText("rovnice je: " + rovnice);
                    }
                    tvujVysledek1.setText("");
                    tvujVysledek2.setText("");
                    hracCislo++;
                    odeslatVysledekJenJednou = true;
                    tvujVysledek1.setEnabled(true);
                    tvujVysledek2.setEnabled(true);
                    casomira = 50;
                    body = 5;
                    casovac3.start();
                    if (hracCislo == 2 && AIBude) {
                        AI = true;
                    }
                    if (hracCislo > pocetHracu) {
                        kolovac++;
                        hracCislo = 1;
                    }
                    labHrac.setText("Hraje hrac cislo: " + hracCislo);
                    panOprava.setVisible(false);
                    odecet = false;
                    ubyvajiciCas = 15;
                    if (kolovac == pocetKol) {
                        panTabule.setVisible(true);
                        if (pocetHracu == 2) {
                            labTabule.setText("<html> počet bodů prvního hráče: " + score1 + "<br>" + "počet bodů druhého hráče: " + score2);
                        }
                        if (pocetHracu == 3) {
                            labTabule.setText("<html> počet bodů prvního hráče: " + score1 + "<br>" + "počet bodů druhého hráče: " + score2 + "<br>" + "počet bodů třetího hráče: " + score3);
                        }
                        if (pocetHracu == 4) {
                            labTabule.setText("<html> počet bodů prvního hráče: " + score1 + "<br>" + "počet bodů druhého hráče: " + score2 + "<br>" + "počet bodů třetího hráče: " + score3 + "<br>" + "počet bodů čtvrtého hráče: " + score4);
                        }
                        if(obtiznost == 4 && score1 > score2) {
                            raiRai.setVisible(true);
                        }else{
                            pikaPika.setVisible(true);
                        }
                        panOdeslat.setVisible(true);
                        kolovac = 0;
                        panHrac.setVisible(false);
                        panRovnice.setVisible(false);
                        tvujVysledek1.setVisible(false);
                        tvujVysledek2.setVisible(false);
                        panVysledek1.setVisible(false);
                        panVysledek2.setVisible(false);
                        panCasomira.setVisible(false);
                        panBody.setVisible(false);
                        panOdeslatVysledek.setVisible(false);
                        pocetHracu1.setEnabled(true);
                        druhRovnice1.setEnabled(true);
                        obtiznost1.setEnabled(true);
                        pocetKol1.setEnabled(true);
                        AIBude = false;
                    }
                }
            }

        }
    });
    static int z;
    static double meziZ;
    static javax.swing.Timer casovac2 = new javax.swing.Timer(100, new ActionListener() {
        public void actionPerformed(ActionEvent e) {
            if (AI == true && hracCislo == 2) {
                tvujVysledek1.setEnabled(false);
                tvujVysledek2.setEnabled(false);
                if (obtiznost == 1) {
                    casomira = 50 - RND.nextInt(20) - 30;
                    z = RND2.nextInt(4) - 2;
                    meziZ = x + z;
                    tvujVysledek1.setText("" + meziZ);
                    z = RND2.nextInt(4) - 2;
                    meziZ = x2 + z;
                    tvujVysledek2.setText("" + meziZ);
                    if (momentalniObtiznost == 0) {
                        tvujVysledek2.setText("");
                    }
                }
                if (obtiznost == 2) {
                    casomira = 50 - RND.nextInt(40) - 10;
                    z = RND2.nextInt(3) - 1;
                    meziZ = x + z;
                    tvujVysledek1.setText("" + meziZ);
                    z = RND2.nextInt(3) - 1;
                    meziZ = x2 + z;
                    tvujVysledek2.setText("" + meziZ);
                    if (momentalniObtiznost == 0) {
                        tvujVysledek2.setText("");
                    }
                }
                if (obtiznost == 3) {
                    casomira = 50 - RND.nextInt(20);
                    z = RND2.nextInt(2) - 1;
                    meziZ = x + z;
                    tvujVysledek1.setText("" + meziZ);
                    z = RND2.nextInt(2) - 1;
                    meziZ = x2 + z;
                    tvujVysledek2.setText("" + meziZ);
                    if (momentalniObtiznost == 0) {
                        tvujVysledek2.setText("");
                    }
                }
                if (obtiznost == 4) {
                    casomira = 50 - RND.nextInt(10);
                    z = RND2.nextInt(100);
                    if (z == 50) {
                        meziZ = x + 1;
                        tvujVysledek1.setText("" + meziZ);
                    } else {
                        tvujVysledek1.setText("" + x);
                    }
                    z = RND2.nextInt(100);
                    if (z == 50) {
                        meziZ = x2 + 1;
                        tvujVysledek2.setText("" + meziZ);
                    } else {
                        tvujVysledek2.setText("" + x2);
                    }
                    if (momentalniObtiznost == 0) {
                        tvujVysledek2.setText("");
                    }

                }
                casovac3.stop();
                if (casomira > 40) {
                    body = 5;
                }
                if (casomira > 30 && casomira < 40) {
                    body = 4;
                }
                if (casomira > 20 && casomira < 30) {
                    body = 3;
                }
                if (casomira > 10 && casomira < 20) {
                    body = 2;
                }
                if (casomira > 0 && casomira < 10) {
                    body = 1;
                }
                if (casomira == 0) {
                    body = 0;
                    x = x + 10;
                    x2 = x2 + 10;
                }
                labCasomira.setText("casomira: " + casomira);
                labBody.setText("body: " + body);
                vysledekTXT = tvujVysledek1.getText();
                vysledekTXT = vysledekTXT.replaceAll(",", ".");
                vysledekTXT = vysledekTXT.replaceAll(" ", "");
                vysledekTXT = vysledekTXT.replaceAll("[^0-9.-]", "");
                try {
                    vysledek1 = Double.parseDouble(vysledekTXT);
                } catch (Exception ex) {
                    vysledek1 = 0;
                }
                panOprava.setVisible(true);

                if (momentalniObtiznost == 1) {
                    vysledekTXT = tvujVysledek2.getText();
                    vysledekTXT = vysledekTXT.replaceAll(",", ".");
                    vysledekTXT = vysledekTXT.replaceAll(" ", "");
                    vysledekTXT = vysledekTXT.replaceAll("[^0-9.-]", "");
                    try {
                        vysledek2 = Double.parseDouble(vysledekTXT);
                    } catch (Exception ex) {
                        vysledek2 = 0;
                    }
                    panOprava.setVisible(true);
                }

                odecet = true;
                if (momentalniObtiznost == 0) {
                    if (vysledek1 == x) {
                        panOprava.setBackground(Color.GREEN);
                        labOprava.setText("SPRÁVNĚ!");
                        if (hracCislo == 1) {
                            score1 += body;
                        }
                        if (hracCislo == 2) {
                            score2 += body;
                        }
                        if (hracCislo == 3) {
                            score3 += body;
                        }
                        if (hracCislo == 4) {
                            score4 += body;
                        }
                    } else {
                        ubyvajiciCas = 50;
                        panOprava.setBackground(Color.RED);
                        labOprava.setText("ŠPATNĚ! výsledek rovnice je: x1 = " + x);
                        body = 0;
                        labBody.setText("body: " + body);
                    }
                }
                if (momentalniObtiznost == 1) {
                    if ((vysledek1 == x && vysledek2 == x2) || (vysledek1 == x2 && vysledek2 == x)) {
                        panOprava.setBackground(Color.GREEN);
                        labOprava.setText("SPRÁVNĚ!");
                        if (hracCislo == 1) {
                            score1 += body;
                        }
                        if (hracCislo == 2) {
                            score2 += body;
                        }
                        if (hracCislo == 3) {
                            score3 += body;
                        }
                        if (hracCislo == 4) {
                            score4 += body;
                        }
                    } else {
                        ubyvajiciCas = 50;
                        panOprava.setBackground(Color.RED);
                        labOprava.setText("ŠPATNĚ! výsledek rovnice je: x1 = " + x + ", x2 = " + x2);
                        body = 0;
                        labBody.setText("body: " + body);
                    }
                }
                AI = false;
            }
        }
    });
    static javax.swing.Timer casovac3 = new javax.swing.Timer(1000, new ActionListener() {
        public void actionPerformed(ActionEvent e) {
            casomira--;
            if (casomira == 40) {
                body--;
            }
            if (casomira == 30) {
                body--;
            }
            if (casomira == 20) {
                body--;
            }
            if (casomira == 10) {
                body--;
            }

            labCasomira.setText("casomira: " + casomira);
            labBody.setText("body: " + body);
            if (casomira == 0) {
                odeslatVysledekJenJednou = false;
                tvujVysledek1.setEnabled(false);
                tvujVysledek2.setEnabled(false);
                body = 0;
                casovac3.stop();
                vysledekTXT = tvujVysledek1.getText(); 
                vysledekTXT = vysledekTXT.replaceAll(",", ".");
                vysledekTXT = vysledekTXT.replaceAll(" ", "");
                vysledekTXT = vysledekTXT.replaceAll("[^0-9.-]", "");
                try {
                    vysledek1 = Double.parseDouble(vysledekTXT);
                } catch (Exception ex) {
                    vysledek1 = 0;
                }
                panOprava.setVisible(true);

                if (momentalniObtiznost == 1) {
                    vysledekTXT = tvujVysledek2.getText();
                    vysledekTXT = vysledekTXT.replaceAll(",", ".");
                    vysledekTXT = vysledekTXT.replaceAll(" ", "");
                    vysledekTXT = vysledekTXT.replaceAll("[^0-9.-]", "");
                    try {
                        vysledek2 = Double.parseDouble(vysledekTXT);
                    } catch (Exception ex) {
                        vysledek2 = 0;
                    }
                    panOprava.setVisible(true);
                }
                odecet = true;
                if (momentalniObtiznost == 0) {
                    if (vysledek1 == x) {
                        panOprava.setBackground(Color.GREEN);
                        labOprava.setText("SPRÁVNĚ!");
                        if (hracCislo == 1) {
                            score1 += body;
                        }
                        if (hracCislo == 2) {
                            score2 += body;
                        }
                        if (hracCislo == 3) {
                            score3 += body;
                        }
                        if (hracCislo == 4) {
                            score4 += body;
                        }
                    } else {
                        ubyvajiciCas = 50;
                        panOprava.setBackground(Color.RED);
                        labOprava.setText("ŠPATNĚ! výsledek rovnice je: x1 = " + x);
                        body = 0;
                        labBody.setText("body: " + body);
                    }
                }
                if (momentalniObtiznost == 1) {
                    if ((vysledek1 == x && vysledek2 == x2) || (vysledek1 == x2 && vysledek2 == x)) {
                        panOprava.setBackground(Color.GREEN);
                        labOprava.setText("SPRÁVNĚ!");
                        if (hracCislo == 1) {
                            score1 += body;
                        }
                        if (hracCislo == 2) {
                            score2 += body;
                        }
                        if (hracCislo == 3) {
                            score3 += body;
                        }
                        if (hracCislo == 4) {
                            score4 += body;
                        }
                    } else {
                        ubyvajiciCas = 50;
                        panOprava.setBackground(Color.RED);
                        labOprava.setText("ŠPATNĚ! výsledek rovnice je: x1 = " + x + ", x2 = " + x2);
                        body = 0;
                        labBody.setText("body: " + body);
                    }
                }
            }

        }
    });

    @Override
    public void mouseReleased(MouseEvent e) {
        mysX = e.getX();
        mysY = e.getY();

        if (mysX >= 500 && mysX <= 600 && mysY >= 75 && mysY <= 125 && panOdeslat.isVisible()) {
            panOdeslat.setVisible(false);
            pikaPika.setVisible(false);
            raiRai.setVisible(false);
            panTabule.setVisible(false);
            pocetHracu = pocetHracu1.getSelectedIndex() + 1;
            druhRovnice = druhRovnice1.getSelectedIndex() + 1;
            obtiznost = obtiznost1.getSelectedIndex() + 1;
            pocetKol = pocetKol1.getSelectedIndex() + 1;
            casomira = 50;
            body = 5;
            casovac3.start();
            if (pocetKol == 2) {
                pocetKol = 3;
            } else {
                if (pocetKol == 3) {
                    pocetKol = 6;
                } else {
                    if (pocetKol == 4) {
                        pocetKol = 12;
                    }
                }
            }
            if (pocetHracu > 1) {
                obtiznost = 0;
            }

            panHrac.setVisible(true);
            panRovnice.setVisible(true);
            tvujVysledek1.setVisible(true);
            tvujVysledek2.setVisible(true);
            panVysledek1.setVisible(true);
            panVysledek2.setVisible(true);
            panCasomira.setVisible(true);
            panBody.setVisible(true);
            panOdeslatVysledek.setVisible(true);
            pocetHracu1.setEnabled(false);
            druhRovnice1.setEnabled(false);
            obtiznost1.setEnabled(false);
            pocetKol1.setEnabled(false);

            x = RND.nextInt(10) - 3;
            pocetClenu = RND.nextInt(7) + 8;
            rovnitko = RND.nextInt(pocetClenu);
            if (rovnitko != 0) {
                rovnitko--;
            }
            a = new double[pocetClenu];
            ax = new boolean[pocetClenu];
            ax2 = new boolean[pocetClenu];
            if (druhRovnice == 1) {
                zaklad();
                momentalniObtiznost = 0;
                labRovnice.setText("rovnice je: " + rovnice);
            }
            if (druhRovnice == 2) {
                kvadra();
                momentalniObtiznost = 1;
                labRovnice.setText("rovnice je: " + rovnice);
            }
            if (druhRovnice == 3) {
                vyberRovnice = RND2.nextInt(2);
                if (vyberRovnice == 0) {
                    zaklad();
                    momentalniObtiznost = 0;
                }
                if (vyberRovnice == 1) {
                    kvadra();
                    momentalniObtiznost = 1;
                }
                labRovnice.setText("rovnice je: " + rovnice);
            }
            if (pocetHracu == 1) {
                pocetHracu++;
                AI = true;
                AIBude = true;
            }
        }

        if (mysX >= 500 && mysX <= 600 && mysY >= 210 && mysY <= 310 && panOdeslatVysledek.isVisible() && odeslatVysledekJenJednou == true) {
            odeslatVysledekJenJednou = false;
            tvujVysledek1.setEnabled(false);
            tvujVysledek2.setEnabled(false);
            casovac3.stop();
            vysledekTXT = tvujVysledek1.getText();
            vysledekTXT = vysledekTXT.replaceAll(",", ".");
            vysledekTXT = vysledekTXT.replaceAll(" ", "");
            vysledekTXT = vysledekTXT.replaceAll("[^0-9.-]", "");
            try {
                vysledek1 = Double.parseDouble(vysledekTXT);
            } catch (Exception ex) {
                vysledek1 = 0;
            }
            panOprava.setVisible(true);

            if (momentalniObtiznost == 1) {
                vysledekTXT = tvujVysledek2.getText();
                vysledekTXT = vysledekTXT.replaceAll(",", ".");
                vysledekTXT = vysledekTXT.replaceAll(" ", "");
                vysledekTXT = vysledekTXT.replaceAll("[^0-9.-]", "");
                try {
                    vysledek2 = Double.parseDouble(vysledekTXT);
                } catch (Exception ex) {
                    vysledek2 = 0;
                }
                panOprava.setVisible(true);
            }
            odecet = true;
            if (momentalniObtiznost == 0) {
                if (vysledek1 == x) {
                    panOprava.setBackground(Color.GREEN);
                    labOprava.setText("SPRÁVNĚ!");
                    if (hracCislo == 1) {
                        score1 += body;
                    }
                    if (hracCislo == 2) {
                        score2 += body;
                    }
                    if (hracCislo == 3) {
                        score3 += body;
                    }
                    if (hracCislo == 4) {
                        score4 += body;
                    }
                } else {
                    ubyvajiciCas = 50;
                    panOprava.setBackground(Color.RED);
                    labOprava.setText("ŠPATNĚ! výsledek rovnice je: x1 = " + x);
                    body = 0;
                    labBody.setText("body: " + body);
                }
            }
            if (momentalniObtiznost == 1) {
                if ((vysledek1 == x && vysledek2 == x2) || (vysledek1 == x2 && vysledek2 == x)) {
                    panOprava.setBackground(Color.GREEN);
                    labOprava.setText("SPRÁVNĚ!");
                    if (hracCislo == 1) {
                        score1 += body;
                    }
                    if (hracCislo == 2) {
                        score2 += body;
                    }
                    if (hracCislo == 3) {
                        score3 += body;
                    }
                    if (hracCislo == 4) {
                        score4 += body;
                    }
                } else {
                    ubyvajiciCas = 50;
                    panOprava.setBackground(Color.RED);
                    labOprava.setText("ŠPATNĚ! výsledek rovnice je: x1 = " + x + ", x2 = " + x2);
                    body = 0;
                    labBody.setText("body: " + body);
                }
            }
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }
}
