package rovnice;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;

public class raichu extends JPanel {

    public void paint(Graphics g) {
        g.setColor(Color.ORANGE);
        g.fillRect(0, 0, 110, 110);
        
        g.setColor(Color.BLACK);
        g.fillRect(5 + 15 + 10 - 5, 0 + 10 - 5, 5, 5);
        g.fillRect(0 + 15 + 10 - 5, 5 + 10 - 5, 5, 5);
        g.fillRect(5 + 15 + 10 - 5, 5 + 10 - 5, 5, 5);
        g.fillRect(5 + 15 + 10 - 5, 10 + 10 - 5, 5, 5);
        g.fillRect(10 + 15 + 10 - 5, 10 + 10 - 5, 5, 5);
        g.fillRect(15 + 15 + 10 - 5, 10 + 10 - 5, 5, 5);
        g.fillRect(0 + 15 + 10 - 5, 10 + 10 - 5, 5, 5);
        g.fillRect(10 + 15 + 10 - 5, 0 + 10 - 5, 5, 5);
        g.fillRect(15 + 15 + 10 - 5, 5 + 10 - 5, 5, 5);
        g.fillRect(5 + 15 + 10 - 5, 15 + 10 - 5, 5, 5);
        g.fillRect(10 + 15 + 10 - 5, 15 + 10 - 5, 5, 5);
        g.setColor(Color.WHITE);
        g.fillRect(10 + 15 + 10 - 5, 5 + 10 - 5, 5, 5);

        g.setColor(Color.BLACK);
        g.fillRect(35 + 35 + 10 - 5, 0 + 10 - 5, 5, 5);
        g.fillRect(30 + 35 + 10 - 5, 5 + 10 - 5, 5, 5);
        g.fillRect(35 + 35 + 10 - 5, 5 + 10 - 5, 5, 5);
        g.fillRect(35 + 35 + 10 - 5, 10 + 10 - 5, 5, 5);
        g.fillRect(40 + 35 + 10 - 5, 10 + 10 - 5, 5, 5);
        g.fillRect(45 + 35 + 10 - 5, 10 + 10 - 5, 5, 5);
        g.fillRect(30 + 35 + 10 - 5, 10 + 10 - 5, 5, 5);
        g.fillRect(40 + 35 + 10 - 5, 0 + 10 - 5, 5, 5);
        g.fillRect(45 + 35 + 10 - 5, 5 + 10 - 5, 5, 5);
        g.fillRect(35 + 35 + 10 - 5, 15 + 10 - 5, 5, 5);
        g.fillRect(40 + 35 + 10 - 5, 15 + 10 - 5, 5, 5);
        g.setColor(Color.WHITE);
        g.fillRect(40 + 35 + 10 - 5, 5 + 10 - 5, 5, 5);

        g.setColor(Color.YELLOW);
        g.fillRect(5 + 10 - 5, 0 + 20 + 10 - 5, 5, 5);
        g.fillRect(0 + 10 - 5, 5 + 20 + 10 - 5, 5, 5);
        g.fillRect(5 + 10 - 5, 5 + 20 + 10 - 5, 5, 5);
        g.fillRect(5 + 10 - 5, 10 + 20 + 10 - 5, 5, 5);
        g.fillRect(10 + 10 - 5, 10 + 20 + 10 - 5, 5, 5);
        g.fillRect(15 + 10 - 5, 10 + 20 + 10 - 5, 5, 5);
        g.fillRect(0 + 10 - 5, 10 + 20 + 10 - 5, 5, 5);
        g.fillRect(10 + 10 - 5, 0 + 20 + 10 - 5, 5, 5);
        g.fillRect(15 + 10 - 5, 5 + 20 + 10 - 5, 5, 5);
        g.fillRect(5 + 10 - 5, 15 + 20 + 10 - 5, 5, 5);
        g.fillRect(10 + 10 - 5, 15 + 20 + 10 - 5, 5, 5);
        g.fillRect(10 + 10 - 5, 5 + 20 + 10 - 5, 5, 5);

        g.fillRect(5 + 80 + 10 - 5, 0 + 20 + 10 - 5, 5, 5);
        g.fillRect(0 + 80 + 10 - 5, 5 + 20 + 10 - 5, 5, 5);
        g.fillRect(5 + 80 + 10 - 5, 5 + 20 + 10 - 5, 5, 5);
        g.fillRect(5 + 80 + 10 - 5, 10 + 20 + 10 - 5, 5, 5);
        g.fillRect(10 + 80 + 10 - 5, 10 + 20 + 10 - 5, 5, 5);
        g.fillRect(15 + 80 + 10 - 5, 10 + 20 + 10 - 5, 5, 5);
        g.fillRect(0 + 80 + 10 - 5, 10 + 20 + 10 - 5, 5, 5);
        g.fillRect(10 + 80 + 10 - 5, 0 + 20 + 10 - 5, 5, 5);
        g.fillRect(15 + 80 + 10 - 5, 5 + 20 + 10 - 5, 5, 5);
        g.fillRect(5 + 80 + 10 - 5, 15 + 20 + 10 - 5, 5, 5);
        g.fillRect(10 + 80 + 10 - 5, 15 + 20 + 10 - 5, 5, 5);
        g.fillRect(10 + 80 + 10 - 5, 5 + 20 + 10 - 5, 5, 5);

        g.setColor(Color.BLACK);
        g.fillRect(0 + 45 + 10 - 5, 0 + 20 + 10 - 5, 5, 5);
        g.fillRect(0 + 50 + 10 - 5, 0 + 20 + 10 - 5, 5, 5);

        g.fillRect(0 + 45 + 10 - 5, 0 + 30 + 10 - 5, 5, 5);
        g.fillRect(0 + 50 + 10 - 5, 0 + 30 + 10 - 5, 5, 5);
        g.fillRect(0 + 35 + 10 - 5, 0 + 35 + 10 - 5, 5, 5);
        g.fillRect(0 + 40 + 10 - 5, 0 + 35 + 10 - 5, 5, 5);
        g.fillRect(0 + 55 + 10 - 5, 0 + 35 + 10 - 5, 5, 5);
        g.fillRect(0 + 60 + 10 - 5, 0 + 35 + 10 - 5, 5, 5);
        g.fillRect(0 + 30 + 10 - 5, 0 + 30 + 10 - 5, 5, 5);
        g.fillRect(0 + 65 + 10 - 5, 0 + 30 + 10 - 5, 5, 5);

        g.fillRect(0 + 35 + 10 - 5, 0 + 60 + 10 - 5, 5, 5);
        g.fillRect(0 + 60 + 10 - 5, 0 + 60 + 10 - 5, 5, 5);
        g.fillRect(0 + 35 + 10 - 5, 0 + 65 + 10 - 5, 5, 5);
        g.fillRect(0 + 60 + 10 - 5, 0 + 65 + 10 - 5, 5, 5);

        g.fillRect(0 + 40 + 10 - 5, 0 + 70 + 10 - 5, 5, 5);
        g.fillRect(0 + 55 + 10 - 5, 0 + 70 + 10 - 5, 5, 5);
        g.fillRect(0 + 40 + 10 - 5, 0 + 75 + 10 - 5, 5, 5);
        g.fillRect(0 + 55 + 10 - 5, 0 + 75 + 10 - 5, 5, 5);
        g.fillRect(0 + 40 + 10 - 5, 0 + 70 + 10 + 10 - 5, 5, 5);
        g.fillRect(0 + 55 + 10 - 5, 0 + 70 + 10 + 10 - 5, 5, 5);
        g.fillRect(0 + 40 + 10 - 5, 0 + 75 + 10 + 10 - 5, 5, 5);
        g.fillRect(0 + 55 + 10 - 5, 0 + 75 + 10 + 10 - 5, 5, 5);
        g.fillRect(0 + 40 + 10 - 5, 0 + 75 + 10 + 10 - 5, 5, 5);
        g.fillRect(0 + 55 + 10 - 5, 0 + 75 + 10 + 10 - 5, 5, 5);
        g.fillRect(0 + 40 + 10 - 5, 0 + 80 + 10 + 10 - 5, 5, 5);
        g.fillRect(0 + 55 + 10 - 5, 0 + 80 + 10 + 10 - 5, 5, 5);
        g.fillRect(0 + 35 + 10 - 5, 0 + 85 + 10 + 10 - 5, 5, 5);
        g.fillRect(0 + 60 + 10 - 5, 0 + 85 + 10 + 10 - 5, 5, 5);
        g.fillRect(0 + 30 + 10 - 5, 0 + 85 + 10 + 10 - 5, 5, 5);
        g.fillRect(0 + 65 + 10 - 5, 0 + 85 + 10 + 10 - 5, 5, 5);
        g.fillRect(0 + 25 + 10 - 5, 0 + 80 + 10 + 10 - 5, 5, 5);
        g.fillRect(0 + 70 + 10 - 5, 0 + 80 + 10 + 10 - 5, 5, 5);
        g.fillRect(0 + 20 + 10 - 5, 0 + 75 + 10 + 10 - 5, 5, 5);
        g.fillRect(0 + 75 + 10 - 5, 0 + 75 + 10 + 10 - 5, 5, 5);
        g.fillRect(0 + 20 + 10 - 5, 0 + 70 + 10 + 10 - 5, 5, 5);
        g.fillRect(0 + 75 + 10 - 5, 0 + 70 + 10 + 10 - 5, 5, 5);
        g.fillRect(0 + 15 + 10 - 5, 0 + 65 + 10 + 10 - 5, 5, 5);
        g.fillRect(0 + 80 + 10 - 5, 0 + 65 + 10 + 10 - 5, 5, 5);
        g.fillRect(0 + 15 + 10 - 5, 0 + 60 + 10 + 10 - 5, 5, 5);
        g.fillRect(0 + 80 + 10 - 5, 0 + 60 + 10 + 10 - 5, 5, 5);
    }
}
