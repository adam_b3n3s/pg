



package futsal;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Random;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class futsal extends JFrame implements MouseMotionListener, MouseListener {

    static JPanel MYS = new JPanel();
    static JFrame F = new JFrame("Florbal");
    static plocha P = new plocha();
    static micek M = new micek();
    static JPanel panNapisPohyb = new JPanel();
    static JLabel labNapisPohyb = new JLabel();
    static JPanel panNapisZacatek = new JPanel();
    static JLabel labNapisZacatek = new JLabel();
    static JPanel panNapisTym = new JPanel();
    static JLabel labNapisTym = new JLabel();
    static JPanel panNapisRozehravka = new JPanel();
    static JLabel labNapisRozehravka = new JLabel();
    static JPanel panNapisPrihravka = new JPanel();
    static JLabel labNapisPrihravka = new JLabel();
    static JPanel panNapisBraneni = new JPanel();
    static JLabel labNapisBraneni = new JLabel();
    static JPanel panNapisKonec = new JPanel();
    static JLabel labNapisKonec = new JLabel();
    static JPanel panNapissCerveni = new JPanel();
    static JLabel labNapissCerveni = new JLabel();
    static JPanel panNapissCerni = new JPanel();
    static JLabel labNapissCerni = new JLabel();
    static JPanel panNapisStrela = new JPanel();
    static JLabel labNapisStrela = new JLabel();

    static int mysX = 0;
    static int mysY = 0;
    static hrac H[] = new hrac[10];
    static int r = 0;
    static int hAkt = 0;
    static int z = 0;
    static boolean tym = true;
    static int pomo = 0;
    static boolean faul = false;
    static boolean bylFaul = false;
    static int t = 1;
    static int sCerveni = 0;
    static int sCerni = 0;
    static int sCerveni2 = 0;
    static int sCerni2 = 0;
    static double pomer = 0;
    static int MX = 0;
    static int MY = 0;
    static int qqq = 0;
    static boolean qqqq = false;
    static boolean qqqqq = false;
    static int brankar = 38;
    static int akce = 0;
    static int akce2 = 3;
    static int akce3 = 3;
    static boolean pohybNe = false;
    static boolean brankar2 = false;
    static boolean pohybNe2 = false;
    static double MD1 = 0;
    static double MD2 = 0;
    static double MD3 = 0;
    static double MD4 = 0;
    static double MD5 = 0;
    static double MD6 = 0;
    static double MD7 = 0;
    static double MD8 = 0;
    public static int hPrih = 0;
    public static int prihX = 0;
    public static int prihY = 0;
    public static int kPrihravky = 0;
    public static double kPrihravky2 = 0;
    public static boolean muzesPrihravat = true;
    public static boolean kontrola = false;
    static int fCerveni = 0;
    static int fCerni = 0;
    
    

    public static void main(String[] args) {
        Font VelkyF = new Font("Sans", Font.BOLD, 26);

        F.setSize(1600, 1000);
        F.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        M.x = 75;
        M.y = 37;
        M.minX = 75;
        M.minY = 37;

        F.add(MYS);
        MYS.setLocation(0, 0);
        MYS.setSize(1600, 1000);
        MYS.setVisible(true);
        MYS.setOpaque(false);

        F.add(panNapisZacatek);
        panNapisZacatek.setLocation(0, 750);
        panNapisZacatek.setSize(1500, 400);
        panNapisZacatek.setBackground(Color.DARK_GRAY);
        panNapisZacatek.setVisible(true);

        panNapisZacatek.add(labNapisZacatek);
        labNapisZacatek.setText("zacatek hry");
        labNapisZacatek.setFont(VelkyF);
        labNapisZacatek.setForeground(Color.WHITE);

        F.add(panNapisPohyb);
        panNapisPohyb.setLocation(0, 750);
        panNapisPohyb.setSize(200, 100);
        panNapisPohyb.setBackground(Color.DARK_GRAY);
        panNapisPohyb.setVisible(false);

        panNapisPohyb.add(labNapisPohyb);
        labNapisPohyb.setText("pohyb");
        labNapisPohyb.setFont(VelkyF);
        labNapisPohyb.setForeground(Color.WHITE);

        F.add(panNapisPrihravka);
        panNapisPrihravka.setLocation(200, 750);
        panNapisPrihravka.setSize(200, 100);
        panNapisPrihravka.setBackground(Color.DARK_GRAY);
        panNapisPrihravka.setVisible(false);

        panNapisPrihravka.add(labNapisPrihravka);
        labNapisPrihravka.setText("prihravka");
        labNapisPrihravka.setFont(VelkyF);
        labNapisPrihravka.setForeground(Color.WHITE);

        F.add(panNapisBraneni);
        panNapisBraneni.setLocation(400, 750);
        panNapisBraneni.setSize(200, 100);
        panNapisBraneni.setBackground(Color.DARK_GRAY);
        panNapisBraneni.setVisible(false);

        panNapisBraneni.add(labNapisBraneni);
        labNapisBraneni.setText("braneni");
        labNapisBraneni.setFont(VelkyF);
        labNapisBraneni.setForeground(Color.WHITE);

        F.add(panNapisStrela);
        panNapisStrela.setLocation(600, 750);
        panNapisStrela.setSize(200, 100);
        panNapisStrela.setBackground(Color.DARK_GRAY);
        panNapisStrela.setVisible(false);

        panNapisStrela.add(labNapisStrela);
        labNapisStrela.setText("strela");
        labNapisStrela.setFont(VelkyF);
        labNapisStrela.setForeground(Color.WHITE);

        F.add(panNapisRozehravka);
        panNapisRozehravka.setLocation(0, 750);
        panNapisRozehravka.setSize(1500, 400);
        panNapisRozehravka.setBackground(Color.BLACK);
        panNapisRozehravka.setVisible(false);

        panNapisRozehravka.add(labNapisRozehravka);
        labNapisRozehravka.setText("rozehravka");
        labNapisRozehravka.setFont(VelkyF);
        labNapisRozehravka.setForeground(Color.WHITE);

        F.add(panNapisTym);
        panNapisTym.setLocation(550, 870);
        panNapisTym.setSize(400, 100);
        panNapisTym.setBackground(Color.BLACK);
        panNapisTym.setVisible(true);

        panNapisTym.add(labNapisTym);
        labNapisTym.setText("CAS: " + t);
        labNapisTym.setFont(VelkyF);
        labNapisTym.setForeground(Color.WHITE);

        F.add(panNapisKonec);
        panNapisKonec.setLocation(0, 0);
        panNapisKonec.setSize(1600, 1000);
        panNapisKonec.setBackground(Color.BLACK);
        panNapisKonec.setVisible(false);

        panNapisKonec.add(labNapisKonec);
        labNapisKonec.setText("Konec hry");
        labNapisKonec.setFont(VelkyF);
        labNapisKonec.setForeground(Color.WHITE);

        F.add(panNapissCerni);
        panNapissCerni.setLocation(150, 870);
        panNapissCerni.setSize(400, 100);
        panNapissCerni.setBackground(Color.BLACK);
        panNapissCerni.setVisible(false);

        panNapissCerni.add(labNapissCerni);
        labNapissCerni.setText("" + sCerni);
        labNapissCerni.setFont(VelkyF);
        labNapissCerni.setForeground(Color.WHITE);

        F.add(panNapissCerveni);
        panNapissCerveni.setLocation(950, 870);
        panNapissCerveni.setSize(400, 100);
        panNapissCerveni.setBackground(Color.RED);
        panNapissCerveni.setVisible(false);

        panNapissCerveni.add(labNapissCerveni);
        labNapissCerveni.setText("" + sCerveni);
        labNapissCerveni.setFont(VelkyF);
        labNapissCerveni.setForeground(Color.WHITE);

        F.add(P);

        futsal m = new futsal();
        MYS.addMouseMotionListener(m);
        MYS.addMouseListener((MouseListener) m);

        F.setVisible(true);

        //Hráči
        for (int r = 0; r < 5; r++) {
            H[r] = new hrac();
            H[r].y = 5;
            H[r].x = (r * 10) + 5;
        }
        for (int r = 5; r < 10; r++) {
            H[r] = new hrac();
            H[r].y = 5;
            H[r].x = (r * 10) + 54;
        }
        Vykresleni();
    }

    public void mouseDragged(MouseEvent e) {
        mysX = (e.getX()) / 10;
        mysY = (e.getY()) / 10;

        F.repaint();
    }

    public void mouseMoved(MouseEvent e) {
        mysX = (e.getX()) / 10;
        mysY = (e.getY()) / 10;

        F.repaint();
    }

    public void mouseClicked(MouseEvent e) {
        mysX = (e.getX()) / 10;
        mysY = (e.getY()) / 10;
        //označení hráče
        if (e.getClickCount() >= 2 && !panNapisZacatek.isVisible() && !panNapisRozehravka.isVisible() && faul == false) {
            for (int r = 0; r < 10; r++) {
                H[r].oznacen = false;
            }
            if(Vzdalenost(13, brankar, mysX, mysY) < 2 && tym == true && M.x == 13 && M.y == brankar){
                panNapisPohyb.setVisible(true);
                labNapisPohyb.setText("prihravka");
                pohybNe = true;
                brankar2 = false;
            }
            if(Vzdalenost(136, brankar, mysX, mysY) < 2 && tym == false && M.x == 136 && M.y == brankar){
                panNapisPohyb.setVisible(true);
                labNapisPohyb.setText("prihravka");
                pohybNe = true;
                brankar2 = true;
            }
            for (int r = 0; r < 5; r++) {
                H[r].oznacen = false;
                if (Vzdalenost(H[r].x, H[r].y, mysX, mysY) < 3 && tym == true) {
                    hAkt = r;
                    panNapisPohyb.setVisible(true);
                    panNapisPrihravka.setVisible(true);
                    panNapisBraneni.setVisible(true);
                    panNapisStrela.setVisible(true);
                    H[r].oznacen = true;
                }
            }

            for (int r = 5; r < 10; r++) {
                H[r].oznacen = false;
                if (H[r].x == mysX && H[r].y == mysY && tym == false) {
                    hAkt = r;
                    panNapisPohyb.setVisible(true);
                    panNapisPrihravka.setVisible(true);
                    panNapisBraneni.setVisible(true);
                    panNapisStrela.setVisible(true);
                    H[r].oznacen = true;
                }
            }

            Magnet();
            Vykresleni();
        }
        
        
        
        
        
        F.repaint();
    }

    public void mousePressed(MouseEvent e) {
        mysX = (e.getX()) / 10;
        mysY = (e.getY()) / 10;

        F.repaint();
    }

    public void mouseReleased(MouseEvent e) {
        mysX = (e.getX()) / 10;
        mysY = (e.getY()) / 10;

        sCerni2 = sCerni;
        sCerveni2 = sCerveni;

//HRA------------------------------------------------
        if (!panNapisZacatek.isVisible() && !panNapisRozehravka.isVisible() && faul == false) {
            muzesPrihravat = true;
            kontrola = false;
            //pohyb----------------------------------------------
            if (mysX >= 0 && mysY >= 75 && mysX <= 20 && mysY <= 85
                    && panNapisPohyb.isVisible() == true
                    && H[hAkt].hracPohyboval == false
                    && qqq == 0 && pohybNe == false) {
                H[hAkt].pohyb = true;
            }

            panNapisPohyb.setVisible(false);
            for (int r = 0; r < 10; r++) {

                if (H[r].pohyb == true
                        && Vzdalenost(mysX, mysY, H[r].x, H[r].y) <= 20
                        && mysX < 150
                        && mysY < 75
                        && mysX >= 0 && mysY >= 0 && qqq == 0) {
                    if (H[r].x == M.x && H[r].y == M.y) {
                        M.x = mysX;
                        M.y = mysY;
                    }
                    H[r].x = mysX;
                    H[r].y = mysY;

                    KontrolaHrace(r);
                    Magnet();

                    H[hAkt].pohyb = false;
                    H[hAkt].hracPohyboval = true;
                }
            }

            //konec pohybu---------------------------------------------
            //prihravka-------------------------------------------------------
            if (mysX >= 0 && mysY >= 75 && mysX <= 20 && mysY <= 85 && qqq == 0 && pohybNe == true) {
                pohybNe2 = true;
                pohybNe = false;
            }
            panNapisPohyb.setVisible(false);

            if (pohybNe2 == true && Vzdalenost(mysX, mysY, M.x, M.y) <= 40 && mysX < 150 && mysY < 75 && mysX >= 0 && mysY >= 0 && qqq == 0 && brankar2 == false) {
                M.rychX = mysX - 13;
                M.rychY = mysY - brankar;
                M.x = mysX;
                M.y = mysY;
                Magnet();
                M.rych = false;
                M.rych2 = false;
                akce++;
                pohybNe = false;
                pohybNe2 = false;
                
            labNapisPohyb.setText("pohyb");
            }
            if (pohybNe2 == true && Vzdalenost(mysX, mysY, M.x, M.y) <= 40 && mysX < 150 && mysY < 75 && mysX >= 0 && mysY >= 0 && qqq == 0 && brankar2 == true) {
                M.rychX = mysX - 136;
                M.rychY = mysY - brankar;
                M.x = mysX;
                M.y = mysY;
                Magnet();
                M.rych = false;
                M.rych2 = false;
                akce++;
                pohybNe = false;
                pohybNe2 = false;
                
            labNapisPohyb.setText("pohyb");
            }
            
            
            
            
            
            
            
            
            
            
            
            
            
            if (mysX >= 20 && mysY >= 75 && mysX <= 40 && mysY <= 85 && panNapisPrihravka.isVisible() == true && H[hAkt].hracPrihraval == false && H[hAkt].x == M.x && H[hAkt].y == M.y && qqq == 0) {
                H[hAkt].prihravka = true;
            }
            panNapisPrihravka.setVisible(false);

            if (H[hAkt].prihravka == true && Vzdalenost(mysX, mysY, M.x, M.y) <= 40 && mysX < 150 && mysY < 75 && mysX >= 0 && mysY >= 0 && H[hAkt].x == M.x && H[hAkt].y == M.y && qqq == 0) {
                M.rychX = mysX - H[hAkt].x;
                M.rychY = mysY - H[hAkt].y;
                
                hPrih = hAkt;
                z = hAkt;
                
                Prihravka();


                Magnet();
                H[hAkt].prihravka = false;
                H[hAkt].hracPrihraval = true;
                M.rych = false;
                M.rych2 = false;
                akce++;
            }

            //konec prihravka---------------------------------------------
            //zacatek strely---------------------------------------
            
            if (mysX >= 60 && mysY >= 75 && mysX <= 80 && mysY <= 85 && panNapisStrela.isVisible() == true
                    && H[hAkt].hracStrilel == false && H[hAkt].x == M.x
                    && H[hAkt].y == M.y && H[hAkt].x > 10 && H[hAkt].x < 140) {
                H[hAkt].strela = true;
                labNapisPohyb.setText("placirka");
                labNapisPrihravka.setText("nart");
                panNapisPohyb.setVisible(true);
                panNapisPrihravka.setVisible(true);
                qqq = 2;
            }
            panNapisStrela.setVisible(false);
            if (qqq == 1 && mysX >= 0 && mysY >= 75 && mysX <= 20 && mysY <= 85) {
                qqqq = true;
            }
            if (qqq == 1 && mysX >= 20 && mysY >= 75 && mysX <= 40 && mysY <= 85) {
                qqqqq = true;
            }
            if (qqq == 0){
                labNapisPohyb.setText("pohyb");
                labNapisPrihravka.setText("prihravka");
                panNapisPohyb.setVisible(false);
                panNapisPrihravka.setVisible(false);
            }
            if (qqq > 0){qqq = qqq -1;}

            if (H[hAkt].strela == true && H[hAkt].x == M.x && H[hAkt].y == M.y && qqqq == true) {
                if (hAkt < 5){
                int w = Vzdalenost(H[hAkt].x, H[hAkt].y, 140, 37);
                int q = kostka(w * w);
                if (akce2 == akce3){
                    q = q * 3;
                }
                if (Math.abs(akce2 - akce3) == 1){
                    q = q * 2;
                }
                if (q < 100){
                    sCerni = sCerni + 1;
                    labNapissCerni.setText("" + sCerni);
                }else{
                    M.x = 136;
                    M.y = brankar;
                    M.rychX = 0;
                    M.rychY = 0;
                    M.minX = 136;
                    M.minY = brankar;
                }
                }
                
                if (hAkt >= 5){
                int w = Vzdalenost(H[hAkt].x, H[hAkt].y, 10, 37);
                int q = kostka(w * w);
                if (akce2 == akce3){
                    q = q * 3;
                }
                if (Math.abs(akce2 - akce3) == 1){
                    q = q * 2;
                }
                if (q < 100){
                    sCerveni = sCerveni + 1;
                    labNapissCerveni.setText("" + sCerveni);
                }else{
                    M.x = 13;
                    M.y = brankar;
                    M.rychX = 0;
                    M.rychY = 0;
                    M.minX = 13;
                    M.minY = brankar;
                }
                }
                

                Magnet();
                H[hAkt].prihravka = false;
                H[hAkt].hracPrihraval = true;
                akce++;
            }
            if (H[hAkt].strela == true && H[hAkt].x == M.x && H[hAkt].y == M.y && qqqqq == true) {
                if (hAkt < 5){
                int w = Vzdalenost(H[hAkt].x, H[hAkt].y, 140, 37);
                int q = kostka(w);
                if (akce2 == akce3){
                    q = q * 3;
                }
                if (Math.abs(akce2 - akce3) == 1){
                    q = q * 2;
                }
                if (q < 8){
                    sCerni = sCerni + 1;
                    labNapissCerni.setText("" + sCerni);
                }else{
                    M.x = 136;
                    M.y = brankar;
                    M.rychX = 0;
                    M.rychY = 0;
                    M.minX = 136;
                    M.minY = brankar;
                }
                }
                
                if (hAkt >= 5){
                int w = Vzdalenost(H[hAkt].x, H[hAkt].y, 10, 37);
                int q = kostka(w);
                if (akce2 == akce3){
                    q = q * 3;
                }
                if (Math.abs(akce2 - akce3) == 1){
                    q = q * 2;
                }
                if (q < 8){
                    sCerveni = sCerveni + 1;
                    labNapissCerveni.setText("" + sCerveni);
                }else{
                    M.x = 13;
                    M.y = brankar;
                    M.rychX = 0;
                    M.rychY = 0;
                    M.minX = 13;
                    M.minY = brankar;
                }
                }
                

                Magnet();
                H[hAkt].prihravka = false;
                H[hAkt].hracPrihraval = true;
                akce++;
            }
            
            qqqq = false;
            qqqqq = false;

            
            
            
            
            
            
            
            //konec strely---------------------------------------
            //zacatek braneni---------------------------------------------
            for (r = 0; r < 10; r++) {
                if (H[r].x == M.x && H[r].y == M.y) {
                    H[r].MaMicek = true;
                    z = r;
                } else {
                    H[r].MaMicek = false;
                }
            }

            if (mysX >= 40 && mysY >= 75 && mysX <= 60 && mysY <= 85 && panNapisBraneni.isVisible() == true && H[hAkt].hracBranil == false && Vzdalenost(H[hAkt].x, H[hAkt].y, H[z].x, H[z].y) < 8) {
                H[hAkt].brani = true;
            }
            panNapisBraneni.setVisible(false);

            if (H[hAkt].brani == true) {

                int d = kostka(30);

                if (d == 1) {
                    M.x = H[hAkt].x;
                    M.y = H[hAkt].y;
                }
                if (d == 2) {
                    M.x = H[hAkt].x;
                    M.y = H[hAkt].y;
                }
                if (d == 3) {
                    M.x = H[hAkt].x;
                    M.y = H[hAkt].y;
                }
                if (d == 4) {
                    M.x = H[hAkt].x;
                    M.y = H[hAkt].y;
                }
                if (d == 5) {
                    M.x = H[hAkt].x;
                    M.y = H[hAkt].y;
                }
                if (d == 6) {
                    M.x = H[hAkt].x;
                    M.y = H[hAkt].y;
                }
                if (d == 7) {
                    bylFaul = true;
                    if (hAkt < 5) {
                        fCerni ++;
                    }
                    if (hAkt >= 5) {
                        fCerveni ++;
                    }
                }
                if (d == 8) {
                    bylFaul = true;
                    if (hAkt < 5) {
                        fCerni ++;
                    }
                    if (hAkt >= 5) {
                        fCerveni ++;
                    }
                }
                if (fCerni > 6) {
                    bylFaul = true;
                    M.x = 23;
                    M.y = 38;
                    fCerni = fCerni -6;
                }
                if (fCerveni > 6) {
                    bylFaul = true;
                    M.x = 126;
                    M.y = 38;
                    fCerveni = fCerveni -6;
                }
                

                Magnet();
                H[hAkt].brani = false;
                H[hAkt].hracBranil = true;
                akce++;
            }

            //konec braneni---------------------------------------------
            if (mysX >= 40 && mysY >= 75 && mysX <= 60 && mysY <= 85 && panNapisBraneni.isVisible() == true && H[hAkt].hracBranil == false && Vzdalenost(H[hAkt].x, H[hAkt].y, H[z].x, H[z].y) < 8) {
                H[hAkt].brani = true;
            }

            //Prepinani hracu - konec tahu-----------------------------------------------------------------------------
            M.minX = M.x;
            M.minY = M.y;
            MX = M.x;
            MY = M.y;

            if (mysX <= 95 && mysX >= 55 && mysY <= 97 && mysY >= 87 && tym == true) {
                tym = false;
                panNapisTym.setBackground(Color.RED);
                for (r = 0; r < 5; r++) {
                    H[r].hracPohyboval = false;
                    H[r].hracPrihraval = false;
                }
                for (r = 0; r < 10; r++) {
                    if (Vzdalenost(H[r].x, H[r].y, M.x, M.y) <= 3) {
                        Magnet();
                        M.rychX = 0;
                        M.rychY = 0;
                    }
                }

                
                    Prihravka();
                    
            } else {
                if (mysX <= 95 && mysX >= 55 && mysY <= 97 && mysY >= 87 && tym == false) {

                    tym = true;
                    panNapisTym.setBackground(Color.BLACK);
                    for (r = 5; r < 10; r++) {
                        H[r].hracPohyboval = false;
                        H[r].hracPrihraval = false;
                    }
                    for (r = 0; r < 10; r++) {
                        if (Vzdalenost(H[r].x, H[r].y, M.x, M.y) <= 3) {
                            Magnet();
                            M.rychX = 0;
                            M.rychY = 0;
                        }
                    }
                    
                    
                    
                    
                    
                    
                    
                    Prihravka();

                }
                if (mysX <= 95 && mysX >= 55 && mysY <= 97 && mysY >= 87) {
                    t++;
                    for (int r = 0; r < 10; r++) {
                        if (H[r].trest > 0) {
                            H[r].trest = H[r].trest - 1;
                            if (H[r].trest == 0) {
                                H[r].x = 75;
                                H[r].y = 73;
                            }
                        }
                    }
                    akce++;
                }
                labNapisTym.setText("CAS: " + t);

                if (t == 20) {
                    panNapisZacatek.setVisible(true);
                    panNapisRozehravka.setVisible(false);
                    for (int r = 0; r < 5; r++) {
                        t = 21;
                        M.x = 75;
                        M.y = 37;
                        H[r] = new hrac();
                        H[r].y = 5;
                        H[r].x = (r * 10) + 5;
                    }
                    for (int r = 5; r < 10; r++) {
                        H[r] = new hrac();
                        H[r].y = 5;
                        H[r].x = (r * 10) + 54;
                    }
                }
                if (t == 40) {
                    panNapisZacatek.setVisible(true);
                    panNapisRozehravka.setVisible(false);
                    for (int r = 0; r < 5; r++) {
                        t = 41;
                        M.x = 75;
                        M.y = 37;
                        H[r] = new hrac();
                        H[r].y = 5;
                        H[r].x = (r * 10) + 5;
                    }
                    for (int r = 5; r < 10; r++) {
                        H[r] = new hrac();
                        H[r].y = 5;
                        H[r].x = (r * 10) + 54;
                    }
                }
            }    //Prepinani hracu konec-----------------------------------------------------------------------------
            int brRedX = 140;
            int brY1 = 32;
            int brY2 = 43;
            int brBlackX = 9;
            
            
            
            
            //System.out.println("micek minuly " + M.minX + " micek soucasny " + M.x);
            H[hAkt].oznacen = false;

            r = 0;
        }
        if (t == 60) {
            panNapisKonec.setVisible(true);
            panNapisZacatek.setVisible(false);
            panNapisRozehravka.setVisible(false);
            panNapisTym.setVisible(false);

        }
        if (bylFaul == true) {
            H[z].hracPohyboval = true;
            H[z].hracPrihraval = false;

            if (mysX < 150 && mysY < 75 && mysX >= 0 && mysY >= 0) {
                if (r >= 0 && r < 5 && (Vzdalenost(H[z].x, H[z].y, mysX, mysY) > 15 || z == r)) {
                    H[r].x = mysX;
                    H[r].y = mysY;
                    H[z].x = M.x;
                    H[z].y = M.y;
                    KontrolaHrace(r);
                }

                if (r >= 5 && r < 10 && (Vzdalenost(H[z].x, H[z].y, mysX, mysY) > 15 || z == r)) {
                    H[r].x = mysX;
                    H[r].y = mysY;
                    H[z].x = M.x;
                    H[z].y = M.y;
                    KontrolaHrace(r);
                }
                if (Vzdalenost(H[z].x, H[z].y, mysX, mysY) > 15) {
                    r++;
                }
                if (z < 5) {
                    tym = true;
                    panNapisTym.setBackground(Color.BLACK);
                }
                if (z >= 5) {
                    tym = false;
                    panNapisTym.setBackground(Color.RED);
                }
                faul = true;
            }
            if (r == 10) {
                faul = false;
                bylFaul = false;
                z = -1;
            }
        }
        

// konec HRY------------------------------------------------------------

if (sCerni != sCerni2 || sCerveni != sCerveni2) {
            panNapisZacatek.setVisible(true);
            panNapisRozehravka.setVisible(false);
            for (int r = 0; r < 5; r++) {
                M.x = 75;
                M.y = 37;
                H[r] = new hrac();
                H[r].y = 5;
                H[r].x = (r * 10) + 5;
            }
            for (int r = 5; r < 10; r++) {
                H[r] = new hrac();
                H[r].y = 5;
                H[r].x = (r * 10) + 54;
            }
        }

// BULLY
        if (panNapisZacatek.isVisible() == false && panNapisRozehravka.isVisible() == true && mysX >= 0 && mysX <= 150 && mysY >= 75 && mysY <= 115) {
            int a = kostka(2);
            MX = 75;
            MY = 37;
            M.minX = 75;
            M.minY = 37;
            if (a == 1) {
                M.x = 70;
                M.y = 37;
                tym = true;
                panNapisTym.setBackground(Color.black);
            }
            if (a == 2) {
                M.x = 80;
                M.y = 37;
                tym = false;
                panNapisTym.setBackground(Color.red);
            }
            panNapisRozehravka.setVisible(false);
        }
        panNapissCerni.setVisible(true);
        panNapissCerveni.setVisible(true);
// BULLY  

//zakladání hráčů
        if (panNapisZacatek.isVisible() && panNapisRozehravka.isVisible() == false) {
            M.minX = 75;
            M.minY = 37;
            if (mysX < 150 && mysY < 75 && mysX >= 0 && mysY >= 0) {
                if (r >= 0 && r < 5) {
                    if (mysX > 53) {
                        mysX = 53;
                    }
                    if (mysX > 48 && mysY < 42 && mysY > 37) {
                        mysX = 48;
                        mysY = 42;
                    }
                    if (mysX > 48 && mysY < 37 && mysY > 32) {
                        mysX = 48;
                        mysY = 32;
                    }
                    if (mysX > 48 && mysY == 37) {
                        mysX = 38;
                        mysY = 37;
                    }
                    if (r == 0) {
                        mysX = 70;
                        mysY = 37;
                    }
                    H[r].x = mysX;
                    H[r].y = mysY;
                    KontrolaHrace(r);

                    BarvaKruhu(3, H[r].x, H[r].y, "black2");
                    P.barva[H[r].x][H[r].y] = "black";
                }
                if (r >= 5 && r < 10) {
                    if (mysX < 97) {
                        mysX = 97;
                    }
                    if (mysX < 102 && mysY < 42 && mysY > 37) {
                        mysX = 102;
                        mysY = 42;
                    }
                    if (mysX < 102 && mysY < 37 && mysY > 32) {
                        mysX = 102;
                        mysY = 32;
                    }
                    if (mysX < 102 && mysY == 37) {
                        mysX = 112;
                        mysY = 37;
                    }
                    if (r == 5) {
                        mysX = 80;
                        mysY = 37;
                    }
                    H[r].x = mysX;
                    H[r].y = mysY;
                    KontrolaHrace(r);

                    BarvaKruhu(3, H[r].x, H[r].y, "red2");
                    P.barva[H[r].x][H[r].y] = "red";
                }
                if (r >= 0 && r < 5) {
                    for (int k = 0; k < r; k++) {
                        P.barva[H[k].x][H[k].y] = "black";
                    }
                }
                if (r >= 5 && r < 10) {
                    for (int k = 5; k < r; k++) {
                        P.barva[H[k].x][H[k].y] = "red";
                    }
                }
                r++;
                if (r == 10) {
                    panNapisZacatek.setVisible(false);
                }
                if (r == 10) {
                    panNapisRozehravka.setVisible(true);
                }
            }//konec zakládání hráčů
        }
                if (M.y < 15){
                    akce3 = 1;
                }
                if (M.y > 15 && M.y < 30){
                    akce3 = 2;
                }
                if (M.y > 30 && M.y < 45){
                    akce3 = 3;
                }
                if (M.y > 45 && M.y < 50){
                    akce3 = 4;
                }
                if (M.y > 50 && M.y < 75){
                    akce3 = 5;
                }
        if (akce % 3 == 0){
                if (M.y < 15){
                    if(Vzdalenost(M.x, M.y, 13, brankar) <= 2){
                        M.y = 34;
                    }
                    if(Vzdalenost(M.x, M.y, 136, brankar) <= 2){
                        M.y = 34;
                    }
                    brankar = 34;
                    akce2 = 1;
                    
                }
                if (M.y > 15 && M.y < 30){
                    if(Vzdalenost(M.x, M.y, 13, brankar) <= 2){
                        M.y = 36;
                    }
                    if(Vzdalenost(M.x, M.y, 136, brankar) <= 2){
                        M.y = 36;
                    }
                    brankar = 36;
                    akce2 = 2;
                }
                if (M.y > 30 && M.y < 45){
                    if(Vzdalenost(M.x, M.y, 13, brankar) <= 2){
                        M.y = 38;
                    }
                    if(Vzdalenost(M.x, M.y, 136, brankar) <= 2){
                        M.y = 38;
                    }
                    brankar = 38;
                    akce2 = 3;
                }
                if (M.y > 45 && M.y < 50){
                    if(Vzdalenost(M.x, M.y, 13, brankar) <= 2){
                        M.y = 40;
                    }
                    if(Vzdalenost(M.x, M.y, 136, brankar) <= 2){
                        M.y = 40;
                    }
                    brankar = 40;
                    akce2 = 4;
                }
                if (M.y > 50 && M.y < 75){
                    if(Vzdalenost(M.x, M.y, 13, brankar) <= 2){
                        M.y = 42;
                    }
                    if(Vzdalenost(M.x, M.y, 136, brankar) <= 2){
                        M.y = 42;
                    }
                    brankar = 42;
                    akce2 = 5;
                }
            }
        
            
        
        
        Magnet();
        Vykresleni();
        F.repaint();
    }

    public void mouseEntered(MouseEvent e) {
        mysX = (e.getX()) / 10;
        mysY = (e.getY()) / 10;

        F.repaint();
    }

    public void mouseExited(MouseEvent e) {
        mysX = (e.getX()) / 10;
        mysY = (e.getY()) / 10;

        F.repaint();
    }
    // vzdalenost - urcuje vzdalenost mezi dvemi body
    public static int Vzdalenost(int a, int b, int aa, int bb) {
        double c = 1;
        int d1 = Math.abs(aa - a);
        int d2 = Math.abs(bb - b);
        c = (d1 * d1) + (d2 * d2);
        c = Math.sqrt(c);
        c = Math.round(c);
        int d = (int) c;
        return d;
    }
    // barva kruhu - obarvi zadany kruh
    public static void BarvaKruhu(int VelikostKruhu, int a, int b, String barva) {
        for (int i = 0; i < 150; i++) {
            for (int j = 0; j < 75; j++) {
                int f = Vzdalenost(i, j, a, b);
                if (f <= VelikostKruhu) {
                    P.barva[i][j] = barva;
                }
            }
        }
    }
    // kontrola hrace - zkontroluje zda li nejsou dfva hraci na stejnem miste a podobne veci
    static void KontrolaHrace(int f) {

        for (int u = 0; u < 10; u++) {
            if (Vzdalenost(H[f].x, H[f].y, H[u].x, H[u].y) <= 3 && f != u) {

                if (H[u].x - H[f].x <= 3 && H[u].x - H[f].x > 0) {
                    H[f].x = H[f].x - 3;
                }

                if (H[u].x - H[f].x < 0 && H[u].x - H[f].x >= -3) {
                    H[f].x = H[f].x + 3;
                }

                if (H[u].y - H[f].y <= 3 && H[u].y - H[f].y > 0) {
                    H[f].y = H[f].y - 3;
                }

                if (H[u].y - H[f].y < 0 && H[u].y - H[f].y >= -3) {
                    H[f].y = H[f].y + 3;
                }

                if (H[u].x == H[f].x && H[u].y == H[f].y) {
                    int k = kostka(4);
                    if (k == 1) {
                        H[f].x = H[f].x - 3;
                    }
                    if (k == 2) {
                        H[f].x = H[f].x + 3;
                    }
                    if (k == 3) {
                        H[f].y = H[f].y - 3;
                    }
                    if (k == 4) {
                        H[f].y = H[f].y + 3;
                    }
                }
                if (H[u].x < 0) {
                    H[u].x = H[u].x + 3;
                }
                if (H[u].y < 0) {
                    H[u].y = H[u].y + 3;
                }
                if (H[u].x > 150) {
                    H[u].x = H[u].x - 3;
                }
                if (H[u].y > 75) {
                    H[u].y = H[u].y - 3;
                }

                KontrolaHrace(f);
            }

        }
    }
    // prihravka - provede pohyb micku predtim urceny
    public static void Prihravka(){
        
        
        if (M.rychY <= 0 && M.rychX <= 0 && M.rych == false && muzesPrihravat == true) {
            kPrihravky = 1;
                    if (Math.abs(M.rychX) < Math.abs(M.rychY) && muzesPrihravat == true){
                        for(int rrr = 0; rrr < Math.abs(M.rychY); rrr++){
                            M.y--;
                            prihY++;
                            Odraz();
                            MD1 = MD1 + Math.abs(M.rychX)/Math.abs(M.rychY);
                            if(MD1 >= 1 && muzesPrihravat == true){
                                MD1 = MD1 - 1;
                                M.x--;
                                prihX++;
                                Odraz();
                            }
                            Mic();
                        }
                        prihX = 0;
                        prihY = 0;
                    }
                    if (Math.abs(M.rychX) > Math.abs(M.rychY) && muzesPrihravat == true){
                        for(int rrr = 0; rrr < Math.abs(M.rychX); rrr++){
                            M.x--;
                            prihX++;
                            Odraz();
                            MD2 = MD2 + Math.abs(M.rychY)/Math.abs(M.rychX);
                            if(MD2 >= 1 && muzesPrihravat == true){
                                MD2 = MD2 - 1;
                                M.y--;
                                prihY++;
                                Odraz();
                            }
                            Mic();
                        }
                    }
                    }
                    
                    
                    if (M.rychY > 0 && M.rychX < 0 && M.rych == false && muzesPrihravat == true) {
                        kPrihravky = 2;
                    if (Math.abs(M.rychX) < Math.abs(M.rychY) && muzesPrihravat == true){
                        for(int rrr = 0; rrr < Math.abs(M.rychY); rrr++){
                            M.y++;
                            prihY++;
                            Odraz();
                            MD3 = MD3 + Math.abs(M.rychX)/Math.abs(M.rychY);
                            if(MD3 >= 1 && muzesPrihravat == true){
                                MD3 = MD3 - 1;
                                M.x--;
                                prihX++;
                                Odraz();
                            }
                            Mic();
                        }
                    }
                    if (Math.abs(M.rychX) > Math.abs(M.rychY) && muzesPrihravat == true){
                        for(int rrr = 0; rrr < Math.abs(M.rychX); rrr++){
                            M.x--;
                            prihX++;
                            Odraz();
                            MD4 = MD4 + Math.abs(M.rychY)/Math.abs(M.rychX);
                            if(MD4 >= 1 && muzesPrihravat == true){
                                MD4 = MD4 - 1;
                                M.y++;
                                prihY++;
                                Odraz();
                            }
                            Mic();
                        }
                    }
                    }
                    
                    
                    if (M.rychY >= 0 && M.rychX >= 0 && M.rych == false && muzesPrihravat == true) {
                        kPrihravky = 3;
                    if (Math.abs(M.rychX) < Math.abs(M.rychY) && muzesPrihravat == true){
                        for(int rrr = 0; rrr < Math.abs(M.rychY); rrr++){
                            M.y++;
                            prihY++;
                            Odraz();
                            MD5 = MD5 + Math.abs(M.rychX)/Math.abs(M.rychY);
                            if(MD5 >= 1 && muzesPrihravat == true){
                                MD5 = MD5 - 1;
                                M.x++;
                                prihX++;
                                Odraz();
                            }
                            Mic();
                        }
                    }
                    if (Math.abs(M.rychX) > Math.abs(M.rychY) && muzesPrihravat == true){
                        for(int rrr = 0; rrr < Math.abs(M.rychX); rrr++){
                            M.x++;
                            prihX++;
                            Odraz();
                            MD6 = MD6 + Math.abs(M.rychY)/Math.abs(M.rychX);
                            if(MD6 >= 1 && muzesPrihravat == true){
                                MD6 = MD6 - 1;
                                M.y++;
                                prihY++;
                                Odraz();
                            }
                            Mic();
                        }
                    }
                    }
                    if (M.rychY < 0 && M.rychX > 0 && M.rych == false && muzesPrihravat == true) {
                        kPrihravky = 4;
                    if (Math.abs(M.rychX) < Math.abs(M.rychY) && muzesPrihravat == true){
                        for(int rrr = 0; rrr < Math.abs(M.rychY); rrr++){
                            M.y--;
                            prihY++;
                            Odraz();
                            MD7 = MD7 + Math.abs(M.rychX)/Math.abs(M.rychY);
                            if(MD7 >= 1 && muzesPrihravat == true){
                                MD7 = MD7 - 1;
                                M.x++;
                                prihX++;
                                Odraz();
                            }
                            Mic();
                        }
                    }
                    if (Math.abs(M.rychX) > Math.abs(M.rychY) && muzesPrihravat == true){
                        for(int rrr = 0; rrr < Math.abs(M.rychX); rrr++){
                            M.x++;
                            prihX++;
                            Odraz();
                            MD8 = MD8 + Math.abs(M.rychY)/Math.abs(M.rychX);
                            if(MD8 >= 1 && muzesPrihravat == true){
                                MD8 = MD8 - 1;
                                M.y--;
                                prihY++;
                                Odraz();
                            }
                            Mic();
                        }
                    }
                    }
    }
    
    
    // kostka - urci nahodne cislo od jedny do zadaneho cisla
    public static int kostka(int b) {
        Random RND = new Random();
        int a = RND.nextInt(b);
        a = a + 1;
        return a;
    }
    // magnet - pritahne micek k hraci ktery je v urcite vzdalenosti od micku
    public static void Magnet() {
        int pom = 0;
        int pom2 = 0;
        for (int u = 0; u < 10; u++) {
            H[u].blizko = false;
            if (Vzdalenost(H[u].x, H[u].y, M.x, M.y) <= 3) {
                pom = pom + 1;
                H[u].blizko = true;
                pom2 = kostka(pom);
            }
        }
        for (int u = 0; u < 10; u++) {
            if (H[u].blizko == true) {
                pom2 = pom2 - 1;
                M.x = H[u].x;
                M.y = H[u].y;
                if (pom2 == 0) {
                    u = 10;
                }
            }
        }
    }
    
    
    
    //odraz - pokud se micek dotkne mantinelu rozehrava druhy hrac
    public static void Odraz(){
        if(M.x <= 0){
            Aut();
        } else {
        if(M.y <= 0){
            Aut();
        }else {
        if(M.x >= 149){
            Aut();
        }else {
        if(M.y >= 74){
            Aut();
        }}}}
    }
    
    // aut - zajistuje to co odraz ale zjednodusuje kod
    public static void Aut(){
        kontrola = true;
        if (hAkt < 5) {
            M.rych = false;
            
            tym = false;
            panNapisTym.setBackground(Color.RED);
            H[hAkt + 5].hracPohyboval = true;
            H[hAkt + 5].hracPrihraval = false;
            if (M.x <= 0) {
                M.x = M.x + 1;
            }
            if (M.y <= 0) {
                M.y = M.y + 1;
            }
            if (M.x >= 149) {
                M.x = M.x - 1;
            }
            if (M.y >= 74) {
                M.y = M.y - 1;
            }
            H[hAkt + 5].x = M.x;
            H[hAkt + 5].y = M.y;
            M.rychX = 0;
            M.rychY = 0;
        }
        if (hAkt >= 5) {
            M.rych = false;

            tym = true;
            panNapisTym.setBackground(Color.BLACK);
            H[hAkt - 5].hracPohyboval = true;
            H[hAkt - 5].hracPrihraval = false;
            if (M.x <= 0) {
                M.x = M.x + 1;
            }
            if (M.y <= 0) {
                M.y = M.y + 1;
            }
            if (M.x >= 149) {
                M.x = M.x - 1;
            }
            if (M.y >= 74) {
                M.y = M.y - 1;
            }
            H[hAkt - 5].x = M.x;
            H[hAkt - 5].y = M.y;
            M.rychX = 0;
            M.rychY = 0;
        }
        
    }
    // mic - kontroluje jestli micek prepotuje brankovou caru za pomoci prihravky
    public static void Mic(){
        if (kontrola == false){
        for (int r = 0; r < 10; r++) {
            if (Vzdalenost(M.x, M.y, H[r].x, H[r].y) <= 3 && hPrih != r){
                M.x = H[r].x;
                M.y = H[r].y;
                M.rychX = 0;
                M.rychY = 0;
                M.rych = true;
            }
        }
        if ((M.x == 9) && M.y >= 32 && M.y <= 43) {
            M.x = 13;
            M.y = brankar;
            M.rychX = 0;
            M.rychY = 0;
            M.rych = true;
        }
        if ((M.x == 140) && M.y >= 32 && M.y <= 43) {
            M.x = 136;
            M.y = brankar;
            M.rychX = 0;
            M.rychY = 0;
            M.rych = true;
        }
        
        }
    }
    
    // zapise na jake barvy ma prekleslit platno
    public static void Vykresleni() {
        for (int i = 0; i < 150; i++) {
            for (int j = 0; j < 75; j++) {
                P.barva[i][j] = "guma";
            }
        }
        if (H[hAkt].pohyb) {
            BarvaKruhu(20, H[hAkt].x, H[hAkt].y, "gray");
        }
        if (H[hAkt].prihravka) {
            BarvaKruhu(40, H[hAkt].x, H[hAkt].y, "gray");
        }
        if (pohybNe2 == true && brankar2 == false) {
            BarvaKruhu(40, 13, brankar, "gray");
        }
        if (pohybNe2 == true && brankar2 == true) {
            BarvaKruhu(40, 136, brankar, "gray");
        }
        
        BarvaKruhu(2, 13, brankar, "black2");
        P.barva[13][brankar] = "black";
        
        BarvaKruhu(2, 136, brankar, "red2");
        P.barva[136][brankar] = "red";
        
        for (int o = 0; o < 5; o++) {
            BarvaKruhu(3, H[o].x, H[o].y, "black2");
            P.barva[H[o].x][H[o].y] = "black";
            if (H[o].trest > 0) {
                BarvaKruhu(3, H[o].x, H[o].y, "");
                P.barva[H[o].x][H[o].y] = "";
            }
        }
        for (int o = 5; o < 10; o++) {
            BarvaKruhu(3, H[o].x, H[o].y, "red2");
            P.barva[H[o].x][H[o].y] = "red";
            if (H[o].trest > 0) {
                BarvaKruhu(3, H[o].x, H[o].y, "");
                P.barva[H[o].x][H[o].y] = "";
            }
        }
        if (H[hAkt].oznacen || H[hAkt].pohyb) {
            BarvaKruhu(3, H[hAkt].x, H[hAkt].y, "yellow2");
            P.barva[H[hAkt].x][H[hAkt].y] = "yellow";
        }
        P.barva[M.x][M.y] = "micek";

        

    F.repaint();
    }



} // konec CLASSAas  ikolú