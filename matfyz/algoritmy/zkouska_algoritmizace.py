class VrcholBinStromu:
    """třída pro reprezentaci vrcholu binárního stromu""" 
    def __init__(self, info = None, levy = None, pravy = None):
        self.info = info      # data
        self.levy = levy      # levé dítě 
        self.pravy = pravy    # pravé dítě

def hladina(koren : VrcholBinStromu, x : int):
    """
    koren : kořen zadaného binárního stromu
    x     : zadané ohodnocení hledaného vrcholu
    vrátí : seznam čísel vrcholů na hladině obsahující x
    """
    global pocitadloHladin1
    pocitadloHladin1 = 0
    kteraHladina(koren, x, 1)
    naKtereHladine = pocitadloHladin1
    #tím jsme převedli úlohu na vypiš konkrétní hladinu
    global pomocnik
    pomocnik = []
    zapisHladinu(koren, x, naKtereHladine, 1)
    return pomocnik


def zapisHladinu(koren : VrcholBinStromu, x : int, naKtereHladine, pocitadloHladinek):
    global pomocnik
    if pocitadloHladinek == naKtereHladine:
        #pokud je na správné hladině přidáme ho tam
        pomocnik.append(koren.info)
    else:
        #tak jestliže jsme ho zapsali určitě nebude nutné zapisovat jeho děti ty jsou totiž o hladinu níže
        if koren.levy != None:
            kteraHladina(koren.levy, x, pocitadloHladinek+1)
        if koren.pravy != None:
            kteraHladina(koren.pravy, x, pocitadloHladinek+1)
    

def kteraHladina(koren : VrcholBinStromu, x : int, pocitadloHladin):
    global pocitadloHladin1
    #nejdříve projdeme strom do šířky, abychom našli danou hodnotu a hladinu, na které se nachází
    fronta = []
    if koren.info == x:
        #pokud narazíme na info, které hledáme vrátíme na jaké je hladine
        pocitadloHladin1 =  pocitadloHladin
    if koren.levy != None:
        kteraHladina(koren.levy, x, pocitadloHladin+1)
    if koren.pravy != None:
        kteraHladina(koren.pravy, x, pocitadloHladin+1)
        
        