import usb.core
import shutil
import os

device = usb.core.find(idVendor=0x04e8, idProduct=0x6860)


paths_to_try = ['/media', '/mnt', '/run/media']
usb_drive_path = None
for path in paths_to_try:
    if os.path.exists(path):
        for item in os.listdir(path):
            item_path = os.path.join(path, item)
            if os.path.isdir(item_path) and 'usb' in item.lower():
                usb_drive_path = item_path
                break
        if usb_drive_path is not None:
            break

if usb_drive_path is None:
    print('USB drive not found')
else:
    print('USB drive path:', usb_drive_path)

if device is None:
    print("Device not found")
else:
    print("Device found")
    src_folder = "./Tablet/styluslabs/write/"
    dst_folder = "/home/adam/Desktop/WT"

    shutil.copytree(src_folder, dst_folder)




