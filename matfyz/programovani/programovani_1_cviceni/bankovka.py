import turtle
zelva = turtle
zelva.penup()
zelva.left(180)
zelva.forward(300)
zelva.left(90)
zelva.forward(100)
zelva.right(90)
zelva.left(180)
zelva.pendown()
zelva.forward(600)
zelva.left(90)
zelva.forward(300)
zelva.left(90)
zelva.forward(600)
zelva.left(90)
zelva.forward(300)
zelva.left(90)
zelva.penup()
zelva.forward(30)
zelva.left(90)
zelva.forward(30)
zelva.pendown()
zelva.right(90)

zelva.forward(540)
zelva.left(90)
zelva.forward(240)
zelva.left(90)
zelva.forward(540)
zelva.left(90)
zelva.forward(240)
zelva.left(90)

def polygon(numberOfSidest, t, lenght):
    for i in range(numberOfSidest):
        t.fd(lenght)
        t.lt(360/numberOfSidest)
        
def circle(t, r):
    k = (3.1415926*r)/15
    polygon(30, t, k)

zelva.penup()
zelva.forward(190)
zelva.left(90)
zelva.forward(80)
zelva.right(90)
zelva.pendown()

circle(zelva, 30)

zelva.penup()

zelva.left(90)
zelva.forward(3)
zelva.right(90)

zelva.pendown()

circle(zelva, 24)

zelva.left(90)
zelva.forward(30)
zelva.right(135)
zelva.forward(30*1.41)
zelva.left(135)
zelva.forward(30)

zelva.penup()
zelva.right(90)
zelva.forward(5)
zelva.pendown()

zelva.right(90)
zelva.forward(30)
zelva.left(90)
zelva.forward(30)
zelva.penup()
zelva.right(180)
zelva.forward(30)
zelva.right(90)
zelva.forward(15)
zelva.pendown()
zelva.right(90)
zelva.forward(20)
zelva.penup()
zelva.right(180)
zelva.forward(20)
zelva.right(90)
zelva.forward(15)
zelva.pendown()
zelva.right(90)
zelva.forward(30)


input()