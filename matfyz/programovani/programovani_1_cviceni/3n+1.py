import time


def vypocti2(n):
    retezec = [n]
    s = str(n)
    while True:
        if n%2 == 0:
            n = n//2
        else:
            n = 3*n+1
        if n in retezec:
            break
        s = s + " " + str(n)
        retezec.append(n)
    return s

def vypocti(n):
    retezec = [n]
    s = str(n)
    while True:
        if n%2 == 0:
            n = n//2
        else:
            n = 3*n+1
        if n in retezec:
            break
        s = s + " " + str(n)
        retezec.append(n)
    print(s)
    print("cyklus:", vypocti2(n))

def vypoctiNejdelsi(n):
    retezec = [n]
    s = str(n)
    while True:
        if n%2 == 0:
            n = n//2
        else:
            n = 3*n+1
        if n in retezec:
            break
        s = s + " " + str(n)
        retezec.append(n)
    a = "cyklus: " + str(vypocti2(n))
    return [[len(retezec)], [s], [a]]

def vsechnyCisla(tim, zapor = True):
    if zapor == False:
        i = -1
        while True:
            print("________________________________________________________________________")
            vypocti(i)
            i = i - 1
            print("________________________________________________________________________")
            time.sleep(tim)
    else:
        i = 1
        while True:
            print("________________________________________________________________________")
            vypocti(i)
            i = i + 1
            print("________________________________________________________________________")
            time.sleep(tim)

def nejdelsiRetezec(k):
    pamatovak = vypoctiNejdelsi(1)
    for i in range (k):
        a = vypoctiNejdelsi(i)
        if a[0] > pamatovak[0]:
            pamatovak = a
    print(pamatovak[1][0])
    print(pamatovak[2][0])



vsechnyCisla(0.2, False)
# vypocti(1)