import math

def calculate_max_reward(num_stages):
    dp = [[0] * 101 for _ in range(num_stages)]

    for i in range(num_stages):
        for j in range(1, 101):
            max_reward = 0
            for k in range(1, j+1):
                reward = math.floor((100000 * k) / j)
                max_reward = max(max_reward, dp[i-1][j-k] + reward)
            dp[i][j] = max_reward

    max_reward = dp[num_stages - 1][100]
    opponents_eliminated = []

    remaining_opponents = 100
    for i in range(num_stages-1, 0, -1):
        found = False
        for j in range(1, min(remaining_opponents+1, 101)):
            if dp[i][j] == max_reward:
                opponents_eliminated.append(j)
                remaining_opponents -= j
                if remaining_opponents > 0:
                    max_reward -= math.floor((100000 * j) / remaining_opponents)
                found = True
                break
        if not found:
            opponents_eliminated.append(0)


    return max_reward, opponents_eliminated

# Example usage:
num_stages = 3
max_reward, opponents_eliminated = calculate_max_reward(num_stages)

print("Maximum Reward:", max_reward)
print("Opponents Eliminated per Stage:", opponents_eliminated)
