﻿// See https://aka.ms/new-console-template for more information
namespace secti_dve_cisla
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("Zadejte prvni cislo:");
            int a = System.Convert.ToInt32(System.Console.ReadLine());
            System.Console.WriteLine("Zadejte druhe cislo:");
            int b = System.Convert.ToInt32(System.Console.ReadLine());
            System.Console.WriteLine("Soucet je: " + (a + b));
        }
    }
}