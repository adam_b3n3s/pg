﻿using static System.Console;
using System.Collections.Generic;

namespace zapoctovy_test
{
    public class variables
    {
        public static int max = 1;
    }
    class longest_path_in_domino
    {
        static void Main(string[] args)
        {
            bool first_skip = true;
            int index = 0;
            bool index_first = false;
            int[,] domino = new int[20, 3];
            string line = ReadLine();
            string[] numbers = line.Split(' ');
            int N = int.Parse(numbers[0]);
            foreach (string number in numbers)
            {
                if (number == "" || first_skip == true)
                {
                    first_skip = false;
                }
                else
                {
                    if (index_first == false)
                    {
                        domino[index, 0] = int.Parse(number);
                        index_first = true;
                    }
                    else
                    {
                        domino[index, 1] = int.Parse(number);
                        index_first = false;
                        index++;
                    }

                }
            }
            while (N > index)
            {
                line = ReadLine();
                numbers = line.Split(' ');
                foreach (string number in numbers)
                {
                    if (number == "")
                    {

                    }
                    else
                    {
                        if (index_first == false)
                        {
                            domino[index, 0] = int.Parse(number);
                            index_first = true;
                        }
                        else
                        {
                            domino[index, 1] = int.Parse(number);
                            index_first = false;
                            index++;
                        }
                    }
                }
            }



            int[,] setup = new int[20, 3];



            
            List<int>used = new List<int>();


            for (int i = 0; i < N; i++)
            {
                used = new List<int>();
                setup[0, 0] = domino[i, 0];

                setup[0, 1] = domino[i, 1];

                used.Add(i);

                rek(1, domino, setup, 0, used, 1);
            }

            if (N == 0)
            {
                WriteLine("0");
            }
            else{
                WriteLine(variables.max);
            }
            



            void rek(int number, int[,] domino, int[,] setup, int index, List<int> used, int index_used)
            {
                for (int i = 0; i < N; i++)
                {
                    bool us = false;
                    
                    if (used.LastIndexOf(i) != -1)
                    {
                        us = true;
                    }
                    if (setup[index, 1] == domino[i, 0] && us == false)
                    {
                        used.Add(i);
                        setup[index + 1, 0] = domino[i, 0];
                        setup[index + 1, 1] = domino[i, 1];
                        if (number + 1 > variables.max)
                        {
                            variables.max = number + 1;
                        }
                        rek(number + 1, domino, setup, index + 1, used, index_used + 1);
                    }
                    if (setup[index, 1] == domino[i, 1] && us == false)
                    {
                        used.Add(i);
                        setup[index + 1, 0] = domino[i, 1];
                        setup[index + 1, 1] = domino[i, 0];
                        if (number + 1 > variables.max)
                        {
                            variables.max = number + 1;
                        }
                        rek(number + 1, domino, setup, index + 1, used, index_used + 1);
                    }
                }
            }
        }
    }
}
