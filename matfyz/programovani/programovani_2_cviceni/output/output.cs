﻿namespace output
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 7;
            int b = 3;
            int reminder;

            int x = dev(a, b, out reminder);

            System.Console.WriteLine(x);
            System.Console.WriteLine(reminder);
        }

        public static int dev(int x, int y, out int reminder)
        {
            reminder = x % y;
            return x / y;
        }
    }
}