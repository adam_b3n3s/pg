﻿using System;
using System.Collections.Generic;

class prelevani_vody
{
    static List<string> vystup = new List<string>();
    
    static void Main()
    {
        string vstup = Console.ReadLine();
        vstup = vstup.Trim();
        
        string[] hodnoty = vstup.Split(' ', StringSplitOptions.RemoveEmptyEntries);
        int[] y = new int[hodnoty.Length];
        for (int i = 0; i < hodnoty.Length; i++)
        {
            y[i] = int.Parse(hodnoty[i]);
        }
        
        int[] objemy = new int[] { y[0], y[1], y[2] };
        int[] nynejsiObjemy = new int[] { y[3], y[4], y[5] };
        
        List<int[]> jizZapocitano = new List<int[]> { nynejsiObjemy };
        
        int pocetPreliti = 0;
        List<int> objemyCoUzMame = new List<int>(nynejsiObjemy);
        
        if (nynejsiObjemy[0] != nynejsiObjemy[1] && nynejsiObjemy[1] != nynejsiObjemy[2] && nynejsiObjemy[0] != nynejsiObjemy[2])
        {
            vystup.Add(nynejsiObjemy[0] + ":0");
            vystup.Add(nynejsiObjemy[1] + ":0");
            vystup.Add(nynejsiObjemy[2] + ":0");
        }
        else if (nynejsiObjemy[1] != nynejsiObjemy[2])
        {
            vystup.Add(nynejsiObjemy[1] + ":0");
            vystup.Add(nynejsiObjemy[2] + ":0");
        }
        else if (nynejsiObjemy[0] != nynejsiObjemy[2])
        {
            vystup.Add(nynejsiObjemy[0] + ":0");
            vystup.Add(nynejsiObjemy[2] + ":0");
        }
        else if (nynejsiObjemy[0] != nynejsiObjemy[1] && nynejsiObjemy[1] != nynejsiObjemy[0])
        {
            vystup.Add(nynejsiObjemy[0] + ":0");
            vystup.Add(nynejsiObjemy[1] + ":0");
        }
        else
        {
            vystup.Add(nynejsiObjemy[0] + ":0");
        }
        
        List<int[]> fronta = new List<int[]>();
        fronta.Add(new int[] { nynejsiObjemy[0], nynejsiObjemy[1], nynejsiObjemy[2], 0 });
        
        while (fronta.Count > 0)
        {
            int[] x = fronta[0];
            int[] preliti = Prelij(0, 1, (int[])x.Clone(), objemy, jizZapocitano, objemyCoUzMame);
            if (preliti != null)
            {
                fronta.Add(preliti);
            }
            preliti = Prelij(1, 0, (int[])x.Clone(), objemy, jizZapocitano, objemyCoUzMame);
            if (preliti != null)
            {
                fronta.Add(preliti);
            }
            preliti = Prelij(0, 2, (int[])x.Clone(), objemy, jizZapocitano, objemyCoUzMame);
            if (preliti != null)
            {
                fronta.Add(preliti);
            }
            preliti = Prelij(2, 0, (int[])x.Clone(), objemy, jizZapocitano, objemyCoUzMame);
            if (preliti != null)
            {
                fronta.Add(preliti);
            }
            preliti = Prelij(1, 2, (int[])x.Clone(), objemy, jizZapocitano, objemyCoUzMame);
            if (preliti != null)
            {
                fronta.Add(preliti);
            }
            preliti = Prelij(2, 1, (int[])x.Clone(), objemy, jizZapocitano, objemyCoUzMame);
            if (preliti != null)
            {
                fronta.Add(preliti);
            }

            fronta.RemoveAt(0);
        }
        
        vystup.Sort((s1, s2) => int.Parse(s1.Split(':')[0]).CompareTo(int.Parse(s2.Split(':')[0])));
        Console.WriteLine(string.Join(" ", vystup));
    }
    
    static int[] Prelij(int a, int b, int[] x, int[] objemy, List<int[]> jizZapocitano, List<int> objemyCoUzMame)
    {
        if (x[a] + x[b] <= objemy[b])
        {
            x[b] += x[a];
            x[a] = 0;
            if (jizZapocitano.Exists(arr => arr[0] == x[0] && arr[1] == x[1] && arr[2] == x[2]))
            {
                return null;
            }
            else
            {
                jizZapocitano.Add(new int[] { x[0], x[1], x[2] });
                x[3]++;
                if (!objemyCoUzMame.Contains(x[0]))
                {
                    objemyCoUzMame.Add(x[0]);
                    vystup.Add(x[0] + ":" + x[3]);
                }
                if (!objemyCoUzMame.Contains(x[1]))
                {
                    objemyCoUzMame.Add(x[1]);
                    vystup.Add(x[1] + ":" + x[3]);
                }
                if (!objemyCoUzMame.Contains(x[2]))
                {
                    objemyCoUzMame.Add(x[2]);
                    vystup.Add(x[2] + ":" + x[3]);
                }
                return x;
            }
        }
        else
        {
            int rozdil = x[a] + x[b] - objemy[b];
            x[a] = rozdil;
            x[b] = objemy[b];
            if (jizZapocitano.Exists(arr => arr[0] == x[0] && arr[1] == x[1] && arr[2] == x[2]))
            {
                return null;
            }
            else
            {
                jizZapocitano.Add(new int[] { x[0], x[1], x[2] });
                x[3]++;
                if (!objemyCoUzMame.Contains(x[0]))
                {
                    objemyCoUzMame.Add(x[0]);
                    vystup.Add(x[0] + ":" + x[3]);
                }
                if (!objemyCoUzMame.Contains(x[1]))
                {
                    objemyCoUzMame.Add(x[1]);
                    vystup.Add(x[1] + ":" + x[3]);
                }
                if (!objemyCoUzMame.Contains(x[2]))
                {
                    objemyCoUzMame.Add(x[2]);
                    vystup.Add(x[2] + ":" + x[3]);
                }
                return x;
            }
        }
    }
}