﻿using static System.Console;
using System.Collections.Generic;
using System.Linq;
class threads
{
    static void Main(string[] args)
    {
        int fib(int n)
        {
            if (n < 0) return 1;
            return fib(n - 1) + fib(n - 2);
        }
        WriteLine(fib(60));
    }
}
