﻿using System;
using System.Collections.Generic;

    

namespace polynomials
{
    class poly
    {
        public String polynom;

        int __degree;

        public List<double> polynom_array = new List<double>();

        public poly(params double[] args)
        {
            int z = 0;
            while (args[z] == 0){
                z++;
            }
            __degree = args.Length-z-1;
            for (int i = 0; i < args.Length; i++)
            {
                if (args[i] != 0)
                {
                    if (args[i] == 1)
                    {
                        if (args.Length-i-1 == 1)
                        {
                            this.polynom = this.polynom + "x" + "+";
                        }
                        else{
                            this.polynom = this.polynom + "x^" + (args.Length-i-1).ToString() + "+";
                        }
                    }
                    else{
                        if (args.Length-i-1 == 1)
                        {
                            this.polynom = this.polynom + args[i].ToString() + "x" + "+";
                        }
                        else{
                            this.polynom = this.polynom + args[i].ToString() + "x^" + (args.Length-i-1).ToString() + "+";
                        }
                        
                    }
                }
                polynom_array.Add(args[i]);
            }
            char last_char = this.polynom.Substring(this.polynom.Length - 1)[0];
            if (last_char == '+')
            {
                this.polynom = this.polynom.Substring(0, this.polynom.Length-1);
            }
            char last_char2 = this.polynom.Substring(this.polynom.Length - 1)[0];
            if (last_char2 == '0')
            {
                this.polynom = this.polynom.Substring(0, this.polynom.Length-3);
                if (this.polynom.Substring(this.polynom.Length - 1)[0] == '+')
                {
                    this.polynom = this.polynom + "1";
                }
            }
        }
        public int degree()
        {
            return this.__degree;
        }
        public static poly operator + (poly v, poly w) {
			int n = Math.Max(v.polynom_array.Count, w.polynom_array.Count);
            double[] a = new double[n];
            List<double> yy = v.polynom_array;
            List<double> zz = w.polynom_array;
            if (yy.Count < zz.Count)
            {
                List<double> qq = zz;
                zz = yy;
                yy = qq;

            }
            while (yy.Count > zz.Count)
            {
                zz.Insert(0, 0);
            }
            for (int i = n-1; i >= 0; i--)
            {

                double y = yy[i];

                double z = zz[i];

                a[i] = z + y;
            }
            poly b = new poly(a);
			return b;
		}

    }
    
    class polynomials
    {
        static void Main(string[] args)
        {
            poly r = new poly(1.1, 1.1, 5, 0, 2, 67);
            
            Console.WriteLine(r.polynom);
            Console.WriteLine(r.degree());
            
            poly f = new poly(6, 8, 1.1, 0, 4, 1);

            Console.WriteLine(f.polynom);
            Console.WriteLine(f.degree());

            poly g = f + r;

            poly q = r + f;

            Console.WriteLine(g.polynom);
            Console.WriteLine(g.degree());
            Console.WriteLine(q.polynom);
            Console.WriteLine(q.degree());
        }
        
    }
}