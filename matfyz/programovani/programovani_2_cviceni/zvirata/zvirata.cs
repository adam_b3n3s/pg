﻿namespace zvirata
{
    abstract class Animal
    {
        protected string name_of_species;
        protected string name;


        public Animal(string name, string name_of_species)
        {
            this.name = name;
            this.name_of_species = name_of_species;
        }
        public abstract void voice();
    }

    class Dog:Animal
    {
        public Dog(string name) : base(name, "Pes")
        {
        }
        public override void voice()
        {
            System.Console.WriteLine("Haf!");
        }
    }



    class Program
    {
        static void Main(string[] args)
        {
            

            Dog pejsek = new Dog("hafak");
            pejsek.voice();
        }
    }
}