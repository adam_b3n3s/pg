﻿// See https://aka.ms/new-console-template for more information
namespace pod_retezce
{
    class Program
    {

        public static bool is_in(string[] pole, string a)
        {
            foreach (string i in pole)
            {
                if (string.Equals(a,i))
                {
                    return false;
                }
            }
            return true;
        }


        public static void Main(string[] args)
        {
            string vypis = "";
            string line = System.Console.ReadLine();
            int delka = (line.Length*line.Length)/2;
            string[] pole = new string[delka];
            int index = 0;
            for(int i = 1; i < line.Length; i++)
            {
                for(int j = 0; j < (line.Length - i + 1); j++)
                {
                    vypis = line[j..(j+i)];
                    if (is_in(pole, vypis))
                    {
                        pole[index] = vypis;
                        index++;
                    }
                }
            }
            foreach (string q in pole)
            {
                System.Console.WriteLine(q);
            }
            System.Console.WriteLine(index);
        }        
    }
}