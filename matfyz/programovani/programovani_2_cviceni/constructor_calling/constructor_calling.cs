﻿using System;
namespace constructor_calling
{
    class constructor_calling
    {
        static void Main(string[] args)
        {
            C lala = new C();

            B uuu = new B();

            lala.how_hany();

            lala.how_hany_c();

        }
    }
    class A
    {
        static private int _counter = 0;
        int _uID;
        public A()
        {
            System.Console.WriteLine("aaa");
            this._uID=A._counter;
            _counter++;
        }
        public void how_hany()
        {
            System.Console.WriteLine(_counter);
            System.Console.WriteLine(_uID);
        }
    }
    class B:A
    {
        public B()
        {
            System.Console.WriteLine("bbb");
        }
    }
    class C:B
    {
        static private int _c_counter;
        public C()
        {
            System.Console.WriteLine("ccc");
            C._c_counter++;
        }
        public void how_hany_c()
        {
            System.Console.WriteLine(_c_counter);
        }
    }
}
