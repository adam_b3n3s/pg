﻿using System;
namespace parametrs
{
    class parametrs
    {
        static void Main(string[] args)
        {
            variable_args v = new variable_args();
            v.print_count_of_arguments(1,2,3,4);
            v.print_count_of_arguments_and_reverse(1,2,3,4);
        }
        class variable_args
        {
            public void print_count_of_arguments(params int[] args)
            {
                Console.WriteLine(args.Length);
            }
            public void print_count_of_arguments_and_reverse(params int[] args)
            {
                Console.WriteLine(args.Length);
                for (int i = args.Length-1; i >= 0; i--)
                {
                    Console.WriteLine(args[i]);
                }
            }
        }
    }
}