import matplotlib.pyplot as plt

# create a blank figure with white background
fig, ax = plt.subplots(figsize=(8, 6), facecolor='white')

# turn off the axis labels and ticks
ax.set_axis_off()

# create a list to store the points that the user has drawn
points = []

def onclick(event):
    # get the x and y coordinates of the mouse click event
    x, y = event.xdata, event.ydata
    # add the coordinates to the list of points
    points.append((x, y))
    # draw a line connecting the new point to the last point
    if len(points) > 1:
        xs, ys = zip(*points)
        ax.plot(xs, ys, 'k-', linewidth=2)
    fig.canvas.draw()

# connect the onclick function to the figure
fig.canvas.mpl_connect('button_press_event', onclick)

plt.show()
