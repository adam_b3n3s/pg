from turtle import Turtle

class VelkaZelva(Turtle):
    def forward(self, kolik):
        super().forward(2*kolik)
        
class WelkaZelva:
    def __init__(self, K=1.0, barva="black"):
        self.mojeZelva = Turtle()
        self.K = K
        self.mojeZelva.color(barva)
    def forward(self, kolik):
        self.mojeZelva.forward(self.K*kolik)
        
