import matplotlib.pyplot as plt
import math

plt.plot([1,2,3,3], "ro-", label="prvni")

plt.plot([5,2,4,1], "mo-", label="druhy")

plt.legend()

plt.xlabel("osa x")

plt.ylabel("osa y")

plt.bar(["myš","žirafa"], [1, 4])

plt.show()

