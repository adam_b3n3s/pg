a = 5
print(a.bit_length())
a = 27
print(a.bit_length())
a = "abcd"
print(dir(a))
print(a.isdecimal())
a = "64"
print(a.isdecimal())
#a = 64
#print(a.isdecimal()) nelze udelat
s = """
První řádek
druhý řádek
třetí řádek
"""
print(s)

a = "abc"
b = "abc"
print(a == b)

a = "abc"
b = "abc"
print(a is b)

a = a + "x"
b = b + "x"
print(a is b)
print(a == b)

print(a[0])
print(a[1])
a = "abcdefghijklmnopq"
print(a[-1])#posledni znak
#print(a[100]) nebude fungovat
print(a[-3:])
print(a[5:9])
print(a[:])
print(a[::2])#kolikaty znak
print(a[::-1])#obraceni retezce

#vypisujme po jednom znaku
i = 0
while i < len(a):
    print(a[i])
    i += 1

s = "12356789"

soustava = 10
x = 0
i = 0
while ((i < len(s) and s[i]>="0") and (s[i]<="9")):
    x = soustava*x + ord(s[i]) - ord("0")
    print(x)
    i += 1

s = "77"

soustava = 8
x = 0
i = 0
while ((i < len(s) and s[i]>="0") and (s[i]<="7")):
    x = soustava*x + ord(s[i]) - ord("0")
    print(x)
    i += 1

d = 5 * [5 * [1]]
print(d)
print(d[1][3])
d[1][3] = 777#chyba
print(d)
print(d[0].index(777))

a = [1, 2, 3, 4, 5, 6, 7, 8, 9]
print(a)
del a[5]
print(a)

a = 5
b = a + 10
s = [1, 2, 3, [4, 5]]
for x in s:
    print(x)
    
for i in range (0, len(s)):
    #prvni paramentr se dá vynechat => běží od nuly
    print(s[i])