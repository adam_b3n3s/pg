def RadkaX():
    print("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")


def Nadpis():
    print("X ", end=" ")
    for i in range(1, 10 + 1):
        print(("     "+ str(i))[-4:], end="")
    print(" X")
    
def Nasobky(k):
    print("X ", end=" ")
    for i in range(1, 10 + 1):
        print(("        " + str(i*k))[-4:], end="")
    print(" X")


RadkaX()
Nadpis()
RadkaX()
for i in range(1, 10 + 1):
    Nasobky(i)
RadkaX()


def G(a, b):
    print(f"a = {a} b = {b}")
G(b = 1, a = 2) #přebiju pořadí

def H(a, b, c = 777):
    print(f"a = {a} b = {b} c = {c}")
H(b = 1, a = 2, c = 10) #přebiju ještě víc

def naplnPole(pole, hodnota):
    for i in range(len(pole)):
        pole[i] = hodnota


#globální proměnná