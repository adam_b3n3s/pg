import time
max = 10000
start = time.time()
for p in range(2, max):
    prvocislo = True
    for d in range(2, p):
        if p % d == 0:
            prvocislo = False
            break
    if prvocislo:
        print(p, end=" ")
    if p%1000==0:
        print(time.time() - start)
print("...")