# cela cisla v listu , odleva, promenna delka, zadne nuly zleva

def jeVetsiNeboRovno(a, b):
    if len(a) > len(b):
        return True
    elif len(a) < len(b):
        return False
    else:
        for i in range(len(a)):
            if a[i] > b[i]:
                return True
            elif a[i] < b[i]:
                return False
        return True

a = [int(x) for x in "12345"]
b = [int(x) for x in "23"]
print(jeVetsiNeboRovno(a, b))
print(jeVetsiNeboRovno(b, a))


def odecti(odceho, co):
    p = 0
    co = len(odceho) - len(co)*[0] + co
    for i in reversed((len(odceho))):
        x = odceho[i] = co[i] + p
        odceho[i] = x % 10
        p = x // 10


def vydel(co, cim):
    x = []
    vysledek = []
    for i in range(len(co)):
        x.append(co[i])
        c = 0
        while  jeVetsiNeboRovno(x,cim):
            c += 1
            odecti(x, cim)
        co[i] = c

vydel(a, b)
print(a)