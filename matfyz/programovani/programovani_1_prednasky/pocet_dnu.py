mesice = [0, 31,28,31,30,31,30,  31,31,30,31,30]
for i in range(1,11+1):
    mesice[i] += mesice[ i-1 ]

#print( mesice )
#input("...")

def CisloDne( d,m,r ):
    assert m <= 12, f"Mesic: {m}"
    if (r%4==0) and ((r%100!=0) or (r%400==0)) and (m>2):
        unor29 = 1
    else:
        unor29 = 0
    return int(r*365.2425) + mesice[m-1]+unor29 + d

def PocetDnu( d1,m1,r1, d2,m2,r2 ):
    return CisloDne( d2,m2,r2 )-CisloDne( d1,m1,r1 )

#print( PocetDnu( *[int(x) for x in input("Zadej data:").split()] ) )

"""
if PocetDnu( 20,11,2022,  29,11,2022 ) != 9:
    print( "CHYBA" )
if PocetDnu( 28,2,2022,  1,3,2022 ) != 1:
    print( "CHYBA" )
if PocetDnu( 28,2,2020,  1,3,2020 ) != 2:
    print( "CHYBA" )
"""
def OtestujPocetDnu( d1,m1,r1, d2,m2,r2,  vystup ):
    global pocetChyb
    global pocetTestu
    pocetTestu +=1
    x = PocetDnu( d1,m1,r1, d2,m2,r2 )
    if x != vystup:
        print( f"CHYBA: {d1},{m1},{r1}, {d2},{m2},{r2}: {x} misto {vystup}" )
        pocetChyb += 1

        
pocetTestu = 0
pocetChyb = 0
OtestujPocetDnu( 20,11,2022,  29,11,2022,  9 )
OtestujPocetDnu( 28,2,2020,  1,3,2020,  2 )
OtestujPocetDnu( 28,2,2022,  1,3,2022,  1 )
OtestujPocetDnu( 28,2,2000,  1,3,2000,  2 )
OtestujPocetDnu( 28,2,1900,  1,3,1900,  1 )

OtestujPocetDnu( 1,1,2020,   2,1,2020,  1 )
OtestujPocetDnu( 2,1,2020,   1,1,2020,  -1 )

#OtestujPocetDnu( 1,13,2020,   2,1,2020,  1 )





print( f"PocetTestu/PocetChyb: {pocetTestu}/{pocetChyb}" )
print(dir())



        
        
        










input( "============ dal uz nechod! ===========" )

# int( "abcd" )


x = 0
v = 123
try:
    try:
        try:
            #f = open( "FN:\abc.txt" )

            assert x!=0, "Pokus o deleni nulou odhalen!!"
            
            x = int(  input( str(1/x) ) )
            x = int(  input( "zadej cislo:" ) )
            print( "nic se nestalo a pokracuju dal" )
        except ValueError as vyjimka:
            print( "CHYBA" )
            v = vyjimka
        #except ZeroDivisionError:
        #    print( "Deleni nulou" )
        #except Exception:
        #    print
    except FileNotFoundError:
        print( "Nenalezeny soubor" )
finally:
    print( "...a jsme na konci" )
    
#print( vyjimka )
print( "vyjimka v:",v )
input( "..." )



    
    




a: int = 123 # a: int
b: str = "456" # b: str
#print( a+b )

print( str(a)+b )

print( a+int(b) )

b = 7
a = "xy"