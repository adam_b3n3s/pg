class Komplex:
    def abs(self):
        return (self.real*self.real+self.imaginary*self.imaginary)**(1/2)
    def __init__(self, Real, Imaginary):
        self.real = Real
        self.imaginary = Imaginary

k = Komplex(3 , 4)
print(k.abs())

print()
print()

class Prvek():
    def __init__(self, x, dalsi):
        self.x = x
        self.dalsi = dalsi

zac = Prvek(10, Prvek(20, Prvek(30, None)))

print(zac.x)
print()
p = zac
while p != None:
    print(p.x, end=" ")
    p = p.dalsi

print()
print()

class LSS:
    def __init__(self):
        self.zac = None
        self.kon = None
    def Vytiskni(self):
        p = self.zac
        while p != None:
            print(p.x, end=" ")
            p = p.dalsi
        print()
    def PridejNaZacatek(self, x):
        self.zac = Prvek(x, self.zac)
        if self.kon == None:
            self.kon = self.zac
    def VymazPrvniPRvek(self):
        self.zac = self.zac.dalsi
        if self.zac == None:
            self.kon = self.zac
    def PridejNaKonec(self, x):
        if self.zac==None:
            self.PridejNaZacatek(x)
        else:
            p = self.kon
            p.dalsi = Prvek(x, None)
            self.kon = p.dalsi
            
    def VymazPosledniPrvek(self):
        if self.zac is self.kon:
            self.VymazPrvniPrvek()
        else:
            p = self.zac
            while p.dalsi.dalsi != None:
                p = p.dalsi
            p.dalsi = None
            self.kon = p
           

seznam = LSS()
seznam.PridejNaZacatek(30)
seznam.PridejNaZacatek(30)
seznam.PridejNaZacatek(100)
seznam.PridejNaZacatek(30)
seznam.PridejNaZacatek(10)
seznam.Vytiskni()
seznam.VymazPrvniPRvek()
seznam.Vytiskni()
for i in range(1000):
    seznam.PridejNaKonec(i)
seznam.Vytiskni()
print()
for i in range(999):
    seznam.VymazPosledniPrvek()
seznam.Vytiskni()