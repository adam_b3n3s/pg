KONEC = None

_zbytek_ = []
def PrectiSlovo():
    """Precte jedno slovo,
zbyvajici slova si pamatuje v promenne _zbytek_,
kdyz neni co cist, tak vrati None

Slovo je text oddeleny bilymi znaky"""
    global _zbytek_
    while _zbytek_==[]:
        _zbytek_ = input().split()
        #if len(_zbytek_)==1:
        #    _zbytek_==[]
            
    if _zbytek_[0]=="...":
        return KONEC

    slovo = _zbytek_.pop(0)
    if slovo.upper()==slovo:
        slovo = "POSTAVA"
    return slovo


_seznam_ = []
_seznam_ = dict()
def ZapocitejSlovo( slovo ):
    """Pamatuje si slova a jejich pocty v seznamu _seznam_"""
    #print( f"ZapocitejSlovo( {slovo} )" )
    if slovo in _seznam_:
        _seznam_[slovo] +=1
    else:
        _seznam_[slovo] = 1
    return

    # list:
    for zaznam in _seznam_:
        if zaznam[0] == slovo:
            zaznam[1] += 1
            return
    _seznam_.append( [slovo, 1] )

def NajdiVypisAZnicNejcastejsiSlovo():
    snej = None
    for slovo in _seznam_:
        if snej==None or _seznam_[slovo] > _seznam_[snej]:
            snej = slovo
    if snej!=None and _seznam_[snej]>0:
        print( f"{snej}: {_seznam_[snej]}" )
        _seznam_[snej]=-1
    return
    
    inej = None
    for i in range(len(_seznam_)):
        if inej==None or _seznam_[i][1] > _seznam_[ inej ][1]:
            inej = i
    if inej==None:
        return None
    if _seznam_[ inej ][1]>0:
        print( _seznam_[ inej ] )
        _seznam_[ inej ][1] = -1
    
    
def VytiskniNejcastejsiSlova():
    for i in range(20):
        NajdiVypisAZnicNejcastejsiSlovo()


slovo = PrectiSlovo()
while slovo != KONEC:
    ZapocitejSlovo( slovo )
    slovo = PrectiSlovo()

VytiskniNejcastejsiSlova()