M = 1000
N = M + 4
global pozicePrvniNenuloveCislice
def dosad(cela):
    return [cela]+N*[0]

def vytiskni(cislo):
    print(cislo[0],",",*cislo[1:], sep="")
"""
a = dosad(1)
vytiskni(a)
"""
def pricti(kcemu, co):
    p = 0
    for i in reversed(range(N+1)):
        x = kcemu[i] + co[i] + p
        kcemu[i] = x % 10
        p = x // 10
    assert p==0, "preteceni!!!"
"""
a = dosad(0)
a[N-1] = 1
while True:
    vytiskni(a)
    pricti(a, a)
"""
def vydel(co, cim):
    x = 0
    pozicePrvniNenuloveCislice = 0
    while pozicePrvniNenuloveCislice < N and co[pozicePrvniNenuloveCislice] == 0:
        pozicePrvniNenuloveCislice = pozicePrvniNenuloveCislice() + 1
    for i in range(pozicePrvniNenuloveCislice, N):
        x = 10 * x + co[i]
        co[i] = x // cim
        x = x % cim
"""
a = dosad(8)
clen = dosad(1)
for i in range(2, 10):
    vydel(a,i)
    vytiskni(a)
"""
pozicePrvniNenuloveCislice = 0
while pozicePrvniNenuloveCislice < N:

soucet = dosad(0)
clen = dosad(1)
for i in range (1,1000):
    pricti(soucet, clen)
    vydel(clen, i)
    i = i + 1

vytiskni(soucet)