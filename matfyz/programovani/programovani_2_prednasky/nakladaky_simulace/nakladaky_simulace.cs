﻿using System.Security.Cryptography.X509Certificates;
using TypCasu = System.Int32;


namespace nakladaky_simulace
{
    enum TypUdalosti { PrijezdDoA, ZacinaNakladat, Nalozeno,
                       PrijezdDoB, ZacinaVykladat, Vylozeno};

    class Udalost
    {
        public TypCasu Kdy;
        public Auto Kdo;
        public TypUdalosti Co;

        public Udalost(int kdy, Auto kdo, TypUdalosti co)
        {
            Kdy = kdy;
            Kdo = kdo;
            Co = co;
        }
    }

    class Auto
    {
        public string Jmeno;
        int nosnost;
        int dobaAB;
        int dobaNakladani;
        int dobaVykladani;
        int kolikVeze;
        Model model;

        public Auto(Model model, string jmeno, int nosnost, int dobaAB, int dobaNakladani, int dobaVykladani)
        {
            this.model = model;
            this.Jmeno = jmeno;
            this.nosnost = nosnost;
            this.dobaAB = dobaAB;
            this.dobaNakladani = dobaNakladani;
            this.dobaVykladani = dobaVykladani;

            model.Naplanuj(0, this, TypUdalosti.PrijezdDoA);
        }

        public void Zpracuj(TypUdalosti Co)
        {
            switch (Co)
            {
                case TypUdalosti.PrijezdDoA:
                    TypCasu kdyZacnuNakladat = model.Cas;
                    if (model.KdyBudeMozneZacitNakladatDalsiAuto > kdyZacnuNakladat)
                        kdyZacnuNakladat = model.KdyBudeMozneZacitNakladatDalsiAuto;
                    model.KdyBudeMozneZacitNakladatDalsiAuto
                        = kdyZacnuNakladat + dobaNakladani;
                    model.Naplanuj(kdyZacnuNakladat, this, TypUdalosti.ZacinaNakladat);

                    break;
                case TypUdalosti.ZacinaNakladat:
                    model.Naplanuj(model.Cas + dobaNakladani, this, TypUdalosti.Nalozeno);
                    break;
                case TypUdalosti.Nalozeno:
                    if (model.PisekVA >= nosnost)
                        kolikVeze = nosnost;
                    else
                        kolikVeze = model.PisekVA;
                    model.PisekVA -= kolikVeze;

                    model.Naplanuj(model.Cas + dobaAB, this, TypUdalosti.PrijezdDoB);
                    break;
                case TypUdalosti.PrijezdDoB:
                    model.Naplanuj(model.Cas, this, TypUdalosti.ZacinaVykladat);
                    break;
                case TypUdalosti.ZacinaVykladat:
                    model.Naplanuj(model.Cas + dobaVykladani, this, TypUdalosti.Vylozeno);
                    break;
                case TypUdalosti.Vylozeno:
                    model.Naplanuj(model.Cas + dobaAB, this, TypUdalosti.PrijezdDoA);
                    model.PisekVB += kolikVeze;
                    break;
                default:
                    break;
            }
        }
    }
    class Kalendar
    {
        const int MAX = 100;
        const string CHYBA_preteceni = "Kalendar uz je plny!";
        const string CHYBA_prazdny = "Kalendar uz je prazdny!";

        Udalost[] seznam;



        int pocet;
        public Kalendar()
        {
            seznam = new Udalost[MAX];
            pocet = 0;
        }
        public void Pridej(Udalost u)
        {
            System.Diagnostics.Debug.Assert(pocet < MAX - 1, CHYBA_preteceni);

            seznam[pocet] = u;
            pocet++;
        }

        public Udalost NajdiAOdeberNejblizsi()
        {
             System.Diagnostics.Debug.Assert(pocet > 0, CHYBA_prazdny);

            int imin = 0;
            for (int i = 1; i < pocet; i++)
            {
                if (seznam[i].Kdy < seznam[imin].Kdy)
                    imin = i;
            }

            Udalost umin = seznam[imin];
            seznam[imin] = seznam[pocet - 1];
            pocet--;

            return umin;
        }

    }
    class Model
    {
        public int PisekVA;
        public int PisekVB;
        public TypCasu Cas;
        Kalendar kalendar;
        public TypCasu KdyBudeMozneZacitNakladatDalsiAuto;
        
        public void Naplanuj(TypCasu kdy, Auto kdo, TypUdalosti co)
        {
            kalendar.Pridej(new Udalost(kdy, kdo, co));
        }
        public virtual void VytvorAuta()
        {
            new Auto(this, "Adam", 10, 120, 120, 5);
            new Auto(this, "Bety", 10, 120, 120, 5);
            new Auto(this, "Cyril", 30, 360, 310, 5);
        }
        public TypCasu JakDlouhoToBudeTrvat(int pisekCelkem)
        {
            PisekVA = pisekCelkem;
            PisekVB = 0;
            Cas = 0;
            KdyBudeMozneZacitNakladatDalsiAuto = Cas;
            kalendar = new Kalendar();
            VytvorAuta();

            while (PisekVB < pisekCelkem)
            {
                Udalost U = kalendar.NajdiAOdeberNejblizsi();
                Cas = U.Kdy;
                //Console.WriteLine($"{U.Kdy}: {U.Kdo.Jmeno} {U.Co}  {PisekVA}..{PisekVB}");

                U.Kdo.Zpracuj(U.Co);
            }
            return Cas;
        }
    }
    //#############################################################
    class Model2 : Model
    {
        protected int pocetAut;

        public Model2(int pocetAut)
        {
            this.pocetAut = pocetAut;
        }

        public override void VytvorAuta()
        {
            for (int i = 1; i <= pocetAut; i++)
            {
                new Auto(this, "Auto_" + i.ToString(), 10, 120, 120, 5);
            }
        }
    }
    class Model3 : Model2
    {
        int pocetLopat;

        public Model3(int pocetAut,int pocetLopat) : base(pocetAut)
        {
            this.pocetLopat = pocetLopat;
        }
        public override void VytvorAuta()
        {
            for (int i = 1; i <= pocetAut; i++)
            {
                new Auto(this, "Auto_" + i.ToString(), 10, 120, 120/pocetLopat, 5);
            }
        }

    }




    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(
                new Model().JakDlouhoToBudeTrvat(3000)
                );
            Console.WriteLine();
            for (int i = 1; i <= 10; i++)
                Console.WriteLine($"{i}: {new Model2(i).JakDlouhoToBudeTrvat(3000)}");

            Console.WriteLine();
            for (int pocetAut = 1; pocetAut <= 10; pocetAut++)
            {
                for (int pocetLopat = 1; pocetLopat <= 10; pocetLopat++)
                {
                    Console.Write($"{new Model3(pocetAut, pocetLopat).JakDlouhoToBudeTrvat(3000)};");
                }
                Console.WriteLine();

            }
        }
    }
}