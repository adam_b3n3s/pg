﻿using System;
using System.Threading;

namespace vlakna
{   
    class vlakenka
    {
        static void Main(string[] args)
        {
            Console.WriteLine(Environment.ProcessorCount);
            DateTime start = DateTime.Now;
            Thread th = new Thread(VytvorNaplnASetridPole);
            th.Start();
            new Thread(VytvorNaplnASetridPole).Start();
            new Thread(VytvorNaplnASetridPole).Start();
            new Thread(VytvorNaplnASetridPole).Start();
            new Thread(VytvorNaplnASetridPole).Start();
            int a = 0;
            object aLock = new object();
            new Thread(delegate()
            {
                VytvorNaplnASetridPole();
                lock (aLock)
                {
                    a++;
                }
            }).Start();
            DateTime end = DateTime.Now;
            Console.WriteLine("Time elapsed: {0}", end - start);
            start = DateTime.Now;
            VytvorNaplnASetridPole();
            VytvorNaplnASetridPole();
            VytvorNaplnASetridPole();
            VytvorNaplnASetridPole();
            VytvorNaplnASetridPole();
            VytvorNaplnASetridPole();
            end = DateTime.Now;
            Console.WriteLine("Time elapsed: {0}", end - start);
        }
        
        public static void VytvorNaplnASetridPole()
        {
            int[] pole = new int[1000000];
            Random rnd = new Random();
            for (int i = 0; i < pole.Length; i++)
            {
                pole[i] = rnd.Next(0, 1000000);
            }
            Array.Sort(pole);
        }
    }
}
