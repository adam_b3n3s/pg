from Crypto.Cipher import DES

def create_des(key: bytes):
    return DES.new(key, DES.MODE_ECB)

def create_key(k1: int, k2: int, k3: int):
    return bytes([k1, k2, k3] + [0x1] * 5)

def parity(k: int) -> bool:
    res = 0
    for i in range(8):
        res ^= k & 1
        k //= 2
    return bool(res)

table = {}

pt = bytes(range(8))

for i in range(256):
    if not parity(i):
        continue
    for j in range(256):
        if not parity(j):
            continue
        for k in range(256):
            if not parity(k):
                continue
            key = create_key(i, j, k)
            des = create_des(key)
            table[des.encrypt(pt)] = [i,j,k]


for i in range(256):
    if not parity(i):
        continue
    for j in range(256):
        if not parity(j):
            continue
        for k in range(256):
            if not parity(k):
                continue
            key = create_key(i, j, k)
            des = create_des(key)
            tmp = des.encrypt(bytes([0xB0,0x20,0x23,0x51,0xD7,0x0E,0xAF,0xD7]))
            if tmp in table:
                print(table[tmp] + [i,j,k])
                exit()