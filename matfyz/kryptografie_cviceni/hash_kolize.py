import hashlib
from random import choice
import string

def hashstring(a):
    a=a.encode()
    return hashlib.sha3_256(a).hexdigest()[:10]

hashdict = {}

while True:
    x = "adambenes"+"".join([choice("qwertzuioplkjhgfdsayxcvbnm") for i in range(20)])

    hx = hashstring(x)
    if hx in hashdict:
        if hashdict[hx] != x:
            print("")
            print(x)
            print(hashdict[hx])
        
    else:
        hashdict[hx] = x