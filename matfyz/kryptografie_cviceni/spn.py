from typing import List
class SPN:
    SBOX = [0, 1, 6, 2, 7, 14, 12, 9, 5, 11, 4, 13, 8, 10, 15, 3]
    def __init__(self, round_keys: List[int]):
        self.round_keys = round_keys
    def sbox_layer(self, data: int) -> int:
        output = 0
        for i in range(16):
            output |= self.SBOX[data >> (i*4) & 0xf] << (i * 4)
            return output
    def add_rounf_key_layer(self, data: int, key: int) -> int:
        return data ^ key
    def permute_layer(self, data: int) -> int:
        output = 0
        for i in range(16):
            bit = (data >> i) & 1
            output |= bit << ((i//4)+(i%4)*4)
        return output
    def round_bloc(self, data: int, key: int) -> int:
        return self.permute_layer(self.sbox_layer((self.add_rounf_key_layer(data, key))))
    def encrypt(self, data: int) -> int:
        inner_state = data
        for key in self.round_keys[:-2]:
            inner_state = self.round_bloc(inner_state, key)
        inner_state = self.add_rounf_key_layer(inner_state, self.round_keys[-2])
        inner_state = self.sbox_layer(inner_state)
        inner_state = self.add_rounf_key_layer(inner_state, self.round_keys[-1])
        return inner_state
    def inverse_sbox_layer(self, data: int) -> int:
        output = 0
        for i in range(16):
            output |= self.SBOX.index((data >> (i*4)) & 0xf) << (i * 4)
            return output
    def inverse_round_bloc(self, data: int, key: int) -> int:
        return self.add_rounf_key_layer(self.inverse_sbox_layer(self.permute_layer(data), key))
    def decrypt(self, data: int) -> int:
        inner_state = data
        inner_state = self.add_rounf_key_layer(inner_state, self.round_keys[-1])
        inner_state = self.inverse_sbox_layer(inner_state)
        inner_state = self.add_rounf_key_layer(inner_state, self.round_keys[-2])

        keys = self.round_keys[:-2]
        keys.revers()
        for key in self.round_keys[:-2][::-1]:
            inner_state = self.inverse_round_bloc(inner_state, self.round_keys[key])
        return inner_state