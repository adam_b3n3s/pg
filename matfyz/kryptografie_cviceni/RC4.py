from typing import Generator

def hsa(key: bytes) -> bytes:
    S = list(range(256))
    j = 0
    for i in range(256):
        j += S[i] + key[i % len(key)]
        j %= 256
        S[i], S[j] = S[j], S[i]
    return S

def keystream_generator(S: bytes) -> Generator[int, None, None]:
    i = 0
    j = 0
    while True:
        i = (i + 1) % 256
        j = (j + S[i]) % 256
        S[i], S[j] = S[j], S[i]
        yield S[(S[i] + S[j]) % 256]
def encrypt_bruth_force(enc):
    result = bytes.fromhex(enc)
    for i in range(256):
        for j in range(256):
            stream = b""
            heslo=bytes([i,j])
            S =hsa(heslo)
            res = keystream_generator(S)
            for x in result:
                stream = stream + bytes([next(res)])
            if stream == result:
                return heslo
    
        
S = hsa(b"Wiki")
pleintext = b"pedia"
res = keystream_generator(S)
str = ""
for i in pleintext:
    str = str + ((hex(next(res) ^ i)).__str__()[2:].zfill(2))
print(str)
enc="EFF85B0B9514FF09CB0E"
print(encrypt_bruth_force(enc))