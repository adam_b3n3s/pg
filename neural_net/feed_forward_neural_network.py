import math
import os
import random
import ast
class FFnet:
    def __init__(self, inputNodes, hiddenNodes, outputNodes):
        self.learningRate = 0.1
        self.bias = 1.0
        self.inputNodes = inputNodes
        self.hiddenNodes = hiddenNodes
        self.outputNodes = outputNodes
        self.hiddenValues = [0]*hiddenNodes
        self.outputValues = [0]*outputNodes
        
        self.hiddenParameters = []
        self.outputParameters = []
        list = []

        for i in range(inputNodes):
            for j in range(hiddenNodes):
                list.append(random.uniform(-1, 1))
            self.hiddenParameters.append(list)
            list = []

        list = []
        for i in range(hiddenNodes):
            for j in range(outputNodes):
                list.append(random.uniform(-1, 1))
            self.outputParameters.append(list)
            list = []
        
        self.hiddenBiasParameters = []
        for i in range(self.hiddenNodes):
            self.hiddenBiasParameters.append(random.uniform(-1, 1))

        self.outputBiasParameters = []
        for i in range(self.outputNodes):
            self.outputBiasParameters.append(random.uniform(-1, 1))

    def feedForward(self, inputValues):
        self.inputValues = inputValues
        for i in range(self.hiddenNodes):
            self.hiddenValues[i] = 0
            for j in range(self.inputNodes):
                self.hiddenValues[i] += inputValues[j]*self.hiddenParameters[j][i]
            self.hiddenValues[i] = self.sigmoid(self.hiddenValues[i]+self.bias*self.hiddenBiasParameters[i])

        for i in range(self.outputNodes):
            self.outputValues[i] = 0
            for j in range(self.hiddenNodes):
                self.outputValues[i] += self.hiddenValues[j]*self.outputParameters[j][i]
            self.outputValues[i] = self.sigmoid(self.outputValues[i]+self.bias*self.outputBiasParameters[i])

    def sigmoid(self, x):
        return 1.0/(1.0+(math.e**(-x)))
    def derSigmoid(self, x):
        return x*(1.0-x)
    def backProp(self, targetValues):
        saveOutputParameters = self.outputParameters
        delta = [0]*self.outputNodes
        for i in range(self.outputNodes):
            delta[i] = (self.outputValues[i] - targetValues[i])*self.derSigmoid(self.outputValues[i])
        for i in range(self.outputNodes):
            self.outputBiasParameters[i] -= self.learningRate*delta[i]*self.bias
            for j in range(self.hiddenNodes):
                self.outputParameters[j][i] -= self.learningRate*delta[i]*self.hiddenValues[j]
        for i in range(self.hiddenNodes):
            deltaError = 0
            for j in range(self.outputNodes):
                deltaError += delta[j]*saveOutputParameters[i][j]
            delta2 = self.learningRate*deltaError*self.derSigmoid(self.hiddenValues[i])
            for j in range(self.inputNodes):
                self.hiddenParameters[j][i] -= delta2*self.inputValues[j]
            self.hiddenBiasParameters[i] -= delta2*self.bias
    def error(self, targetValues):
        sum = 0
        for i in range(self.outputNodes):
            sum += 1/2*(targetValues[i] - self.outputValues[i])**2
        return sum
    
    def save(self, location):
        with open(location, "w") as file:
            file.write(
            str(self.learningRate) +
            "\n" + str(self.bias) +
            "\n" + str(self.inputNodes) +
            "\n" + str(self.hiddenNodes) +
            "\n" + str(self.outputNodes) +
            "\n" + str(self.hiddenValues) +
            "\n" + str(self.outputValues) +
            "\n" + str(self.hiddenParameters) +
            "\n" + str(self.outputParameters) + 
            "\n" + str(self.hiddenBiasParameters) +
            "\n" + str(self.outputBiasParameters)
            )
    def load(self, location, location2 = 0):
        if os.path.exists(location) and os.path.getsize(location) > 3:
            with open(location, "r") as file:
                line = file.readline(); x = type(self.learningRate); self.learningRate = x(line)
                line = file.readline(); x = type(self.bias); self.bias = x(line)
                line = file.readline(); x = type(self.inputNodes);self.inputNodes = x(line)
                line = file.readline(); x = type(self.hiddenNodes); self.hiddenNodes = x(line)
                line = file.readline(); x = type(self.outputNodes); self.outputNodes = x(line)
                line = file.readline(); self.hiddenValues = ast.literal_eval(line)
                line = file.readline(); self.outputValues = ast.literal_eval(line)
                line = file.readline(); self.hiddenParameters = ast.literal_eval(line)
                line = file.readline(); self.outputParameters = ast.literal_eval(line)
                line = file.readline(); self.hiddenBiasParameters = ast.literal_eval(line)
                line = file.readline(); self.outputBiasParameters = ast.literal_eval(line)

        elif location2 != 0:
            if os.path.exists(location2) and os.path.getsize(location2) > 3:
                with open(location2, "r") as file:
                    line = file.readline(); x = type(self.learningRate); self.learningRate = x(line)
                    line = file.readline(); x = type(self.bias); self.bias = x(line)
                    line = file.readline(); x = type(self.inputNodes);self.inputNodes = x(line)
                    line = file.readline(); x = type(self.hiddenNodes); self.hiddenNodes = x(line)
                    line = file.readline(); x = type(self.outputNodes); self.outputNodes = x(line)
                    line = file.readline(); self.hiddenValues = ast.literal_eval(line)
                    line = file.readline(); self.outputValues = ast.literal_eval(line)
                    line = file.readline(); self.hiddenParameters = ast.literal_eval(line)
                    line = file.readline(); self.outputParameters = ast.literal_eval(line)
                    line = file.readline(); self.hiddenBiasParameters = ast.literal_eval(line)
                    line = file.readline(); self.outputBiasParameters = ast.literal_eval(line)


    
    def print(self):
        print("Learning Rate:", self.learningRate)
        print("Bias:", self.bias)
        print("Input Nodes:", self.inputNodes)
        print("Hidden Nodes:", self.hiddenNodes)
        print("Output Nodes:", self.outputNodes)
        print("Hidden Values:", self.hiddenValues)
        print("Output Values:", self.outputValues)
        print("Hidden Parameteres:", self.hiddenParameters)
        print("Output Parameteres:", self.outputParameters)
        print("Hidden Bias Parameteres:", self.hiddenBiasParameters)
        print("Output Bias Parameteres:", self.outputBiasParameters)

