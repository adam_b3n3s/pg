import threading
import feed_forward_neural_network
from tkinter import *
from io import BytesIO

WIDTH = 1000
HEIGHT = 1000

global stop_flag
stop_flag = True
global mouseHolding
mouseHolding = False

net = feed_forward_neural_network.FFnet(1, 1, 1)
img = []
for i in range(WIDTH):
    l = []
    for i in range(HEIGHT):
        l.append(0)
    img.append(l)

net.load("./neural_net/save_neural_net.txt", "./neural_net/save_neural_net_zaloha.txt")

root = Tk()
root.geometry("1000x1000")
c = Canvas(root, width=WIDTH, height=HEIGHT)
c.pack()

def recognize():
    
    NWIDTH = 28
    NHEIGHT = 28
    rimg = [0]*NWIDTH*NHEIGHT
    scale_x = NWIDTH / WIDTH
    scale_y = NHEIGHT / HEIGHT
    for y in range(NHEIGHT):
        for x in range(NWIDTH):
            rimg[x+y*NWIDTH] = float(img[int(x/scale_x)][int(y/scale_y)])
    for y in range(NHEIGHT):
        for x in range(NWIDTH):
            pass
            #print("XX" if rimg[x+y*NWIDTH] > 0 else "oo", end='')
        #print()
    net.feedForward(rimg)
    print(net.outputValues.index(max(net.outputValues)))
    for i in range(WIDTH):
        for j in range(HEIGHT):
            img[i][j] = 0
    c.delete("all")

def mouseDown(event):
    global mouseHolding
    mouseHolding = True

def mouseUp(event):
    global mouseHolding
    mouseHolding = False

W = 50
H = 50
def draw(event):
    if mouseHolding:
        c.create_rectangle(event.x,event.y,event.x+W,event.y+H,fill="black")
        if (event.x >= 0 and event.x + W < WIDTH and event.y >= 0 and event.y + H < HEIGHT):
            for i in range(event.x, min(WIDTH, event.x + W)):
                for j in range(event.y, min(HEIGHT, event.y + H)):
                    img[i][j] = 1.0
def press(event):
    recognize()

root.bind("<Button-1>", mouseDown)
root.bind("<ButtonRelease-1>", mouseUp)
root.bind("<Motion>", draw)
root.bind("<KeyPress>", press)

root.mainloop()

